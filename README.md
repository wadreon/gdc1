#Purpose

Springs Window Fashions Cellular/Roller Shade Z-Wave Firmware repository.

See the CSZ1Status.docx file for a complete diary of the firmware development. Issues(bugs) are tracked using the BitBucket Issues tracker.

Both the Cellular and Roller shade firmware is built from this repository. The differences in the code is minor and handled using #defines within the code. There are two Keil .uvproj files - one for each shade type.

#Contacts
 - Eric Ryherd - Express Controls firmware engineer - eric@ExpressControls.com
 - Wesley Adreon - Springs Window Fashions - Lead engineer - wes.adreon@springswindowfashions.com

---

# Managing the respository

 - git clone "https://<username>@bitbucket.org/DrZWave/csz1.git" <directory_name> - will create a local repository
 - git status - Prints out which files in your local directory have changed or need to be checked in
 - git pull - updates local directory/repository with the main branch
 - git commit -a -m "comment" - commits changed files TO YOUR LOCAL REPOSITORY - not to BitBucket!
 - git push - pushes your commits up to BitBucket - always do a GIT PULL before committing

---

