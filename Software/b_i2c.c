/***************************************************************************
 * Copyright 2015-2016
 * Springs Window Fashions - Middleton WI USA
 * All Rights Reserved
 *---------------------------------------------------------------------------
 * Author: Eric Ryherd - Eric@ExpressControls.com
 * Original Code based on the SDK sample code by Sigma FAE Ben Garcia
 * Date: 6 Jan 2016
 *
 * Last Changed By:  $Author: eric $
 * Revision:         $Revision: 1000 $
 * Last Changed:     $Date: 2016-03-11 16:21:08 -0500 (Fri, 11 Mar 2016) $
 *
 * Description: b_i2c.c
 * Low level I2C routines to talk to the shade motor.
 *
 * All routines return an error byte if there is an error. If return 0, then everything is OK.
 * Generally the Error byte can be ORed together and returned furthur up the chain.
 * If a bit is set, then the error condition occured and the transaction was canceled and SCL/SDA=1
 * Bit : Error
 *   0 : NACKed (may not necessarily be an error)
 * 1-3 : Reserved
 *   4 : Clock Stretch Timeout reached
 *   5 : SCL stuck either high or low
 *   6 : SDA stuck either high or low
 *   7 : Arbitration lost - another master on the bus
 */

// TODO - need to rewrite a lot of this to speed it up - best speed is 50kHz right now. Largely switch to inline code and #defines instead of all the subroutines...

#include "b_i2c.h"
#include <intrins.h> // NOP

extern IBYTE i2c_crc;

IBYTE i2c_i;    // used by the SCL_HIGH and QBIT_DELAY routines - see b_i2c.h

BOOL started = FALSE;

//------------------------------------------------------------------------------
// 	Routine:	I2C_delay
//	Inputs:		count=number of times thru the loop - each count is about 1usec 
//	Purpose:	small delay for i2c shaping - typically used as a half-bit timing
//	Author:		ER
//------------------------------------------------------------------------------
#pragma OT(8,SIZE)
void I2C_delay(BYTE count)
{
//	register BYTE i; 
//	for (i=count; i; --i); // this is a 4 instruction loop - MOV/JZ/DEC/SJMP which should simply be a DJNZ but I can't get Keil to generate it!
//  while(count) --count; // this results in the exact same 4 instruction loop.
  while(--count); // this results in a DJNZ! - post increment (count--) however results in the 4 instruction loop.  This is the shortest loop possible on the 8051.
} // however, because this is a subroutine, it takes much longer than the #define version which is inlined

//------------------------------------------------------------------------------
// 	Routine:	i2c_start_cond
//	Return:		0=OK otherwise an error code
//	Purpose:	Generates an I2C start condition - SCL falls when SCL is high
//	Author:		ER
//------------------------------------------------------------------------------
#pragma OT(11,SPEED)    // Switch to speed optimization for this routine
BYTE i2c_start_cond(void) {
    BYTE retval=I2C_ERR_NONE;
    BYTE j;
    if (started) { // if started, SCL is already low so need to set it high to do a restart 
        SDA_HIGH;
        QBIT_DELAY;
        SCL_HIGH;   // waits for SCL to go high in the event of a clock stretch
        QBIT_DELAY;
    }
    SCL_HIGH;       // make sure SCL is high in the event of a collision with another master
    if (!SCL_GET) return(I2C_ERR_SCL);  // If SCL is stuck low just exit - not much else can be done but to retry later
    if (!SDA_GET) {                     // Try clocking a few times to see if SDA will release
        for (j=0;j<9 && !(SDA_GET);j++) { // clock up to 9 more times which any slave should tristate SDA for the ACK bit
            QBIT_DELAY; 
            SCL_LOW;
            QBIT_DELAY;
            QBIT_DELAY;
            SCL_HIGH;
            QBIT_DELAY; 
        }
        if (!(SDA_GET)) return(I2C_ERR_SDA); // SDA is completely stuck so just exit
    }
    SDA_LOW;            // This is the START condition - SDA falls when SCL is high
    QBIT_DELAY;
    QBIT_DELAY;         // SDA fall to SCL fall Min is 4usec
    SCL_LOW;
    QBIT_DELAY;
    if (SDA_GET) { // error if one of the signals is stuck high
        SDA_HIGH;
        retval|=I2C_ERR_SDA;
    }
    if (SCL_GET) {
        SCL_HIGH;
        retval|=I2C_ERR_SCL;
    }
    if (I2C_ERR_NONE==retval) started = TRUE;
    return(retval);
}

//------------------------------------------------------------------------------
// 	Routine:	i2c_stop_cond
//	Return:		0=OK else error condition
//	Purpose:	forms i2c stop condition - rising edge on SDA when SCL is high
//	Author:		ER
//------------------------------------------------------------------------------
BYTE i2c_stop_cond(void)
{
    BYTE retval=I2C_ERR_NONE;
    SCL_LOW;    // SCL should already be low but just to be 100% certain
    SDA_LOW;
    QBIT_DELAY;
    QBIT_DELAY;
    SCL_HIGH; // will wait for SCL to be high
    QBIT_DELAY; // Stop bit setup time, minimum 4us
    SDA_HIGH;       // SDA going high when SCL is high is a STOP condition
    QBIT_DELAY;
    if (!SDA_GET)  retval=I2C_ERR_SDA;  // both signals are tristated and must be high otherwise an error
    if (!SCL_GET)  retval|=I2C_ERR_SCL;
    started = FALSE;
    return(retval);
}
	



//------------------------------------------------------------------------------
// 	Routine:	i2c_write_bit
//	Inputs:		bit value to be put on i2c bus
//	Return:		0 if OK else error code on failed arbitration
//	Purpose:	sends a single bit
//	Author:		ER
//------------------------------------------------------------------------------
#pragma OT(11,SPEED)    // Switch to speed optimization for this routine
BYTE i2c_write_bit(BOOL abit) {
    BYTE retval=I2C_ERR_NONE;
    if (abit) {
        SDA_HIGH;
    } else {
        SDA_LOW;
    }
    QBIT_DELAY;
    SCL_HIGH; // release SCL - will wait for SCL to go high including clock stretch
    if (i2c_i==0) retval|=I2C_ERR_STRETCH;   // if clock stretch is more than we can wait, signal an error
    QBIT_DELAY;
    // If SDA is high, check that nobody else is driving SDA which indicates an arbitration loss - usually on the I2C slave address byte 
    if (abit && (0==SDA_GET)) {
        retval|=I2C_ERR_ARBLOST;
    }
    SCL_LOW;
    // No delay here since the return from subroutine takes so long that IS the delay
    return(retval);
} // i2c_write_bit
	
//------------------------------------------------------------------------------
// 	Routine:	i2c_read_bit
//	Return:		Bit read from the I2C bus - no error conditions
//	Purpose:	Clocks in 1 bit of read data from the I2C bus
//	Author:		ER
//------------------------------------------------------------------------------
BOOL i2c_read_bit(void) {
    BOOL abit;
    SDA_HIGH; // Let the slave drive data
    QBIT_DELAY;
    SCL_HIGH; // waits for SCL to go high
    QBIT_DELAY;
    abit = SDA_GET; // sample SDA at the end of the clock pulse which is the safest time
    SCL_LOW;
    return abit;
} // i2c_read_bit

//------------------------------------------------------------------------------
// 	Routine:	i2c_write_byte
//	Inputs:		BOOL flag to do a start, BOOL flag to do a stop, unsigned char to send
//	Return:		0=OK (ACKed) else error code (bit 1 if NACK)
//	Purpose:	write a byte to i2c
//	Author:		ER
//------------------------------------------------------------------------------
BYTE i2c_write_byte(BOOL send_start, BOOL send_stop, BYTE val) {
    BYTE retval=I2C_ERR_NONE;
    crc8(val);              // compute the CRC so far
    if (send_start) {
        retval|=i2c_start_cond();
        if (I2C_ERR_NONE!=retval) return(retval); // exit now if there is an error
    }
    // inlined for speed - must immediately exit on arbitration lost - other errors will just accumulate until the byte is over
    retval|=i2c_write_bit((val & 0x80) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x40) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x20) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x10) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x08) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x04) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x02) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    retval|=i2c_write_bit((val & 0x01) != 0);
    if (retval&I2C_ERR_ARBLOST) {SCL_HIGH; return(retval);}
    if (i2c_read_bit()) retval|=I2C_ERR_NACK;
    if (send_stop) {
        i2c_stop_cond();
    }
    return(retval);
} // i2c_write_byte

//------------------------------------------------------------------------------
// 	Routine:	i2c_read_byte
//	Inputs:		BOOL flag to send ack=0/nack=1, BOOL flag to do a stop
//	Outputs:	Updates global i2c_crc with the byte that was read
//	Return:		unsigned char read from i2c - no errors
//	Purpose:	read a byte to i2c - assumes the I2C Slave Address has already been sent and the slave is ready to return data
//	Author:		ER
//------------------------------------------------------------------------------
BYTE i2c_read_byte(BOOL nack, BOOL send_stop) {
    BYTE retval;
    retval =(BYTE)i2c_read_bit()<<7;
    retval|=(BYTE)i2c_read_bit()<<6;
    retval|=(BYTE)i2c_read_bit()<<5;
    retval|=(BYTE)i2c_read_bit()<<4;
    retval|=(BYTE)i2c_read_bit()<<3;
    retval|=(BYTE)i2c_read_bit()<<2;
    retval|=(BYTE)i2c_read_bit()<<1;
    retval|=(BYTE)i2c_read_bit()   ;
    i2c_write_bit(nack);
    if (send_stop) 
    {
        i2c_stop_cond();
    }
    crc8(retval);
    return(retval);
} // i2c_read_byte

//------------------------------------------------------------------------------
// 	Routine:	crc8
//	Inputs:		byte to update the global variable  i2c_crc
//	Outputs:	updates the global variable i2c_crc
//	Return:		none
//	Purpose:	calculates crc represented by polynomial, C(x) = x^8 + x^2 + x^1 + 1 
//	Author:		BG
//------------------------------------------------------------------------------
void crc8(unsigned char newbyte) 
{ 
    unsigned char index; 

    i2c_crc ^= newbyte;

    for(index = 0; index < 8; index++) 
    { 
        if((i2c_crc&0x80)==0) 
        { 
            i2c_crc<<=1; 
        } else { 
            i2c_crc<<=1; 
            i2c_crc^=0x07; 
        }      
    } 
} // crc8
#pragma OT(8,SIZE)
