#include <ZW_sysdefs.h>
#include <ZW_typedefs.h>

BYTE GetUpperLimit(void);
BYTE GetLowerLimit(void);
BYTE GetCurrentPosition(void);
BYTE GoToTarget(void);
BYTE StopMovement(void);
BYTE ShadeJog(void);
BYTE FactoryLimits(void);
BOOL IsMotorRunning(void);
WORD GetBattery(void);
void GetMotorVersion(void);
BYTE SetUpperLimit(void);
BYTE SetLowerLimit(void);
BYTE ToggleHandBit(void);
BYTE StatusRegClear(void);
BYTE GetMotorMetrics(BYTE * ptr);


