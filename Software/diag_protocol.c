/***************************************************************************
 * Copyright 2016-2018
 * Springs Window Fashions - Middleton WI USA
 * All Rights Reserved
 *---------------------------------------------------------------------------
 * Author: Eric Ryherd - Eric@ExpressControls.com
 * Diagnostic Protocol code for Z-Wave shades and controls.
 * Date: 27 May 2016
 * Updated: 8 May 2018
 *
 * Description: diag_protocol.c
 * The "PrPF_Doc142461_002_MCZ1_UART.doc" file is the protocol implemented here.
 * The concept is to provide a means of testing each unit on the production line via UART commands.
 *
 * A command is sent out the UART immediately after power up (approximately 50ms after power is applied).
 * If a response comes back on the UART, then commands are decoded and executed.
 * If there is no response after 1 second, then the this routine is exited and normal operation begins.
 *
 * This code constantly inspects the UART and waits for a command to do something within certain timings.
 * See the spec for more details.
 *
 * UART Frame Format: Min length=3 bytes, max=63
 * Byte #   Name    Value   Description
 * 1        HEADER          Bit7=Multi-Frames (always 0 for now)
 *                          Bit6=NO ACK (0=ACK expected, 1=No ACK)
 *                          Bit5-0=Length of the all bytes that follow including the checksum but not this byte
 * 2        CMD             Command - See the doc file for details on the commands
 * n        PARAMS          Parameters for the commands
 * n+1      CHKSUM          Classic checksum of all bytes including HEADER. 
 *                          Sum of all bytes plus checksum will be 0 if good, or non-zero if bad.
 *
 * Commands that have requested and ACK will return 4 bytes - 43, 05, ACK, CHK.
 * Where FF=ACK and any non-FF indicates an error code.
 * Error Codes:
 *  TBD
 *
 * See the comments in the code below for more details on each function.
 *
 * The code is written to be used on all SWF products but is checked into each repository to avoid cross linking them.
 * Look thru the different projects to find the latest version by checking the Updated date above.
 * There are #defines in the respective config_app.h to customize the code for the specific hardware.
 * The majority of the code is the same for all projects and is kept in 1 file to ensure they are the same.
 *
 * Note that the RF testing command is REQUIRED to go thru InitHW to enable the ZW_SENDCONST commands.
 * The only way to exit RF test mode is to power cycle.
 */

//  ==========================================================================
//                   Include Files

#include "config_app.h"
#include "eeprom.h"
#include <ZW_classcmd.h>
#include <ZW_uart_api.h>
#include <ZW_basis_api.h> // Watchdog and RF test mode routines
#include <ZW_power_api.h>
#include <misc.h>
#include <math.h>
#include <intrins.h>
#include <ZW_pindefs.h>
#include <ZW_firmware_bootloader_defs.h>    // NVM access
#if defined(MCZ1)
#include "MCZ1_display.h"
#include "MCZ1NonZero.h"
#endif

// DEFINES all start with D_ for Diagnostics
#define D_UART_BUFFER_SIZE 16 /* Buffer size */
#define D_NOACK            0x40   /* NOACK bit set in the header byte */
#define D_READY_FOR_TST    0x01   /* Ready for test - sent at power on to tell Tester that the unit is in test mode */
#define D_START_TST        0x03   /* Start Test mode */
#define D_END_TST          0x04   /* End Test mode */
#define D_ACK_CMD          0x05   /* ACK command */
#define D_WRITE_IO_REQ     0x0E   /* Write IO Request - turn IO pins on or off */
#define D_WRITE_SLEEP_REQ  0x0F   /* Write a request to put the module to sleep - can only wake with power cycle or reset pin */
#define D_READ_IO_REQ      0x10   /* Read IO Request - return the battery % */
#define D_READ_IO_ANS      0x11   /* Read IO answer - battery % (0-100) */
#define D_SIGN_REQ         0x16   /* Signature Request */
#define D_SIGN_ANS         0x17   /* Signature Answer */
#define D_WR_TSTPARAM_REQ  0x18   /* Write Test Parameter Request */
#define D_WR_TSTPARAM_ANS  0x19   /* Write Test Parameter Answer */
#define D_RD_TSTPARAM_REQ  0x1A   /* Read Test Parameter Request */
#define D_RD_TSTPARAM_ANS  0x1B   /* Read Test Parameter Answer */
#define D_WRITE_VERSION    0x20   /* Write the Version string */
#define D_WRITE_VER_ANS    0x21   /* Write the Version answer */
#define D_NVM_WRITE_REQ    0x30   /* Write to NVM */
#define D_NVM_WRITE_ANS    0x31   /* Write to NVM */
#define D_NVM_READ_REQ     0x32   /* Read from NVM */
#define D_NVM_READ_ANS     0x33   /* Read from NVM */
#define D_START_KEYPAD_TST 0x40   /* Start the keypad test */
#define D_KEY_PRESSED      0x41   /* Key has been pressed and status is provided */
#define D_STOP_KEYPAD_TST  0x43   /* Stop the keypad test */
#define D_RF_TEST_MODE     0x57   /* Enter RF Test mode - must power cycle to exit this mode */
#define D_ACK_OK           0xFF   /* ACK is OK - KO is any non-FF value */
#define D_BAD_CRC          0x01   /* bit0=1 is the bad CRC ACK value */
#define D_UNKNOWN_CMD      0x02   /* bit1=1 is the unknown error command code */
#define D_LENGTH_MASK      0x3F   /* low 6 bits of the header are the length */
#define D_NOACKBIT_MASK    0x40   /* NOACK bit mask - if 1, no ack requested, if 0, ACK is requested */
#define D_ONE_SECOND       100    /* approximately 1 second in 10ms increments - use the TickTime for 10ms counting*/
#define DELAY_ONE_MS        500   /* 8051 spin loop which is about 1ms */

#if defined(MCZ1)
#define WRITE_IO_OLED_POWERED_OFF    0x00   //  (all non-zero values turn the OLED ON)
#define WRITE_IO_SCREEN_BLACK        0x01   // SCREEN BLACK - all pixels off
#define WRITE_IO_SCREEN_WHITE        0x02   //  - all pixels on
#define WRITE_IO_CHECKERBOARD8       0x03   //  - Every 8th pixel on in both X and Y, all others off 
                                            //    EX: pixels 0:0-7:0, 0:0-0:7 are on, all others off - repeated across the entire screen
#define WRITE_IO_CHECKERBOARDINV8    0x04   //  - Every 8th pixel OFF in both X and Y, all others on
#define WRITE_IO_ADDR_EQ_DATA        0x05   //  - Address of each pair of bytes is written as the data value 
                                            //    EX: bytes 0 and 1 are written with 0x0000, bytes 2 and 3 are written with 0x0001, etc.
#define WRITE_IO_SET_PIXEL_ON        0x06   //  - set one pixel on and the rest off
#define WRITE_IO_SET_PIXEL_OFF       0x07   //  - set one pixel off and the rest on
#endif

//
//  ==========================================================================
//                   Function Prototypes
void SendUart( BYTE len, BYTE * ptr, BYTE ack);
void DecodeUartCommand(void);

//  ==========================================================================
//                   Global Variables
BYTE UTxBuf[D_UART_BUFFER_SIZE];  // UART transmit buffer
BYTE URxBuf[D_UART_BUFFER_SIZE];  // UART receive buffer
BOOL TestModeEnabled=FALSE;       // When true, the test mode is enabled and will remain in the special diagnostic test mode until the END command is sent
BOOL KeypadTestEnabled=FALSE;     // when true, the keypad test is enabled and anytime there is a change in the pins of the keypad will be sent out the UART
BYTE KeypadState;                 // bit0=UP, bit1=DOWN, bit2=HOME, bit3=PROG
BYTE KeypadStateLast;
#if defined(MCZ1)
BOOL DisplayInitialized=FALSE;
extern IBYTE ButtonPressed;
#else
#define ButtonPressed 0
#endif
DWORD offset;                   // 24 bit offset into the NVM
WORD  length;                   // Length of an NVM read/write
BOOL RFTestRunning=FALSE;       // TRUE once the RF test mode has been activated or the mode changed. Mode is changed by pulsing RESET_N while MISO is held low.
extern XBYTE RFTestState;       // Must be in NonZeroVars.c as this value must remain unchanged when reset is pulsed.


static void diag_protocol_done(void);

enum {
    DIAG_DISABLED,
    DIAG_IDLE,
    DIAG_ASSEMBLING
};

static BYTE diag_state=DIAG_DISABLED;
static BYTE diag_i;
static BYTE diag_length;
static BYTE diag_checksum;
static BYTE diag_NoACK_bit;
static WORD diag_TimeOut;
static WORD MyTickTime;
static WORD MyTickTimeLast;

BYTE diag_protocol_enabled(void) { // returns FALSE once the diag_protocol has expired or exited
    return diag_state!=DIAG_DISABLED;
} // diag_protocol_enabled

/** ==========================  diag_protocol_init() ==============================
 * This is the initialization routine that sets up the diagnostic protocol handling.
 * It must be called in InitSW when the POR is the cause of the reset.
 * A READY command is sent out the UART letting the controller know we are in diag mode.
 * The test station must return a command to enter diag mode before the timeout (typically 1s).
 */
void diag_protocol_init(void) {
    ZW_UART0_init(192, TRUE, TRUE); // enable UART0 at 19.2Kbaud
    UTxBuf[0]=D_READY_FOR_TST;
    SendUart(1,UTxBuf,0);         // Send the Tester the READY_FOR_TST message
    ZW_UART0_rx_int_clear();
    diag_state = DIAG_IDLE;
    diag_TimeOut=DIAG_PROTOCOL_TIMEOUT; // defined in config_app.h
} // diag_protocol_init

/** ==========================  diag_protocol_poll() ==============================
 * This is the main routine that does all the diagnostic protocol handling.
 * It must be called in ApplicationPoll when the POR is the cause of the reset.
 * If a command is received within 1 second, then stay in diag mode until the END command is receieved.
 * The UART is polled instead of interrupt driven since this is just a diagnostic mode.
 */
int diag_protocol_poll(void) {
    BYTE i;
    
    switch (diag_state) {
    case DIAG_DISABLED: // all done - just exit
        TestModeEnabled=FALSE;
        break;
    case DIAG_IDLE:
        if (ZW_UART0_rx_int_get()) { // Then a HEADER has arrived
            ZW_UART0_rx_int_clear();
            i=ZW_UART0_rx_data_get(); // Get the byte from the UART
            if ((i&0x80)!=0) { // drop the byte if the MSB is 1 as it is required to be 0
                return TRUE;   // skip the byte and hope the next one is a header - this is a weakness in the protocol. If they are out of sync, have to power cycle to get them back into sync. I recommended having an SOF byte but Somfy didn't go for it...
            } else {
                URxBuf[0]=i;
                diag_checksum=i;
                diag_i=1;
                diag_length=i&D_LENGTH_MASK;
                diag_NoACK_bit=i&D_NOACKBIT_MASK;
                if (diag_length==0) {
                    diag_state=DIAG_IDLE;
                } else {
                    diag_state=DIAG_ASSEMBLING;
                }
            }
        }
        break;
    case DIAG_ASSEMBLING:
        if (ZW_UART0_rx_int_get()) { // Then a HEADER has arrived
            ZW_UART0_rx_int_clear();
            URxBuf[diag_i]=ZW_UART0_rx_data_get();
            diag_checksum+=URxBuf[diag_i];
            if (++diag_i>diag_length) {
                if (diag_checksum==0x00) { // checksum is good so decode the command
                    DecodeUartCommand();
                } else {            // bad checksum so ACK with KO
                    UTxBuf[0]=D_ACK_CMD;
                    UTxBuf[1]=D_BAD_CRC;
                    SendUart(2,UTxBuf,0);
                }
                diag_state=DIAG_IDLE;
            }
        }
        break;
    }
    
    MyTickTime=getTickTime();                           // getTickTime returns a word that increments every 10ms
    if (MyTickTime!=MyTickTimeLast && !TestModeEnabled) { // if no command received within 1s, then exit and resume normal operation
        if (--diag_TimeOut==0 || ButtonPressed != 0) {      // exit if a button is pressed or timeout counts down the 1s
            diag_protocol_done();
        }
    }
    MyTickTimeLast=MyTickTime;

#if defined(MCZ1)
    if (KeypadTestEnabled) { // then check the keypad and note any changes in it
        KeypadState=ButtonPressed;
        if (KeypadState!=KeypadStateLast) {
            UTxBuf[0]=D_KEY_PRESSED;
            UTxBuf[1]=KeypadState;
            SendUart(2,UTxBuf,0);
        }
        KeypadStateLast=KeypadState;
    }
#endif

    return diag_protocol_enabled();
} // diag_protocol_poll

static void diag_protocol_done(void) {  // resume normal operation and disable the UART.
    ZW_UART0_init(192, FALSE, FALSE); // disable UART0 and return the 2 pins to their normal function
#if defined(MCZ1)
    if (DisplayInitialized) {
        OLED_off();
    }
#endif
    diag_state=DIAG_DISABLED;
    TestModeEnabled=FALSE;
} // diag_protocol_done

#if 0   // no longer used - delete me...
// The reboot routine will reboot the software. Note that peripherals are not altered.
void reboot (void) {
    ((void (code *) (void)) 0x0000) ();
}
#endif

// Decode the command in URxBuf and execute it
void DecodeUartCommand(void) {
    BOOL cmdok=FALSE;
    BYTE i,j;
    switch(URxBuf[1]) {         // URxBuf[0]=header, [1]=command, [2-n]=parameters
        case D_START_TST:
            UTxBuf[0]=D_ACK_CMD;
            UTxBuf[1]=D_ACK_OK;
            SendUart(2,UTxBuf,0);
            TestModeEnabled=TRUE;
            break;
        case D_WRITE_IO_REQ:
            if (URxBuf[2]==0x01) { // set the Motor Enable pin P26
#if defined(CSZ1) || defined(RSZ1)
                cmdok=TRUE;
                if (URxBuf[3]==0x01) {  // turn on
                    MOTOR_ENB_HIGH;     // doesn't do anything on the remote, just the motor radio.
                } else {                // anything else turns it off (low)
                    MOTOR_ENB_LOW;
                }
            } else if (URxBuf[2]==0x02) { // set the LEDS
                cmdok=TRUE;
                if (URxBuf[3]&0x01) {
                    LED1REDON;
                } else {
                    LED1REDOFF;
                }
                if (URxBuf[3]&0x02) {
                    LED1GREENON;
                } else {
                    LED1GREENOFF;
                }
                if (URxBuf[3]&0x04) {
                    LED2REDON;
                } else {
                    LED2REDOFF;
                }
                if (URxBuf[3]&0x08) {
                    LED2GREENON;
                } else {
                    LED2GREENOFF;
                }
#endif 
            } else if (URxBuf[2]==0x03) { // only process OLED commands for MCZ1
#if defined(MCZ1)
                cmdok=TRUE;
                if (!DisplayInitialized && URxBuf[3] != WRITE_IO_OLED_POWERED_OFF) {
                    OLED_init(0x00);
                    OLED_ClearScreen();
                    DisplayInitialized=TRUE;
                }
                switch (URxBuf[3]) {
                    case WRITE_IO_OLED_POWERED_OFF: //  (all non-zero values turn the OLED ON)
                        if (DisplayInitialized) {
                            DisplayInitialized=FALSE;
                            OLED_off();
                        }
                        break;
                    case WRITE_IO_SCREEN_BLACK:     // SCREEN BLACK - all pixels off
                        OLED_FillScreen(0x00);
                        break;
                    case WRITE_IO_SCREEN_WHITE:     //  - all pixels on
                        OLED_FillScreen(0xff);
                        break;
                    case WRITE_IO_CHECKERBOARD8:    //  - Every 8th pixel on in both X and Y, all others off
                        OLED_Checkerboard8(FALSE);
                        break;
                    case WRITE_IO_CHECKERBOARDINV8: //  - Every 8th pixel OFF in both X and Y, all others on
                        OLED_Checkerboard8(TRUE);
                        break;
                    case WRITE_IO_ADDR_EQ_DATA:     //  - Address of each pair of bytes is written as the data value
                        OLED_AddrEqData();
                        break;
                    case WRITE_IO_SET_PIXEL_ON:     //  - One pixel on
                        OLED_OnePixel(URxBuf[4],URxBuf[5],TRUE);
                        break;
                    case WRITE_IO_SET_PIXEL_OFF:    //  - One pixel of
                        OLED_OnePixel(URxBuf[4],URxBuf[5],FALSE);
                        break;
                    default:
                        //OLED_printHex(URxBuf[2],0,0);
                        break;
                } // Switch
#endif
            }
            UTxBuf[0]=D_ACK_CMD;
            if (cmdok) {
                UTxBuf[1]=D_ACK_OK;
            } else {
                UTxBuf[1]=D_UNKNOWN_CMD;
            }
            SendUart(2,UTxBuf,0);
            break;
        case D_READ_IO_REQ:
            if (URxBuf[2]==0x01) {
                UTxBuf[0]=D_READ_IO_ANS;
                UTxBuf[1]=0x01;
                GET_BATTERY_MEASUREMENT;
                UTxBuf[2]=BATTERYPERCENTAGE;
                SendUart(3,UTxBuf,0);
            } else {
                UTxBuf[0]=D_ACK_CMD;
                UTxBuf[1]=D_UNKNOWN_CMD;
                SendUart(2,UTxBuf,0);
            }
            break;
        case D_WRITE_SLEEP_REQ:
#if defined(MCZ1)
            DisplayInitialized=FALSE;
            OLED_off();
						POWER_DOWN_NVM;
            ZW_SetSleepMode(ZW_STOP_MODE,0,0);
            // don't bother sending an ACK because it doesn't get delivered before we go to sleep
#endif
            break;
        case D_START_KEYPAD_TST:
            KeypadTestEnabled=TRUE;
            KeypadStateLast=0xFF; // invalid state of the pins which will force a send of the current state
#if defined(CSZ1) || defined(RSZ1)
            KEYPAD_INIT;            // initialize the IOs - note that the ZWave keypad API is NOT used here and the state of the GPIOs is polled
#endif
            UTxBuf[0]=D_ACK_CMD;
            UTxBuf[1]=D_ACK_OK;
            SendUart(2,UTxBuf,0);
            break;
        case D_STOP_KEYPAD_TST:
            KeypadTestEnabled=FALSE;
#if defined(CSZ1) || defined(RSZ1)
            KEYPAD_EXIT;            // release the IOs in preparation for a full boot
#endif
            UTxBuf[0]=D_ACK_CMD;
            UTxBuf[1]=D_ACK_OK;
            SendUart(2,UTxBuf,0);
            break;
        case D_RF_TEST_MODE: // RF test mode via a UART command is not allowed. Sigma requires assertion of a PIN (MISO)
            UTxBuf[0]=D_ACK_CMD;
            UTxBuf[1]=D_UNKNOWN_CMD;
            SendUart(2,UTxBuf,0);
            break;
        case D_WRITE_VERSION:   // save the Hardware Version string in NVM and 
            i=(URxBuf[0] & D_LENGTH_MASK) -2; // subtract the command and checksum bytes
            if (i>16) i=16; // max length allowed is 16 bytes
            j=MemoryPutByte((WORD)&EEOFFSET_HardwareVersionLen_far, i);
            j|=MemoryPutBuffer((WORD)&EEOFFSET_HardwareVersion_far, &URxBuf[2],i,NULL);
            if (j) { // then the write failed for some reason - KO
                UTxBuf[0]=D_WRITE_VER_ANS;
                UTxBuf[1]=0x0C;
            } else {    // OK
                UTxBuf[0]=D_WRITE_VER_ANS;
                UTxBuf[1]=0xFF;
            }
            SendUart(2,UTxBuf,0);
            break;
        case D_SIGN_REQ:   // request the signature value (hardware/software version string)
            if (URxBuf[2]==0x00) { // Hardware signature
                i=MemoryGetByte((WORD)&EEOFFSET_HardwareVersionLen_far);
                if (i>16) i=16; // fix it if 0xFF
                MemoryGetBuffer((WORD)&EEOFFSET_HardwareVersion_far, &UTxBuf[1],i);
                UTxBuf[0]=D_SIGN_ANS;
                SendUart(i+1,UTxBuf,0);
            }  else if (URxBuf[2]==0x01) { // Software signature
                UTxBuf[0]=D_SIGN_ANS;
                UTxBuf[1]='0';
                if (APP_VERSION==0x0b) UTxBuf[2]='B'; // APP_VERSION is expected to be 0x0B, anything else is UNKNOWN or 'U'
                else UTxBuf[2]='U';
                UTxBuf[3]=(APP_REVISION>>4)  >9 ? 'A'+(APP_REVISION>>4)  -10 : (APP_REVISION>>4)  +'0'; // bin2hex
                UTxBuf[4]=(APP_REVISION&0x0f)>9 ? 'A'+(APP_REVISION&0x0f)-10 : (APP_REVISION&0x0f)+'0'; // bin2hex
                SendUart(5,UTxBuf,0);
            } else {
                UTxBuf[0]=D_ACK_CMD;
                UTxBuf[1]=D_UNKNOWN_CMD;
                SendUart(2,UTxBuf,0);
            }
            break;
        case D_WR_TSTPARAM_REQ:   // Write Test Parameter Request
#if defined(BRZ1) || defined(VCZ1)      // the remote and the motor radio have different parameters so they require different code in this instance.
            if ((URxBuf[2]+URxBuf[3])>2) { // only 2 parameters so the offset has to be 0 or 1 otherwise it's a bad command
                UTxBuf[0]=D_ACK_CMD;
                UTxBuf[1]=D_UNKNOWN_CMD;
                SendUart(2,UTxBuf,0);
            } else {    // command is OK - write the parameters
                j=0;
                if (URxBuf[2]==0 && URxBuf[3]>0) {
                    if (MemoryPutByte((WORD)&EEOFFSET_NumberWatchDogResets_far, URxBuf[4])) j++;
                    if (URxBuf[3]==2) {
                        if (MemoryPutByte((WORD)&EEOFFSET_NumberPORResets_far, URxBuf[5])) j++;
                    }
                } else if (URxBuf[2]==1 && URxBuf[3]>0) {
                    if (MemoryPutByte((WORD)&EEOFFSET_NumberPORResets_far, URxBuf[4])) j++;
                }
                UTxBuf[0]=D_WR_TSTPARAM_ANS;
                UTxBuf[1]=j==0 ? 0xFF: URxBuf[2]; // offset
                UTxBuf[2]=j;                        // number of values written
                SendUart(3,UTxBuf,0);
            }
#elif defined(RSZ1) || defined(CSZ1)
            if ((URxBuf[2]+URxBuf[3])>2) { // only 2 parameters so the offset has to be 0 or 1 otherwise it's a bad command
                UTxBuf[0]=D_ACK_CMD;
                UTxBuf[1]=D_UNKNOWN_CMD;
                SendUart(2,UTxBuf,0);
            } else {    // command is OK - write the parameters
                j=0;
                if (URxBuf[2]==0 && URxBuf[3]>0) {
                    if (MemoryPutByte((WORD)&EEOFFSET_NumberWatchDogResets_far, URxBuf[4])) j++;
                    if (URxBuf[3]==2) {
                        if (MemoryPutByte((WORD)&EEOFFSET_NumberPORResets_far, URxBuf[5])) j++;
                    }
                } else if (URxBuf[2]==1 && URxBuf[3]>0) {
                    if (MemoryPutByte((WORD)&EEOFFSET_NumberPORResets_far, URxBuf[4])) j++;
                }
                UTxBuf[0]=D_WR_TSTPARAM_ANS;
                UTxBuf[1]=j==0 ? 0xFF: URxBuf[2]; // offset
                UTxBuf[2]=j;                        // number of values written
                SendUart(3,UTxBuf,0);
            }
#endif
            break;
        case D_RD_TSTPARAM_REQ:   // Read Test Parameter Request
#if defined(BRZ1) || defined(VCZ1)      // the remote and the motor radio have different parameters so they require different code in this instance.
            if ((URxBuf[2]+URxBuf[3])>2) { // only 2 parameters so the offset has to be 0 or 1 otherwise it's a bad command
                UTxBuf[0]=D_ACK_CMD;
                UTxBuf[1]=D_UNKNOWN_CMD;
                SendUart(2,UTxBuf,0);
            } else {    // command is OK - read the parameters
                if (URxBuf[2]==0 && URxBuf[3]>0) {
                    UTxBuf[2]=MemoryGetByte((WORD)&EEOFFSET_NumberWatchDogResets_far);
                    if (URxBuf[3]==2) {
                        UTxBuf[3]=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
                    }
                } else if (URxBuf[2]==1 && URxBuf[3]>0) {
                    UTxBuf[2]=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
                }
                UTxBuf[0]=D_RD_TSTPARAM_ANS;
                UTxBuf[1]=URxBuf[3];        // number of parameters read
                SendUart(URxBuf[3]+2,UTxBuf,0);
            }
#elif defined(RSZ1) || defined(CSZ1)
            if ((URxBuf[2]+URxBuf[3])>2) { // only 2 parameters so the offset has to be 0 or 1 otherwise it's a bad command
                UTxBuf[0]=D_ACK_CMD;
                UTxBuf[1]=D_UNKNOWN_CMD;
                SendUart(2,UTxBuf,0);
            } else {    // command is OK - read the parameters
                if (URxBuf[2]==0 && URxBuf[3]>0) {
                    UTxBuf[2]=MemoryGetByte((WORD)&EEOFFSET_NumberWatchDogResets_far);
                    if (URxBuf[3]==2) {
                        UTxBuf[3]=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
                    }
                } else if (URxBuf[2]==1 && URxBuf[3]>0) {
                    UTxBuf[2]=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
                }
                UTxBuf[0]=D_RD_TSTPARAM_ANS;
                UTxBuf[1]=URxBuf[3];        // number of parameters read
                SendUart(URxBuf[3]+2,UTxBuf,0);
            }
#endif
            break;
       case D_NVM_WRITE_REQ: // Write data into the external NVM - BE CAREFUL! The Z-Wave routing tables can be damaged!
            offset=(DWORD)URxBuf[2]<<16 | (DWORD)URxBuf[3]<<8 | (DWORD)URxBuf[4]; // Header,0x30,Addr[23:16],Addr[15:8],Addr[7:0], Length (up to 63 bytes)
            length=(WORD)URxBuf[5];
            NVM_ext_write_long_buffer(offset, &URxBuf[6],length);
            UTxBuf[0]=D_NVM_WRITE_ANS;
            UTxBuf[1]=D_ACK_OK;
            SendUart(2,UTxBuf,0);
            break;
        case D_NVM_READ_REQ: // Read from the external NVM
            offset=(DWORD)URxBuf[2]<<16 | (DWORD)URxBuf[3]<<8 | (DWORD)URxBuf[4];
            length=(WORD)URxBuf[5];
            NVM_ext_read_long_buffer(offset, &UTxBuf[1], length);
            UTxBuf[0]=D_NVM_READ_ANS;
            SendUart(length+1,UTxBuf,0);
            break;
        case D_END_TST:
            UTxBuf[0]=D_ACK_CMD;
            UTxBuf[1]=D_ACK_OK;
            SendUart(2,UTxBuf,0);
            diag_protocol_done();
            break;
        default: // unknown commands return the KO with the unknown command error code
            UTxBuf[0]=D_ACK_CMD;
            UTxBuf[1]=D_UNKNOWN_CMD;
            SendUart(2,UTxBuf,0);
            break;
    } // switch
} // DecodeUartCommand

void PutByte( BYTE dat) { // waits until the UART is free then sends the DAT byte
    unsigned int retries=60000;
    while (ZW_UART0_tx_active_get() && --retries>0);
    if (retries>0) ZW_UART0_tx_data_set(dat);
} // PutByte

// Add header and checksum and send it out the UART - ACK is non-zero if an ACK is desired
void SendUart( BYTE len, BYTE * ptr, BYTE ack) {
    BYTE chksum;
    BYTE i;
    i=(len+1)&0x3F;    // length now includes the checksum byte
    if (ack==0) i|=D_NOACK;   // the polarity of NOACK is really confusing - if you do NOT want an ACK, set the bit.
    chksum=i;
    PutByte(i); // Send header
    for (i=0;i<len;i++) {    // send the data
        PutByte(*ptr);
        chksum+=*ptr;
        ptr++;
    }
    PutByte(-chksum);
} // SendUart

void RFTest(void) { // Enter RF Test Mode if the MISO pin is held low when booting.
    // pulse reset to increment to the next mode.
    // The first mode leaves the RF in Receive mode.
    // Next is the 908MHz unmodulated carrier, then 916, then those 2 again modulated.
    // The RF carrier is not setup properly unless you go thru reset. Idiots.

    if (RFTestRunning) return; // ApplicationTestPoll is called constantly but just return once the call has been made until RESET is pulsed
    RFTestRunning=TRUE; // Will be false the first time after reset is pulsed
    switch(RFTestState) {   // RFTestState is a NonZero RAM variable which is NOT zeroed when RESET is pulsed.
        case 0:
            ZW_SendConst(0, 0, ZW_RF_TEST_SIGNAL_CARRIER); // Disable carrier
            ZW_SET_RX_MODE(TRUE); // default test mode just turns on RX. To get to the next mode, pulse RESET pin.
            // Just in case there is a hardware failure in the field, this mode will not result in a jammer.
            break;
        case 1:
            ZW_SendConst(1, 0, ZW_RF_TEST_SIGNAL_CARRIER); // enable 916 carrier
            break;
        case 2:
            ZW_SendConst(1, 1, ZW_RF_TEST_SIGNAL_CARRIER); // enable 908 carrier
            break;
        case 3:
            ZW_SendConst(1, 0, ZW_RF_TEST_SIGNAL_CARRIER_MODULATED); // enable 916 carrier modulated
            break;
        case 4:
            ZW_SendConst(1, 1, ZW_RF_TEST_SIGNAL_CARRIER_MODULATED); // enable 908 carrier modulated
            break;
        default:    // should not happen but cleanup just in case
            ZW_SendConst(0, 0, ZW_RF_TEST_SIGNAL_CARRIER); // Disable carrier
            ZW_SET_RX_MODE(FALSE);
            RFTestState=0xFF;
    } // switch
    RFTestState++; // on the next pulse of reset switch to the next RF Test mode
    if (RFTestState>4) RFTestState=0;
} // RFTest
