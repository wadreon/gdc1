// This file is where all variables that need to persist while sleeping are stored.
// There is a small section of RAM in the ZM5x0x that remains powered even when the rest of the chip is powered off.
//
#ifdef __C51__
#pragma userclass (xdata = NON_ZERO_VARS_APP)
#endif
/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/

#include <ZW_typedefs.h>
#include "config_app.h"

// ===========================================================================
// NON_ZERO_START_ADDR is 42 bytes of RAM that retains state while sleeping.
// The rest of RAM is zeroed on powerup.
TwoBytesUnion BatteryMilliVolts ; // millivolt reading of the battery level which is updated after every move.
XBYTE BatteryPercentage          ; // battery level in percentage - 0-100%
XBYTE DirectionLast              ; // direction of last travel - 0=down, FF=UP
FourBytesUnion currentposition  ; // current motor position - signed! 32 bit value, 0=upperlimit
FourBytesUnion upperlimit       ; // Upper limit motor Ticks - usually close to 0 but can be negative
FourBytesUnion lowerlimit       ; // Lower limit motor Ticks - usually a positive number and always larger than UL - 500 LSBs=1"
FourBytesUnion homeposition     ; // The current HOME position
XBYTE currentpercentage          ; // current motor position as a percentage - not as accurate as the currentposition
XBYTE Lastpercentage             ; // last motor position as a percentage - used to compute when to measure the battery
XBYTE MotorVersion               ; // Version of the motor - 1 if Motor=5120199 otherwise 0
XBYTE MotorSubVersion            ; // SubVersion of the motor - last 2 bytes of a 0x9B command converted from ASCII to binary
XBYTE PowerLevelTestNodeID       ; // nodeID to send the Tests to - these two need to remain valid even after sleeping so the GET will report the result of the test in the CTT
XWORD PowerLevelGoodCounter      ; // Number of test frames that were good
XBYTE BatteryAboveBattLow        ; // Flag indicating that the previous battery measurement was above BATTERY_LOW so we know when to send a low battery warning
XBYTE BatteryLast                ; // Battery percentage the last time it was reported to the lifeline
XBYTE ListeningMode              ; // 00=250ms FLiRS, 01=1000ms FLiRS, 02=Always On, anything else is also 1s FLiRS
XBYTE RFTestState                ; // RF test mode - The active mode is the value in this variable minus 1.
XBYTE debug[4]                   ; // Debugging variables that last across sleep cycles

/** -----------copied from battery_non_zero_vars.c  from the SDK because of Address Space Overflow errors
 * This variable contains the Supervision session ID which must be kept during powerdown.
 */
XBYTE m_sessionId;
XBYTE previously_receive_session_id;
XBYTE previously_rxStatus;
MULTICHAN_DEST_NODE_ID xdata previously_received_destination;
/**
 * This variable is used by the Battery Monitor.
 */
XBYTE lowBattReportAcked;

/**
 * This variable is used by the Battery Monitor.
 */
XBYTE st_battery;

