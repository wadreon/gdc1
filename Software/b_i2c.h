// b_i2c.h - defines for the I2C of the Springs Window Fashions shades

#include <ZW_sysdefs.h>
#include <ZW_typedefs.h>
#include <ZW_pindefs.h>

// I2CSPEED is the speed approximately usec/bit of the I2C bus (3=100kHz, 1=400kHz approximately)
#define 	I2CSPEED 	3	

// The timeout is the number of times thru a loop to wait for a timeout - roughly uSecs - set to the max for now
#define I2C_TIMEOUT 255u

// Slave address READ/WRITE bit is bit 0. 0=WRITE,1=READ
#define I2C_READ  0x01
#define I2C_WRITE 0x00

// Error conditions on the I2C bus
#define I2C_ERR_NONE    0x00 /* everything OK */
#define I2C_ERR_NACK    0x01 /* slave address NACKed or a write byte NACKed */
#define I2C_ERR_RETRY   0x02 /* Command was retried at least once */
#define I2C_ERR_CRC     0x08 /* CRC failure */
#define I2C_ERR_STRETCH 0x10 /* SCL clock stretch too long */
#define I2C_ERR_SCL     0x20 /* SCL stuck */
#define I2C_ERR_SDA     0x40 /* SDA stuck */
#define I2C_ERR_ARBLOST 0x80 /* Arbitration lost */

void I2C_delay(BYTE count); 
BYTE i2c_start_cond(void);
BYTE i2c_stop_cond(void);
BYTE i2c_write_byte(BOOL, BOOL, unsigned char);
BYTE i2c_read_byte(BOOL, BOOL);
void crc8(unsigned char); 

// Using #defines makes the code fast because instead of calling subroutines this is all #defines which results in in-line code

#define QBIT_DELAY  i2c_i=I2CSPEED;while(--i2c_i); /* I2C quarter of a bit delay */
// Note that PIN_IN with the pullup=0 when the pin is high will glitch low, so first PIN_IN with the pullup on, then with it off to avoid the glitch and let the external pullup pull the signal high
#define SCL_HIGH    PIN_IN(P35,1);PIN_IN(P35,0);for (i2c_i=I2C_TIMEOUT;!PIN_GET(P35) && i2c_i;i2c_i--) I2C_delay(255); /* SCL tristates and waits until it goes high in case there is a clock stretch which can be as much as 12ms! */
#define SCL_GET     PIN_GET(P35)
#define SDA_GET     PIN_GET(P34)
#define SCL_LOW     PIN_OUT(P35);PIN_LOW(P35);
#define SDA_HIGH    PIN_IN(P34,1);PIN_IN(P34,0);
#define SDA_LOW     PIN_OUT(P34);PIN_LOW(P34);
