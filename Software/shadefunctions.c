/***************************************************************************
 * Copyright 2015-2018
 * Springs Window Fashions - Middleton WI USA
 * All Rights Reserved
 *---------------------------------------------------------------------------
 * Author: Eric Ryherd - Eric@ExpressControls.com
 * Date: 06 Jan 2015
 *
 * Description: ShadeFunctions.c
 * Various handy routines that read/write commands to the shade motor via I2C.
 * Relies on the b_i2c.c files for low level I2C Commands.
 */
#include "b_i2c.h"
#include "config_app.h"
#include <ZW_pindefs.h>
//#include <zw_mem_api.h>
#include <intrins.h>
#include <string.h>     // strncmp
#include "eeprom.h"
#include "shadefunctions.h"

// Global variables
IBYTE i2c_crc;                          // Global variable used in the I2C routines to compute the running CRC
BYTE i2c_data[16];                       // I2C data buffer

extern FourBytesUnion upperlimit;       // upper limit is always a lower number than the lower limit. Either or both maybe negative but the span is always Lower-Upper which will always be positive.
extern FourBytesUnion lowerlimit;
extern FourBytesUnion targetoffset;
extern FourBytesUnion currentposition;
extern TwoBytesUnion BatteryMillivolts;
extern BYTE MotorVersion;
extern BYTE MotorSubVersion;
extern BYTE MotorStatus;
extern BYTE currentpercentage;
extern BOOL MotorMoving;
extern BYTE LimitModeEnabled;
extern BYTE LimitModeTimer;     // number of 10ms since the last START command received - used to accellerate the motor when setting limits
extern BOOL LimitModeTimerClear;    // clear the timer once the motor has finished moving to start counting until the next START comes in
extern BOOL ReedSwClosed;

void scope_debug(BYTE d); // debugging routine

// Motor Commands
#define SMB_ACCESS_REQUEST              0x00 /* Enable/Disable access to the EEPROM */
#define SMB_COMMAND_FACTORY_LIMITS      0x05 /* set limits back to the original factory setting */
#define SMB_COMMAND_STOP                0x06 /* STOP movement */
#define SMB_COMMAND_TRANSFER_UPLIMIT    0x07 /* Transfer the current position to be the new upper limit */
#define SMB_COMMAND_TRANSFER_DNLIMIT    0x08 /* Transfer the current position to be the new lower limit */
#define SMB_COMMAND_HAND_TOGGLE         0x09 /* Toggle the HAND bit - direction of travel for roller shades */
#define SMB_COMMAND_STATUS              0x7A /* Get STATUS_INPUT register */
#define SMB_COMMAND_BATT_VOLTS          0x8b /* Get Battery */
#define SMB_COMMAND_MOTOR_LIFT_METRICS  0x96 /* Motor LIFT Metrics 11 bytes */
#define SMB_COMMAND_BRD_FIRMWARE_ID     0x9b /* Motor Version */
#define SMB_COMMAND_LIMIT_UP            0xA1 /* Get the Upper Limit */
#define SMB_COMMAND_LIMIT_DOWN          0xA3 /* Get the Lower Limit */
#define SMB_COMMAND_LIMIT_CURRENT       0xA4 /* CURRENT_POSITION */
#define SMB_COMMAND_JOG                 0xD0 /* Jog */
#define SMB_COMMAND_DISTANCE_MOVE       0xD1 /* move */
#define SMB_COMMAND_PROGRAM_MOVE        0xD2 /* extend limits move - allows movement past limit DANGER may damage shade! */

// ReadMotor - sends the motor the CMD and then reads NumBytes number of bytes which are returned in the PTR pointer.
// Will attempt the read up to 3 times if there is an error.
// Returns 0 if OK and an error code if it fails.
BYTE ReadMotor( BYTE cmd, BYTE NumBytes, BYTE * ptr) {
    BYTE retries;
    BOOL done=FALSE;
    BYTE err=I2C_ERR_NONE;
    BYTE err2;
    BYTE i;
    for (retries=0; retries<3 && !done; retries++ ) { // 
        i2c_crc=0;						//initialize the CRC Global
        err = i2c_write_byte(TRUE,  FALSE, MOTOR_I2C_ADDR); // Start, no stop, Slave address is 0x16, WRITE
        if (I2C_ERR_NONE==err) {  // ACK so keep going
            err2 = i2c_write_byte(FALSE, FALSE, cmd);  // I2C command
            err2|= i2c_write_byte(TRUE,  FALSE, MOTOR_I2C_ADDR|I2C_READ); // Start, no stop, Slave address is 0x16, READ
            if (I2C_ERR_NONE==err2) {        // everything is OK - keep going
                for (i=0; i<NumBytes; i++) {
                    *(ptr+i)= i2c_read_byte(FALSE,FALSE); // read the data
                }

                i = i2c_read_byte(FALSE,TRUE);        // CRC
                if (0==i2c_crc) {                     // everything is OK
                    done=TRUE;                        // all done so exit the loop
                } else {               // if the CRC fails, retry twice then give an error
                    err2|=I2C_ERR_CRC;   // failed CRC
                }
            }
        } else if (!(I2C_ERR_ARBLOST&err)) {    // failed to ACK the slave address byte - STOP and clean up but if arbitration was lost just exit as we'll have already tristated SCL/SDA
            i2c_stop_cond();
        }
        if (!done && retries<2) { // wait a little while before trying again if the read failed
            I2C_delay(255);
        }
        err|=err2;
    } // for
    if (retries>1) err|=I2C_ERR_RETRY;
    return(err);
} // ReadMotor

// Write motor - sends the motor the command pointed to by PTR with the NumBytes number of bytes
// If the CRC is NACKed it means the CRC failed so retry up to 3 times. Also check each byte and if nacked then retry.
BYTE WriteMotor(BYTE NumBytes, BYTE * ptr) {
    BYTE retries;
    BOOL done=FALSE;
    BYTE err=I2C_ERR_NONE;
    BYTE i;
    for (retries=0; retries<3 && !done; retries++ ) { // Send up to 3 times if there is an error of some kind
        i2c_crc=0;						//initialize the CRC Global
        err = i2c_write_byte(TRUE,  FALSE, MOTOR_I2C_ADDR); // Start, no stop, Slave address is 0x16, WRITE
        if (I2C_ERR_NONE==err) {  // ACK so keep going
            for (i=0; i<NumBytes && I2C_ERR_NONE==err; i++) {
                err |= i2c_write_byte(FALSE, FALSE, *(ptr+i));  // send the data if there is no error
            }
            if (I2C_ERR_NONE==err) {
                err|= i2c_write_byte(FALSE, TRUE,  i2c_crc); // send CRC then STOP
            }
            if (I2C_ERR_NONE==err) done=TRUE; // if we get this far then we are done
        } else if (!(I2C_ERR_ARBLOST&err)) {    // failed to ACK the slave address byte - STOP and clean up but if arbitration was lost just exit as we'll have already tristated SCL/SDA
            i2c_stop_cond();
        }
        if (!done && retries<2) { // wait a little while before trying again if the read failed
            I2C_delay(255);
        }
    } // for
    if (retries>1) err|=I2C_ERR_RETRY;
    return(err);
} // WriteMotor

// GetBattery gets the current battery level from the motor and returns the battery voltage in millivolts.
// A value of 0 indicates there was an I2C error of some kind.
WORD GetBattery(void) { 
    WORD retval=0; // error value
    BYTE i;
    i=ReadMotor(SMB_COMMAND_BATT_VOLTS, 2, &i2c_data[0]); // 2 byte read - returns an error code if it fails
    if (0==i) { // read worked OK
        if (0x80==i2c_data[0] && 0x80==i2c_data[1]) {             // special value indicating the battery level has not be read since the last time so reuse the last value read
            i2c_data[0]=BatteryMillivolts.b.lsb;
            i2c_data[1]=BatteryMillivolts.b.msb;
        }
        retval=(WORD)((WORD)i2c_data[1]<<8)|(WORD)i2c_data[0]; // pack the 2 bytes
    } else { // read failed so return an error code
        scope_debug(0x88);
        scope_debug(i);
        retval=0;
    }
    return(retval);
} // GetBattery

// GetMotorVersion gets the version of the motor which is placed in the globals MotorVersion and MotorSubVersion
void GetMotorVersion(void) { 
    BYTE ascii[17];
    BYTE i;
    i=ReadMotor(SMB_COMMAND_BRD_FIRMWARE_ID, 17, &ascii[0]);
    if (0==i) {
        if (0==strncmp(&ascii[1],"5120199",7)) { // if the Product ID string matches then make MotorVersion 1.
            MotorVersion=1;
        } else {
            MotorVersion=0;
        }
        if (ascii[10]>='A') i=ascii[10]-'A'+10; // convert the 2 bytes from ASCII hex to binary
        else i=ascii[10]-'0';
        MotorSubVersion=((ascii[9]-'0')<<4)|i; // assuming the upper byte never gets above 9...
    } else {
        MotorVersion   =i; // TODO
//        MotorVersion   =0xde; // flag that the motor read I2C failed for some reason
        MotorSubVersion=0xad;
    }
} // GetMotorVersion

//------------------------------------------------------------------------------
// 	Routine:	GetUpperLimit
//	Inputs:		none
//	Outputs:	update global upperlimit[4] array
//	Return:		0=OK otherwise an error code is returned
//	Purpose:	Reads upper limit from motor via I2C
//	Author:		ER
//------------------------------------------------------------------------------
BYTE GetUpperLimit(void)
{
    BYTE retval;
    retval=ReadMotor(SMB_COMMAND_LIMIT_UP, 5, &i2c_data[0]);
    upperlimit.b[0]=i2c_data[1]; // pick out the correct bytes
    upperlimit.b[1]=i2c_data[2];
    upperlimit.b[2]=i2c_data[3];
    upperlimit.b[3]=i2c_data[4];
    return(retval);
} // GetUpperLimit
//
//------------------------------------------------------------------------------
// 	Routine:	GetLowerLimit
//	Inputs:		none
//	Outputs:	update global Lowerlimit[4] array
//	Return:		0=OK otherwise an error code is returned
//	Purpose:	Reads Lower limit from motor via I2C
//	Author:		ER
//------------------------------------------------------------------------------
BYTE GetLowerLimit(void)
{
    BYTE retval;
    retval=ReadMotor(SMB_COMMAND_LIMIT_DOWN, 5, &i2c_data[0]);
    lowerlimit.b[0]=i2c_data[1]; // pick out the correct bytes
    lowerlimit.b[1]=i2c_data[2];
    lowerlimit.b[2]=i2c_data[3];
    lowerlimit.b[3]=i2c_data[4];
    return(retval);
} // GetLowerLimit

//------------------------------------------------------------------------------
// 	Routine:	GetCurrentPosition
//	Inputs:		none
//	Outputs:	reads the current position from the motor and updates global currentposition[4] array and currentpercentage.
//	Return:		success/failure
//	Purpose:	Reads current postion from motor control
//	Author:		ER
//------------------------------------------------------------------------------
//BYTE GetCurrentPosition(void)
//{
//    BYTE retval;
//    signed long int temp;   // temporary 32 bit variable

//    retval=ReadMotor(SMB_COMMAND_LIMIT_CURRENT, 5, &i2c_data[0]);
//    currentposition.b[0]=i2c_data[1];
//    currentposition.b[1]=i2c_data[2];
//    currentposition.b[2]=i2c_data[3];
//    currentposition.b[3]=i2c_data[4];
//    // Now calulate the current percentage based which is a simple calculation based on the relative position as a fraction of the upper and lower limits
//    temp = lowerlimit.wrd-upperlimit.wrd; // this is the total range which is typically only around 2400. Note that either or both could be negative so we need a signed value. 500 ticks is about 1".
//    temp=(lowerlimit.wrd-currentposition.wrd)*100/temp; // TODO does this need some rounding?
//    if (temp<0) temp=0;             // clip off any overshoot
//    else if (temp>99) temp=99; // ZWave has 99% (0x63) as meaning 100%
//    currentpercentage=(BYTE)temp;
//    return(retval);
//} // GetCurrentPosition


BYTE GetCurrentPosition(void)
{
		BYTE retval;
		BYTE tempval;

		if (PIN_GET(P34)) { // The pushbutton is not pressed
			ReedSwClosed=FALSE;
			tempval = 99;
			currentpercentage=tempval;
		}
		else{
			ReedSwClosed=TRUE;
			tempval = 0;
			currentpercentage=tempval;
		}
		return(retval);
			
//		//read reed sensor for closed position for timetakestoclose,, if not reed assert- Door is open, else Door is closed
//		// Report back current position at 0 or 0x63

}

// Check to see if motor is moving, if so, return TRUE(moving) else return FALSE(not moving)
// Also updates the global MotorStatus
BOOL IsMotorRunning(void)
{
    BOOL retval;        // return value
    if (0==ReadMotor(SMB_COMMAND_STATUS, 2, &i2c_data[0])) { // Motor read OK?
        MotorStatus=i2c_data[1]; 
    } else {    // Motor read failure
        return(FALSE); // if the read fails, it's usually because the motor is still asleep which means it's not moving
    }
    if (i2c_data[1] & 0x80) retval=FALSE; // motor not moving - the bit name is Motor Active but it is really Motor Idle
    else                   retval=TRUE;  // motor  is moving
    return(retval);
} // IsMotorRunning

//------------------------------------------------------------------------------
// 	Routine:	GoToTarget
//	Inputs:		Global targetoffet[4] array
//	Return:		0=OK otherwise an error code
//	Author:		ER
//------------------------------------------------------------------------------
BYTE GoToTarget(void)
{
    BYTE rtnval;
//    if (LimitModeEnabled) {                         // Limit Mode
//        i2c_data[0]=SMB_COMMAND_PROGRAM_MOVE;
//        if (0xffffffff==targetoffset.wrd) {             // go up
//            if (LimitModeTimer<35) {
//                targetoffset.wrd=-LIMIT_SET_MOVE_LONG; // move negative
//            } else {
//                targetoffset.wrd=-LIMIT_SET_MOVE_SHORT;
//            }
//        } else {                                       // go down - should only ever get FF or 01 in limit set mode but if any other values come in just assume to go down.
//            if (LimitModeTimer<35) {
//                targetoffset.wrd=LIMIT_SET_MOVE_LONG; // move positive
//            } else {
//                targetoffset.wrd=LIMIT_SET_MOVE_SHORT;
//            }
//        }
//    } else {                                    // Normal mode
//        i2c_data[0]=SMB_COMMAND_DISTANCE_MOVE;
//    }
//    i2c_data[1]=4; // 4 more bytes to send
//    i2c_data[2]=targetoffset.b[0];
//    i2c_data[3]=targetoffset.b[1];
//    i2c_data[4]=targetoffset.b[2];
//    i2c_data[5]=targetoffset.b[3];
//    rtnval=WriteMotor(6, &i2c_data[0]);    // Send the I2C command to the Motor

		PIN_OUT(P36); // mENABLE
		PIN_HIGH(P36); // mENABLE line put the motor to sleep
		

				

    MotorMoving=TRUE;	                // start polling the motor so see when it has stopped moving
//    if (LimitModeEnabled && LimitModeTimerClear) LimitModeTimer=0; // clear the timer after the motor has moved
	return(rtnval);
} // GoToTarget

//------------------------------------------------------------------------------
// 	Routine:	StopMovement
//	Return:		0=OK else error vode
//	Purpose:	stops motor if moving
//	Author:		ER
//------------------------------------------------------------------------------
BYTE StopMovement(void)
{
	BYTE stat;
	scope_debug(0x44);
    i2c_data[0]=SMB_COMMAND_STOP;
    stat=WriteMotor(1, &i2c_data[0]);    // Send the I2C command to the Motor
	// Motormoving is not cleared here because it may take some time for the motor to stop moving. The polling routine will clear it.
	return(stat);
} // StopMovement

BYTE ShadeJog(void)             // as the name implies - causes the shade to jog
{
	BYTE stat;
    BYTE i;
    i2c_data[0]=SMB_COMMAND_JOG;
// 3/27/2018 - The desire was to shorten the jog on Roller Shade by 2/3 of the default (125 ticks?) But experimentation yielded that the minimum jog is 0.9". A value of 83 00 (decimal) yielded a jog of 1.7". 30, 20, 5 and 0 all resulted in 0.9".
// Wes decided to put the jog distance in NVM and program that specifically for each unit so that the motor firmware can be updated at a later time to properly support this
    i=MemoryGetByte((WORD)&EEOFFSET_JogDistance_far); // Get the jog distance from NVM.
    if (i==0xFF) i=0;   // NVM is unprogrammed so use the default of 0
    i2c_data[1]=i; // LSB - jog by the distance from NVM
    i2c_data[2]=0; // MSB 
    stat=WriteMotor(3, &i2c_data[0]);    // Send the I2C command to the Motor
  //  MotorMoving=TRUE;	
	return(stat);
} // ShadeJog

BYTE FactoryLimits(void)        // Reset the limits to the factory default. The current position becomes 0 so always run when the shade is at the upper limit.
{
	BYTE stat;
    i2c_data[0]=SMB_COMMAND_FACTORY_LIMITS;
    stat=WriteMotor(1, &i2c_data[0]);    // Send the I2C command to the Motor
	return(stat);
} // FactoryLimits

BYTE SetUpperLimit(void)        // Set the current position to be the upper Limit
{
	BYTE stat;
    i2c_data[0]=SMB_COMMAND_TRANSFER_UPLIMIT;
    stat=WriteMotor(1, &i2c_data[0]);    // Send the I2C command to the Motor
	return(stat);
} // SetUpperLimit

BYTE SetLowerLimit(void)        // Set the current position to be the lower Limit
{
	BYTE stat;
    i2c_data[0]=SMB_COMMAND_TRANSFER_DNLIMIT;
    stat=WriteMotor(1, &i2c_data[0]);    // Send the I2C command to the Motor
	return(stat);
} // SetLowerLimit

BYTE ToggleHandBit(void)        // Toggles the direction of the HAND bit - the direction of UP and DOWN - used for roller shades that are wrapped the opposite direction
{
	BYTE stat;
    i2c_data[0]=SMB_COMMAND_HAND_TOGGLE;
    stat=WriteMotor(1, &i2c_data[0]);    // Send the I2C command to the Motor
	return(stat);
} // ToggleHandBit

BYTE StatusRegClear(void)        // clear the status register
{
	BYTE stat;
    i2c_data[0]=SMB_COMMAND_STATUS;
    i2c_data[1]=0xFF;
    i2c_data[2]=0xFF;
    stat=WriteMotor(3, &i2c_data[0]);    // Send the I2C command to the Motor
	return(stat);
} // ToggleHandBit

BYTE GetMotorMetrics(BYTE * ptr)
{
    BYTE retval;
    retval=ReadMotor(SMB_COMMAND_MOTOR_LIFT_METRICS, 11, ptr);
    return(retval);
} // GetUpperLimit
