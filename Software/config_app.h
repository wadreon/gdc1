/**
 * Copyright 2015-2018 Springs Window Fashions - Middleton WI USA
 * All Rights Reserved
 *
 * @file config_app.h
 *
 * @brief This header file contains defines for application version.
 *
 * Author: Eric Ryherd - eric@expresscontrols.com
 *
 */
#ifndef _CONFIG_APP_H_
#define _CONFIG_APP_H_

#ifdef __C51__
#include "ZW_product_id_enum.h"
#include <commandClassManufacturerSpecific.h>
#endif

/****************************************************************************
 * Define the VERSION and REVISION of the firmware
 * APP_VERSION of 0x00 indicates a BETA release and the watchdog is OFF!
 * APP_REVISION is the point revision and is incremented by bug fixes and minor 
 * feature changes.
 */
// During development, comment out the next line which will disable the WatchDog
#define PRODUCTION_RELEASE
//#define ENABLE_EYE

#ifdef PRODUCTION_RELEASE
	#ifdef ENABLE_EYE
		#define APP_VERSION 0x0A
	#else
		#define APP_VERSION 0x0B
	#endif
#define APP_REVISION 02
#else       // BETA release! Watchdog is turned off!
#define APP_VERSION 0x01
#define APP_REVISION 58
#endif
 
#ifdef PRODUCTION_RELEASE
 #define WATCHDOG_ENABLED
// Compile out the debugging pin toggles. The MISO pin is used for debug messages as the UART cannot be used since the motor uses those pins.
#define scope_debug(dat) 
#endif

/****************************************************************************
 *
 * Defines used to initialize the Z-Wave Plus Info Command Class.
 *
 ****************************************************************************/
#define APP_ROLE_TYPE ZWAVEPLUS_INFO_REPORT_ROLE_TYPE_SLAVE_SLEEPING_LISTENING
#define APP_NODE_TYPE ZWAVEPLUS_INFO_REPORT_NODE_TYPE_ZWAVEPLUS_NODE
#define APP_ICON_TYPE ICON_TYPE_GENERIC_WINDOW_COVERING_POSITION_ENDPOINT_AWARE
#define APP_USER_ICON_TYPE ICON_TYPE_GENERIC_WINDOW_COVERING_POSITION_ENDPOINT_AWARE

#define REQUESTED_SECURITY_AUTHENTICATION SECURITY_AUTHENTICATION_SSA
// Normally S2_Authenticated will be used as part of SmartStart.
#define REQUESTED_SECURITY_KEYS SECURITY_KEY_S2_AUTHENTICATED_BIT | SECURITY_KEY_S2_UNAUTHENTICATED_BIT

/****************************************************************************
 *
 * Defines used to initialize the Manufacturer Specific Command Class.
 *
 ****************************************************************************/
#define APP_MANUFACTURER_ID     0x026E	/*Springs Window Fashions */
#if defined(CSZ1)
#define APP_PRODUCT_TYPE_ID     (WORD)((WORD)'G'<<8 | (WORD)'D')
#define APP_PRODUCT_ID          (WORD)((WORD)'C'<<8 | (WORD)'1')
#elif defined(RSZ1)
#define APP_PRODUCT_TYPE_ID     (WORD)((WORD)'R'<<8 | (WORD)'S')
#define APP_PRODUCT_ID          (WORD)((WORD)'Z'<<8 | (WORD)'1')
#endif

////////Firmware ID is required to be the same for all versions of the product but unique for each product.
#if defined(CSZ1)
#define APP_FIRMWARE_ID         0x1031
#elif defined(RSZ1)
#define APP_FIRMWARE_ID         0x8283
#endif

#define APP_DEVICE_ID_TYPE      DEVICE_ID_TYPE_PSEUDO_RANDOM
#define APP_DEVICE_ID_FORMAT    DEVICE_ID_FORMAT_BIN

//#define GENERIC_TYPE    GENERIC_TYPE_SWITCH_MULTILEVEL
//#define SPECIFIC_TYPE   SPECIFIC_TYPE_CLASS_C_MOTOR_CONTROL

#define GENERIC_TYPE    GENERIC_TYPE_SWITCH_BINARY
#define SPECIFIC_TYPE   SPECIFIC_TYPE_POWER_SWITCH_BINARY
/**
 * See ZW_basic_api.h for ApplicationNodeInformation field deviceOptionMask
 */
#define DEVICE_OPTIONS_MASK   APPLICATION_FREQ_LISTENING_MODE_1000ms | APPLICATION_NODEINFO_OPTIONAL_FUNCTIONALITY
// Listening modes are 00=250ms FLiRS, 01=1000ms FLiRS, 02=Always ON, anything else is the default.
#define DEFAULT_LISTENING_MODE 0x00

/****************************************************************************
 *
 * Defines used to initialize the Association Group Information (AGI) Command Class.
 * Only the LifeLine group (1) is supported a single NodeID which is normally the Z-Wave System Controller (aka Hub).
 ****************************************************************************/
#define NUMBER_OF_ENDPOINTS         0
#define MAX_ASSOCIATION_GROUPS      1
#define MAX_ASSOCIATION_IN_GROUP    1

#define AGITABLE_LIFELINE_GROUP \
  {COMMAND_CLASS_SWITCH_MULTILEVEL, SWITCH_MULTILEVEL_REPORT}, \
  {COMMAND_CLASS_BATTERY, BATTERY_REPORT}, \
  {COMMAND_CLASS_FIRMWARE_UPDATE_MD_V2, FIRMWARE_UPDATE_MD_REQUEST_REPORT_V2}, \
  {COMMAND_CLASS_DEVICE_RESET_LOCALLY, DEVICE_RESET_LOCALLY_NOTIFICATION}

#define  AGITABLE_ROOTDEVICE_GROUPS NULL /* Only have Group 1 so this is null */

// Number of 10ms periods to wait before trying to talk to the motor (150ms is minimum)
#define MOTOR_POWER_UP_DELAY 17
 
// These 2 defines set the distance of the 0xD2 move during a Limit Set mode. The SHORT is sent when there has been more than 300ms between START commands, the LONG when it has been less.
#if defined(RSZ1)
	#define LIMIT_SET_MOVE_SHORT   5 /* Short move distance in Limit set mode - note that it is much smaller for roller shades because of their larger movement per tick*/
    #define LIMIT_SET_MOVE_LONG  200 /* long move distance in Limit set mode */
#elif defined(CSZ1)
	#define LIMIT_SET_MOVE_SHORT  25 /* Short move distance in Limit set mode */
    #define LIMIT_SET_MOVE_LONG  230 /* long move distance in Limit set mode */
#endif

#define LIMIT_SET_MINIMUM_SPACING 240 /* Minimum number of Ticks between the upper and lower limits - not allowed to set it less than this */
	

#ifdef __C51__
typedef union { // fast way to shift between bytes and words
    struct { 
			BYTE msb;
      BYTE lsb;
    }b;
		WORD wrd;
} TwoBytesUnion;
typedef union { // fast way to shift between bytes and words
		BYTE b[4]; // 0 is the MSB and 3 is the LSB (Big endian) which matches the Keil C51 compiler signed long integer
		signed long int wrd; // Note this is a signed 32-bit value and can be positive or negative. Typically the upperlimit is close to 0 and lower limit is 0x240.
} FourBytesUnion;
#endif
#ifdef __C51__
typedef enum _LED_MODE_TYPE_
{
   LED_OFF,
   LED_G, //green
   LED_Y, //yellow
   LED_R  //red
} LED_MODE_TYPE;

typedef enum _LED_DUTY_TYPE_
{
   LED_SOLID,   // On 100%
   LED_BLINK,   // 50/50 duty cycle
   LED_WINKON,  // on 10%, off 90%
   LED_WINKOFF  // on 90%, off 10%
} LED_DUTY_TYPE;	
#endif

#define MOTOR_I2C_ADDR 0x16 /* Motor I2C slave address in bits 7:1. Bit 0 is the READ bit */
#define I2C_RETRIES    10 /* Number of times an I2C Command is retried if there is an error of some kind before giving up */

#define HOME_DISABLED 0x8842b00b		/* if HOME is set to this value then HOME is disabled */
#define HOME_DEADBAND 25						/* If current position is within this many ticks of HOME then a move to HOME is ignored to avoid "ticks" because the motor overshoots a little */

// Travel_Ticks_Per_Sec is the number of "counts" or "ticks" the motor moves in 1 second.
// The calculated value for Cellular Shades should be 478*2 but the values were empirically derived thru measurements.
#if defined(RSZ1)
#define TRAVEL_TICKS_PER_SEC (signed long int)(191)
#else // CSZ1
#define TRAVEL_TICKS_PER_SEC (signed long int)(700)
#endif


// Battery level thresholds - CRITICAL is RED, LOW is yellow
#define BATTERY_CRITICAL 5
#define BATTERY_LOW      20

// Set Limit Mode timeout is the number of minutes until Limit Mode expires
#define LIMIT_MODE_TIMEOUT 4

// Timing parameters for blinking the LED in 10msec	
// 100=1Hz, 25=4Hz, Accelerate mode is flagged with a 255. This is a BYTE value.
#define LED_SLOW   150
#define LED_MEDIUM 50
#define LED_FAST   25
#define LED_ACCEL  255

// Time in 10ms periods to stay awake after receving a command.
#define AWAKE_AFTER_RF_CMD  500

// time the LED is on SOLID after completing an operation	
#define LED_SOLID_TIMEOUT 100
// time the PGM button has to be held down before Learn Mode is entered and NodeInfo is sent
#define SEND_NODE_INFO_TIMEOUT 200

// time in 10ms for Inclusion mode to expire to allow time for setting up the LifeLine and other housekeeping tasks
#define INCLUSION_TIMEOUT 500

// time in 10ms for the Diagnostic Uart Protocol to exit upon powerup
#define DIAG_PROTOCOL_TIMEOUT 20

#define START_INCLUSION_MODE  2 /* seconds button held down until enter inclusion mode */
#define START_ZWAVE_RESET     7 /* seconds button held down until enter Z-Wave Factory Reset mode */
#define START_FACTORY_RESET  11 /* seconds button held down until enter Factory Limit Reset mode */
#define FULL_RESET_TIME      16 /* seconds button held down until the full factory reset happens - both Z-Wave and Limits*/

// LED defines - TODO: should OFF put the pin in tristate to be lower power? Pullup should on or off?
#define GRNON   PIN_HIGH(P04)
#define GRNOFF  PIN_LOW(P04)
#define REDON   PIN_HIGH(P10)
#define REDOFF  PIN_LOW(P10)

#define FIRMWARE_UPGRADABLE		0xFF /* 00=Not, 0xFF=upgradeable*/
#define ZAF_BUILD_NO 52445				/* no idea what this is for but the Sigma Makefiles set it to this value */

// LED mode defines for calls to LED_set_mode - note that this fills in all 3 parameters
#define LED_GREEN_FAST_BLINK  	LED_G,  LED_FAST,   LED_BLINK
#define LED_GREEN_FAST_WINKON  	LED_G,  LED_FAST,   LED_WINKON
#define LED_GREEN_MEDIUM_BLINK 	LED_G,  LED_MEDIUM, LED_BLINK
#define LED_GREEN_SLOW_BLINK 	  LED_G,  LED_SLOW,   LED_BLINK
#define LED_GREEN_SOLID         LED_G,  LED_SLOW,   LED_SOLID
#define LED_YELLOW_FAST_BLINK 	LED_Y,  LED_FAST,   LED_BLINK
#define LED_YELLOW_MEDIUM_BLINK LED_Y,  LED_MEDIUM, LED_BLINK
#define LED_YELLOW_FAST_WINKON  LED_Y,  LED_FAST,   LED_WINKON
#define LED_YELLOW_SLOW_BLINK   LED_Y,  LED_SLOW,   LED_BLINK
#define LED_YELLOW_SOLID        LED_Y,  LED_SLOW,   LED_SOLID
#define LED_RED_ACCEL           LED_R,  LED_ACCEL,  LED_BLINK
#define LED_RED_SOLID          	LED_R,  LED_SLOW,   LED_SOLID
#define LED_RED_FAST_BLINK  	  LED_R,  LED_FAST,   LED_BLINK
#define LED_RED_FAST_WINKON  	  LED_R,  LED_FAST,   LED_WINKON
#define LED_ALL_OFF            	LED_OFF,LED_SLOW,   LED_SOLID

// diagnostic defines - See diag_protocol.c for more details
#define MOTOR_ENB_HIGH PIN_OUT(P36);PIN_HIGH(P36);
#define MOTOR_ENB_LOW  PIN_OUT(P36);PIN_LOW(P36);
#define LED1REDON    PIN_HIGH(P10); PIN_OUT(P10);
#define LED1REDOFF   PIN_LOW( P10); PIN_OUT(P10);
#define LED1GREENON  PIN_HIGH(P04); PIN_OUT(P04);
#define LED1GREENOFF PIN_LOW( P04); PIN_OUT(P04);
#define LED2REDON    
#define LED2REDOFF   
#define LED2GREENON  
#define LED2GREENOFF 
#ifdef __C51__
void MeasureBattery(void);
extern BYTE BatteryPercentage;
#define GET_BATTERY_MEASUREMENT 
#define BATTERYPERCENTAGE BatteryPercentage       /* battery percentage cannot be measured by the Motor Radio - has to come from the motor - this variable may not have a valid value until after the motor is powered up */
#define KEYPAD_INIT PIN_IN(P11,0);   /* make sure the pushbutton is an input which it will be already and has an external pullup */
#define KEYPAD_EXIT                  /* nothing needed here */
#define GET_KEYPAD_STATE KeypadState=(PIN_GET(P11) ? 0:8); /* the pin will be low when pressed */
#define D_PUSHBUTTON_GET PIN_GET(P11)
#endif
#endif /* _CONFIG_APP_H_ */
