/****************************************************************************
 *
 * Copyright (c) 2001-2014
 * Sigma Designs, Inc.
 * All Rights Reserved
 *
 *---------------------------------------------------------------------------
 *
 * Description: Application NVM variable definitions
 *
 * Author:  Erik Friis Harck
 *
 * Last Changed By:  $Author: eric $
 * Revision:         $Revision: 1046 $
 * Last Changed:     $Date: 2016-06-29 14:51:57 -0400 (Wed, 29 Jun 2016) $
 *
 ****************************************************************************/
#pragma USERCLASS(CONST=NVM)

/* Make sure compiler won't shuffle around with these variables,            */
/* as there are external dependencies.                                      */
/* Note from Keil C51 manual:                                               */
/* Variables with memory type, initilalization, and without initilalization */
/* have all different tables. Therefore only variables with the same        */
/* attributes are kept within order.                                        */
/* Note from Keil knowledgebase: http://www.keil.com/support/docs/901.htm   */
/* "The order is not necessarily taken from the variable declarations, but  */
/* the first use of the variables."                                         */
/* Therefore, when using #pragma ORDER to order variables, declare them in  */
/* the order they should be in a collection. And none of them may be        */
/* declared or known in any way from other header files.                    */
#pragma ORDER

/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/
#include <ZW_basis_api.h>
#include "eeprom.h"
#include <ZW_nvm_descriptor.h>

/****************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                       */
/****************************************************************************/

/************************************/
/*      NVM variable definition     */
/************************************/

/*--------------------------------------------------------------------------*/
/* NVM layout SwitchOnOff (as in t_nvmModule) (begin)                       */

/* Offset from &nvmModule where nvmModuleDescriptor structure is placed */
t_NvmModuleSize far nvmApplicationSize = (t_NvmModuleSize)&_FD_EEPROM_L_;

/* NVM variables for your application                                       */
BYTE far EEOFFSET_ASSOCIATION_START_far[(NUMBER_OF_ENDPOINTS_NVM_MAX + 1) * ASSOCIATION_SIZE_NVM_MAX];
BYTE far EEOFFSET_ASSOCIATION_MAGIC_far;
BYTE far EEOFFSET_MAGIC_far;           /* MAGIC */
BYTE far EEOFFSET_MAN_SPECIFIC_DEVICE_ID_far[MAN_DEVICE_ID_SIZE];
BYTE far EEOFFSET_MAN_SPECIFIC_MAGIC_far;           /* MAGIC */
BYTE far EEOFFSET_TEST_NODE_ID_far;
BYTE far EEOFFSET_TEST_POWER_LEVEL_far;
BYTE far EEOFFSET_TEST_FRAME_COUNT_SUCCESS_far[2];
BYTE far EEOFFSET_TEST_STATUS_far;
BYTE far EEOFFSET_TEST_SOURCE_NODE_ID_far;
BYTE far EEOFFSET_HomePosition_far[4];       /* Motor position of the HOME position */
BYTE far EEOFFSET_LISTENING_far;				/* if non-zero, then set the listening bit and remain awake */
BYTE far EEOFFSET_MotorStatusMove_far;		/* Motor Status error count of motor move errors */
BYTE far EEOFFSET_MotorStatusEep_far;		/* Motor Status error count of EEPROM errors */
BYTE far EEOFFSET_MotorStatusI2C_far;		/* Motor Status error count of I2C errors */
BYTE far EEOFFSET_HardwareVersion_far[16];	/* Hardware Version string - typically 11 bytes */
BYTE far EEOFFSET_HardwareVersionLen_far;	/* Hardware Version string length */
BYTE far EEOFFSET_NumberPORResets_far;
BYTE far EEOFFSET_NumberWatchDogResets_far;

#ifdef SECURITY
EEOFFS_NETWORK_SECURITY_STRUCT far EEOFFS_SECURITY;
#endif
BYTE far EEOFFSET_ASSOCIATION_ENDPOINT_START_far[(NUMBER_OF_ENDPOINTS_NVM_MAX + 1) * ASSOCIATION_SIZE_NVM_MAX];
EEOFFSET_TRANSPORT_CAPABILITIES_STRUCT far EEOFFSET_TRANSPORT_CAPABILITIES_START_far[(NUMBER_OF_ENDPOINTS_NVM_MAX + 1) * ASSOCIATION_SIZE_NVM_MAX];
BYTE far EEOFFSET_ASSOCIATION_ENDPOINT_MAGIC_far;
#if (NUMBER_OF_ENDPOINTS_NVM_MAX > 0)
// COLSEN FW TODO: Should this #if be added in the header file as well or is the NVM allocated?
BYTE far EEOFFSET_SWITCH_ALL_MODE_far[NUMBER_OF_ENDPOINTS_NVM_MAX];
#else
BYTE far EEOFFSET_SWITCH_ALL_MODE_far[1];
#endif

// Used by battery_plus module.
DWORD far EEOFFSET_SLEEP_PERIOD_far;

BYTE far EEOFFSET_MAGIC_SDK_6_70_ASSOCIATION_SECURE_far;


//////////ADD ALL NEW NVM variable down here so the MAGIC numbers are not damaged!!!
BYTE far EEOFFSET_JogDistance_far;          // distance used for a Jog
BYTE far EEOFFSET_JogDistanceNOT_far;       // OnesComplement of JogDistanceFar - if they are mismatched then the default (0) is written to ensure the data is clean


/* NVM module descriptor for module. Located at the end of NVM module.      */
/* During the initialization phase, the NVM still contains the NVM contents */
/* from the old version of the firmware.                                    */
t_nvmModuleDescriptor far nvmApplicationDescriptor =
{
  (t_NvmModuleSize)&_FD_EEPROM_L_,      /* t_NvmModuleSize wNvmModuleSize   */
  NVM_MODULE_TYPE_APPLICATION,          /* eNvmModuleType bNvmModuleType    */
  (WORD)&_APP_VERSION_                  /* WORD wNvmModuleVersion           */
};

/* NVM layout SwitchOnOff (as in t_nvmModule) (end)                         */
/*--------------------------------------------------------------------------*/

/* NVM module update descriptor for this new version of firmware. Located   */
/* in code space.                                                           */
const t_nvmModuleUpdate code nvmApplicationUpdate =
{
  (p_nvmModule)&nvmApplicationSize,     /* nvmModulePtr                     */
  /* nvmApplicationDescriptor is the first new NVM variable since devkit_6_5x_branch */
  (t_NvmModuleSize)((WORD)&nvmApplicationDescriptor),  /* wNvmModuleSizeOld */
  {
    (t_NvmModuleSize)&_FD_EEPROM_L_,    /* t_NvmModuleSize wNvmModuleSize   */
    NVM_MODULE_TYPE_APPLICATION,        /* eNvmModuleType bNvmModuleType    */
    (WORD)&_APP_VERSION_                /* WORD wNvmModuleVersion           */
  }
};

