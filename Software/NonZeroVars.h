// This file is where all variables that need to persist while sleeping are stored.
// There is a small section of RAM in the ZM5x0x that remains powered even when the rest of the chip is powered off.
//

/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/

// ===========================================================================
// NON_ZERO_START_ADDR is 42 bytes of RAM that retains state while sleeping.
// The rest of RAM is zeroed on powerup.
extern TwoBytesUnion BatteryMilliVolts ; // millivolt reading of the battery level which is updated after every move.
extern BYTE BatteryPercentage          ; // battery level in percentage - 0-100%
extern BYTE DirectionLast              ; // direction of last travel - 0=down, FF=UP
extern FourBytesUnion currentposition  ; // current motor position - signed! 32 bit value, 0=upperlimit
extern FourBytesUnion upperlimit       ; // Upper limit motor Ticks - usually close to 0 but can be negative
extern FourBytesUnion lowerlimit       ; // Lower limit motor Ticks - usually a positive number and always larger than UL - 500 LSBs=1"
extern FourBytesUnion homeposition     ; // The current HOME position
extern BYTE currentpercentage          ; // current motor position as a percentage - not as accurate as the currentposition
extern BYTE Lastpercentage             ;
extern BYTE MotorVersion               ; // Version of the motor - 1 if Motor=5120199 otherwise 0
extern BYTE MotorSubVersion            ; // SubVersion of the motor - last 2 bytes of a 0x9B command converted from ASCII to binary
extern BYTE PowerLevelTestNodeID       ; // nodeID to send the Tests to - these two need to remain valid even after sleeping so the GET will report the result of the test in the CTT
extern WORD PowerLevelGoodCounter      ; // Number of test frames that were good
extern BYTE BatteryAboveBattLow        ; // Flag indicating that the previous battery measurement was above BATTERY_LOW so we know when to send a low battery warning
extern BYTE BatteryLast                ; // Battery percentage the last time it was reported to the lifeline
extern BYTE ListeningMode              ; // 00=250ms FLiRS, 01=1000ms FLiRS, 02=Always On, anything else is also 1s FLiRS
extern XBYTE RFTestState;
extern XBYTE debug[];
