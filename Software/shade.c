/***************************************************************************
 * Copyright 2015-2016
 * Springs Window Fashions - Middleton WI USA
 * All Rights Reserved
 *---------------------------------------------------------------------------
 * Author: Eric Ryherd - Eric@ExpressControls.com
 * Original Code based on the SDK sample code by Sigma FAE Ben Garcia
 * Date: 31 Dec 2015
 *
 * Last Changed By:  $Author: eric $
 * Revision:         $Revision: 1058 $
 * Last Changed:     $Date: 2016-08-26 17:00:16 -0400 (Fri, 26 Aug 2016) $
 *
 * Description: Shade.c
 * SDS11847-13 Device Type document requires that the following mapping take place.
 * The Device Type MUST support:
 *  Basic Set = 255 maps to Multilevel Switch = 255
 *  Basic Set = 0 maps to Multilevel Switch = 0
 *  Basic Set = 1-99 maps to Multilevel Switch = 1-99
 *  Basic Get/Report maps to Multilevel Switch Get/Report.
 *  A battery low warning (battery report of 0xFF) is required to be sent when the battery is below BATTERY_CRITICAL.
 *    The low battery warning is only sent when the level changes to be below BATTERY_CRITICAL.
 *    When the battery level is measured to be above BATTERY_LOW, then the low battery warning is enabled again.
 *    Battery readings are only made when the shade moves.
 *
 * Z-Wave radio for the Springs Window Fashions cellular shade/roller shade motors.
 * The main function of the Z-Wave radio is to take the Z-Wave commands and convert
 * them into the appropriate I2C commands to be sent to the Motor controller.
 *
 * IO Interface: ZM5202 is used so the pinout is limited but sufficient for this project.
 * P11 - pushbutton - goes low when pressed - wakes up chip - external pullup
 * P10 - RED LED - HIGH turns the LED on
 * P04 - GREEN LED - HIGH turns the LED on
 * P36 - Motor_enable - high wakes it up, low puts it to sleep. Must give 150ms before sending any I2C commands after going high.
 * P35 - I2C SCL - Clock line to motor
 * P34 - I2C SDA - Data line to motor
 * The other pins are used for the SPI FLASH and for programming the ZM5202.
 *
 * The motor has a 150ms delay before it is ready to communicate over I2C.
 * Since most of the I2C commands take ~2ms to complete, we can only do 1 every ApplicationPoll.
 * This gives the Sigma code time to process any RF traffic between I2C commands.
 * Sigma requires that ApplicationPoll return every couple of ms or RF data may be lost.
 * Thus, each of the commands has a xxxNow boolean the queues the command and 1 I2C transfer happens each ApplicationPoll.
 */

/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/


#include "config_app.h"

#include <ZW_slave_api.h>
#ifdef ZW_SLAVE_32
#include <ZW_slave_32_api.h>
#else
#include <ZW_slave_routing_api.h>
#endif  /* ZW_SLAVE_32 */

#include <ZW_classcmd.h>
#include <ZW_mem_api.h>
#include <ZW_TransportLayer.h>

#include <eeprom.h>
//#include <ZW_uart_api.h>

#include <misc.h>

#ifdef BOOTLOADER_ENABLED
#include <ota_util.h>
#include <CommandClassFirmwareUpdate.h>
#endif

#include <math.h>
#include <intrins.h>
#include "shadefunctions.h"

#include <ZW_power_api.h>

#include <association_plus.h>
#include <agi.h>
#include <CommandClassAssociation.h>
#include <CommandClassAssociationGroupInfo.h>
#include <CommandClassVersion.h>
#include <CommandClassPowerLevel.h>
#include <CommandClassDeviceResetLocally.h>
#include <CommandClassBasic.h>
#include <CommandClassMultilevelSwitch.h>//bg
#include <CommandClassWakeUp.h>//bg
#include <ZW_plus_version.h>
#include <intrins.h>        // NOP defined in here
#include <ZAF_pm.h>
#include <ZW_nvr_api.h>
#include "NonZeroVars.h"

/****************************************************************************/
/*                      PRIVATE TYPES and DEFINITIONS                       */
/****************************************************************************/

//  ==========================================================================
//                   function Prototypes
void LoadConfiguration(void);
void SetDefaultConfiguration(void);
void SaveConfiguration(void);
void ZCB_DeviceResetLocallyDone( BYTE status);
void SendBattery(BYTE dstNodeID, BYTE value);
void StopMovementNow(void);
void Led10ms(void);
void ZCB_PowerLevelTestCallback(BYTE txStatus);
void I2CCommand(BYTE cmd);
void SetHomeFiftyPercent(void);
void SendMultilevelReport(void);
void SendMotorMetrics(BYTE SourceNodeID);
void OneUsDelay(unsigned char delay );
void RFTest(void);
BYTE diag_protocol_enabled(void);
void diag_protocol_init(void);
int diag_protocol_poll(void);

#ifdef BOOTLOADER_ENABLED
void ZCB_OTAFinish(OTA_STATUS otaStatus);
BOOL ZCB_OTAStart();
#endif

void scope_debug(BYTE dat);
void LED_set_mode(LED_MODE_TYPE mode, BYTE speed, LED_DUTY_TYPE duty);
void DebugSendData(BYTE dstNodeID, BYTE d0, BYTE d1, BYTE D2, BYTE D3, BYTE D4) ;
BYTE SetMotorPosition(BYTE val);
received_frame_status_t MyhandleCommandClassPowerLevel( RECEIVE_OPTIONS_TYPE_EX *rxOpt, ZW_APPLICATION_TX_BUFFER *pCmd, BYTE   cmdLength);
received_frame_status_t MyhandleCommandClassMultiLevelSwitch( RECEIVE_OPTIONS_TYPE_EX *rxOpt, ZW_APPLICATION_TX_BUFFER *pCmd, BYTE   cmdLength);

TRANSMIT_OPTIONS_TYPE_SINGLE_EX TxOptionsEx_dat;  // Structure that holds all the options for SendData - way more complicated than it needs to be!
TRANSMIT_OPTIONS_TYPE_SINGLE_EX *pTxOptionsEx=&TxOptionsEx_dat; // pointer to the structure. Used for unsolicited messages.
MULTICHAN_NODE_ID destNode;                     // Structure that holds unsolicited destination NodeID used in TRANSMIT_OPTIONS_TYPE_SINGLE_EX.

void ZCB_LearnModeCallback(BYTE bStatus, BYTE nodeID);

// ActiveJobs is from the 6.61 which changed to ZAF_mutex_isActive() in SDK 6.81 - why Sigma changes this stuff I have NO IDEA!
#define ActiveJobs() ZAF_mutex_isActive()

/****************************************************************************/
/*                              PRIVATE DATA                                */
/****************************************************************************/

/***
 *     _        _______  ______   _______ _________ _        _______  _______ 
 *    ( (    /|(  ___  )(  __  \ (  ____ \\__   __/( (    /|(  ____ \(  ___  )
 *    |  \  ( || (   ) || (  \  )| (    \/   ) (   |  \  ( || (    \/| (   ) |
 *    |   \ | || |   | || |   ) || (__       | |   |   \ | || (__    | |   | |
 *    | (\ \) || |   | || |   | ||  __)      | |   | (\ \) ||  __)   | |   | |
 *    | | \   || |   | || |   ) || (         | |   | | \   || (      | |   | |
 *    | )  \  || (___) || (__/  )| (____/\___) (___| )  \  || )      | (___) |
 *    |/    )_)(_______)(______/ (_______/\_______/|/    )_)|/       (_______)
 *                                                                            
 * List of Z-Wave Command Classes (CC) supported by this product
 */
static code BYTE nodeInfo[] = {         // Command Classes sent in the NIF during classic inclusion
    COMMAND_CLASS_ZWAVEPLUS_INFO,       // Required to be 1st CC in the list
    COMMAND_CLASS_SWITCH_MULTILEVEL,    // SDS12657 Table 1 recommends the first 6 CCs be the most important as some hubs only read the first 6.
    COMMAND_CLASS_ASSOCIATION,
    COMMAND_CLASS_ASSOCIATION_GRP_INFO,
    COMMAND_CLASS_MANUFACTURER_SPECIFIC,
    COMMAND_CLASS_VERSION,
    COMMAND_CLASS_DEVICE_RESET_LOCALLY,
    COMMAND_CLASS_POWERLEVEL,
    COMMAND_CLASS_FIRMWARE_UPDATE_MD_V2,
//    COMMAND_CLASS_SECURITY_2,         // commented out for SDK 6.81.01  - but all logic remains in place.
    COMMAND_CLASS_SUPERVISION,
    COMMAND_CLASS_TRANSPORT_SERVICE_V2,
    COMMAND_CLASS_BATTERY               // Battery CC is last so it can be dropped if the listening bit it set due to the device being wall powered
};

/* Secure nodeInfolist. These command classes are required to be sent encrypted if joined securely. */
static code BYTE SecNodeInfo[] = {
    COMMAND_CLASS_SWITCH_MULTILEVEL,
    COMMAND_CLASS_ASSOCIATION,
    COMMAND_CLASS_ASSOCIATION_GRP_INFO,
    COMMAND_CLASS_MANUFACTURER_SPECIFIC,
    COMMAND_CLASS_VERSION,
    COMMAND_CLASS_DEVICE_RESET_LOCALLY,
    COMMAND_CLASS_POWERLEVEL,
    COMMAND_CLASS_FIRMWARE_UPDATE_MD_V2,
    COMMAND_CLASS_BATTERY               // Battery CC is last so it can be dropped if the listening bit it set due to the device being wall powered
};

/**
 * Unsecure node information list when included Securely.
 **/
static code BYTE cmdClassListNonSecureIncludedSecure[] =
{
  COMMAND_CLASS_ZWAVEPLUS_INFO,
  COMMAND_CLASS_SECURITY_2,
  COMMAND_CLASS_TRANSPORT_SERVICE_V2,
  COMMAND_CLASS_SUPERVISION
};

/**
 * Structure includes application node information list's and device type.
 */
APP_NODE_INFORMATION m_AppNIF =
{
  nodeInfo, sizeof(nodeInfo),
  cmdClassListNonSecureIncludedSecure, sizeof(cmdClassListNonSecureIncludedSecure),
  SecNodeInfo, sizeof(SecNodeInfo),
  DEVICE_OPTIONS_MASK, GENERIC_TYPE, SPECIFIC_TYPE
};

const char GroupName[]   = "Lifeline";
CMD_CLASS_GRP  agiTableLifeLine[] = {AGITABLE_LIFELINE_GROUP};

BYTE myNodeID = 0;

IWORD myTickTime;            // SDK global variable that increments every 10mSec
IWORD myTickTimeLast;        // the value of tickTime the last time it was checked

typedef struct _ASSOCIATION_GROUP_
{
  MULTICHAN_NODE_ID subGrp[MAX_ASSOCIATION_IN_GROUP];
} ASSOCIATION_GROUP;
extern ASSOCIATION_GROUP groups[NUMBER_OF_ENDPOINTS+1][MAX_ASSOCIATION_GROUPS];

extern BOOL RFTestRunning;  // RF Test Mode is currently running

// Timer variables
BOOL TenmSecNow=FALSE;      // goes true once every 10ms
BOOL OneSecondNow=FALSE;    // goes true once every one second
BOOL OneMinuteNow=FALSE;    // True once/minute
BOOL OneHourNow=FALSE;      // true once/hour
IBYTE OneSecondCount=0;
BYTE OneMinuteCount=0;
BYTE ActiveJobsCount=0;     // seconds that the Mutex has been set - used as a trigger for the Watchdog as mutex should never be set for very long

BOOL ButtPressed;            // TRUE when the button is pressed and false when it is not
BOOL ButtPressedLast=FALSE;  // state of the pushbutton the last time we checked
BYTE ButtPressedCount;       // number of seconds the pushbutton has been pressed

BOOL ResetSent=FALSE;        // Factory reset has been sent, just waiting for the watchdog to trigger and go thru reset

WORD SleepCounter=200;    // number of 10ms periods before going to sleep - 2sec is a timeout just in case
WORD SleepCounterLast;

LED_MODE_TYPE LED_mode=LED_OFF;         // Current operating mode of the LED
BYTE LED_speed=LED_SLOW;                // Current blink rate
LED_DUTY_TYPE LED_duty=LED_BLINK;       // Current duty cycle
IBYTE LED_count;                        // LED counter of number of 10ms periods to do something
WORD LEDOnTimer;                        // Number of  10ms periods before turning the LED off
BOOL LED_state=FALSE;                   // TRUE=ON, FALSE=OFF
BYTE LED_BlinkCount;                    // Number of times to blink the LED then turn off
BOOL FirmwareDownloadUnderway=FALSE;    // FirmwareDownload in process so don't go to sleep
BYTE wakeupReason;                      // reason for waking up from InitHW

FourBytesUnion targetoffset;            // delta motor ticks for the next shade movement

static BYTE onOffState; // percentage setting of the shade 0-100% or FF. 0=closed, 100=open, FF=HOME

BYTE MotorPowerUpDelay;     // wait until this has counted to 0 before talking to the motor
BOOL MotorMoving;           // TRUE when the motor is moving and we are tracking it's position - set in shadefunctions.c, only cleared in this file
BYTE MotorMovingCounter=10; // 10ms periods to check on the status of the motor
BYTE RelayOnCount=2;
BYTE gdDelay=6;		//increments of 200ms
BOOL ReedSwClosed; 



// Motor Status is read via a 0x7A command which is done every 200ms while the motor is moving
#define MOTOR_STATUS_BATT_READY 0x40
#define MOTOR_STATUS_MOVE_ERROR 0x20
#define MOTOR_STATUS_EEP_ERROR  0x10
#define MOTOR_STATUS_I2C_ERROR  0x08
#define MOTOR_STATUS_ERROR      (MOTOR_STATUS_MOVE_ERROR | MOTOR_STATUS_EEP_ERROR | MOTOR_STATUS_I2C_ERROR)
BYTE MotorStatus;           // Most recent read of the Motor STATUS_INPUT register (0x7A) - note that it may not have valid data.
                            // 7=Motor Idle (1=idle, 0=moving)
                            // 6=Battery valid (1=ok to read battery, 0=don't read it)
                            // 5=Motor Move Error - typically over current but read 0x96 for more details
                            // 4=EEPROM write error
                            // 3=I2C error detected by the motor
                            // 2=shade is above UL
                            // 1=shade is below LL
                            // 0=RESERVED
BYTE MotorStatusMove;       // counts of errors during this power up cycle
BYTE MotorStatusEep;
BYTE MotorStatusI2C;

BYTE TimerHandleDevRst=0xFF; // timer handle - FF is the flag that the timer is not running - used in Device Locally Reset

// All variables are 0 out of reset - the Sigma code zeroes all of RAM
BOOL GetMotorULimitNow;     // Each of these gets the respective block of data via I2C
BOOL GetMotorLLimitNow;
BOOL MotorLimitsHaveChanged;
BOOL GetMotorCurrentPositionNow;
BOOL GetMotorVersionNow;
BYTE GetMotorMetricsNow;
BYTE SendMotorMetricsNow;
BYTE SetMotorPositionNow=0xFE;    // desired new motor value - 0xFE means IDLE (don't change)
BYTE SetMotorPositionLast=0xFE;   // The last command to set the motor position. Duplicate commands are discarded.
BOOL SendJogNow=FALSE;            // Send a jog command to the motor once it is awake
BOOL SendFactoryLimitsNow;        // Send a FactoryLimits command to the motor once it is awake
BYTE LimitModeEnabled;            // number of minutes limit mode will be enabled
BYTE LimitModeTimer;              // number of 10ms periods since the last START command received when in Limit Set mode - used to accellerate the motor when HOLDing UP/DOWN
BOOL LimitModeTimerClear;         // Clear the LimitModeTimer once the motor has finished moving
BOOL AdjustingLowerLimit;         // Lower limit is being adjusted
BOOL AdjustingUpperLimit;         // Upper limit is being adjusted
BOOL ToggleHandBitEnabled;        // Hand bit toggle mode has been entered.
BOOL Waiting4Stop=FALSE;          // only issue 1 stop command and then wait for the motor to stop moving otherwise the motor gets all confused.
WORD PowerLevelTestCounter;       // Number of test frames left to send
BYTE PowerLevelSourceNodeID;      // nodeID to send the Test report to at the end of the test
BYTE PowerLevelTestPowerLevel;    // power level
BYTE PowerLevelDelay;             // delay sending the next power level test 
BYTE PowerLevelSec;               // Number of seconds until the RF power returns to normal
BOOL SendBatteryCriticalNow;      // Send a Battery low warning when true
BOOL SendBatteryReportNow;        // Send a Battery report when true
BYTE LimitSetModeControllerID;    // NodeID of the controller that has entered us into Limit Set mode - only it can send commands while in LSM.
BYTE SendMultilevelReportNow;     // Send a multilevel report when non-zero
BYTE SendMultilevelReportSoon;    // Send a multilevel report when non-zero but just before going to sleep
BYTE FirmwareBlockCount;          // Count of the number of firmware blocks downloaded for blinking the LED
BOOL ListeningBitPending=FALSE;   // Set ONLY if not already joined to a Z-Wave network and the PROG button is held down when power is applied. Then if joined to a z-wave network the listening bit will be set and won't go to sleep. NOTE: This ERGO commented out 5/8/2018.
BYTE MotorMetrics[11];            // motor metrics
BYTE SendSupervisionReportNow;    // NodeID of the node to send the report to
BYTE SendSupervisionReportSID=0xFE;    // Supervision Session ID which will change with each new command that is encapsulated in Supervision. The power value is invalid so we don't drop what might appear to be duplicates if we set this to 0.
BYTE SendSupervisionReportStatus; // Status to be reported - 
BYTE SendSupervisionReportLater;  // NodeID to send the status upon completion of movement if the MoreStatusUpdates bit is set in the Supervision GET.
BYTE SendSupervisionReportLaterSec;// Security Key to be used for the report later

#define MY_LEARN_MODE_IDLE    0
#define MY_LEARN_MODE_CLASSIC 1
#define MY_LEARN_MODE_NWI     2
#define MY_LEARN_MODE_EXPLORE 3
#define MY_LEARN_MODE_TIMEOUT 0xDE
#define CLASSIC_TIMEOUT 200
#define NWI_TIMEOUT     600
BYTE MyLearnMode;                   // Learn Mode is active when non-zero
WORD MyLearnModeTimeout;            // 10ms ticks until the current learn mode timesout

ZW_APPLICATION_TX_BUFFER pCmd_last;
BYTE   cmdLength_last;             

signed long int duration;       // temporary variable for calulating duration

ZW_APPLICATION_TX_BUFFER *pRFBuf; // buffer to send RF messages

AGI_PROFILE lifelineProfile = {
    ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL,
    ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL_LIFELINE
};

/**===========================   ApplicationRfNotify   ===========================
 *    Notify the application when the radio switch state
 *    Called from the Z-Wave PROTOCOL When radio switch from Rx to Tx or from Tx to Rx
 *    or when the modulator PA (Power Amplifier) turn on/off
 *    Not used 
 */
void  ApplicationRfNotify( BYTE rfState) {
  UNUSED(rfState);
}

#ifndef PRODUCTION_RELEASE
void BlinkLED(BYTE num, const BYTE color) {
    IWORD j;
    while (num>0) { 
        if (color==1) { // Green
            GRNON; REDOFF;
        } else if (color==2) { // Red
            GRNOFF; REDON;
        } else if (color==3) { // yellow
            GRNON; REDON;
        }
        for (j=0;j<15000;j++) ZW_WatchDogKick();
        GRNOFF; REDOFF;
        for (j=0;j<15000;j++) ZW_WatchDogKick();
        num--;
    }
}
#else
#define BlinkLED(a,b)
#endif

/**============================   ApplicationInitHW   ========================
 *    Initialization of non Z-Wave module hardware
 *       Returning FALSE from this function will force the API into production test mode.
 */
BYTE ApplicationInitHW( BYTE bWakeupReason) 
{
    wakeupReason=bWakeupReason; // need this later in InitSW

    ZW_IOS_enable(TRUE);    // enable the IOs to be manipulated

    scope_debug(0xaa);
    scope_debug(bWakeupReason);

    PIN_IN(P23,1);// P23 is MISO on the SPI bus. Pull it up. If it is grounded, go into RF test mode.

    PIN_OUT(P10); // RED LED
    PIN_OUT(P04); // GREEN LED
    PIN_LOW(P10); // Turn them off (they don't turn off until after we exit InitHW)
    PIN_LOW(P04);
	
	
		
    PIN_LOW(P36); // Motor is off to start with otherwise it floats high
    PIN_OUT(P36); // mENABLE

		PIN_IN(P35, 1);
		PIN_IN(P34, 1);
		
	
	
    PIN_IN(P11,0);// P11 is the Pushbutton - low when pressed, high when not pressed. Has an external pullup so don't need to turn the ZM5202 one on.

    if (0==PIN_GET(P23)) // if MISO is low, then return FALSE and go into production test mode
        return(FALSE); 
    return(TRUE); // MISO is high so go into normal mode
} // InitHW

/**===========================   ApplicationInitSW   =========================
 *    Initialization of the Application Software variables and states
 */
BYTE ApplicationInitSW( ZW_NVM_STATUS nvmStatus ) { // nvmStatus=ZW_NVM_INITIALIZED indicating corrupt or virgin NVM, ZW_NVM_VALID is normal, ZW_NVM_UPDATED indicates an OTA update has changed the format of the NVM but otherwise is ready to go.
BYTE i;
DWORD timeout;
BYTE PubKey[4]; 

    scope_debug(0x55);
    scope_debug(nvmStatus);

    ZAF_pm_Init(wakeupReason); // initialize the applicationFramework

    MotorMoving = FALSE;    // Motor is not moving at powerup since it's asleep!

    LoadConfiguration(); // load NVM variables

    if (ZW_WAKEUP_RESET==wakeupReason || ZW_WAKEUP_POR==wakeupReason || ZW_WAKEUP_WATCHDOG==wakeupReason ) { // power cycle - initialize a few more things
        GetMotorULimitNow=TRUE; // get the motor details
        GetMotorLLimitNow=TRUE;
        GetMotorCurrentPositionNow=TRUE;
        GetMotorVersionNow=TRUE;
        BatteryAboveBattLow=TRUE;
        BatteryPercentage=BATTERY_LOW; // initially set the battery to the low level so we don't send any battery reports until the shade has moved and a real battery measurement is made.
        BatteryLast=BATTERY_LOW;
        BatteryMilliVolts.wrd=10050;    // default battery voltage on powerup is 20%. Once the motor moves, then we'll get a real value.

        if (wakeupReason==ZW_WAKEUP_POR) {
            i=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
            if (i<255) MemoryPutByte((WORD)&EEOFFSET_NumberPORResets_far, i+1); // increment test parameter 
            // Go into the diagnostic protocol mode for 1 second then exit
            diag_protocol_init();
            for (timeout=0;timeout<650000 && diag_protocol_enabled();timeout++) {  // large timeout to make sure we eventually exit. 65K is about 1s but we need a much bigger timeout to make it 10s so use a DWORD.
                diag_protocol_poll();
            }
            if (2==ListeningMode) { // always on
                LED_set_mode(myNodeID ? LED_GREEN_SLOW_BLINK:LED_YELLOW_SLOW_BLINK);
                LED_BlinkCount=5;
            } else if (1==ListeningMode) { // 1000ms FLiRS
                LED_set_mode(myNodeID ? LED_GREEN_SLOW_BLINK:LED_YELLOW_SLOW_BLINK);
                LED_BlinkCount=2;
            } else {    // 250ms FLiRS
                LED_set_mode(myNodeID ? LED_GREEN_SLOW_BLINK:LED_YELLOW_SLOW_BLINK);
                LED_BlinkCount=1;
            }
        } else if (wakeupReason==ZW_WAKEUP_WATCHDOG) {
            i=MemoryGetByte((WORD)&EEOFFSET_NumberWatchDogResets_far);
            if (i<255) MemoryPutByte((WORD)&EEOFFSET_NumberWatchDogResets_far, i+1); // increment test parameter 
        }
				
#if 0 // The entry into smartstart learn mode has been disabled until the delay for the last callback is sped up from the 6.81.01 version of nearly 30s.
            ZW_NVRGetValue(offsetof(NVR_FLASH_STRUCT, aSecurityPublicKey), 4, &PubKey);
            if (PubKey[0]!=0xFF || PubKey[1]!=0xFF || PubKey[2]!=0xFF || PubKey[3]!=0xFF) { // if the Public Key is valid, then start SmartStart. If there is no key, then don't!
                scope_debug(0x99);
                ZW_NetworkLearnModeStart(E_NETWORK_LEARN_MODE_INCLUSION_SMARTSTART);    // Start SmartStart inclusion on power up
                LED_set_mode(LED_YELLOW_MEDIUM_BLINK);
                MyLearnMode=TRUE;
                SleepCounter=36000; // 3min for SmartStart
            }
#endif
    } // either a Z-Wave command has come in or the button has pressed so fully wakeup

#if 0
    if (wakeupReason==ZW_WAKEUP_SMARTSTART) {   // not expecting this to occur but in case it does we want to cancel upon timeout
        scope_debug(0x44);
        ZW_NetworkLearnModeStart(E_NETWORK_LEARN_MODE_INCLUSION_SMARTSTART);    // Start SmartStart inclusion
        MyLearnMode=TRUE;
    }
#endif

//w    PIN_HIGH(P36);  //mENABLE line - wake up the motor because we probably need to do something with it. Takes 150ms before we can talk to it.
//w    MotorPowerUpDelay=MOTOR_POWER_UP_DELAY; // can't talk to the motor for 150ms and this counter will track that
		PIN_IN(P34,1);  // I2C pins will be pulled up by the motor
		PIN_IN(P35,1);
		
//w could also initialize gdc variables here instead of up top
		
		
    ZW_SetRFReceiveMode(TRUE); // turn the RF on to be able to receive radio commands

    OtaInit(ZCB_OTAStart, NULL, ZCB_OTAFinish);       // Initialize the Over-The-Air firmware update code

    if (0x00==ListeningMode) {  // change the FLiRS to 250ms
        m_AppNIF.deviceOptionsMask =  APPLICATION_FREQ_LISTENING_MODE_250ms | APPLICATION_NODEINFO_OPTIONAL_FUNCTIONALITY;
    } else if ((0x02==ListeningMode) || ListeningBitPending) {  // if wall powered, force the Listening bit to be ON!
        m_AppNIF.deviceOptionsMask =  APPLICATION_NODEINFO_LISTENING | APPLICATION_NODEINFO_OPTIONAL_FUNCTIONALITY;
        m_AppNIF.cmdClassListNonSecureCount = sizeof(nodeInfo)-1; // remove Battery CC since this is an always powered node. Note that the commands are still there, just the CC is not advertised in the NIF.
        m_AppNIF.cmdClassListSecureCount = sizeof(SecNodeInfo)-1;
    }

    Transport_OnApplicationInitSW(&m_AppNIF); // Initialize the Transport Framework

    AGI_Init();     // setup the Association group which is just the LifeLine
    AGI_LifeLineGroupSetup(agiTableLifeLine, (sizeof(agiTableLifeLine)/sizeof(CMD_CLASS_GRP)), GroupName, ENDPOINT_ROOT );
    
    AssociationInit(FALSE); // restore the association NodeID

#ifdef WATCHDOG_ENABLED
    ZW_WatchDogEnable();    // enable the watchdog AFTER diag mode has exited
#endif

    if (0x02==ListeningMode) return(APPLICATION_NODEINFO_LISTENING);            // always on so the Listening bit is set
    if (0x00==ListeningMode) return(APPLICATION_FREQ_LISTENING_MODE_250ms);     // 1/4 sec FLiRS
    return(APPLICATION_FREQ_LISTENING_MODE_1000ms);                             // 1 sec FLiRS
} // ApplicationInitSW

/**============================   ApplicationTestPoll   ======================
 *    Function description
 *      This function is called when the slave enters test mode when InitHW returns FALSE (MISO held low)
 *      This routine is only used for FCC and factory testing. In normal operating mode it is not enabled.
 */
void ApplicationTestPoll(void) {
    diag_protocol_poll();
    if (RFTestRunning) return;
    diag_protocol_init();
    switch (RFTestState) {
    case 0: // OFF
        GRNOFF; REDOFF;
        break;
    case 1:
    case 3:
        GRNON; REDOFF;
        break;
    case 2:
    case 4:
        GRNON; REDON;
        break;
    default:
        break;
    } // switch

    RFTest(); // Run the common RF test mode across several products - see diag_protocol.c

} // ApplicationTestPoll

/** =============================  ApplicationPoll   =========================
 *    Application poll - this is where all radio transmissions are launched from.
 *     ___                ___            __  _             ____        ____
 *    /   |  ____  ____  / (_)________ _/ /_(_)___  ____  / __ \____  / / /
 *   / /| | / __ \/ __ \/ / / ___/ __ `/ __/ / __ \/ __ \/ /_/ / __ \/ / / 
 *  / ___ |/ /_/ / /_/ / / / /__/ /_/ / /_/ / /_/ / / / / ____/ /_/ / / /  
 * /_/  |_/ .___/ .___/_/_/\___/\__,_/\__/_/\____/_/ /_/_/    \____/_/_/   
 *       /_/   /_/                                                         
 */
E_APPLICATION_STATE ApplicationPoll( E_PROTOCOL_STATE bProtocolState ) {
    BYTE i;
    WORD temp;
    BYTE err;

    //scope_debug(0xC0);
    //scope_debug(bProtocolState);

    myTickTime=getTickTime();

    if (myTickTime != myTickTimeLast) { // then about 10ms has passed - increment various counters. Accurate enough for our purposes. Could use a ZWave timer or even the hardware timers if you wanted high accuracy.
        myTickTimeLast=myTickTime;
        TenmSecNow=TRUE;
        //scope_debug(0xC1);
        if (OneSecondCount==0) {
            OneSecondCount=99;
            OneSecondNow=TRUE;
            //scope_debug(0xC2);
            if (OneMinuteCount==0) {
                OneMinuteCount=59;
                OneMinuteNow=TRUE;
            } else OneMinuteCount--;
        } else OneSecondCount--;
    } // If 10ms has passed

    if (ActiveJobs()) {     // if there is a pending send for over 30 seconds then things have gone bad and we should go thru WatchDog reset to clear them up
        if (OneSecondNow) ActiveJobsCount++;
    } else {
        ActiveJobsCount=0;
    }
    if (ActiveJobsCount<30) ZW_WatchDogKick(); // If Mutex has been stuck for 30 seconds, then stop kicking the dog so we reset.

    if (OneMinuteNow) { // every 1 minute
        OneMinuteNow=FALSE;
        if (LimitModeEnabled) {
            LimitModeEnabled--;
            if (0==LimitModeEnabled) {  // limit mode has expired
                LED_set_mode(LED_RED_FAST_BLINK);
                LEDOnTimer=LED_SOLID_TIMEOUT; // RED LED for 1s because limit mode expired and no changes made
            }
        }
    } // One Minute

    if (OneSecondNow) { // every 1 second
        OneSecondNow=FALSE;
        if (ButtPressed) { // count up to the full reset time - all we do here is change the LED or JOG - the real work happens when the button is released
            if (ButtPressedCount<255) ButtPressedCount++;
            if (FULL_RESET_TIME==ButtPressedCount) {
                LED_set_mode(LED_ALL_OFF);
                SendJogNow=TRUE;
            } else if (START_FACTORY_RESET==ButtPressedCount) {
                LED_set_mode(LED_RED_ACCEL);
            } else if (START_ZWAVE_RESET==ButtPressedCount) {
                LED_set_mode(LED_YELLOW_FAST_BLINK);
                SendJogNow=TRUE;
            } else if (START_INCLUSION_MODE==ButtPressedCount) {
                LED_set_mode(LED_GREEN_FAST_BLINK);
            }
        } // Button pressed
        if (LimitModeEnabled) { // toggle the LED between red and green every second
            if (LED_R==LED_mode) {
                LED_set_mode(LED_GREEN_SOLID);
            } else {
                LED_set_mode(LED_RED_SOLID);
            }
            SleepCounter=255; // Stay awake while in limit set mode
        } // Limit mode
        if (PowerLevelSec) { // RF power level test underway
            PowerLevelSec--;
            SleepCounter=255; // Stay awake while in RF power level set mode
            if (0==PowerLevelSec) { // timeout has expired, return to normal power
                ZW_RFPowerLevelSet( normalPower );
            }
        }
    } // Once per Second

    if (TenmSecNow) { // Do some things every 10ms
        TenmSecNow=FALSE;
        if (PIN_GET(P11)) { // The pushbutton is not pressed
            ButtPressed=FALSE;
        } else { // the pushbutton is pressed
            ButtPressed=TRUE;
            SleepCounter=200; // stay awake while the button is pressed
        }
        //scope_debug(0xC9);
        //scope_debug(ButtPressed);
        //scope_debug(ButtPressedLast);
        if (ButtPressed!=ButtPressedLast) { // button has changed state
            if (ButtPressed) { // button pressed just now
                LED_set_mode(LED_GREEN_SOLID);          // just turn on the LED - it's when the button is released that things happen
                OneSecondCount=99; // start the OneSecondCounter at a known time so the timing from pressing the button is always the same
            } else {           // button released
                if (ButtPressedCount>=FULL_RESET_TIME) { // Factory reset mode
                    if (!ResetSent) { // don't send it if we have already sent it
                        SendFactoryLimitsNow=TRUE;
                        handleCommandClassDeviceResetLocally(&lifelineProfile,ZCB_DeviceResetLocallyDone);   // Send the Lifeline the Device Locally reset and then reset and reboot which will happen later
                        // The factory reset is completed via handle ZCB_DeviceResetLocallyDone
                        LED_set_mode(LED_GREEN_SOLID);
                        SleepCounter=2000; // need to stay awake long enough for the explorer frames to fail if the hub is not responding
                        ResetSent=TRUE;
                    }
                } else if (ButtPressedCount>=START_FACTORY_RESET) { // cancel factory limits mode
                    LED_set_mode(LED_RED_SOLID);
                } else if (ButtPressedCount>=START_ZWAVE_RESET) { // Reset the Z-Wave network
                    handleCommandClassDeviceResetLocally(&lifelineProfile,ZCB_DeviceResetLocallyDone);   // Send the Lifeline the Device Locally reset and then reset and reboot which will happen later
                    LED_set_mode(LED_YELLOW_FAST_WINKON);
                } else if (ButtPressedCount>=START_INCLUSION_MODE) { // enter Learn Mode
                    MyLearnMode=MY_LEARN_MODE_CLASSIC;
                    MyLearnModeTimeout=CLASSIC_TIMEOUT;
                    SleepCounter=INCLUSION_TIMEOUT;                   // stay awake longer during inclusion
                    ZW_SetLearnMode(ZW_SET_LEARN_MODE_CLASSIC, ZCB_LearnModeCallback); // start with classic then move to NWI if that fails
                    ZW_SendNodeInformation(NODE_BROADCAST, 0, NULL);
#if 0
                    if (myNodeID) {
                        ZW_NetworkLearnModeStart(E_NETWORK_LEARN_MODE_EXCLUSION);
                    } else {
                        ZW_NetworkLearnModeStart(E_NETWORK_LEARN_MODE_INCLUSION);
                    }
#endif
                    LED_set_mode(LED_GREEN_FAST_WINKON);
                } else { // pressed less than 2 seconds so send shade to opposite limit
                    if (MotorMoving) { // STOP if moving
                        StopMovementNow();
                    } else {                // move to opposite limit
                        if (DirectionLast) SetMotorPositionNow=0;
                        else SetMotorPositionNow=0x63;
                    }
                    LED_set_mode(LED_ALL_OFF);
                } 
                ButtPressedCount=0;
            } // button released
        } // ButtonPressed has changed
        ButtPressedLast=ButtPressed; // keep track of the state of the button

        if (SleepCounter && !(FirmwareDownloadUnderway || MotorMoving || LimitModeEnabled || LED_BlinkCount)) {     // is it time to go to sleep?
            SleepCounter--;
#if 0
            if (SleepCounter<5 || (SleepCounter+1)!=SleepCounterLast) {
            scope_debug(0xC3);
            scope_debug(SleepCounter);
            }
            SleepCounterLast=SleepCounter;
#endif
            if (0x02==ListeningMode) { // don't go to sleep if wall powered
                if (0==SleepCounter) {
                    LED_set_mode(LED_ALL_OFF);  // just turn off the LEDs
                    // Discussed turning the motor off with Wes but for now just leaving it on. Uses an additional 3mA which is not significant (36 vs. 33mA).
                    SendSupervisionReportLater=0;   // reset a few variables to ensure we're in a clean state similar to sleeping.
                    SendSupervisionReportSID=0xFE;
                    cmdLength_last=0;
                }
            } else {                // Normal FLiR device so check things and go to sleep when appropriate
                if (0==SleepCounter) { // time to go to sleep!
                    LED_set_mode(LED_ALL_OFF);
//w                    PIN_OUT(P36); // mENABLE
//w                    PIN_LOW(P36); // mENABLE line put the motor to sleep
//w                    PIN_IN(P34,0); // Make sure the I2C pins do no have their pullups enabled to avoid phantom power out of those
//w                    PIN_IN(P35,0);
                    // if the error counts have changed then write them out
                    if (MemoryGetByte((WORD)&EEOFFSET_MotorStatusMove_far!=MotorStatusMove)) MemoryPutByte((WORD)&EEOFFSET_MotorStatusMove_far, MotorStatusMove);
                    if (MemoryGetByte((WORD)&EEOFFSET_MotorStatusEep_far!=MotorStatusEep))   MemoryPutByte((WORD)&EEOFFSET_MotorStatusEep_far, MotorStatusEep);
                    if (MemoryGetByte((WORD)&EEOFFSET_MotorStatusI2C_far!=MotorStatusI2C))   MemoryPutByte((WORD)&EEOFFSET_MotorStatusI2C_far, MotorStatusI2C);
                } else if ((LED_SOLID_TIMEOUT/2)==SleepCounter) { // Signal the battery level just before going to sleep
                    if (BatteryPercentage<BATTERY_CRITICAL) LED_set_mode(LED_RED_SOLID); 
                    else if (BatteryPercentage<BATTERY_LOW) LED_set_mode(LED_YELLOW_SOLID);
                }
            }
        } // sleepcounter

        if (LimitModeEnabled) {
            if (LimitModeTimer<255) LimitModeTimer++; // LimitModeTimer counts the number of 10ms periods since the last START command to accellerate the motor when in limit set mode
            if (ButtPressed) { // exit limit mode right now
                LimitModeEnabled=0;
                LED_set_mode(LED_RED_FAST_BLINK);
                LEDOnTimer=LED_SOLID_TIMEOUT; // RED LED for 1s because limit mode expired and no changes made
            }
        } // LimitModeEnabled
				

				
				
        if (MotorMoving) {  // monitor the motor status and position while moving
							LED_set_mode(LED_GREEN_SOLID);
						// set P36 Pin High
						// Set a counter for 200-500ms
						// set P36 Low when counter elapses
						
            if (0==MotorMovingCounter) { // get the position of the motor
                MotorMovingCounter=20; // check position every 200ms
							
								if (0==RelayOnCount){
										PIN_OUT(P36); // Turn 
										PIN_LOW(P36); // Turn Off Relay
								}
								else
									RelayOnCount--;							
							
							
							
								
								
                if (0 == gdDelay) { // motor stopped?
									
										GetCurrentPosition();  // update the position
                    MotorMoving=FALSE;   // yes, go to sleep soon
                    SetMotorPositionLast=0xFE;  // set to idle when the motor stops moving.
										LED_set_mode(LED_ALL_OFF);
//                    if (MotorStatus&MOTOR_STATUS_BATT_READY) { // is there a new battery measurement available?
//                        if (currentpercentage>(Lastpercentage+10)) {    // only measure and report the battery when there has been a more than 10% upward movement which causes the battery to sag
//                            temp=GetBattery();                          // measure the battery
//                            if (0!=temp) BatteryMilliVolts.wrd=temp;    // ignore the value if there was an error
//                        }
//                    }
//                    Lastpercentage=currentpercentage;
//                    // update the BatteryPercentage based on the mV value
//                    if (BatteryMilliVolts.wrd<9500) {
//                        BatteryPercentage=0;    // battery below 9.5V is dead zero.
//                    } else {
//                        BatteryPercentage=(BatteryMilliVolts.wrd-9500u)/25; // 12V-9.5V=2.5V range so divide by 25 to get 0-100
//                        if (BatteryPercentage>100) BatteryPercentage=100;
//                    }
//                    if (0x02!=ListeningMode) { // don't send any battery reports if always on
//                        if (BatteryPercentage<BATTERY_CRITICAL && BatteryAboveBattLow) {
//                            SendBatteryCriticalNow=TRUE;
//                            BatteryAboveBattLow=FALSE;
//                        }
//                        if (BatteryPercentage>=BATTERY_LOW) BatteryAboveBattLow=TRUE;
//                        if (BatteryPercentage<BATTERY_CRITICAL || ((BatteryPercentage+5)<BatteryLast || (BatteryPercentage-5)>BatteryLast)) { // If the battery is less than critical or has changed by more than 5% since the last change then send a battery report
//                            SendBatteryReportNow=TRUE; // only send a battery report if a FLiR node
//                        }
//                    }
                    SendMultilevelReportSoon=groups[0][0].subGrp[0].node.nodeId; // if LifeLine NodeID is not zero then we'll send the Home Automation Hub an update on the state of the shade when movement stops so they don't have to poll.
                    pTxOptionsEx->pDestNode=&destNode; // unsolicited frames need somewhere to store this data so link in a pointer to a structure in RAM
                    pTxOptionsEx->txOptions=TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_EXPLORE | ZWAVE_PLUS_TX_OPTIONS;
                    pTxOptionsEx->pDestNode->node.nodeId=SendMultilevelReportSoon;
                    pTxOptionsEx->pDestNode->nodeInfo.security=GetHighestSecureLevel(ZW_GetSecurityKeys());

                    if (SendSupervisionReportLater) {    // if a supervision frame is pending, send it now that the motor has stopped
                        if (MotorStatus&MOTOR_STATUS_ERROR) {   // Return FAIL if the motor has failed for some reason.
                            SendSupervisionReportStatus=0x02;   // CC_SUPERVISION_STATUS_FAIL
                        } else {
                            SendSupervisionReportStatus=0xFF;   // CC_SUPERVISION_STATUS_SUCCESS
                        }
                        SendSupervisionReportNow=SendSupervisionReportLater;    // send the report
                        SendSupervisionReportSID&=0x7F;   // clear the More Status Updates field
                        SendSupervisionReportLater=0;
                    }
                    if (MotorStatus&MOTOR_STATUS_ERROR) {   // if there are any errors we care about, log them and clear the register
                        LED_set_mode(LED_RED_SOLID);
                        if (MotorStatus&MOTOR_STATUS_MOVE_ERROR) if (MotorStatusMove<255) MotorStatusMove++;
                        if (MotorStatus&MOTOR_STATUS_EEP_ERROR)  if (MotorStatusEep<255)  MotorStatusEep++;
                        if (MotorStatus&MOTOR_STATUS_I2C_ERROR)  if (MotorStatusI2C<255)  MotorStatusI2C++;
                        StatusRegClear(); // clear the status register
                    }
                    Waiting4Stop=FALSE; // motor has stopped so a new command can now be sent
                    SendSupervisionReportStatus=RECEIVED_FRAME_STATUS_SUCCESS;
                } // motor stopped
								else	gdDelay--;
								
            } else { // wait until time to check the status again
                MotorMovingCounter--;
            }
        }// MotorMoving

				
				
				
				
				
				
				
        if (SendBatteryCriticalNow && !ActiveJobs()) { // Then send a battery warning notification
            if (groups[0][0].subGrp[0].node.nodeId) { // only send if there is a lifeline NodeID to send it to
                SendBattery(groups[0][0].subGrp[0].node.nodeId, 0xFF);
                SendBatteryCriticalNow=FALSE;
            }
        }

        if (SendBatteryReportNow && !ActiveJobs()) { // Then send a battery report
            if (groups[0][0].subGrp[0].node.nodeId) { // only send if there is a lifeline NodeID to send it to
                SendBattery(groups[0][0].subGrp[0].node.nodeId, BatteryPercentage);
                SendBatteryReportNow=FALSE;
                BatteryLast=BatteryPercentage;
            }
        }

        if (SendMultilevelReportNow && !ActiveJobs()) { // Send a report of the shade state to whoever asked for it
            SendMultilevelReport();
            SendMultilevelReportNow=0;
        }

        if (SendMultilevelReportSoon && !ActiveJobs() && (SleepCounter<(100+(myNodeID&0x0f)))) { // Send a report of the shade state to the lifeline 1s before going to sleep so that we only send one after movements have stopped
            SendMultilevelReport();
            SendMultilevelReportSoon=0;
        }

#if 0
        if (SendMotorMetricsNow && !ActiveJobs()) { // Send the motor metrics
            SendMotorMetrics(SendMotorMetricsNow);
            SendMotorMetricsNow=0;
        }
#endif
        if (PowerLevelTestCounter && !ActiveJobs()) { // then a power level test is underway
            if (PowerLevelDelay) { // wait a bit longer
                PowerLevelDelay--;
                SleepCounter=LED_SOLID_TIMEOUT<<1; // Stay awake during test frames
            } else {
                PowerLevelDelay=100; // wait up to 1s between sends to let all 3 NOPs fail
                if (ZW_SendTestFrame(PowerLevelTestNodeID, PowerLevelTestPowerLevel, ZCB_PowerLevelTestCallback)) { // send the test frame
                    PowerLevelTestCounter--; // only decrement if the send was OK.
                }
            }
        } // PowerLevelTestCounter

        if (SendSupervisionReportNow && !ActiveJobs() && 0==MotorPowerUpDelay && SetMotorPositionNow==0xFE) { // Send the supervision report once the motor is powered up otherwise the values are all zero
            pRFBuf = GetResponseBuffer();
            if (NULL != pRFBuf) {
                pRFBuf->ZW_SupervisionReportFrame.cmdClass    = COMMAND_CLASS_SUPERVISION; 
                pRFBuf->ZW_SupervisionReportFrame.cmd         = SUPERVISION_REPORT; 
                pRFBuf->ZW_SupervisionReportFrame.properties1 = SendSupervisionReportSID&0xBF;  // more status updates and reserved must be 0
                pRFBuf->ZW_SupervisionReportFrame.status      = SendSupervisionReportStatus;
                if (MotorMoving) { // estimate the time to complete the move - very approximate
                    pRFBuf->ZW_SupervisionReportFrame.status      = SUPERVISION_REPORT_WORKING;
                    // Compute the seconds to reach the target which is in seconds
                    if (targetoffset.wrd==0xFFFFFFFF) {         // up to limit
                        duration=(signed long int)(currentposition.wrd-upperlimit.wrd);
                    } else if (targetoffset.wrd==0x00000001) {  // down to limit
                        duration=(signed long int)(lowerlimit.wrd-currentposition.wrd);
                    } else {
                        duration=abs((signed long int)targetoffset.wrd);
                    }
                    scope_debug(targetoffset.b[0]);
                    scope_debug(targetoffset.b[1]);
                    scope_debug(targetoffset.b[2]);
                    scope_debug(targetoffset.b[3]);
                    scope_debug(currentposition.b[0]);
                    scope_debug(currentposition.b[1]);
                    scope_debug(currentposition.b[2]);
                    scope_debug(currentposition.b[3]);
                    duration=duration/TRAVEL_TICKS_PER_SEC; // divide by the number of motor ticks per second
                    scope_debug(duration>>24);
                    scope_debug(duration>>16);
                    scope_debug(duration>>8);
                    scope_debug(duration);
                    scope_debug(MotorMoving);
                    scope_debug(SetMotorPositionNow);
                    if (duration>127 || duration<0) i=0xFE; // anything more than 127 seconds is simply reported as unknown (FE) as the scale changes to minutes
                    else i=(BYTE)duration;
                } else { // already at target
                    i=0;
                    SendSupervisionReportSID&=0x7F;   // clear the More Status Updates field
                }
                pRFBuf->ZW_SupervisionReportFrame.duration    = i;
                pTxOptionsEx->pDestNode->nodeInfo.security=SendSupervisionReportLaterSec;    // If this is a "later" report then use the security key sent at that time.
                SendSupervisionReportLaterSec=0;                            // clear this field now that it has been used.
                if (ZW_TX_IN_PROGRESS != Transport_SendResponseEP((BYTE *)pRFBuf, sizeof(pRFBuf->ZW_SupervisionReportFrame), pTxOptionsEx, ZCB_ResponseJobStatus)) {
                    FreeResponseBuffer(); /*Job failed, free transmit-buffer pTxBuf by clearing mutex */
                }
            }
            SendSupervisionReportNow=0;
        } // SendSupervision

        if (MotorPowerUpDelay) { // wait for the motor to be ready before sending I2C commands to it
            MotorPowerUpDelay--;
        }

        if (MyLearnModeTimeout) {   // if learnmode is active count down the timeout
            MyLearnModeTimeout--;
                scope_debug(0x92);
                scope_debug(MyLearnModeTimeout);
            if (0==MyLearnModeTimeout) { // Timeout so call the callback
                ZCB_LearnModeCallback(MY_LEARN_MODE_TIMEOUT, myNodeID);
            }
        }

        Led10ms();          // set the LEDs based on the current mode
    } // Ten MilliSeconds =====================================================


    if (0==MotorPowerUpDelay) { // motor is ready to receive I2C commands
        // Only do one I2C command each ApplicationPoll because they take a long time and RF data might be lost
        err=0; // flag if there is an error of some kind
				GetCurrentPosition();
        if (GetMotorVersionNow) {
            //GetMotorVersion();  // initializes global variables
            GetMotorVersionNow=FALSE;
        } else if (GetMotorULimitNow) {
            //err = GetUpperLimit(); // TODO do something with the return error code???
            GetMotorULimitNow=FALSE;
            //if (MotorLimitsHaveChanged) SetHomeFiftyPercent(); // reset the HOME position to the 50% anytime the limits have changed
        } else if (GetMotorLLimitNow) { 
            //err = GetLowerLimit(); // TODO do something with the return error code???
            GetMotorLLimitNow=FALSE;
            //if (MotorLimitsHaveChanged) SetHomeFiftyPercent(); // reset the HOME position to the 50% anytime the limits have changed
        } else if (GetMotorCurrentPositionNow) {
            err = GetCurrentPosition();
            GetMotorCurrentPositionNow=FALSE;
        } else if (SetMotorPositionNow!=0xFE) { // FE is the IDLE state
            if (MotorMoving && !LimitModeEnabled) { // always stop the motor before issuing another movement command except when setting limits. Otherwise the limits get  blown up due to bugs in the motor.
                StopMovementNow();  // note that SetMotorPosition is left at the target and once the motor stops, it will then move to the desired location.
            } else {
                err=SetMotorPosition(SetMotorPositionNow);
                SetMotorPositionLast=SetMotorPositionNow;
                SetMotorPositionNow=0xFE;
            }
        } else if (SendJogNow) {
           // err=ShadeJog();
            SendJogNow=FALSE;
        } else if (SendFactoryLimitsNow) {
						SendFactoryLimitsNow=FALSE;
//            if (!MotorMoving) { // make sure the jog is done before setting the limit
//                err=FactoryLimits();
//                if (err) LED_set_mode(LED_RED_SOLID); // failed
//                else   LED_set_mode(LED_GREEN_SOLID); // worked
//                GetMotorULimitNow=TRUE; // refresh the limits now that they have changed
//                GetMotorLLimitNow=TRUE;
//                SendFactoryLimitsNow=FALSE; // don't clear this flag until after the factory limits have been reset so we don't reset before that has been completed.
//            }
        } else if (GetMotorMetricsNow) {    // read the metrics out of the motor and send them to the requesting Node
            // TODO these cause the motor to hang - need to debug this before letting it thru...
            //GetMotorMetrics(MotorMetrics); // TODO this should be done first!!! then send the metrics...
            //SendMotorMetricsNow=GetMotorMetricsNow;
            GetMotorMetricsNow=0;
        }
//        if (err && (0x02==ListeningMode)) { // then the motor has returned an error so try putting it to sleep and wake it back up and see if that helps if in always-on mode
//            scope_debug(0xDD);
//            scope_debug(err);
//            REDON;
//            LED_set_mode(LED_RED_SOLID);
//            LEDOnTimer=LED_SOLID_TIMEOUT;
//            PIN_OUT(P36); // mENABLE
//            PIN_LOW(P36); // force the motor to sleep just to clean it up if needed
//            for (i=0;i<10;i++) OneUsDelay(50); // roughly 500usec turn off time
//            MotorPowerUpDelay=MOTOR_POWER_UP_DELAY; // can't talk to the motor for 150ms and this counter will track that
//            PIN_HIGH(P36);  // mENABLE - wake up the motor
//        }
    } // OK to talk to motor

    if ((1==SleepCounter) && MyLearnMode && 0x02!=ListeningMode) { // shut down learn mode just before going to sleep
        scope_debug(0xCF);
//        ZW_NetworkLearnModeStart(E_NETWORK_LEARN_MODE_DISABLE); // Note that as an always-on device SmartStart runs forever until joined
    }
    return((0x02!=ListeningMode && SleepCounter==0) ? E_APPLICATION_STATE_READY_FOR_POWERDOWN : E_APPLICATION_STATE_ACTIVE);
} // ApplicationPoll


BOOL PreviousFrameWasMulticast;         // TRUE when the previous frame was a multicast frame - when wake from sleep this will be false

BOOL DuplicateFrame(    // returns TRUE if this frame is identical to the previous one indicating it should be dropped
  RECEIVE_OPTIONS_TYPE_EX *rxOpt,
  ZW_APPLICATION_TX_BUFFER *pCmd,
  BYTE   cmdLength)              
{
    BOOL retval=FALSE;                  // default is the frame is NOT a duplicate so process it
#if 1
    if (PreviousFrameWasMulticast) {
        if (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_SWITCH_MULTILEVEL || pCmd->ZW_Common.cmdClass==COMMAND_CLASS_BASIC) {
            retval=TRUE; // Drop the frame is it is the singlecast followup to a multicast switch (typically from pressing the HOME button) or a BASIC
        }
    }
    PreviousFrameWasMulticast=(rxOpt->rxStatus & RECEIVE_STATUS_TYPE_MULTI) ? TRUE:FALSE;
#else   // THE CODE BELOW TENDS TO ELIMINATE TOO MANY COMMANDS AND MAKES THE SHADES FRUSTRATINGLY UNRESPONSIVE but is here for possible future review...
    if (!LimitModeEnabled) {            // in Limit Set Mode we need the duplicates
        if (cmdLength==cmdLength_last) {     // Different length is easy to check 
            if (0==memcmp(pCmd,&pCmd_last,cmdLength)) { // command class, command and data are compared
                if (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_BASIC || 
                    pCmd->ZW_Common.cmdClass==COMMAND_CLASS_CONFIGURATION) {  // check only some CCs as others are required to pass the CTT
                    retval=TRUE;    // This appears to be a duplicate of the previous frame so drop it
                } else if (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_SWITCH_MULTILEVEL && pCmd->ZW_Common.cmd!=SWITCH_MULTILEVEL_GET) { // MCZ1 uses a rapid stream of MultiLevelSwitch Gets to track the progress of the shade to a limit
                    retval=TRUE;    // This appears to be a duplicate of the previous frame so drop it
                }
            }   // The rxOpt field is not compared as it may change depending on the routing used to deliver the frame which would still be considered a duplicate
        }
    }
    cmdLength_last=cmdLength; // save the command to compare to the next one - note that when we wakeup these will be zeroed so there are no long-term duplicates
    memcpy(&pCmd_last,pCmd,cmdLength);
#endif
    return(retval);
} // DuplicateFrame

/*========================   Transport_ApplicationCommandHandler   ====================
**    Handling of a received application commands and requests
**--------------------------------------------------------------------------*/
received_frame_status_t Transport_ApplicationCommandHandlerEx(
  RECEIVE_OPTIONS_TYPE_EX *rxOpt, /* IN receive options of type RECEIVE_OPTIONS_TYPE_EX  */
  ZW_APPLICATION_TX_BUFFER *pCmd, /* IN  Payload from the received frame */
  BYTE   cmdLength)               /* IN Number of command bytes including the command */
{
    received_frame_status_t frame_status = RECEIVED_FRAME_STATUS_NO_SUPPORT;
    BYTE * ptr; // temporary pointer

    scope_debug(0xBB);
    scope_debug(pCmd->ZW_Common.cmdClass);
    scope_debug(pCmd->ZW_Common.cmd);
    scope_debug(pCmd->ZW_SwitchMultilevelStartLevelChangeV3Frame.properties1);
    scope_debug(MotorMoving);
    scope_debug(Waiting4Stop);

    if (SleepCounter<AWAKE_AFTER_RF_CMD) SleepCounter=AWAKE_AFTER_RF_CMD; // after any RF command stay awake in case the hub wants to send another command

    if ((rxOpt->rxStatus&RECEIVE_STATUS_TYPE_MASK)!=0x00) { // if multicast only a few SET commands are accepted - all others are ignored
        // Bit 2 is Broadcast, bit 3 is Multicast
        // Multicast can eliminate popcorn by having multiple shades receive the same command at the same time
        // Note that a singlecast version of the same command will follow this which needs to be properly handled.
        if (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_BASIC) {
            if (pCmd->ZW_Common.cmd!=BASIC_SET) {   // basic_set is allowed
                return(frame_status);
            }
        } else if (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_SWITCH_MULTILEVEL) {
            if (!((pCmd->ZW_Common.cmd==SWITCH_MULTILEVEL_SET_V4) || // only these 3 SET commands are multicast supported
                  (pCmd->ZW_Common.cmd==SWITCH_MULTILEVEL_START_LEVEL_CHANGE_V4) ||
                  (pCmd->ZW_Common.cmd==SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE_V4))) {
                return(frame_status);
            }
        } else if (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_SUPERVISION) {   // supervision needs to pass the command thru if its basic or Switch.
            ptr=&pCmd->ZW_Common.cmdClass;
            ptr+=4;
            if (!((*ptr==COMMAND_CLASS_BASIC) ||
                  (*ptr==COMMAND_CLASS_SWITCH_MULTILEVEL))) {
                return(frame_status);
            }
        } else {// ignore any other command classes for multicast
            return(frame_status);
        } 
    } // if Multicast
    
    if (0!=myNodeID && GetHighestSecureLevel(ZW_GetSecurityKeys())!=rxOpt->securityKey) { // If the frame doesn't match our security level then ignore it
        if (!(pCmd->ZW_Common.cmdClass==COMMAND_CLASS_ZWAVEPLUS_INFO) ||                // unless its one of the non-secure CCs.
             (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_TRANSPORT_SERVICE) ||
             (pCmd->ZW_Common.cmdClass==COMMAND_CLASS_SUPERVISION)) {
            return(frame_status); 
        }
    } // security level mismatches

    if (DuplicateFrame(rxOpt,pCmd,cmdLength)) return(RECEIVED_FRAME_STATUS_FAIL); // drop all duplicate frames to avoid confusing the motor. 

    scope_debug(0xBC);

    RxToTxOptions(rxOpt, &pTxOptionsEx);

    if (LimitModeEnabled && rxOpt->sourceNode.nodeId!=LimitSetModeControllerID) return(frame_status); // When in Limit Set Mode, ignore all commands except from the controller setting the limits.
    /* Call command class handlers */
    switch (pCmd->ZW_Common.cmdClass)
    {
        case COMMAND_CLASS_VERSION:
            GetMotorVersionNow=TRUE; // get the motor version and save it in the NON_ZERO_RAM. Do this anytime the version is requested for motor testing. Technically would only need to do it at powerup.
            frame_status=handleCommandClassVersion(rxOpt, pCmd, cmdLength);
            break;

#ifdef BOOTLOADER_ENABLED
        case COMMAND_CLASS_FIRMWARE_UPDATE_MD_V2:
            frame_status=handleCommandClassFWUpdate(rxOpt, pCmd, cmdLength);
            if (FIRMWARE_UPDATE_MD_REPORT_V2==pCmd->ZW_Common.cmd) { // Blink the LED with each block recevied
                if ((FirmwareBlockCount&0x07)==0x07) { // toggle LED every 8 blocks
                    LED_state=~LED_state; // toggle LED state
                    if (LED_state) {
                        GRNON;
                    } else {
                        GRNOFF;
                    }
                }
                FirmwareBlockCount++;
            }
            break;
#endif

        case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
            frame_status=handleCommandClassAssociationGroupInfo( rxOpt, pCmd, cmdLength);
            break;

        case COMMAND_CLASS_ASSOCIATION:
            frame_status=handleCommandClassAssociation(rxOpt, pCmd, cmdLength);
            break;


        case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
            frame_status=handleCommandClassManufacturerSpecific(rxOpt, pCmd, cmdLength);
            break;

        case COMMAND_CLASS_ZWAVEPLUS_INFO:          // there is only 1 GET which requires 1 report and to support the variable listening bit had to make a copy and add that functionality here
            if (pCmd->ZW_Common.cmd == ZWAVEPLUS_INFO_GET) {
                pRFBuf =  GetResponseBuffer();
                if(NULL != pRFBuf)
                {
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.cmdClass = COMMAND_CLASS_ZWAVEPLUS_INFO;
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.cmd = ZWAVEPLUS_INFO_REPORT;
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.zWaveVersion = ZW_PLUS_VERSION;
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.roleType = (0x02==ListeningMode) || ListeningBitPending ? ZWAVEPLUS_INFO_REPORT_ROLE_TYPE_SLAVE_ALWAYS_ON : APP_ROLE_TYPE; // always on if listening bit is set
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.nodeType = APP_NODE_TYPE;
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.installerIconType1 = (APP_ICON_TYPE >> 8);
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.installerIconType2 = (APP_ICON_TYPE & 0xff);
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.userIconType1 = (APP_USER_ICON_TYPE >> 8);
                    pRFBuf->ZW_ZwaveplusInfoReportV2Frame.userIconType2 = (APP_USER_ICON_TYPE & 0xff);

                    if (ZW_TX_IN_PROGRESS != Transport_SendResponseEP((BYTE *)pRFBuf, sizeof(pRFBuf->ZW_ZwaveplusInfoReportV2Frame), pTxOptionsEx, ZCB_ResponseJobStatus)) {
                        FreeResponseBuffer(); /*Job failed, free transmit-buffer pTxBuf by clearing mutex */
                    }
                }
                frame_status=RECEIVED_FRAME_STATUS_SUCCESS;
            }
            break;

        case COMMAND_CLASS_BASIC:
            frame_status=handleCommandClassBasic(rxOpt, pCmd, cmdLength);
            break;

        case COMMAND_CLASS_BATTERY: // very simple CC so no need for an entire handler to send a simple report
            if (BATTERY_GET==pCmd->ZW_Common.cmd) {
                SendBattery(rxOpt->sourceNode.nodeId, BatteryPercentage);
                frame_status=RECEIVED_FRAME_STATUS_SUCCESS;
            }
            break;		

        case COMMAND_CLASS_CONFIGURATION: // limited support of only 2 registers for setting the HOME position and entering/exiting Limit set mode - NOTE! the Configuration CC is NOT in the NIF and is not officially supported.
            switch (pCmd->ZW_Common.cmd) {  // decode the command
                case CONFIGURATION_GET:
                    if (pCmd->ZW_ConfigurationGetFrame.parameterNumber==0x42) { // Only register 0x42 can be read which dumps out a bunch of handy variables
                        pRFBuf = GetResponseBuffer(); // check if the RF is free
                        if (pRFBuf!=NULL) { // Buffer is available
                            pRFBuf->ZW_ConfigurationReport1byteFrame.cmdClass = COMMAND_CLASS_CONFIGURATION;
                            pRFBuf->ZW_ConfigurationReport1byteFrame.cmd      = CONFIGURATION_REPORT;
                            pRFBuf->ZW_ConfigurationReport1byteFrame.parameterNumber = pCmd->ZW_ConfigurationGetFrame.parameterNumber;
                            pRFBuf->ZW_ConfigurationReport1byteFrame.level = 1; // parameters are all 1 byte
                            ptr=&pRFBuf->ZW_ConfigurationReport1byteFrame.configurationValue1;
                            *ptr++=currentposition.b[0];
                            *ptr++=currentposition.b[1];
                            *ptr++=currentposition.b[2];
                            *ptr++=currentposition.b[3];
                            *ptr++=upperlimit.b[0];
                            *ptr++=upperlimit.b[1];
                            *ptr++=upperlimit.b[2];
                            *ptr++=upperlimit.b[3];
                            *ptr++=lowerlimit.b[0];
                            *ptr++=lowerlimit.b[1];
                            *ptr++=lowerlimit.b[2];
                            *ptr++=lowerlimit.b[3]; // 12
                            *ptr++=0xAA;    // Marker
                            *ptr++=BatteryMilliVolts.b.msb;
                            *ptr++=BatteryMilliVolts.b.lsb;
                            *ptr++=MotorStatusMove;
                            *ptr++=MotorStatusEep;
                            *ptr++=ListeningMode;
                            *ptr++=MemoryGetByte((WORD)&EEOFFSET_JogDistance_far);
                            *ptr++=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
                            *ptr++=MemoryGetByte((WORD)&EEOFFSET_NumberWatchDogResets_far);
                            if(ZW_TX_IN_PROGRESS != Transport_SendResponseEP( (BYTE *)pRFBuf, sizeof(ZW_CONFIGURATION_REPORT_1BYTE_FRAME)+22 , pTxOptionsEx, ZCB_ResponseJobStatus))
                            {
                                FreeResponseBuffer();          /*Job failed, free transmit-buffer pTxBuf by cleaing mutex */
                            }
                        }
                    } else if (pCmd->ZW_ConfigurationGetFrame.parameterNumber==0x29) {
                        pRFBuf = GetResponseBuffer(); // check if the RF is free
                        if (pRFBuf!=NULL) { // Buffer is available
                            pRFBuf->ZW_ConfigurationReport1byteFrame.cmdClass = COMMAND_CLASS_CONFIGURATION;
                            pRFBuf->ZW_ConfigurationReport1byteFrame.cmd      = CONFIGURATION_REPORT;
                            pRFBuf->ZW_ConfigurationReport1byteFrame.parameterNumber = pCmd->ZW_ConfigurationGetFrame.parameterNumber;
                            pRFBuf->ZW_ConfigurationReport1byteFrame.level = 1; // parameters are all 1 byte
                            pRFBuf->ZW_ConfigurationReport1byteFrame.configurationValue1 = ListeningMode;
                            if(ZW_TX_IN_PROGRESS != Transport_SendResponseEP( (BYTE *)pRFBuf, sizeof(ZW_CONFIGURATION_REPORT_1BYTE_FRAME), pTxOptionsEx, ZCB_ResponseJobStatus))
                            {
                                FreeResponseBuffer();          /*Job failed, free transmit-buffer pTxBuf by cleaing mutex */
                            }
                        }
                    } else if (pCmd->ZW_ConfigurationGetFrame.parameterNumber==0x43) {
                        GetMotorMetricsNow=rxOpt->sourceNode.nodeId;
                    }
                    break;
                case CONFIGURATION_SET:
                    switch (pCmd->ZW_ConfigurationGetFrame.parameterNumber) {
                        case 0x27:  // register 39 is used for production testing - a 1 will turn the respective LED on, 0 will turn them off. bit0=green, bit1=red
                            if      (0x01==(pCmd->ZW_ConfigurationSet1byteFrame.configurationValue1&0x03)) LED_set_mode(LED_GREEN_SOLID);
                            else if (0x02==(pCmd->ZW_ConfigurationSet1byteFrame.configurationValue1&0x03)) LED_set_mode(LED_RED_SOLID);
                            else if (0x03==(pCmd->ZW_ConfigurationSet1byteFrame.configurationValue1&0x03)) LED_set_mode(LED_YELLOW_SOLID);
                            else LED_set_mode(LED_ALL_OFF);
                            SleepCounter=1000; // stays on for 10 seconds
                            break;
                        case 0x28:  // register 40 will set the JogDistance parameter
                            MemoryPutByte((WORD)&EEOFFSET_JogDistance_far, pCmd->ZW_ConfigurationSet1byteFrame.configurationValue1);
                            MemoryPutByte((WORD)&EEOFFSET_JogDistanceNOT_far, ~pCmd->ZW_ConfigurationSet1byteFrame.configurationValue1);
                            break;
                        case 0x29:  // register 41 will set the ListeningMode parameter - note that this will not take effect until the shade is excluded and re-included. Also note that a factory reset sets this to the default.
                            ListeningMode= pCmd->ZW_ConfigurationSet1byteFrame.configurationValue1;
                            if (ListeningMode>0x02) ListeningMode=0x00;
                            MemoryPutByte((WORD)&EEOFFSET_LISTENING_far, ListeningMode);
                            break;
                        case 0x2A:    // 0x2A=42 is the answer to life, the universe and everything! Hitchhikers Guide to the Galaxy which is popular with the Sigma RF engineers as the 9.6K baud frequency is 908.42 and the Magic Number is 42.
                            if ((pCmd->ZW_ConfigurationSet4byteFrame.configurationValue1=='E') && // Enter Limit mode
                                (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue2=='N') &&
                                (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue3=='T') &&
                                (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue4=='R')) { 
                                LimitModeEnabled=LIMIT_MODE_TIMEOUT;
                                if (currentposition.wrd>(lowerlimit.wrd-250)) { // adjusting the lower limit
                                    AdjustingLowerLimit=TRUE;
                                    AdjustingUpperLimit=FALSE;
                                } else if (currentposition.wrd<upperlimit.wrd+250) { // adjusting the upper limit
                                    AdjustingUpperLimit=TRUE;
                                    AdjustingLowerLimit=FALSE;
                                } else { // change the HAND bit - direction of travel for the motor - usually swaps the wrap direction of roller shades
                                    ToggleHandBitEnabled=TRUE; // HAND bit swap mode is enabled - if EXIT then swap it. Up or Down will cancel.
                                } 
                                if (MotorMoving) { // just stop the motor if it is moving
                                    StopMovementNow();
                                } else {
                                    SendJogNow=TRUE; // send a jog command to the motor once it's awake
                                }
                                LimitSetModeControllerID=rxOpt->sourceNode.nodeId; // only the controller that enters LSM can control the shade
                                LED_set_mode(LED_GREEN_SOLID); // LED will toggle between RED and GREEN every second
                            } else if ((pCmd->ZW_ConfigurationSet4byteFrame.configurationValue1=='e') && // Exit Limit mode but don't save the limits - limit mode has expired
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue2=='x') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue3=='i') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue4=='t') &&
                                       (LimitSetModeControllerID==rxOpt->sourceNode.nodeId)) { 
                                LimitModeEnabled=0;
                                LED_set_mode(LED_RED_SOLID);
                            } else if ((pCmd->ZW_ConfigurationSet4byteFrame.configurationValue1=='E') && // Exit Limit mode
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue2=='X') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue3=='I') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue4=='T') &&
                                       (LimitSetModeControllerID==rxOpt->sourceNode.nodeId)) { 
                                if (MotorMoving) { // just stop the motor if it is moving
                                    StopMovementNow();
                                } else if (LimitModeEnabled) { // store the results and disable Limit Set Mode
                                    LimitModeEnabled=0;
                                    if (AdjustingUpperLimit) {
                                        if (lowerlimit.wrd-currentposition.wrd>=LIMIT_SET_MINIMUM_SPACING) { // Only set if the distance between the limits is at least the minimum
                                            SetUpperLimit();
                                            MotorLimitsHaveChanged=TRUE; // update the HOME position to 50%
                                            LED_set_mode(LED_GREEN_FAST_BLINK);
                                            SendJogNow=TRUE; // send a jog command to the motor once it's awake
                                        } else {
                                            LED_set_mode(LED_RED_SOLID);
                                        }
                                    } else if (AdjustingLowerLimit) {
                                        if (currentposition.wrd-upperlimit.wrd>=LIMIT_SET_MINIMUM_SPACING) { // Only set if the distance between the limits is at least the minimum
                                            SetLowerLimit();
                                            MotorLimitsHaveChanged=TRUE; // update the HOME position to 50%
                                            LED_set_mode(LED_GREEN_FAST_BLINK);
                                            SendJogNow=TRUE; // send a jog command to the motor once it's awake
                                        } else {
                                            LED_set_mode(LED_RED_SOLID);
                                        }
                                    } else { // Toggle the HAND bit is ignored here as we've already toggled it going into Limit Set Mode
                                        if (ToggleHandBitEnabled) { // Then it is OK to toggle the hand bit - note that any duplicated Z-Wave EXIT commands will be ignored
                                            ToggleHandBitEnabled=FALSE;
                                            ToggleHandBit(); // Send the I2C command to toggle the HAND bit
                                            LED_set_mode(LED_GREEN_FAST_BLINK);
                                            SendJogNow=TRUE; // send a jog command to the motor once it's awake
                                        }
                                    }
                                    GetMotorULimitNow=TRUE; // update the limits
                                    GetMotorLLimitNow=TRUE;
                                } // if limit mode was not already entered then ignore the command
                            } else if ((pCmd->ZW_ConfigurationSet4byteFrame.configurationValue1=='H') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue2=='O') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue3=='M') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue4=='E')) { // then go set the current position as the HOME position
                                if (MotorMoving) {
                                    StopMovementNow();
                                } else {
                                    if ((currentposition.wrd<(homeposition.wrd+HOME_DEADBAND)) && (currentposition.wrd>(homeposition.wrd-HOME_DEADBAND))) {
                                        homeposition.wrd=HOME_DISABLED; // home position is disabled if set at the home position
                                        MemoryPutBuffer((WORD)&EEOFFSET_HomePosition_far, &homeposition.wrd,4,NULL);
                                        LED_set_mode(LED_YELLOW_SOLID);
                                        SendJogNow=TRUE;
                                    } else {
                                        if ((currentposition.wrd+HOME_DEADBAND)>upperlimit.wrd && (currentposition.wrd-HOME_DEADBAND)<lowerlimit.wrd) { // HOME must be between the limits but a deadband is applied so HOME can be the limit
                                            if (currentposition.wrd<upperlimit.wrd) currentposition.wrd=upperlimit.wrd;
                                            if (currentposition.wrd>lowerlimit.wrd) currentposition.wrd=lowerlimit.wrd;
                                            homeposition.wrd=currentposition.wrd; // set the current position to be the home position
                                            MemoryPutBuffer((WORD)&EEOFFSET_HomePosition_far, &homeposition.wrd,4,NULL);
                                            LED_set_mode(LED_GREEN_SOLID);
                                            SendJogNow=TRUE;
                                        } else { // current position is outside the limits so ignore the command (enter limit set mode, move the limit, then wait for the timeout, then set HOME)
                                            LED_set_mode(LED_RED_SOLID);
                                        }
                                    }
                                }
                            } else if ((pCmd->ZW_ConfigurationSet4byteFrame.configurationValue1=='J') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue2=='O') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue3=='G') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue4=='1')) { // then jog the motor and cancel learn mode as the associate is complete
                                SendJogNow=TRUE;                          // send a jog command to the motor once it's awake
                                ZW_SetLearnMode(ZW_SET_LEARN_MODE_DISABLE, ZCB_LearnModeCallback);    // cancel learn mode because we are done being associated and not actually doing inclusion/exclusion
//                                ZW_NetworkLearnModeStart(E_NETWORK_LEARN_MODE_DISABLE);    // cancel learn mode because we are done
                            } else if ((pCmd->ZW_ConfigurationSet4byteFrame.configurationValue1=='C') && // clear the error metrics
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue2=='L') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue3=='R') &&
                                       (pCmd->ZW_ConfigurationSet4byteFrame.configurationValue4=='E')) { 
                                MotorStatusMove=0;
                                MotorStatusEep=0;
                                MotorStatusI2C=0;
                            }
                                                break; 
                        default: // ignore other parameter numbers
                                                break; 
                    } // if parameter number is OK
                    break;
                default: // ignore other commands - note that BULK commands are not supported as Config CC is not officially supported otherwise it would fail CTT
                    break;
            } // switch
            break;

        case COMMAND_CLASS_POWERLEVEL:
            frame_status=MyhandleCommandClassPowerLevel(rxOpt, pCmd, cmdLength);
            break;

        case COMMAND_CLASS_SWITCH_MULTILEVEL:
            frame_status=MyhandleCommandClassMultiLevelSwitch(rxOpt, pCmd, cmdLength);
            break;		

        case COMMAND_CLASS_SUPERVISION: // unwrap the command and execute it and return a report.
            if (pCmd->ZW_Common.cmd == SUPERVISION_GET ) {
                if (SendSupervisionReportSID!=pCmd->ZW_SupervisionGetFrame.properties1) { // drop all duplicates per CC:006C.01.01.11.009
                    SendSupervisionReportNow=rxOpt->sourceNode.nodeId; // send a report now
                    SendSupervisionReportSID=pCmd->ZW_SupervisionGetFrame.properties1;
                    if (SendSupervisionReportSID&SUPERVISION_GET_PROPERTIES1_STATUS_UPDATES_BIT_MASK) {
                        SendSupervisionReportLater=SendSupervisionReportNow; // send a report when movement is complete
                        SendSupervisionReportLaterSec=rxOpt->securityKey;
                    } else {
                        SendSupervisionReportLater=0;
                    }
                    SendSupervisionReportStatus=Transport_ApplicationCommandHandlerEx( 
                            rxOpt, &pCmd->ZW_SupervisionGetFrame.encapsulatedCommandLength+1, cmdLength-4); // run the command which should be SET - if it not supported then return a not supported result
                    if (SendSupervisionReportStatus==RECEIVED_FRAME_STATUS_NO_SUPPORT) SendSupervisionReportSID&=0x7F; 
                    frame_status=RECEIVED_FRAME_STATUS_SUCCESS;
                }
            }
            break;		
        default:    // the command is ignored
            break;
    } // end switch
    return(frame_status);
}

#if 0
void SendMotorMetrics(BYTE SourceNodeID) { // Configuration GET of register 0x43 returns the 11 byte motor metrics
    BYTE * ptr;
    BYTE i;
    pRFBuf = GetResponseBuffer(); // check if the RF is free

    if (pRFBuf!=NULL) { // Buffer is available
        pRFBuf->ZW_ConfigurationReport1byteFrame.cmdClass = COMMAND_CLASS_CONFIGURATION;
        pRFBuf->ZW_ConfigurationReport1byteFrame.cmd      = CONFIGURATION_REPORT;
        pRFBuf->ZW_ConfigurationReport1byteFrame.parameterNumber = 0x43;
        pRFBuf->ZW_ConfigurationReport1byteFrame.level = 1; // parameters are all 1 byte
        ptr=&pRFBuf->ZW_ConfigurationReport1byteFrame.configurationValue1;
        for (i=0;0<11;i++) {
            *ptr++=MotorMetrics[i];
        }
        if(ZW_TX_IN_PROGRESS != Transport_SendResponseEP( (BYTE *)pRFBuf, sizeof(ZW_CONFIGURATION_REPORT_1BYTE_FRAME)+11 , pTxOptionsEx, ZCB_ResponseJobStatus))
        {
            FreeResponseBuffer();          /*Job failed, free transmit-buffer pTxBuf by cleaing mutex */
        }
    }
} // Send Motor Metrics
#endif

/*========================   handleCommandClassVersionAppl  =========================
**   Application specific Command Class Version handler
**   return none
**
**   Side effects: none
**--------------------------------------------------------------------------*/
BYTE
handleCommandClassVersionAppl( BYTE cmdClass )
{
  BYTE commandClassVersion = UNKNOWN_VERSION;

  switch (cmdClass)
  {
    case COMMAND_CLASS_VERSION:
     commandClassVersion = CommandClassVersionVersionGet();
      break;

    case COMMAND_CLASS_FIRMWARE_UPDATE_MD:
      commandClassVersion = CommandClassFirmwareUpdateMdVersionGet();
      break;

    case COMMAND_CLASS_POWERLEVEL:
     commandClassVersion = 1;
      break;

    case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
     commandClassVersion = CommandClassManufacturerVersionGet();
      break;

    case COMMAND_CLASS_ASSOCIATION:
     commandClassVersion = CommandClassAssociationVersionGet();
      break;

    case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
     commandClassVersion = CommandClassAssociationGroupInfoVersionGet();
      break;
		
	  case COMMAND_CLASS_BATTERY:
     commandClassVersion = 1;   // there is only 1 version of battery
      break;	

    case COMMAND_CLASS_DEVICE_RESET_LOCALLY:
     commandClassVersion = CommandClassDeviceResetLocallyVersionGet();
      break;

    case COMMAND_CLASS_ZWAVEPLUS_INFO:
     commandClassVersion = ZWAVEPLUS_INFO_VERSION_V2;
      break;

    case COMMAND_CLASS_SECURITY_2:
     commandClassVersion = SECURITY_2_VERSION;
      break;

    case COMMAND_CLASS_TRANSPORT_SERVICE:
     commandClassVersion = TRANSPORT_SERVICE_VERSION_V2;
      break;

    case COMMAND_CLASS_SUPERVISION:
     commandClassVersion = SUPERVISION_VERSION;
      break;

    case COMMAND_CLASS_BASIC:
     commandClassVersion =  CommandClassBasicVersionGet();
      break;

    case COMMAND_CLASS_SWITCH_MULTILEVEL:
     commandClassVersion = SWITCH_MULTILEVEL_VERSION_V4;   // V3 is the minimum required by the CTT but no support is provided for duration.
      break;

    default:
     commandClassVersion = UNKNOWN_VERSION;
  }
  return commandClassVersion;
}


/*==========================   ApplicationSlaveUpdate   =======================
**   Inform a slave application that a node information is received.
**   Called from the slave command handler when a node information frame
**   is received and the Z-Wave protocol is not in a state where it is needed.
**
**--------------------------------------------------------------------------*/
void
ApplicationSlaveUpdate(
  BYTE bStatus,     /*IN  Status event */
  BYTE bNodeID,     /*IN  Node id of the node that send node info */
  BYTE* pCmd,       /*IN  Pointer to Application Node information */
  BYTE bLen)       /*IN  Node info length                        */
{
  UNUSED(bStatus);
  UNUSED(bNodeID);
  UNUSED(pCmd);
  UNUSED(bLen);
}

#if 0 // ER 2016-06-27 - this has been removed per Somfy request to use the UART interface instead. This I2C code was found to be just too slow as the 8051 can't even do 10K baud let alone 100K.
/**   I2C_Slave
 * Crude I2C Slave handler via GPIOs - has to be pretty slow for the 8051 to keep up 
 * Normally only used for diagnostics.
 * Must be called every ApplicationPoll to keep up with the data rate.
 * Not meant to be a fully compliant I2C slave - just enough for diags...
 */
#define I2C_IDLE        0
#define I2C_ADDR        2
#define I2C_READ        3
#define I2C_WRITE       4
#define I2C_ACK1        5
#define I2C_ACK2        6
#define I2C_CHK_ACK     7
#define I2C_SLAVE_ADDR  0x42
BYTE I2Cstate;
BYTE I2CBitCount;       // bit number about to be clocked in
BYTE I2CDataIn;
BYTE I2CDataOut;
BOOL I2CRead;           // TRUE a read cycle in progress, FALSE=write
BOOL scl_last;
BOOL sda_last;
void I2C_Slave(void) {
    BOOL scl, sda;
    scl=PIN_GET(P35) ? 1 : 0; // convert to booleans
    sda=PIN_GET(P34) ? 1 : 0;
    if (!(scl==scl_last && sda==sda_last)) { // then a tranistion on the IOs has happened so figure out what it was
        if (scl && sda!=sda_last) { // STOP or START
            if (sda&!sda_last) { // STOP
                I2Cstate=I2C_IDLE;
                PIN_IN(P34,0); // make sure we are no longer driving the IO
            } else { // START
                I2Cstate=I2C_ADDR;
                I2CBitCount=7; // first bit in is bit 7 the msb
                I2CDataIn=0;
            }
        }
        switch (I2Cstate) {
            case I2C_IDLE: // ignore transitions
                break;
            case I2C_ADDR: // clock in the address
                if (scl && !scl_last) { //rising egde of scl - clock in the data
                    I2CDataIn|=(BYTE)sda<<I2CBitCount; // capture the data bit
                    if (I2CBitCount) { // done?
                        I2CBitCount--; // no - decrement to next bit
                    } else {           // yes - ACK if addr matches
                        if ((I2CDataIn&0xFE)==I2C_SLAVE_ADDR<<1) {
                            I2Cstate=I2C_ACK1;
                            I2CRead=I2CDataIn&0x01;
                            I2CDataOut=PIN_GET(P11) ? 1 : 0; // capture the state of the PROG pin and return that
                        } else {
                            I2Cstate=I2C_IDLE; // not for us so back to idle
                        }
                    }
                }
                break;
            case I2C_ACK1: // Drive ACK on falling edge
                if (!scl && scl_last) { //falling egde of scl - drive the ack low
                    PIN_OUT(P34);
                    PIN_LOW(P34);
                    if (I2CRead) I2Cstate=I2C_READ;
                    else         I2Cstate=I2C_ACK2;
                }
                I2CBitCount=7;
                break;
            case I2C_ACK2: // release ACK on falling edge
                if (!scl && scl_last) { //falling egde of scl - release sda
                    PIN_IN(P34,0);
                    if (I2CRead) I2Cstate=I2C_READ;
                    else         I2Cstate=I2C_WRITE;
                    I2CDataIn=0;
                }
                break;
            case I2C_WRITE: // byte being written
                if (scl && !scl_last) { //rising egde of scl - clock in the data
                    I2CDataIn|=(BYTE)sda<<I2CBitCount; // capture the data bit
                    if (I2CBitCount) { // done?
                        I2CBitCount--; // no - decrement to next bit
                    } else {           // yes - ACK
                        I2Cstate=I2C_ACK1;
                        I2CCommand(I2CDataIn); // run the command just received
                    }
                }
                break;
            case I2C_READ: // byte being read
                if (!scl && scl_last) { //falling egde of scl - drive the data
                    if (I2CDataOut&(1<<I2CBitCount)) {
                        PIN_IN(P34,0);
                    } else {
                        PIN_OUT(P34);
                        PIN_LOW(P34);
                    }
                    if (I2CBitCount) { // done?
                        I2CBitCount--; // no - decrement to next bit
                    } else {           // yes - ACK
                        I2Cstate=I2C_CHK_ACK;
                        PIN_IN(P34,0); // release for the ACK
                    }
                }
                break;
            case I2C_CHK_ACK: // wait for the end of ACK on a read
                if (scl && !scl_last) { //rising egde of scl
                    if (sda) I2Cstate=I2C_IDLE; // NACK just go to idle
                    else I2Cstate=I2C_READ;     // read next byte
                }
                break;
            default: // should not happen
                I2Cstate=I2C_IDLE;
                break;
        } // switch
    }
    scl_last=scl;   // update the GPIO state for the next time thru
    sda_last=sda;
} // I2C_Slave

void I2CCommand(BYTE cmd) { // A command has come in on the I2C slave - perform the command
    if (cmd>0 && cmd<6) {
        ZW_WatchDogDisable();   // turn off the watchdog because we are in test mode and never exit
        while(1); // reboot into test mode
    }
} // I2CCommand
#endif

#if 0 // commented out in 6.81 as this has become a function which calls a routing within the SDK but where does that routine get it's data from?
/*============================ handleNbrFirmwareVersions ===================
** Function description
** Read number of firmwares in application.
**
**-------------------------------------------------------------------------*/
BYTE
handleNbrFirmwareVersions(void)
{
  return(2); // two firmware versions - 0=Z-Wave, 1=Motor
}
#endif

/*============================ handleGetFirmwareVersion ====================
** Function description
** Read application firmware version informations
**
**-------------------------------------------------------------------------*/
void
handleGetFirmwareVersion( BYTE bFirmwareNumber, VG_VERSION_REPORT_V2_VG* pVariantgroup)
{
  /*firmware 0 version and sub version*/
  if(bFirmwareNumber == 0) {
    pVariantgroup->firmwareVersion = APP_VERSION;
    pVariantgroup->firmwareSubVersion = APP_REVISION;
  } else if(bFirmwareNumber == 1) {
    pVariantgroup->firmwareVersion = MotorVersion;
    pVariantgroup->firmwareSubVersion = MotorSubVersion;
  } else {
    /*Just set it to 0 if firmware n is not present*/
    pVariantgroup->firmwareVersion = 0;
    pVariantgroup->firmwareSubVersion = 0;
  }
}

void SetHomeFiftyPercent(void) { // Set the HOME position to the default of the 50% point between the limits
    homeposition.wrd=((lowerlimit.wrd-upperlimit.wrd))/2+upperlimit.wrd; // default position of home is the 50% point
    MemoryPutBuffer((WORD)&EEOFFSET_HomePosition_far, &homeposition.wrd,4,NULL);
}


// This version of learn is used on 6.81.01 as the ZW_NetworkLearnModeStart has a 30s delay before the callback
// On Inclusion typically get an ASSIGN_NODEID_DONE followed by a ASSIGN_RANGE_INFO_UPDATE within a few hundred ms. Both include the NodeID.
// On Exclusion get an ASSIGN_COMPLETE with a nodeID of zero. Note that you don't get any of the other callbacks.
code const void (code * ZCB_LearnModeCallback_p)(BYTE bStatus, BYTE nodeID) = &ZCB_LearnModeCallback;
void ZCB_LearnModeCallback(BYTE bStatus, BYTE nodeID) {  // multiple callbacks occur during inclusion/exclusion
    IBYTE i;
    scope_debug(0x97);
    scope_debug(bStatus);
    scope_debug(nodeID);
    switch (bStatus) {
        case ASSIGN_COMPLETE:   // 0x00
            MyLearnMode=MY_LEARN_MODE_IDLE;
            if (nodeID==0) {
                AssociationInit(TRUE);              // Clear all associations
                SetDefaultConfiguration();          // Reset everything to the defaults. Limits stay where they were.
                LED_set_mode(LED_YELLOW_SOLID);     // Yellow=Exclude, Green=Include
                Led10ms();
                ShadeJog();                     // because we'll reboot now, we have to jog, then wait for the jog to complete, then reboot.
                for (i=0;i<255 && IsMotorRunning();i++); // wait for the jog to finish.
                ZW_WatchDogEnable();           // Reboot just to make sure everything is clean
                while(1);
            }
            break;
        case ASSIGN_NODEID_DONE: // 0x01
            SetHomeFiftyPercent();
            LED_set_mode(LED_GREEN_SOLID);
            SleepCounter=INCLUSION_TIMEOUT;                   // stay awake longer after inclusion
            SendJogNow=TRUE; // send a jog command to the motor once it's awake
            Transport_OnLearnCompleted(nodeID);
            myNodeID = nodeID;
            break;
        case ASSIGN_RANGE_INFO_UPDATE: //0x02 - Must not sleep until after this is done.
            MyLearnMode=MY_LEARN_MODE_IDLE;
            break;
        case MY_LEARN_MODE_TIMEOUT: //0xDE - Timed out - if in classic then switch to nwi and send explorer then give up.
            if (MyLearnMode==MY_LEARN_MODE_CLASSIC) {
                SleepCounter=INCLUSION_TIMEOUT;                   // stay awake longer
                MyLearnMode=MY_LEARN_MODE_NWI;
                MyLearnModeTimeout=NWI_TIMEOUT;
                if (myNodeID) {
                    ZW_SetLearnMode(ZW_SET_LEARN_MODE_NWE, ZCB_LearnModeCallback);
                    ZW_ExploreRequestExclusion();
                } else {
                    ZW_SetLearnMode(ZW_SET_LEARN_MODE_NWI, ZCB_LearnModeCallback);
                    ZW_ExploreRequestInclusion();
                }
            } else {
                ZW_SetLearnMode(ZW_SET_LEARN_MODE_DISABLE, ZCB_LearnModeCallback);    // cancel learn mode
            }
            break;
        default:
            break;
    }
}

void ApplicationNetworkLearnModeCompleted( BYTE bNodeID) {  // callback during and after Learn Mode - NOTE!!! THIS VERSION IS NOT USED IN 6.81.01 due to the 30s delay in the final callback. It is required to be linked in but should never be called.
    IBYTE i;
    scope_debug(0x96);
    scope_debug(bNodeID);
    MyLearnMode=FALSE;    // generally we are done when we get to here
    if (APPLICATION_NETWORK_LEARN_MODE_COMPLETED_SMART_START_IN_PROGRESS == bNodeID) { // SmartStart underway
        LED_set_mode(LED_GREEN_MEDIUM_BLINK);
        SleepCounter=36000; // 3 minutes in SmartStart mode
        MyLearnMode=TRUE;
    } else if (APPLICATION_NETWORK_LEARN_MODE_COMPLETED_FAILED == bNodeID) {   // failed
        LED_set_mode(LED_RED_SOLID);
        LEDOnTimer=LED_SOLID_TIMEOUT;
    } else if (APPLICATION_NETWORK_LEARN_MODE_COMPLETED_TIMEOUT == bNodeID) {   // reboot if NodeID is zero otherwise just ignore the timeout
        if (myNodeID == 0) {
            ZW_WatchDogEnable(); // Reboot
            while(1);
        }
    } else {                                        // worked
        myNodeID = bNodeID;
        if (myNodeID == 0) // Exclusion - clear various things = LED=YELLOW
        {
            AssociationInit(TRUE);              // Clear all associations
            SetDefaultConfiguration();          // Reset everything to the defaults. Limits stay where they were.
            LED_set_mode(LED_YELLOW_SOLID);     // Yellow=Exclude, Green=Include
            Led10ms();
            ShadeJog();                     // because we'll reboot now, we have to jog, then wait for the jog to complete, then reboot.
            for (i=0;i<255 && IsMotorRunning();i++); // wait for the jog to finish.
            ZW_WatchDogEnable();           // Reboot which will restart SmartStart as required for certification
            while(1);
        } else {            // inclusion - LED=GREEN
            SetHomeFiftyPercent();
            LED_set_mode(LED_GREEN_SOLID);
            SleepCounter=INCLUSION_TIMEOUT;                   // stay awake longer after inclusion
            if (ListeningBitPending) {          // if listening bit is pending and we have now joined a Z-Wave network, then set the listening bit permanently
                ListeningMode=0x02; // Always listening only if 0x02. By making FLiRS mode enabled for all values except 0x02 will remain a sleeping node if a bit is corrupted.
                SaveConfiguration();
            }
        }
        SendJogNow=TRUE; // send a jog command to the motor once it's awake
    }
    Transport_OnLearnCompleted(bNodeID);
} // ApplicationNetworkLearnCompleted

/*========================   GetMyNodeID   =================================
**    Get the device node ID
**
**   Side effects: none
**--------------------------------------------------------------------------*/
BYTE GetMyNodeID(void) {
	return myNodeID;
}

code const void (code * ZCB_DeviceResetLocallyDone_p)(BYTE txStatus) = &ZCB_DeviceResetLocallyDone;
/**=============================   ZCB_DeviceResetLocallyDone====================
 *  Function:  callback function perform reset device
 *  Called by the ApplicationCommandHandlers once the Hub has been notified (if there is one) that the device has been removed from the Z-Wave network.
 */
void ZCB_DeviceResetLocallyDone(BYTE status)
{
    UNUSED(status);
    if (SendFactoryLimitsNow) { // The I2C command to the motor takes some time so wait for that to complete before resetting
        if (TimerHandleDevRst==0xFF) {
            TimerHandleDevRst = ZW_TIMER_START(ZCB_DeviceResetLocallyDone, TIMER_ONE_SECOND, TIMER_FOREVER); // Start a timer and wait for the I2C command to be finished
        }
    } else {
        AssociationInit(TRUE);    // Force all associations to be cleared
        Transport_SetDefault();
        ZW_SetDefault();
        ListeningMode = DEFAULT_LISTENING_MODE;    // Always return to the default state of a FLiR device upon factory reset
        SetDefaultConfiguration();
        ZW_WatchDogEnable();      // enable the WatchDog reset
       // PIN_LOW(P36); // force the motor to sleep just to clean it up if needed
        //PIN_OUT(P36); // mENABLE
        for (;;) {}               // ZM5202 will reboot by the WatchDog in about 2 seconds
    }
} // ZCB_DeviceResetLocallyDone


#ifdef BOOTLOADER_ENABLED
/*============================ OTA_Finish ===============================
** Function description
** OTA is finish.
**
** Side effects:
**
**-------------------------------------------------------------------------*/
code const void (code * ZCB_OTAFinish_p)(OTA_STATUS otaStatus) = &ZCB_OTAFinish;
void
ZCB_OTAFinish(OTA_STATUS otaStatus) /*Status on OTA*/
{
    UNUSED(otaStatus);
    /*Just reboot node to cleanup and start on new FW.*/
    ZW_WatchDogEnable(); /*reset asic*/
   // PIN_LOW(P36); // force the motor to sleep just to clean it up just in case it needs it
   // PIN_OUT(P36); // mENABLE
    while(1);
}

/*============================ OTA_Start ===============================
** Function description
** Ota_Util calls this function when firmware update is ready to start.
** Return FALSE if OTA should be rejected else TRUE
**
** Side effects:
**
**-------------------------------------------------------------------------*/
code const BOOL (code * ZCB_OTAStart_p)(void) = &ZCB_OTAStart;
BOOL   /*Return FALSE if OTA should be rejected else TRUE*/
ZCB_OTAStart()
{
  BOOL  status = TRUE;
  FirmwareDownloadUnderway=TRUE;
  return status;
}
#endif

/**=======================   handleBasicSetCommand   ===========================
 *    Handling of a Basic Set Command
 */
void handleBasicSetCommand( BYTE val , BYTE ep) {
    UNUSED(ep);
    if (val==0xFF) val=0x63; // on FF set the shade to full open for basic which is the way it should work.
		
    if (val!=SetMotorPositionLast) { // ignore duplicate messages
        SetMotorPositionNow=val;
    } else {
        SendSupervisionReportSID&=0x7F; 
    }
} // handleBasicSetCommand

// ===============SetMotorPosition ===========================================
// Set the motor position to the desired position 0=100%. All of the various flavors of moving the shade flow thru this routine.
// 0 or 99 sends it to the limit.
// FF sends the shade to the home position.
// Note that this should not be called until after the motor is awake
// returns 0 if Motor is error free or the error condition
//BYTE SetMotorPosition(BYTE val) {
//    BYTE resultz=0;
//    signed long int temp,temp2;
//    if (val<100 || val==0xFF) { // legal values
//        if (LimitModeEnabled) { // check that we are not trying to extend the upper limit beyond the lower or vice-versa
//            if ((AdjustingUpperLimit && currentposition.wrd>=lowerlimit.wrd && val==0x00) ||
//                    (AdjustingLowerLimit && currentposition.wrd<=upperlimit.wrd && val==0x63)) return(0); // ignore the command if trying to move beyond the limit
//        }
//        if (val) DirectionLast=0xFF;          // keep track of the last direction of travel
//        else DirectionLast=0;
//        if (val == 0x00) { // closed - send the go to lower limit (down)
//            targetoffset.wrd=0x0000000001;
//            resultz=GoToTarget();
//        } else if (val == 0xFF) {  // Go to HOME position
//            if (((currentposition.wrd<(homeposition.wrd+HOME_DEADBAND)) && (currentposition.wrd>(homeposition.wrd-HOME_DEADBAND)))  // inside the deadband
//                    || homeposition.wrd==HOME_DISABLED) {           // or if HOME is disabled
//                // ignore the command
//            } else { // OK to move to home
//                targetoffset.wrd= homeposition.wrd - currentposition.wrd ;
//                resultz=GoToTarget();
//            }
//        } else if (val == 0x63) {  // Go to upper limit (up)
//            targetoffset.wrd=0xFFFFFFFF;
//            resultz=GoToTarget();
//        }
//        else if ( (val <= 0x63) && (val >= 0x01) ) // compute how far to move the shade
//        {
//            temp=lowerlimit.wrd-upperlimit.wrd;
//            temp2=(signed int)currentpercentage-(signed int)val;
//            targetoffset.wrd=temp2*temp/100;
//            if (0!=targetoffset.wrd) { // don't bother trying to move if the difference is 0.
//                resultz=GoToTarget();
//            }
//        }
//        Waiting4Stop=FALSE;     // sent a move command so not waiting for the motor to stop moving anymore
//    } // ignore illegal values
//    return(resultz);
//}

BYTE SetMotorPosition(BYTE val) {
    BYTE resultz=0;
		BOOL eyeEnable; 
    signed long int temp,temp2;
	
		//READ EYE_SENSOR HERE -  IF BLOCKED, IGNORE COMMAND AND SET RED LED
	
		PIN_IN(P35, 1);
	
#ifdef ENABLE_EYE
		eyeEnable = PIN_GET(P35);
#else
		eyeEnable = 1;
#endif
	
	
	
	
    if ((val<100 || val==0xFF) && (eyeEnable)) { // legal values

			
        if (val) DirectionLast=0xFF;          // keep track of the last direction of travel
        else DirectionLast=0;
        if (val == 0x00) { // closed - send the go to lower limit (down)
            targetoffset.wrd=0x0000000001;
            resultz=GoToTarget();
        } else if (val == 0xFF) {  // Go to HOME position
            targetoffset.wrd=0xFFFFFFFF;
            resultz=GoToTarget();
            
        } else if (val == 0x63) {  // Go to upper limit (up)
            targetoffset.wrd=0xFFFFFFFF;
            resultz=GoToTarget();
        }
        else if ( (val <= 0x63) && (val >= 0x01) ) // compute how far to move the shade
        {
						if(val<32)	targetoffset.wrd=0x00000001;
						else				targetoffset.wrd=0xFFFFFFFF;
						resultz=GoToTarget();
					
        }
        Waiting4Stop=FALSE;     // sent a move command so not waiting for the motor to stop moving anymore
    } // ignore illegal values
		else
			LED_set_mode(LED_RED_SOLID);
		
		
		
		
    return(resultz);
}

uint8_t CC_Version_getNumberOfFirmwareTargets_handler(void) {
    return(1);  // only have 1 firmware target (the SDK code will subtract 1 from this value for the report)
}

/**
 * @brief Report the target value
 * @return target value.
 */
BYTE getAppBasicReportTarget( BYTE endpoint ) {
  UNUSED(endpoint);
  return 0; // not supported
}

/**
 * @brief Report transition duration time.
 * @return duration time.
 */
BYTE getAppBasicReportDuration( BYTE endpoint ) {
    UNUSED(endpoint);
    return 0; // not supported
}


/**========================   getAppBasicReport   ===========================
 *    return the relative position of the shade 0-99%
 */
BYTE getAppBasicReport(BYTE ep) {
    UNUSED(ep);
    return(currentpercentage);
}

void StopMovementNow(void) {
    if (!Waiting4Stop) {   // send only 1 STOP command and then wait for the motor to stop before doing anything else
        StopMovement();    // Send the STOP command to the motor
        Waiting4Stop=TRUE; 
    }
}

void HandleStartChangeCmd( BYTE bStartLevel,        // MultiLevel Switch START command says to go up or down - this is the main command the BRZ1/VCZ1 remote will send
                     BOOL boIgnoreStartLvl,
                     BOOL boDimUp,                  // This is the only parameter used
                     BYTE bDimmingDuration) {

    UNUSED(bStartLevel);
    UNUSED(boIgnoreStartLvl);
    if (LimitModeEnabled) {  // when in Limit Set mode, the remote will constantly send START commands when the UP/DOWN is held
        if ((0==DirectionLast && 0==boDimUp) || (0xFF==DirectionLast && 1==boDimUp)) { // direction is the same so reset the timer.
            LimitModeTimerClear=TRUE;   // clear the timer once the motor command has been sent
        } else {                    // direction has changed so run the motor slow
            LimitModeTimer=0xff;
        }
        if (ToggleHandBitEnabled) { // immediately cancel Limit set mode and ToggleHandBitMode and return to normal mode
            LimitModeEnabled=FALSE;
            ToggleHandBitEnabled=FALSE;
        } else {
            if(boDimUp == 1)        // direction is UP
            {
                SetMotorPositionNow=0x63; // UP (open to let more light in)
            } else {
                SetMotorPositionNow=0x00; // DOWN (less light in the room)
            }
        }
    } else { // normal mode
        if (!MotorMoving) { // normal processing
            if(boDimUp == 1)        // direction is UP
            {
                SetMotorPositionNow=0x63; // UP (open to let more light in)
            } else {
                SetMotorPositionNow=0x00; // DOWN (less light in the room)
            }
        } else {
            if (!((0==DirectionLast && 0==boDimUp) || (0xFF==DirectionLast && 1==boDimUp))) { // direction is changing so just STOP, if not changing direction just ignore it
                StopMovementNow(); // Send the STOP command to the motor
            }
        }
    }
} // HandleStartChangeCmd

void SendBattery(const BYTE dstNodeID, const BYTE value) { // no return because if unable to send, it doesn't really matter, we'll send it next time.
    IBYTE i;
    BYTE * ptr; // temporary pointer
    pRFBuf = GetRequestBuffer(NULL);
    if (NULL != pRFBuf) {
        pRFBuf->ZW_BatteryReportFrame.cmdClass = COMMAND_CLASS_BATTERY;
        pRFBuf->ZW_BatteryReportFrame.cmd = BATTERY_REPORT;
        pRFBuf->ZW_BatteryReportFrame.batteryLevel = value;
#ifndef PRODUCTION_RELEASE // easy way to debug various issues with the motor - just get a battery report with lots of extra data.
        ptr=&pRFBuf->ZW_BatteryReportFrame.batteryLevel;    
        ptr++;  
        *ptr++=BatteryMilliVolts.b.msb; 
        *ptr++=BatteryMilliVolts.b.lsb; 
        *ptr++=OneMinuteCount;
        *ptr++=myTickTime>>8;
        *ptr++=myTickTime;
        *ptr++=OneSecondCount;
        *ptr++=MotorPowerUpDelay>>8;
        *ptr++=MotorPowerUpDelay;
        *ptr++=MemoryGetByte((WORD)&EEOFFSET_NumberPORResets_far);
        *ptr++=MemoryGetByte((WORD)&EEOFFSET_NumberWatchDogResets_far);
        MemoryPutByte((WORD)&EEOFFSET_NumberWatchDogResets_far, 0);
        MemoryPutByte((WORD)&EEOFFSET_NumberPORResets_far, 0);
        *ptr++=debug[0];
        *ptr++=debug[1];
        *ptr++=debug[2];
        *ptr++=debug[3];
        debug[0]=0;
        debug[1]=0;
        debug[2]=0;
        debug[3]=0;
        *ptr++=0xaa;     // marker
        //*ptr++=upperlimit.b[0];
        //*ptr++=upperlimit.b[1];
        //*ptr++=upperlimit.b[2];
        //*ptr++=upperlimit.b[3];
        //*ptr++=lowerlimit.b[0];
        //*ptr++=lowerlimit.b[1];
        //*ptr++=lowerlimit.b[2];
        //*ptr++=lowerlimit.b[3];
        //*ptr++=currentposition.b[0];
        //*ptr++=currentposition.b[1];
        //*ptr++=currentposition.b[2];
        //*ptr++=currentposition.b[3];
        //*ptr++=homeposition.b[0];
        //*ptr++=homeposition.b[1];
        //*ptr++=homeposition.b[2];
        //*ptr++=homeposition.b[3];
        *ptr++=SendBatteryReportNow;
        *ptr++=SendBatteryCriticalNow;
        *ptr++=BatteryAboveBattLow;
        *ptr++=BatteryLast;
        *ptr++=duration>>24;
        *ptr++=duration>>16;
        *ptr++=duration>>8;
        *ptr++=duration;
        *ptr++=currentpercentage;
        *ptr++=MotorStatus;
        if (ZW_TX_IN_PROGRESS != Transport_SendRequestEP(pRFBuf, sizeof(ZW_BATTERY_REPORT_FRAME)+23, pTxOptionsEx, ZCB_RequestJobStatus)) 
#else
            if (ZW_TX_IN_PROGRESS != Transport_SendRequestEP(pRFBuf, sizeof(ZW_BATTERY_REPORT_FRAME), pTxOptionsEx, ZCB_RequestJobStatus))
#endif
            {
                FreeRequestBuffer(); // Unable to send so free the buffer
            }
    }
} // Send Battery

/**=============================   MyhandleCommandClassPowerLevel  ============
 * PowerLevel CC is actually quite simple but the SDK doesn't handle a FLiRs device very well and stores stuff in NVM which has to be restored on wakeup.
 * But we don't want to waste power loading all that stuff for something that will virtually never be used.
 * So this is MUCH simpler!
 */
received_frame_status_t MyhandleCommandClassPowerLevel(
  RECEIVE_OPTIONS_TYPE_EX *rxOpt,
  ZW_APPLICATION_TX_BUFFER *pCmd,
  BYTE   cmdLength)
{
    received_frame_status_t frame_status = RECEIVED_FRAME_STATUS_NO_SUPPORT;
    RxToTxOptions(rxOpt, &pTxOptionsEx);
    switch (pCmd->ZW_Common.cmd) {
        case POWERLEVEL_GET:
            pRFBuf =  GetResponseBuffer();
            if (NULL != pRFBuf) {
                pRFBuf->ZW_PowerlevelReportFrame.cmdClass   = COMMAND_CLASS_POWERLEVEL;
                pRFBuf->ZW_PowerlevelReportFrame.cmd        = POWERLEVEL_REPORT;
                pRFBuf->ZW_PowerlevelReportFrame.powerLevel = ZW_RFPowerLevelGet();
                pRFBuf->ZW_PowerlevelReportFrame.timeout    = PowerLevelSec;
                if (ZW_TX_IN_PROGRESS != Transport_SendResponseEP( (BYTE *)pRFBuf, sizeof(ZW_POWERLEVEL_REPORT_FRAME), pTxOptionsEx, ZCB_ResponseJobStatus)) {
                    FreeResponseBuffer(); /*Job failed, free mutex */
                }
            } // if no buffer then just drop it and wait for a retry
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            break;

        case POWERLEVEL_SET:
            if (pCmd->ZW_PowerlevelSetFrame.powerLevel<=POWERLEVEL_SET_MINUS9DBM) {   // ignore invalid power level settings - Minus9DBM is the largest valid value
                if (pCmd->ZW_PowerlevelSetFrame.timeout == 0 ||          //If timeout is 0 stop test
                        (pCmd->ZW_PowerlevelSetFrame.powerLevel == normalPower))  // If powerLevel is normalPower stop test
                {
                    ZW_RFPowerLevelSet( normalPower ); // go to normal power
                    PowerLevelSec = 0;
                } else {
                    ZW_RFPowerLevelSet(pCmd->ZW_PowerlevelSetFrame.powerLevel); // Set the power level
                    PowerLevelSec=pCmd->ZW_PowerlevelSetFrame.timeout;     // which will expire after this many seconds.
                }
            }
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            break;

        case POWERLEVEL_TEST_NODE_SET: // store the variables - the test packets are sent every 10ms from inside ApplicationPoll
            PowerLevelSourceNodeID  =rxOpt->sourceNode.nodeId;
            PowerLevelTestNodeID    =pCmd->ZW_PowerlevelTestNodeSetFrame.testNodeid;
            PowerLevelTestPowerLevel=pCmd->ZW_PowerlevelTestNodeSetFrame.powerLevel;
            PowerLevelTestCounter   = (WORD)((WORD)pCmd->ZW_PowerlevelTestNodeSetFrame.testFrameCount1 << 8)
                + pCmd->ZW_PowerlevelTestNodeSetFrame.testFrameCount2;
            PowerLevelGoodCounter=0;
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            break;

        case POWERLEVEL_TEST_NODE_GET:
            pRFBuf =  GetResponseBuffer();
            if (NULL != pRFBuf) {
                pRFBuf->ZW_PowerlevelTestNodeReportFrame.cmdClass         = COMMAND_CLASS_POWERLEVEL;
                pRFBuf->ZW_PowerlevelTestNodeReportFrame.cmd              = POWERLEVEL_TEST_NODE_REPORT;
                pRFBuf->ZW_PowerlevelTestNodeReportFrame.testNodeid       = PowerLevelTestNodeID;
                pRFBuf->ZW_PowerlevelTestNodeReportFrame.statusOfOperation= PowerLevelTestCounter ? POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_INPROGRESS : 
                                                                            PowerLevelGoodCounter ? POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_SUCCES : POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_FAILED;
                pRFBuf->ZW_PowerlevelTestNodeReportFrame.testFrameCount1  = PowerLevelGoodCounter>>8;
                pRFBuf->ZW_PowerlevelTestNodeReportFrame.testFrameCount2  = PowerLevelGoodCounter;
                if (ZW_TX_IN_PROGRESS != Transport_SendResponseEP((BYTE *)pRFBuf, sizeof(ZW_POWERLEVEL_TEST_NODE_REPORT_FRAME),pTxOptionsEx , ZCB_ResponseJobStatus)) {
                    FreeResponseBuffer(); /*Job failed, free mutex */
                }
            }
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            break;

        default: // ignore any other commands
            break;
    } // Switch
    return(frame_status);
} // MyHandleCommandClassPowerLevel

//////////////////////////////////////////////////////////////////////////////////////////////
code const void (code * ZCB_PowerLevelTestCallback_p)(void) = &ZCB_PowerLevelTestCallback;
void ZCB_PowerLevelTestCallback(BYTE txStatus) {
    PowerLevelDelay=5; // Send the next one in about 50ms
    if (txStatus == TRANSMIT_COMPLETE_OK) PowerLevelGoodCounter++; // got an ACK so increment the count
    if (PowerLevelTestCounter==0) {
        pRFBuf =  GetResponseBuffer();
        if (NULL != pRFBuf) {
            pRFBuf->ZW_PowerlevelTestNodeReportFrame.cmdClass         = COMMAND_CLASS_POWERLEVEL;
            pRFBuf->ZW_PowerlevelTestNodeReportFrame.cmd              = POWERLEVEL_TEST_NODE_REPORT;
            pRFBuf->ZW_PowerlevelTestNodeReportFrame.testNodeid       = PowerLevelTestNodeID;
            pRFBuf->ZW_PowerlevelTestNodeReportFrame.statusOfOperation= PowerLevelTestCounter ? POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_INPROGRESS : 
                PowerLevelGoodCounter ? POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_SUCCES : POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_FAILED;
            pRFBuf->ZW_PowerlevelTestNodeReportFrame.testFrameCount1  = PowerLevelGoodCounter>>8;
            pRFBuf->ZW_PowerlevelTestNodeReportFrame.testFrameCount2  = PowerLevelGoodCounter;
            pTxOptionsEx->pDestNode->node.nodeId=PowerLevelSourceNodeID;
            pTxOptionsEx->pDestNode->nodeInfo.security=GetHighestSecureLevel(ZW_GetSecurityKeys());
            if(ZW_TX_IN_PROGRESS != Transport_SendResponseEP( (BYTE *)pRFBuf, sizeof(ZW_POWERLEVEL_TEST_NODE_REPORT_FRAME) , pTxOptionsEx, ZCB_ResponseJobStatus)) {
                FreeResponseBuffer(); /*Job failed, free mutex */
            }
        }
    }
}

void SendMultilevelReport(void) { // Send a multilevel report to the the destination in pTxOptionsEx
    pRFBuf = GetRequestBuffer(NULL);
    if (NULL != pRFBuf) {
        pRFBuf->ZW_SwitchMultilevelReportV4Frame.cmdClass = COMMAND_CLASS_SWITCH_MULTILEVEL;
        pRFBuf->ZW_SwitchMultilevelReportV4Frame.cmd = SWITCH_MULTILEVEL_REPORT;
        pRFBuf->ZW_SwitchMultilevelReportV4Frame.currentValue = currentpercentage;
        pRFBuf->ZW_SwitchMultilevelReportV4Frame.targetValue = MotorMoving ? SetMotorPositionLast : currentpercentage; // If the motor is moving, then the TARGET is where we are headed.
        pRFBuf->ZW_SwitchMultilevelReportV4Frame.duration = MotorMoving ? 0xFE : 0x00; // duration is UNKNOWN(SDS12657-9 table 88) if moving. Already at target level (0) if not moving.
        // The pTxOptionsEx has already been set to the desired values based on the source of the request
        if (ZW_TX_IN_PROGRESS != Transport_SendRequestEP(pRFBuf, sizeof(ZW_SWITCH_MULTILEVEL_REPORT_V4_FRAME), pTxOptionsEx, ZCB_RequestJobStatus)) {
            FreeRequestBuffer(); /*Job failed, free transmit-buffer pTxBuf by cleaing mutex */
        }
    }
} // SendMultilevelReport

/**=============================   MyhandleCommandClassMultiLevelSwitch  ============
 * Originally copied from the SDK but made a lot simpler since a shade does not support duration nor endpoints.
 * The original version would call the STOP command anytime a new command was received but for the shade we do not want to do that.
 */
received_frame_status_t MyhandleCommandClassMultiLevelSwitch(
  RECEIVE_OPTIONS_TYPE_EX *rxOpt,
  ZW_APPLICATION_TX_BUFFER *pCmd,
  BYTE   cmdLength)
{
    received_frame_status_t frame_status = RECEIVED_FRAME_STATUS_NO_SUPPORT;
    IBYTE destendpoint = rxOpt->destNode.endpoint;
    BOOL boDimUp = FALSE;
    BOOL boIgnoreLevel;
    switch (pCmd->ZW_Common.cmd) {
        case SWITCH_MULTILEVEL_GET:
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            SendMultilevelReportNow=rxOpt->sourceNode.nodeId;   // queue the report
            pTxOptionsEx->pDestNode->node.nodeId=SendMultilevelReportNow;
            break;

        case SWITCH_MULTILEVEL_SET:
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            if (pCmd->ZW_SwitchMultilevelSetV3Frame.value==0xFF && MotorMoving) { // then just stop, a 2nd press is required to move to the home position
                StopMovementNow(); // Send the STOP command to the motor
            } else {
                if (SetMotorPositionLast!=pCmd->ZW_SwitchMultilevelSetV3Frame.value) { // ignore duplicate set commands which cause the motor to stutter
                    SetMotorPositionNow=pCmd->ZW_SwitchMultilevelSetV3Frame.value;
                } else {
                    SendSupervisionReportSID&=0x7F; 
                }
            }
            break;

        case SWITCH_MULTILEVEL_START_LEVEL_CHANGE:
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            boIgnoreLevel = (pCmd->ZW_SwitchMultilevelStartLevelChangeFrame.level & SWITCH_MULTILEVEL_START_LEVEL_CHANGE_LEVEL_IGNORE_START_LEVEL_BIT_MASK);
            if ((!boIgnoreLevel) &&
                    (pCmd->ZW_SwitchMultilevelStartLevelChangeFrame.startLevel > 99) &&
                    (pCmd->ZW_SwitchMultilevelStartLevelChangeFrame.startLevel != 0xFF))
            {
                return(frame_status); // ignore invalid levels
            }
            if (!(pCmd->ZW_SwitchMultilevelStartLevelChangeV3Frame.properties1&
              SWITCH_MULTILEVEL_START_LEVEL_CHANGE_PROPERTIES1_UP_DOWN_MASK_V3)) {
                boDimUp = TRUE; /*primary switch Up/Down bit field value says go Up*/
            }
            HandleStartChangeCmd(pCmd->ZW_SwitchMultilevelStartLevelChangeFrame.startLevel,
                    boIgnoreLevel,
                    boDimUp,
                    pCmd->ZW_SwitchMultilevelStartLevelChangeV3Frame.dimmingDuration);
            break;

        case SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE:
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            StopMovementNow();
            break;

        case SWITCH_MULTILEVEL_SUPPORTED_GET_V3:
            frame_status = RECEIVED_FRAME_STATUS_SUCCESS;
            pRFBuf =  GetResponseBuffer();
            if(NULL != pRFBuf) {
                RxToTxOptions(rxOpt, &pTxOptionsEx);

                pRFBuf->ZW_SwitchMultilevelSupportedReportV3Frame.cmdClass = COMMAND_CLASS_SWITCH_MULTILEVEL;
                pRFBuf->ZW_SwitchMultilevelSupportedReportV3Frame.cmd = SWITCH_MULTILEVEL_SUPPORTED_REPORT_V3;
                pRFBuf->ZW_SwitchMultilevelSupportedReportV3Frame.properties1 = MULTILEVEL_SWITCH_DOWN_UP;  // 0x02);//bg from table 72 of multilevel v3 CC
                pRFBuf->ZW_SwitchMultilevelSupportedReportV3Frame.properties2 = 0x00; /*The secondary switch type is deprectaed, thus the value should be not support (0x02)*/

                if (ZW_TX_IN_PROGRESS != Transport_SendResponseEP( (BYTE *)pRFBuf, sizeof(ZW_SWITCH_MULTILEVEL_SUPPORTED_REPORT_V3_FRAME), pTxOptionsEx, ZCB_ResponseJobStatus)) {
                    FreeResponseBuffer(); /*Job failed, free transmit-buffer pTxBuf by cleaing mutex */
                }
            }
            break;

        default:
            break;
    }
    return(frame_status);
}


/*============================   SaveConfiguration   ======================
**    This function saves the current configuration to EEPROM
**--------------------------------------------------------------------------*/
void SaveConfiguration(void) {
    /* Mark stored configuration as OK */
    MemoryPutBuffer((WORD)&EEOFFSET_HomePosition_far, &homeposition.wrd,4,NULL);
    MemoryPutByte((WORD)&EEOFFSET_LISTENING_far, ListeningMode);
    MemoryPutByte((WORD)&EEOFFSET_MAGIC_far, APPL_MAGIC_VALUE);
    if (MemoryGetByte((WORD)&EEOFFSET_JogDistance_far)!=~MemoryGetByte((WORD)&EEOFFSET_JogDistanceNOT_far)) { // if the JogDistance is corrupted, fix it and make it the default of 0
        MemoryPutByte((WORD)&EEOFFSET_JogDistance_far, 0);
        MemoryPutByte((WORD)&EEOFFSET_JogDistanceNOT_far, ~0);
    }
}


/*============================   SetDefaultConfiguration   ======================
**    Function resets configuration to default values.
**
**    Side effects:
**
**--------------------------------------------------------------------------*/
void SetDefaultConfiguration(void) {
  onOffState = 0;
  AssociationInit(TRUE);  // Clear association NodeID(s)
  SaveConfiguration();
}


/*============================   LoadConfiguration   ======================
 *    This function loads the application settings from EEPROM.
 *    If no settings are found, default values are used and saved.
 *    Side effects:
 *
 */
void LoadConfiguration(void) {
    IBYTE i;
    MemoryGetID( NULL, &myNodeID); // Fetch the NodeID
    ManufacturerSpecificDeviceIDInit();
    /* Check to see, if any valid configuration is stored in the EEPROM */
    if (MemoryGetByte((WORD)&EEOFFSET_MAGIC_far) == APPL_MAGIC_VALUE)
    {
MAGIC_OK:
        /* There is a configuration stored, so load it */
        MemoryGetBuffer((WORD)&EEOFFSET_HomePosition_far, &homeposition.wrd,4);
        ListeningMode=MemoryGetByte((WORD)&EEOFFSET_LISTENING_far);
        if (ListeningMode>0x02) {
            ListeningMode=DEFAULT_LISTENING_MODE;
            MemoryPutByte((WORD)&EEOFFSET_LISTENING_far, ListeningMode);
        }
        MotorStatusMove=MemoryGetByte((WORD)&EEOFFSET_MotorStatusMove_far);
        MotorStatusEep=MemoryGetByte((WORD)&EEOFFSET_MotorStatusEep_far);
        MotorStatusI2C=MemoryGetByte((WORD)&EEOFFSET_MotorStatusI2C_far);
    } else {  // There is no valid configuration in EEPROM, so load default values and save them to EEPROM.
        for (i=0;i<100;i++) OneUsDelay(100); // wait about 10mSec - maybe power is glitching and EEPROM is not yet running?
        if (MemoryGetByte((WORD)&EEOFFSET_MAGIC_far) == APPL_MAGIC_VALUE) goto MAGIC_OK; // EEPROM is OK now so return to normal processing
        // Either virgin device or NVM is corrupted so set the default configuration
        Transport_SetDefault(); // Initialize transport layer NVM
        ZW_SetDefault();        // Reset protocol
        MotorStatusMove=0;      // error counts
        MotorStatusEep=0;
        MotorStatusI2C=0;
        ListeningMode = DEFAULT_LISTENING_MODE;
        SetDefaultConfiguration();
    }
} // LoadConfiguration

void LED_set_mode(LED_MODE_TYPE mode, BYTE speed, LED_DUTY_TYPE duty) { // set the LED mode, speed and duty cycle
    LED_mode=mode;
    LED_speed=speed;
    LED_duty=duty;
    LED_count=0;
    LED_state=TRUE;
}

void LED_count_next(void) { // set the next LED_count based on the current duty cycle and state
    if (LED_ACCEL==LED_speed) { // acceleration mode - LED blinks faster and faster
        if (FULL_RESET_TIME>=ButtPressedCount) LED_count=FULL_RESET_TIME-ButtPressedCount+1;
        else LED_count=1;
    } else {
        switch (LED_duty) {
            case LED_SOLID:
                LED_count=1;
                break;
            case LED_BLINK:
                LED_count=LED_speed/2;
                break;
            case LED_WINKON:
                if (LED_state) {
                    LED_count=(LED_speed>>8)+1;
                } else {
                    LED_count=LED_speed - ((LED_speed>>8)+1);
                }
                break;
            case LED_WINKOFF:
                if (!LED_state) {
                    LED_count=(LED_speed>>8)+1;
                } else {
                    LED_count=LED_speed - ((LED_speed>>8)+1);
                }
                break;
            default:
                LED_duty=LED_SOLID;
                break;
        } // switch
    }
} // LED_duty_set

/**
 * Led10ms must be called every 10ms.
 * It will blink the LED based on the current mode.
 * LED_Mode: - the LED can be either Green or Red or both (yellow).
 *  LED_OFF
 *  LED_G
 *  LED_Y (both on)
 *  LED_R
 * LED_Speed: - The blink speed is based on defines in config_app.h
 *  LED_SLOW
 *  LED_MEDIUM
 *  LED_FAST
 *  LED_ACCEL - blink rate goes faster and faster from LED_SLOW to LED_FAST in 15 seconds
 * LED_Duty: - duty cycle of the LED ON vs. OFF time
 *  LED_SOLID (on solid)
 *  LED_BLINK (50/50)
 *  LED_WINKON  (On 10 Off 90)
 *  LED_WINKOFF (On 90 off 10)
 */
void Led10ms(void) {
    if (LED_count) { // count down until something interesting to do
        LED_count--;
    } else {        // counted down to zero so time to update
        if (LEDOnTimer) { // Time to turn the LED off?
            LEDOnTimer--;
            if (0==LEDOnTimer) LED_mode=LED_OFF; // turn the LED off
        }
        switch (LED_mode) {
            case LED_OFF: // LEDs off
                GRNOFF; REDOFF;
                break;
            case LED_G: // green
                if (LED_state) {
                    GRNON; REDOFF;
                } else {
                    GRNOFF; REDOFF;
                }
                break;
            case LED_Y: // yellow
                if (LED_state) {
                    GRNON; REDON;
                } else {
                    GRNOFF; REDOFF;
                }
                break;
            case LED_R: // RED
                if (LED_state) {
                    GRNOFF; REDON;
                } else {
                    GRNOFF; REDOFF;
                }
                break;
            default: // should never happen
                LED_mode=LED_OFF;
                break;
        } // switch
        LED_count_next();           // compute the time for the next toggle
        if (LED_mode!=LED_OFF) { // always toggle the LED except when off
            if (LED_duty!=LED_SOLID) { // don't toggle if the LED is just on
                if (LED_BlinkCount && LED_state) {
                    LED_BlinkCount--;
                    if (0==LED_BlinkCount) LED_mode=LED_OFF;
                    else LEDOnTimer=LED_SOLID_TIMEOUT;
                }
                LED_state=~LED_state; // toggle LED state
            }
        } // LED_MODE
    } // if LED_Count==0
} // LED10ms

void OneUsDelay(unsigned char delay )
{
    do {
        _nop_(); // the simulator says this is only couple hundred ns but a scope measures it at ~1usec
    } while(--delay);  // This compiles into a single DJNZ and the NOP
}
#if ! defined(PRODUCTION_RELEASE)
////////////////////////scope_debug////////////////////////////////////////////////////////////////
// Toggle the GPIO pin with an 8-bit value - goes high for a longer time when a 1 and shorter for a 0.
// Easy to read the value when observed on a scope - MSB is on the left and LSB is on the right.
// A wider duty cycle is a 1 and a narrow one is a 0.
//___   _     _     _     ___   _     _     ___   _     ___
//   |_| |___| |___| |___|   |_| |___| |___|   |_| |___/     = 0x12
//      7     6     5     4     3     2     1     0
// Takes about 35usec which is a lot faster than a UART so works pretty well for real-time debug.
// Toggles P22 which is the MOSI which is on the Z-Wave header pin 4
// Easily changed to be any spare IO pin - LED pins are good choices
#pragma OT(11,SPEED)    // Switch to speed optimization otherwise Keil makes subroutines which are very slow.
void scope_debug(BYTE dat) {
    EA=0;   // must briefly disable interrupts to keep timing consistent - might want to save it and restore it
    PIN_LOW(P22);
    PIN_OUT(P22);   // SCK pin
    if (dat&0x80) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x40) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x20) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x10) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x08) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x04) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x02) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    if (dat&0x01) {PIN_HIGH(P22)}
    OneUsDelay(1);
    PIN_HIGH(P22);
    PIN_LOW(P22);
    PIN_IN(P22,1);
    EA=1;
} // scope_debug
#endif

/**
* Set up security keys to request when joining a network.
* These are taken from the config_app.h header file.
*/
BYTE ApplicationSecureKeysRequested(void)
{
  return REQUESTED_SECURITY_KEYS;
}

/**
* Set up security S2 inclusion authentication to request when joining a network.
* These are taken from the config_app.h header file.
*/
BYTE ApplicationSecureAuthenticationRequested(void)
{
  return REQUESTED_SECURITY_AUTHENTICATION;
}

/*
 * @brief Called when protocol needs to inform Application about a Security Event.
 * @details If the app does not need/want the Security Event the handling can be left empty.
 *
 *    Event E_APPLICATION_SECURITY_EVENT_S2_INCLUSION_REQUEST_DSK_CSA
 *          If CSA is needed, the app must do the following when this event occures:
 *             1. Obtain user input with first 4 bytes of the S2 including node DSK
 *             2. Store the user input in a response variable of type s_SecurityS2InclusionCSAPublicDSK_t.
 *             3. Call ZW_SetSecurityS2InclusionPublicDSK_CSA(response)
 *
 */
void
ApplicationSecurityEvent(
  s_application_security_event_data_t *securityEvent)
{
  switch (securityEvent->event)
  {
#ifdef APP_SUPPORTS_CLIENT_SIDE_AUTHENTICATION // which we don't...
    case E_APPLICATION_SECURITY_EVENT_S2_INCLUSION_REQUEST_DSK_CSA:
      {
        ZW_SetSecurityS2InclusionPublicDSK_CSA(&sCSAResponse);
      }
      break;
#endif /* APP_SUPPORTS_CLIENT_SIDE_AUTHENTICATION */

    default:
      break;
  }
}

/**
 * Function return firmware Id of target n (0 => is device FW ID)
 * n read version of firmware number n (0,1..N-1)
 * @return firmaware ID.
 */
WORD handleFirmWareIdGet( BYTE n) {
    if(n == 0)
    {
        return APP_FIRMWARE_ID;
    }
    else if (n == 1)
    {
        return 0x1234;
    }
    return 0;
}

