@echo off
rem ER - 2014-07-14 - based on the Zensys script but cleaned up to run faster and in a single script instead of 2.
rem ER - 2017-10-19 - Changed to match the SDK 6.81 release where the bootloader is supplied in the release and
rem                   we don't have to build it.
rem usage: mymakeboot.bat <build_prj/projname/rels/filename.hex>
rem output: creates 2 Intel hex files in the build_prj/<projname> directory:
rem     filename_OTA.otz is the file to download for Over The Air Firmware Update
rem     filename_BOOT.hex is the file to program using a wired programmer - this version includes the bootloader code

set HEXPATHFILE=%1
set HEXPATH=%~p1
set HEXNAME=%~n1
set BOOTPATHFILE=C:\DevKit_6_81_00\Z-Wave\lib\bootloader_ZW050x\bootloader_ZW050x.hex
set BOOTLOADER_SIZE=0x1800

rem delete the target files so if the script fails you are not left with stale hex files
if exist %HEXNAME%_OTA.otz (
    del %HEXNAME%_OTA.otz
    del %HEXNAME%_BOOT.hex
)

if exist %BOOTPATHFILE% (
rem first cleanup any issues in the hex file by passing it thru srec and copy to this directory
%TOOLSDIR%\HexTools\srec_cat.exe %HEXPATHFILE% -Intel -Output %HEXNAME%.hex -Intel -address-length=3 -Line_Length 44

rem add the 16-bit CRC at the end of the first 32K
%TOOLSDIR%\fixbootcrc\fixbootcrc.exe 0 %HEXNAME%.hex > %HEXNAME%_banks.txt
del %HEXNAME%.hex
rename %HEXNAME%-crc.hex %HEXNAME%.hex

rem overlay the bootloader code for the wired programmer version
%TOOLSDIR%\HexTools\srec_cat.exe %HEXNAME%.hex -Intel %BOOTPATHFILE% -Intel -Output %HEXNAME%_BOOT.hex -Intel -address-length=3 -DO -Line_Length 44
rem no idea why srec fails to put the end of file marker in the file so add it here...
echo :00000001FF>> %HEXNAME%_BOOT.hex

rem next create the compressed .OTZ file for OTA firmware update
rem convert the file to binary
%TOOLSDIR%\HexTools\srec_cat.exe %HEXNAME%_BOOT.hex -intel -exclude 0x0 %BOOTLOADER_SIZE% -offset -%BOOTLOADER_SIZE% -o %HEXNAME%.bin -binary
rem compress it
%TOOLSDIR%\..\Z-Wave\lib\otacompress\otacompress.exe %HEXNAME%.bin %HEXNAME%.bin.lz1
rem convert back to intel hex
%TOOLSDIR%\HexTools\srec_cat.exe %HEXNAME%.bin.lz1 -binary -o %HEXNAME%_OTA.otz -intel -line-length=43

del %HEXNAME%.bin
del %HEXNAME%.bin.lz1
del %HEXNAME%.hex

move %HEXNAME%_OTA.otz build_prj/%HEXNAME% >NUL
move %HEXNAME%_BOOT.hex build_prj/%HEXNAME% >NUL

type %HEXNAME%_banks.txt
del  %HEXNAME%_banks.txt

) else (
    echo       ###################################################
    echo =================================================================
    echo Unable to find %BOOTPATHFILE% - files not created
    echo =================================================================
    echo       ###################################################
)
