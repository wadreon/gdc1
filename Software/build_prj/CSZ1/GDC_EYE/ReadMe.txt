The CSZ1_ota_Rev06.01_INTERMEDIATE.hex file is the file that MUST be
downloaded to a Rev 0B.XX CSZ1 BEFORE attempting to OTA the latest release.
The 0B.XX versions were built on the Sigma 6.61.00 SDK which uses a 
different Bootloader than the latest release. The INTERMEDIATE release
is required to install the new Bootloader.

So the process is:
OTA CSZ1_ota_Rev06.01_INTERMEDIATE.hex
power cycle the shade
Then OTA the latest release and it should work.
There is a  likelyhood that either of the OTA updates may make the shade
forget which Z-Wave network it was attached to. Try resetting and re-including
to the Z-Wave network.

This process has only a small chance of "bricking" the shade. As long as power
is cleanly applied during the entire process it should work OK.
It is likely the process will take two or three tries for each step as if
there is a data error the process will have to be restarted. The code is
checked with several checksum to ensure the data is accurate so the chance of
a failure is remote.
