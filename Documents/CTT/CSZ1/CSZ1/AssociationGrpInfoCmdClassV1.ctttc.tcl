﻿PACKAGE AssociationGrpInfoCmdClassV1; // do not modify this line

USE AssociationGrpInfo CMDCLASSVER = 1;
USE Association CMDCLASSVER = 1;

/**
 * Association Group Info Command Class Version 1 Test Script
 * Last Update: February, 27th, 2017
 * Command Class Specification: SDS12657-9
 * Formatting Conventions: Version 2016-05-04
 *
 * ChangeLog:
 *
 * October 15th, 2015   - VerifyGroupInfoListMode is able to deal with multiple group info reports
 * October 15th, 2015   - VerifyGroupInfo prints profile categories
 * October 15th, 2015   - VerifyGroupInfo checks for correct profile bytes for group 1 (Lifeline)
 * April 13th, 2016     - Refactoring
 * April 13th, 2016     - Bugfix in VerifyGroupInfoListMode test sequence
 * May 19th, 2016       - Refactoring
 * June 2nd, 2016       - Minor improvements
 * February 27th, 2017  - Irrigation CC added
 *
 */


/**
 * VerifyGroupNames
 * Verifies the Association Group names
 *
 * CC versions: 1
 */

TESTSEQ VerifyGroupNames: "Verify Association Group Names"

    SEND Association.GroupingsGet();
    EXPECT Association.GroupingsReport($supportedGroupings = SupportedGroupings);

    LOOP ($grp; 1; $supportedGroupings)
    {
        SEND AssociationGrpInfo.AssociationGroupNameGet(GroupingIdentifier = $grp);
        EXPECT AssociationGrpInfo.AssociationGroupNameReport(
            GroupingIdentifier == $grp,
            $length = LengthOfName,
            $name = Name);
        IF (LENGTH($name) != $length)
        {
            MSGFAIL ("Error in length field of Assocation Group Name Report for group {0}. Reported 'LengthOfName' is {1}, real length is {2}.", UINT($grp), UINT($length), UINT(LENGTH($name)));
        }
        MSG ("UTF-8 Encoded name of Group {0} is: {1}", UINT($grp), $name);
    }

TESTSEQ END


/**
 * VerifyGroupInfoListModeOff
 * Verifies the Association Group Info with List Mode = 0
 *
 * CC versions: 1
 */

TESTSEQ VerifyGroupInfoListModeOff: "Verify Association Group Info with List Mode = 0"

    SEND Association.GroupingsGet();
    EXPECT Association.GroupingsReport($supportedGroupings = SupportedGroupings);

    LOOP ($grp; 1; $supportedGroupings)
    {
        SEND AssociationGrpInfo.AssociationGroupInfoGet(
            Reserved = 0,
            ListMode = 0,
            RefreshCache = 0,
            GroupingIdentifier = $grp);
        EXPECT AssociationGrpInfo.AssociationGroupInfoReport(
            GroupCount == 1,
            DynamicInfo in (0, 1),
            ListMode == 0,
            $payload = vg1);

        IF (LENGTH($payload) != 7)
        {
            MSGFAIL ("Error in payload length Assocation Group Info Report for group {0}. Received {1}, expected 7.", UINT($grp), UINT(LENGTH($payload)));
        }
        IF ($payload[0] != UINT($grp))
        {
            MSGFAIL ("Expected Group Info for Group {0} received {1}", UINT($grp), UINT($payload[0]));
        }
        IF ($payload[1] != 0)
        {
            MSGFAIL ("Mode = 0 not set to 0. Received: {0}", UINT($payload[1]));
        }

        MSG ("Association Group {0} has profile byte MSB 0x{1:X2} and LSB 0x{2:X2} referencing the following categories:", UINT($grp), $payload[2], $payload[3]);

        IF ($payload[2] == 0x00)
        {
            IF     ($payload[3] == 0x00) { MSG ("General:Not Applicable - There is no specific class of events for this association group."); }
            ELSEIF ($payload[3] == 0x01) { MSG ("General:Lifeline - This association group is intended for all events relevant for the Lifeline group."); }
            ELSE                         { MSGFAIL ("General:Unknown - Invalid Profile LSB ({0:X2}).", $payload[3]); }
        }
        ELSEIF ($payload[2] == 0x20)
        {
            MSG ("Control Key 0x{0:X2} - Members of this association group are controlled in response to user input for key 0x{0:X2}.", $payload[3]);
        }
        ELSEIF ($payload[2] == 0x31)
        {
            MSG ("Sensor Type 0x{0:X2} - Members of this association group are controlled when the sensor value changes or receives a sensor report of the given sensor type.", $payload[3]);
        }
        ELSEIF ($payload[2] == 0x71)
        {
            MSG ("Notification Type 0x{0:X2} - Members of this association group are controlled when an event is detected or receives a notification of the given notification type.", $payload[3]);
        }
        ELSE
        {
            MSGFAIL ("Invalid or unknown Profile Bytes: MSB {0:X2}, LSB {1:X2}.", $payload[2], $payload[3]);
        }

        IF ($grp == 1) // lifeline group
        {
            IF ($payload[2] != 0x00 || $payload[3] != 0x01)
            {
                MSGFAIL ("Wrong profile bytes for group 1 (Lifeline): required values: MSB 0x00, LSB 0x01");
            }
        }

        IF ($payload[4] != 0)
        {
            MSGFAIL ("Reserved field not set to 0. Received: 0x{0:X2}", $payload[4]);
        }

        IF (($payload[5] != 0) || ($payload[6] != 0))
        {
            MSGFAIL ("Event Code MSB and LSB not set to 0. Received: 0x{0:X2}{1:X2}", $payload[5], $payload[6]);
        }
    }

TESTSEQ END


/**
 * VerifyGroupInfoListeModeOn
 * Verifies the Association Group Info with List Mode = 1
 *
 * CC versions: 1
 */

TESTSEQ VerifyGroupInfoListModeOn: "Verify Association Group Info in List Mode"

    SEND Association.GroupingsGet();
    EXPECT Association.GroupingsReport($supportedGroupings = SupportedGroupings);
    MSG ("Suppored groupings: {0}", UINT($supportedGroupings));

    SEND AssociationGrpInfo.AssociationGroupInfoGet(
        Reserved = 0,
        ListMode = 1,
        RefreshCache = 0,
        GroupingIdentifier = 0);
    EXPECT AssociationGrpInfo.AssociationGroupInfoReport(
        $groupCount = GroupCount in (1 ... $supportedGroupings),
        DynamicInfo in (0, 1),
        ListMode == 1,
        $payload = vg1);
    $receivedGroupCount = $groupCount;

    IF ($receivedGroupCount == $supportedGroupings)
    {
        MSG ("All group infos have been received in first report.");
        IF (LENGTH($payload) != $supportedGroupings * 7)
        {
            MSGFAIL ("Error in payload length. Received {0} expected {1}.", UINT(LENGTH($payload)), UINT($supportedGroupings * 7));
        }
    }
    // if first report did not contain all groups: expect the remaining group info reports in a loop
    ELSEIF ($receivedGroupCount < $supportedGroupings)
    {
        MSG ("{0} of {1} group infos have been received in first report.", UINT($receivedGroupCount), UINT($supportedGroupings));
        IF (LENGTH($payload) != $groupCount * 7)
        {
            MSGFAIL ("Error in payload length. Received {0} expected {1}.", UINT(LENGTH($payload)), UINT($groupCount * 7));
        }

        MSG ("Expecting reports for {0} remaining groups...", UINT($supportedGroupings - $receivedGroupCount));
        LOOP ($LoopCounter; $receivedGroupCount + 1; $supportedGroupings)
        {
            EXPECT AssociationGrpInfo.AssociationGroupInfoReport(
                $groupCount = GroupCount in (1 ... $supportedGroupings - $receivedGroupCount),
                DynamicInfo in (0, 1),
                ListMode == 1,
                $payload = vg1);
            $receivedGroupCount = $receivedGroupCount + $groupCount;
            IF ($groupCount > 0)
            {
                MSG ("{0} group info(s) have been received in next report.", UINT($groupCount));
            }
            IF (LENGTH($payload) != $groupCount * 7)
            {
                MSGFAIL ("Error in payload length. Received {0} expected {1}.", UINT(LENGTH($payload)), UINT($groupCount * 7));
            }
            IF ($receivedGroupCount < $supportedGroupings)
            {
                MSG ("Expecting reports for {0} remaining groups...", UINT($supportedGroupings - $receivedGroupCount));
            }
        }
    }
    ELSE
    {
        MSGFAIL ("Number of received group infos ({0}) exceeds number of supported groups ({1}).", UINT($groupCount), UINT($supportedGroupings));
    }

TESTSEQ END


/**
 * VerifyGroupCommandList
 * Verifies the Association Group Command List
 *
 * CC versions: 1
 */

TESTSEQ VerifyGroupCommandList: "Verify Association Group Command List"

    SEND Association.GroupingsGet();
    EXPECT Association.GroupingsReport($supportedGroupings = SupportedGroupings);

    LOOP ($grp; 1; $supportedGroupings)
    {
        SEND AssociationGrpInfo.AssociationGroupCommandListGet(
            Reserved = 0,
            AllowCache = 0,
            GroupingIdentifier = $grp);

        EXPECT AssociationGrpInfo.AssociationGroupCommandListReport(
            GroupingIdentifier == $grp,
            $listLength = ListLength in (0 ... 255),
            $commands = Command);

        $numberOfCommands = $listLength / 2; // We do not support extended Command Classes yet

        LOOP ($n; 0; $numberOfCommands - 1)
        {
            $commandClass = $commands[$n * 2];
            $command = $commands[($n * 2) + 1];

            IF ($commandClass == 0x71)
            {
                IF     ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - EVENT_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - EVENT_SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_NOTIFICATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x22)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLICATION_STATUS - BUSY command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLICATION_STATUS - REJECTED_REQUEST command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_APPLICATION_STATUS - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x9B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_COMMAND_CONFIGURATION - COMMAND_RECORDS_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_COMMAND_CONFIGURATION - COMMAND_RECORDS_SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_COMMAND_CONFIGURATION - COMMAND_CONFIGURATION_SET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_COMMAND_CONFIGURATION - COMMAND_CONFIGURATION_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_COMMAND_CONFIGURATION - COMMAND_CONFIGURATION_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_COMMAND_CONFIGURATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x85)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - REMOVE command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - GROUPINGS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - GROUPINGS_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x95)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_CONTENT_BROWSE_MD_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_CONTENT_BROWSE_MD_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_CONTENT_BROWSE_MD_BY_LETTER_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_CONTENT_BROWSE_MD_BY_LETTER_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_CONTENT_BROWSE_MD_CHILD_COUNT_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_CONTENT_BROWSE_MD_CHILD_COUNT_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_MATCH_ITEM_TO_RENDERER_MD_GET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - AV_MATCH_ITEM_TO_RENDERER_MD_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_DIRECTORY_MD - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x97)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_SEARCH_MD - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_SEARCH_MD - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_AV_CONTENT_SEARCH_MD - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x96)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_RENDERER_STATUS - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_RENDERER_STATUS - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_AV_RENDERER_STATUS - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x99)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_TAGGING_MD - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_AV_TAGGING_MD - REPORT command", UINT($grp));}
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_AV_TAGGING_MD - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x36)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC_TARIFF_INFO - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC_TARIFF_INFO - GET command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC_TARIFF_INFO - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x50)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC_WINDOW_COVERING - START_LEVEL_CHANGE command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC_WINDOW_COVERING - STOP_LEVEL_CHANGE command", UINT($grp));}
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC_WINDOW_COVERING - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x20)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC - SET command", UINT($grp));}
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_BASIC - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x80)
            {
                IF     ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_BATTERY - GET command", UINT($grp));}
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_BATTERY - REPORT command", UINT($grp));}
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_BATTERY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x2A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_CHIMNEY_FAN - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_CHIMNEY_FAN - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_CHIMNEY_FAN - REPORT command", UINT($grp));}
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CHIMNEY_FAN - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x46)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - CHANGED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - CHANGED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - OVERRIDE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - OVERRIDE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - OVERRIDE_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CLIMATE_CONTROL_SCHEDULE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x81)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_CLOCK - SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_CLOCK - GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_CLOCK - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CLOCK - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x70)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - BULK_SET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - BULK_GET command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - BULK_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CONFIGURATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x21)
            {
                IF     ($command == 0x31) { MSG ("Association Group {0} can send COMMAND_CLASS_CONTROLLER_REPLICATION - TRANSFER_GROUP command", UINT($grp)); }
                ELSEIF ($command == 0x32) { MSG ("Association Group {0} can send COMMAND_CLASS_CONTROLLER_REPLICATION - TRANSFER_GROUP_NAME command", UINT($grp)); }
                ELSEIF ($command == 0x33) { MSG ("Association Group {0} can send COMMAND_CLASS_CONTROLLER_REPLICATION - TRANSFER_SCENE command", UINT($grp)); }
                ELSEIF ($command == 0x34) { MSG ("Association Group {0} can send COMMAND_CLASS_CONTROLLER_REPLICATION - TRANSFER_SCENE_NAME command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CONTROLLER_REPLICATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x56)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_CRC_16 - ENCAP command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CRC_16 - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x3A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_CONFIG - DCP_LIST_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_CONFIG - DCP_LIST_SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_CONFIG - DCP_LIST_SET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_CONFIG - DCP_LIST_REMOVE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_CONFIG - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x3B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_MONITOR - DCP_LIST_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_MONITOR - DCP_LIST_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_MONITOR - DCP_EVENT_STATUS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_MONITOR - DCP_EVENT_STATUS_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_DCP_MONITOR - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x4C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK_LOGGING - RECORDS_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK_LOGGING - RECORDS_SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK_LOGGING - RECORD_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK_LOGGING - RECORD_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK_LOGGING - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x62)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - OPERATION_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - OPERATION_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - OPERATION_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - CONFIGURATION_SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - CONFIGURATION_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - CONFIGURATION_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_DOOR_LOCK - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x90)
            {
                IF     ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ENERGY_PRODUCTION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ENERGY_PRODUCTION - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ENERGY_PRODUCTION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x7A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - UPDATE_MD_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - UPDATE_MD_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - UPDATE_MD_REQUEST_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - UPDATE_MD_REQUEST_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - UPDATE_MD_STATUS_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_FIRMWARE_UPDATE_MD - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x8C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_GEOGRAPHIC_LOCATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_GEOGRAPHIC_LOCATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_GEOGRAPHIC_LOCATION - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_GEOGRAPHIC_LOCATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x7B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_GROUPING_NAME - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_GROUPING_NAME - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_GROUPING_NAME - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_GROUPING_NAME - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x82)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_HAIL - HAIL command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_HAIL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x39)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - MODE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - MODE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - MODE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - BYPASS_SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - BYPASS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - BYPASS_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - VENTILATION_RATE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - VENTILATION_RATE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - VENTILATION_RATE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x0A) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - MODE_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x0B) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - MODE_SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_CONTROL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x37)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_STATUS - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_STATUS - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_STATUS - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_STATUS - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_HRV_STATUS - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x87)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_INDICATOR - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_INDICATOR - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_INDICATOR - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_INDICATOR - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x9A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_CONFIGURATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_CONFIGURATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_CONFIGURATION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_CONFIGURATION - RELEASE command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_CONFIGURATION - RENEW command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_IP_CONFIGURATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x6B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_INFO_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_INFO_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_STATUS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_STATUS_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_CONFIG_SET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_CONFIG_GET command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_CONFIG_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_INFO_GET command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_INFO_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x0A) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_CONFIG_SET command", UINT($grp)); }
                ELSEIF ($command == 0x0B) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_CONFIG_GET command", UINT($grp)); }
                ELSEIF ($command == 0x0C) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_CONFIG_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x0D) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_RUN command", UINT($grp)); }
                ELSEIF ($command == 0x0E) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_TABLE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x0F) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_TABLE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x10) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_TABLE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x11) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - VALVE_TABLE_RUN command", UINT($grp)); }
                ELSEIF ($command == 0x12) { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - SYSTEM_SHUTOFF command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_IRRIGATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x89)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_LANGUAGE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_LANGUAGE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_LANGUAGE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_LANGUAGE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x76)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_LOCK - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_LOCK - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_LOCK - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_LOCK - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x91)
            {
                MSG ("Association Group {0} can send COMMAND_CLASS_MANUFACTURER_PROPRIETARY - Unknown Command {1}", UINT($grp), $command);
            }
            ELSEIF ($commandClass == 0x72)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_MANUFACTURER_SPECIFIC - GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_MANUFACTURER_SPECIFIC - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_MANUFACTURER_SPECIFIC - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0xEF)
            {
                MSG ("Association Group {0} can send COMMAND_CLASS_MARK - Unknown Command {1}", UINT($grp), $command);
            }
            ELSEIF ($commandClass == 0x35)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_PULSE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_PULSE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_METER_PULSE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x3C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_CONFIG - PORINT_ADM_NO_SET command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_CONFIG - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x3D)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_MONITOR - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_MONITOR - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_MONITOR - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_MONITOR - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x3E)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_PUSH - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_PUSH - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_PUSH - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_METER_TBL_PUSH - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x32)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_METER - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_METER - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_METER - RESET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_METER - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_METER - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_METER - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x51)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_MTP_WINDOW_COVERING - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_MTP_WINDOW_COVERING - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_MTP_WINDOW_COVERING - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_MTP_WINDOW_COVERING - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x8E)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - REMOVE command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - GROUPINGS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - GROUPINGS_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x60)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - CMD_ENCAP command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - ENDPOINT_GET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - ENDPOINT_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - CAPABILITY_GET command", UINT($grp)); }
                ELSEIF ($command == 0x0A) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - CAPABILITY_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x0B) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - ENDPOINT_FIND command", UINT($grp)); }
                ELSEIF ($command == 0x0C) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - ENDPOINT_FIND_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x0D) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - CMD_ENCAP_V2 command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CHANNEL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x8F)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CMD - CMD_ENCAP command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_MULTI_CMD - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x52)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_NETWORK_MANAGEMENT_PROXY - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_NETWORK_MANAGEMENT_PROXY - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_NETWORK_MANAGEMENT_PROXY - CACHED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_NETWORK_MANAGEMENT_PROXY - CACHED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_NETWORK_MANAGEMENT_PROXY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x00)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_NO_OPERATION - NOP command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_NO_OPERATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x77)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - NAME_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - NAME_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - NAME_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - LOCATION_SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - LOCATION_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - LOCATION_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_NODE_NAMING - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0xF0)
            {
                MSG ("Association Group {0} can send COMMAND_CLASS_NON_INTEROPERABLE - Unknown Command {1}", UINT($grp), $command);
            }
            ELSEIF ($commandClass == 0x73)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - TEST_NODE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - TEST_NODE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - TEST_NODE_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_POWERLEVEL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x41)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT_ENCAPSULATION - CMD_ENCAP command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT_ENCAPSULATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x3F)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT - BALANCE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT - BALANCE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_PREPAYMENT - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x88)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_PROPRIETARY - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_PROPRIETARY - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_PROPRIETARY - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_PROPRIETARY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x75)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_PROTECTION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_PROTECTION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_PROTECTION - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_PROTECTION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x48)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_CONFIG - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_CONFIG - REMOVE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_CONFIG - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x49)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_MONITOR - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_MONITOR - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_MONITOR - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_RATE_TBL_MONITOR - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x7C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_REMOTE_ASSOCIATION_ACTIVATE - ACTIVATE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_REMOTE_ASSOCIATION_ACTIVATE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x7D)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_REMOTE_ASSOCIATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_REMOTE_ASSOCIATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_REMOTE_ASSOCIATION - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_REMOTE_ASSOCIATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x2B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_ACTIVATION - SET command", UINT($grp)); }
                ELSE { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_ACTIVATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x2C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_ACTUATOR_CONF - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_ACTUATOR_CONF - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_ACTUATOR_CONF - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_ACTUATOR_CONF - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x2D)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_CONTROLLER_CONF - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_CONTROLLER_CONF - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_CONTROLLER_CONF - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SCENE_CONTROLLER_CONF - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x4E)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - ENABLE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - ENABLE_ALL_SET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - WEEK_DAY_SET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - WEEK_DAY_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - WEEK_DAY_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - YEAR_DAY_SET command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - YEAR_DAY_GET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - YEAR_DAY_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x0A) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE_ENTRY_LOCK - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x93)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCREEN_ATTRIBUTES - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SCREEN_ATTRIBUTES - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SCREEN_ATTRIBUTES - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x92)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCREEN_MD - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SCREEN_MD - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SCREEN_MD - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x24)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_MODE - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_MODE - SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_MODE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_MODE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_MODE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_MODE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x2F)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - INSTALLED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - INSTALLED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - TYPE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - TYPE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - STATE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - STATE_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE_SENSOR - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x2E)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - NUMBER_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - TYPE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - TYPE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - STATE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - STATE_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY_PANEL_ZONE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x98)
            {
                IF     ($command == 0x81) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY - MESSAGE_ENCAPSULATION command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY - MESSAGE_ENCAPSULATION_NOUNCE_GET command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SECURITY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x9C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_ALARM - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_ALARM - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_ALARM - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_ALARM - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_ALARM - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x30)
            {
                IF     ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_BINARY - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_BINARY - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_BINARY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x9E)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_CONFIGURATION - TRIGGER_LEVEL_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_CONFIGURATION - TRIGGER_LEVEL_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_CONFIGURATION - TRIGGER_LEVEL_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_CONFIGURATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x31)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_MULTILEVEL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_MULTILEVEL - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SENSOR_MULTILEVEL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x9D)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SILENCE_ALARM - SET command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SILENCE_ALARM - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x94)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SIMPLE_AV_CONTROL - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SIMPLE_AV_CONTROL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SIMPLE_AV_CONTROL - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SIMPLE_AV_CONTROL - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SIMPLE_AV_CONTROL - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SIMPLE_AV_CONTROL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x27)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_ALL - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_ALL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_ALL - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_ALL - ON command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_ALL - OFF command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_ALL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x25)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_BINARY - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_BINARY - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_BINARY - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_BINARY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x26)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_MULTILEVEL - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_MULTILEVEL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_MULTILEVEL - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_MULTILEVEL - START_LEVEL_CHANGE command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_MULTILEVEL - STOP_LEVEL_CHANGE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_MULTILEVEL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x28)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_BINARY - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_BINARY - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_BINARY - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_BINARY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x29)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_MULTILEVEL - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_MULTILEVEL - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_MULTILEVEL - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SWITCH_TOGGLE_MULTILEVEL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x4A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_CONFIG - SUPPLIER_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_CONFIG - SET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_CONFIG - REMOVE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_CONFIG - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x4B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - SUPPLIER_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - SUPPLIER_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - COST_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - COST_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_TARIFF_TBL_MONITOR - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x44)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_MODE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_MODE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_MODE - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_MODE - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_MODE - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_MODE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x45)
            {
                IF     ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_STATE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_STATE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_FAN_STATE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x38)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - MODE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - MODE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - MODE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - SETPOINT_SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - SETPOINT_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - SETPOINT_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - RELAY_STATUS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x0A) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - RELAY_STATUS_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x0B) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - STATUS_SET command", UINT($grp)); }
                ELSEIF ($command == 0x0C) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - STATUS_GEt command", UINT($grp)); }
                ELSEIF ($command == 0x0D) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - STATUS_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_HEATING - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x40)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_MODE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_MODE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_MODE - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_MODE - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_MODE - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_MODE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x42)
            {
                IF     ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_OPERATING_STATE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_OPERATING_STATE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_OPERATING_STATE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x47)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETBACK - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETBACK - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETBACK - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETBACK - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x43)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETPOINT - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETPOINT - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETPOINT - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETPOINT - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETPOINT - SUPPORTED_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_THERMOSTAT_SETPOINT - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x8B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME_PARAMETERS - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME_PARAMETERS - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME_PARAMETERS - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_TIME_PARAMETERS - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x8A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME - TIME_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME - TIME_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME - DATE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_TIME - DATE_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_TIME - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x55)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_TRANSPORT_SERVICE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_TRANSPORT_SERVICE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_TRANSPORT_SERVICE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_TRANSPORT_SERVICE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x63)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_USER_CODE - CODE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_USER_CODE - CODE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_USER_CODE - CODE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_USER_CODE - NUMBER_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_USER_CODE - NUMBER_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_USER_CODE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x86)
            {
                IF     ($command == 0x11) { MSG ("Association Group {0} can send COMMAND_CLASS_VERSION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x12) { MSG ("Association Group {0} can send COMMAND_CLASS_VERSION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x13) { MSG ("Association Group {0} can send COMMAND_CLASS_VERSION - COMMAND_CLASS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x14) { MSG ("Association Group {0} can send COMMAND_CLASS_VERSION - COMMAND_CLASS_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_VERSION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x84)
            {
                IF     ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_WAKE_UP - SET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_WAKE_UP - GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_WAKE_UP - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_WAKE_UP - NOTIFICATION command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_WAKE_UP - NO_MORE_INFORMATION command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_WAKE_UP - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x4F)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x23)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x57)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLICATION_CAPABILITY - NOT_SUPPORTED command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_APPLICATION_CAPABILITY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x33)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - CAPABILITY_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - CAPABILITY_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - STATE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - STATE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - STATE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - START_CAPABILITY_LEVEL_CHANGE command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - STOP_STATE_CHANGE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_COLOR_CONTROL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x53)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - COMMAND_SCHEDULE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - COMMAND_SCHEDULE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - COMMAND_SCHEDULE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - SCHEDULE_REMOVE command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - SCHEDULE_STATE_SET command", UINT($grp)); }
                ELSEIF ($command == 0x08) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - SCHEDULE_STATE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x09) { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - SCHEDULE_STATE_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_SCHEDULE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x58)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ND - ZIP_NODE_ADVERTISEMENT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ND - ZIP_NODE_SOLICITATION command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ND - ZIP_INV_NODE_SOLICITATION command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_ND - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x59)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - GROUP_NAME_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - GROUP_NAME_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - GROUP_INFO_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - GROUP_INFO_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - GROUP_COMMAND_LIST_GET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - GROUP_COMMAND_LIST_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ASSOCIATION_GRP_INFO - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x5A)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_DEVICE_RESET_LOCALLY - NOTIFICATION command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_DEVICE_RESET_LOCALLY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x5B)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_CENTRAL_SCENE - SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_CENTRAL_SCENE - SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_CENTRAL_SCENE - NOTIFICATION command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_CENTRAL_SCENE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x5C)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_ASSOCIATION - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_ASSOCIATION - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_ASSOCIATION - REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_IP_ASSOCIATION - REMOVE command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_IP_ASSOCIATION - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x5D)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ANTITHEFT - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ANTITHEFT - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ANTITHEFT - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ANTITHEFT - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x5E)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ZWAVEPLUS_INFO - GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ZWAVEPLUS_INFO - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ZWAVEPLUS_INFO - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x5F)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_GATEWAY - SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_GATEWAY - GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_GATEWAY - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_GATEWAY - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x61)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_PORTAL - GATEWAY_CONFIGURATION_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_PORTAL - GATEWAY_CONFIGURATION_STATUS command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_PORTAL - GATEWAY_CONFIGURATION_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_PORTAL - GATEWAY_CONFIGURATION_REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_ZIP_PORTAL - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x64)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - TYPE_GET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - TYPE_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - PROGRAM_SUPPORTED_GET command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - PROGRAM_SUPPORTED_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - SET command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - GET command", UINT($grp)); }
                ELSEIF ($command == 0x07) { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - REPORT command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_APPLIANCE - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSEIF ($commandClass == 0x65)
            {
                IF     ($command == 0x01) { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - ADDRESS_SET command", UINT($grp)); }
                ELSEIF ($command == 0x02) { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - ADDRESS_GET command", UINT($grp)); }
                ELSEIF ($command == 0x03) { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - ADDRESS_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x04) { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - CAPABILITY_GET command", UINT($grp)); }
                ELSEIF ($command == 0x05) { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - CAPABILITY_REPORT command", UINT($grp)); }
                ELSEIF ($command == 0x06) { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - DATA_40 command", UINT($grp)); }
                ELSE                      { MSG ("Association Group {0} can send COMMAND_CLASS_DMX - Unknown Command {1}", UINT($grp), $command); }
            }
            ELSE
            {
                MSG ("Association Group {0} can send Unknown Command Class 0x{1:X2} - Unknown Command 0x{2:X2}", UINT($grp), $commandClass, $command);
            }
        }
    }

TESTSEQ END
