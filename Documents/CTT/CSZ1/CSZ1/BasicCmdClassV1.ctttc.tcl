﻿PACKAGE BasicCmdClassV1; // do not modify this line
USE Basic CMDCLASSVER = 1;

/**
 * Basic Command Class Version 1 Test Script
 * Last Update: March 15, 2012
 */

TESTSEQ AdjustAnnouncement: "Basic Command Class adjust announcement"
MSGFAIL ("Please note: In order to run the Basic Test script uncomment the Test Sequence according to the Generic Device Class of the DUT.");
MSGFAIL ("The mappings of the Basic Command Class for Generic/Specific Device Classes not mentioned in this test script require manual testing.");
MSGFAIL ("Please deactivate this Test Sequence in Test Case Selection Tree (Project->Setup Test Cases) when this test script has been adjusted.");
TESTSEQ END

/*
TESTSEQ BinarySensorGenDevClass: "Binary Sensor Generic Device Class"
SEND Basic.Get( );
EXPECT Basic.Report($value = Value);

IF ($value == 0x00)
{
	MSGPASS ("Value: {0:X} - Idle State", $value);
}
ELSEIF ($value == 0xFF)
{
	MSGPASS ("Value: {0:X} - Event detected", $value);
}
ELSE
{
	MSGFAIL ("Value: {0:X} - Only 0x00 or 0xFF allowed", $value);
}
TESTSEQ END
*/
/*
TESTSEQ BinarySwitchGenDevClass: "Binary Switch Generic Device Class"
MSG ("Check initial values");
SEND Basic.Get( );
EXPECT Basic.Report($value = Value in (0x00, 0xFF));

IF ($value == 0x00)
{	
	MSGPASS ("Value: {0:X} - Off/Diabled", $value);
}
ELSEIF ($value == 0xFF)
{
	MSGPASS ("Value: {0:X} - On/Enabled", $value);
}
ELSE
{
	MSGFAIL ("Value: {0:X} - Only 0x00 or 0xFF allowed", $value);
}

MSG ("Turn on and off");
SEND Basic.Set(Value = 0x00);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

SEND Basic.Set(Value = 0xFF);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0xFF);

SEND Basic.Set(Value = 0x00);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

MSG ("Try intermediate values and invalid values");
SEND Basic.Set(Value = 0x01 );
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0xFF);

SEND Basic.Set(Value = 0x00);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

SEND Basic.Set(Value = 0x63 );
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0xFF);

SEND Basic.Set(Value = 0x00 );
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

MSG ("Check the device ignores reserved values");
SEND Basic.Get( );
EXPECT Basic.Report($value = Value);

SEND Basic.Set(Value = 0x64);
SEND Basic.Get( );
EXPECT Basic.Report(Value == $value);

SEND Basic.Set(Value = 0xFE);
SEND Basic.Get( );
EXPECT Basic.Report(Value == $value);
TESTSEQ END
*/

TESTSEQ MultilevelSwitchGenDevClass : "Multilevel Switch Generic Device Class"
MSG ("Check initial values");
SEND Basic.Get( );
EXPECT Basic.Report(Value in (0x00...0x63, 0xFF));

MSG ("Set the end levels in a device");
SEND Basic.Set(Value = 0x63);
WAIT (20000);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x63);

SEND Basic.Set(Value = 0x00);
WAIT (20000);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

// ER added this to check for roundoff errors in the percentage calculations
MSG ("set a few moves and make sure we hit it within +/- 1%");
SEND Basic.Set(Value = 10);
WAIT (2000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (9 ... 11));
SEND Basic.Set(Value = 23);
WAIT (4000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (22 ... 24));
SEND Basic.Set(Value = 47);
WAIT (6000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (46 ... 48));
SEND Basic.Set(Value = 55);
WAIT (3000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (54 ... 56));
SEND Basic.Set(Value = 81);
WAIT (6000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (80 ... 82));
SEND Basic.Set(Value = 92);
WAIT (3000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (91 ... 93));
SEND Basic.Set(Value = 81);
WAIT (3000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (80 ... 82));
SEND Basic.Set(Value = 91);
WAIT (3000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (90 ... 92));
SEND Basic.Set(Value = 84);
WAIT (3000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (83 ... 85));
SEND Basic.Set(Value = 87);
WAIT (2000);
SEND Basic.Get( );
EXPECT Basic.Report(Value in (86 ... 88));

/* FF goes to the pre-programmed HOME position so this part of the test is not valid and thus commented out
MSG ("Check the DUT resumes to last active level on receiving a 0xFF");
SEND Basic.Set(Value = 0x50);
WAIT (6000);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x50);

SEND Basic.Set(Value = 0x00);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

SEND Basic.Set(Value = 0xFF);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x50);
*/
MSG ("Check the DUT ignores reserved values");
SEND Basic.Set(Value = 0x00);
WAIT (20000);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

SEND Basic.Set(Value = 0x64);
WAIT (2000);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);

SEND Basic.Set(Value = 0xFE);
WAIT (1000);
SEND Basic.Get( );
EXPECT Basic.Report(Value == 0x00);
TESTSEQ END


/*
TESTSEQ ThermostatGeneralV2SpeDevClass  : "Thermostat General V2 Specific Device Class"
MSG ("Check initial values");
SEND Basic.Get();
EXPECT Basic.Report(Value in (0, 0xFF));

MSG ("Check energy saving and comfort mode");
SEND Basic.Set (Value = 0x00);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0x00);
SEND Basic.Set (Value = 0xFF);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);
SEND Basic.Set (Value = 0x00);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0x00);

MSG ("Check invalid values are ignored");
SEND Basic.Set (Value = 0xFF);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);
SEND Basic.Set (Value = 0x23);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);
TESTSEQ END
*/

/*
TESTSEQ SetbackThermostatSpeDevClass : "Setback Thermostat Specific Device Class"
MSG ("Check initial values");
SEND Basic.Get();
EXPECT Basic.Report(Value in (0, 0xFF));

MSG ("Check energy saving and comfort mode");
SEND Basic.Set (Value = 0x00);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0x00);
SEND Basic.Set (Value = 0xFF);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);
SEND Basic.Set (Value = 0x00);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0x00);

MSG ("Check invalid values are ignored");
SEND Basic.Set (Value = 0xFF);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);
SEND Basic.Set (Value = 0x23);
WAIT (4000);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);
TESTSEQ END
*/

/*
TESTSEQ SetbackScheduleThermostatSpeDevClass : "Setback Schedule Thermostat Specific Device Class"
$temporaryOverride = 0x01;
$energySavingMode = 0x7A;

MSG ("Check initial values");
SEND Basic.Get();
EXPECT Basic.Report($value = Value in (0, 0xFF));

SEND ClimateControlSchedule.ScheduleOverrideGet();
IF ($value == 0x00)
{
	EXPECT ClimateControlSchedule.ScheduleOverrideReport(
		OverrideType  == 0x01,
        Reserved 	  == 0,
        OverrideState == 0x00
    );
}
ELSEIF ($value == 0xFF)
{
	EXPECT ClimateControlSchedule.ScheduleOverrideReport(
		OverrideType  == 0x01,
        Reserved 	  == 0,
        OverrideState == 0x7A
    );
}
ELSE
{
	MSGFAIL ("Only Values 0x00 or 0xFF allowed");
}

MSG ("Basic Set/Get test");
SEND Basic.Set(Value = 0x00);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0x00);

SEND ClimateControlSchedule.ScheduleOverrideGet();
EXPECT ClimateControlSchedule.ScheduleOverrideReport(
	OverrideType  == $temporaryOverride,
    Reserved 	  == 0,
    OverrideState == 0x00
);

SEND Basic.Set(Value = 0xFF);
SEND Basic.Get();
EXPECT Basic.Report(Value == 0xFF);

SEND ClimateControlSchedule.ScheduleOverrideGet();
EXPECT ClimateControlSchedule.ScheduleOverrideReport(
	OverrideType  == $temporaryOverride,
    Reserved 	  == 0,
    OverrideState == $energySavingMode
);
TESTSEQ END
*/