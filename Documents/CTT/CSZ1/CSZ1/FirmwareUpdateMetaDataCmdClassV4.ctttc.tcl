﻿PACKAGE FirmwareUpdateMetaDataCmdClassV4; // do not modify this line
USE FirmwareUpdateMd CMDCLASSVER = 4;

/**
 * Firmware Update Meta Data Command Class Version 4 Test Script
 * Last Update: February 24th, 2017
 * Command Class Specification: SDS13782-1
 * Formatting Conventions: Version 2016-05-19
 *
 * PLEASE NOTE: This script cannot test the update process itself and the commands
 *              New Firmware Update Activation Set/Status Command.
 *              Testers MUST perform additional tests with the Z-Wave PC Controller
 *              program and DUT-related firmware files.
 *
 * ChangeLog:
 *
 * November 2nd, 2016   - Initial release, derived from V3
 * February 1st, 2017   - Review improvements, Manufacturer list removed
 * February 24th, 2017  - New check with valid firmware fragments and checksums
 *
 */


/**
 * GetCurrentFirmwareData
 * Report the meta data of the current firmware
 *
 * CC versions: 3, 4
 */

TESTSEQ GetCurrentFirmwareData: "Report current firmware meta data"

    SEND FirmwareUpdateMd.FirmwareMdGet( );
    EXPECT FirmwareUpdateMd.FirmwareMdReport(
        $manufacturerId = ManufacturerId in (0x0000 ... 0xFFFF),
        $firmware0Id = Firmware0Id in (0x0000 ... 0xFFFF),
        $firmware0Checksum = Firmware0Checksum in (0x0000 ... 0xFFFF),
        $firmwareUpgradable = FirmwareUpgradable in (0x00, 0xFF),
        $numberOfFirmwareTargets = NumberOfFirmwareTargets in (0x00 ... 0xFF),
        $maxFragmentSize = MaxFragmentSize in (0x0000 ... 0xFFFF),
        $firmwareIds = vg1);
    MSG ("Firmware IDs = {0}", $firmwareIds);

    IF (ISNULL($manufacturerId)) { MSGFAIL ("Report missing"); }
    ELSE
    {
        MSG ("Manufacturer ID:      0x{0:X4}", UINT($manufacturerId));
        MSG ("Firmware 0 ID:        0x{0:X4}", UINT($firmware0Id));
        MSG ("Firmware 0 Checksum:  0x{0:X4}", UINT($firmware0Checksum));
        IF     ($firmwareUpgradable == 0xFF) { MSG     ("Firmware Upgradable:  0x{0:X2} (yes)", $firmwareUpgradable); }
        ELSEIF ($firmwareUpgradable == 0x00) { MSG     ("Firmware Upgradable:  0x{0:X2} (no)", $firmwareUpgradable); }
        ELSE                                 { MSGFAIL ("Firmware Upgradable:  0x{0:X2} (invalid, must be 0xFF or 0x00)", $firmwareUpgradable); }
        MSG ("Number of Fw Targets: 0x{0:X2} = {0}", UINT($numberOfFirmwareTargets));
        MSG ("Max Fragment Size:    0x{0:X4} = {0}", UINT($maxFragmentSize));
        IF ($numberOfFirmwareTargets > 0)
        {
            LOOP ($n; 0; $numberOfFirmwareTargets - 1)
            {
                $firmwareNId = UINT($firmwareIds[2 * $n])* 256 + UINT($firmwareIds[(2 * $n) + 1]);
                MSG ("Firmware {0} ID:        0x{1:X4}", $n + 1, UINT($firmwareNId));
                IF (UINT($firmware0Id) == UINT($firmwareNId))
                {
                    MSG ("Firmware {0} ID (0x{1:X4}) == Firmware 0 ID (0x{2:X4})", $n + 1, UINT($firmwareNId), UINT($firmware0Id));
                }
            }
        }
    }

TESTSEQ END


/**
 * InvalidID
 * Checks if the DUT sends a RequestReport with Status 0x00 (Invalid IDs), if invalid Manufacturer ID and/or
 * Firmware ID is provided:
 * The Firmware Update Meta Data Request Report Command MUST be returned in response to the Get command.
 * The DUT should return the status code 0x00, but we tolerate any ERROR status in that case, as long as it does not return 0xFF OK.
 *
 * CC versions: 4
 */

TESTSEQ InvalidID: "Check for correct behavior if invalid IDs are provided"

    $recoveryTime = 5;                 // Recovery Time in seconds. Some devices need a recovery time after this test. You MAY change this value.

    SEND FirmwareUpdateMd.FirmwareMdGet( );
    EXPECT FirmwareUpdateMd.FirmwareMdReport(
        $manufacturerId = ManufacturerId in (0x0000 ... 0xFFFF),
        $firmware0Id = Firmware0Id in (0x0000 ... 0xFFFF),
        $firmware0Checksum = Firmware0Checksum in (0x0000 ... 0xFFFF),
        $firmwareUpgradable = FirmwareUpgradable in (0x00, 0xFF),
        $numberOfFirmwareTargets = NumberOfFirmwareTargets in (0x00 ... 0xFF),
        $maxFragmentSize = MaxFragmentSize in (0x0000 ... 0xFFFF),
        $firmwareIds = vg1);
    MSG ("Firmware IDs = {0}", $firmwareIds);

    IF     ($numberOfFirmwareTargets == 0) { $firmwareTargets = [0x00]; }
    ELSEIF ($numberOfFirmwareTargets == 1) { $firmwareTargets = [0x00, 0x01]; }
    ELSE                                   { $firmwareTargets = [0x00, 0x01, $numberOfFirmwareTargets]; }

    LOOP ($target; 0; LENGTH($firmwareTargets) - 1)
    {
        $firmwareTarget = $firmwareTargets[$target];
        IF (($target == 0) && ($firmwareUpgradable == 0x00))
        {
            MSG ("Test is not applicable for Firmware Target 0.");
        }
        ELSE
        {
            IF ($target == 0) { $testFirmwareId = $firmware0Id; }
            ELSE              { $testFirmwareId = $firmwareIds[($firmwareTargets[$target] - 1) * 2] * 256 + $firmwareIds[(($firmwareTargets[$target] - 1) * 2) + 1]; }

            $testManufIds = [$manufacturerId + 1, $manufacturerId, $manufacturerId + 1];
            $testFirmwIds = [$testFirmwareId + 1, $testFirmwareId + 1, $testFirmwareId];
            LOOP ($i; 0; 2)
            {
                IF     ($i == 2) { MSG ("Send RequestGet with invalid Manufacturer ID to target 0x{0:X2}...", $firmwareTarget); }
                ELSEIF ($i == 1) { MSG ("Send RequestGet with invalid Firmware ID to target 0x{0:X2}...", $firmwareTarget); }
                ELSE             { MSG ("Send RequestGet with invalid Manufacturer ID and Firmware ID to target 0x{0:X2}...", $firmwareTarget); }

                SEND FirmwareUpdateMd.RequestGet(
                    ManufacturerId = CONV($testManufIds[$i], 2),
                    FirmwareId = CONV($testFirmwIds[$i], 2),
                    Checksum = CONV(0x0001, 2),
                    FirmwareTarget = $firmwareTarget,
                    FragmentSize = CONV($maxFragmentSize, 2),
                    Activation = 0,
                    Reserved = 0xFE);
                EXPECT FirmwareUpdateMd.RequestReport($status = Status in (0x00, 0x03));

                IF (ISNULL($status))     { MSGFAIL ("Status Report missing (both IDs invalid)"); }
                ELSEIF ($status == 0xFF) { MSGFAIL ("Status 0xFF: Firmware update is initiated: Waiting 150 seconds for timeout...");
                                           WAIT (150000); }
                ELSEIF ($status == 0x00) { MSGPASS ("Status 0x00: DUT Manufacturer ID and/or Firmware ID does not match the RequestGet Command"); }
                ELSEIF ($status == 0x01) { MSGFAIL ("Status 0x01: Missing expected authentication event."); }
                ELSEIF ($status == 0x02) { MSGFAIL ("Status 0x02: Fragment size exceeds Max Fragment Size."); }
                ELSEIF ($status == 0x03) { MSGPASS ("Status 0x03: Firmware target is not upgradable."); }
                ELSE                     { MSGFAIL ("Invalid Status 0x{0:X2}", $status); }

                WAIT ($recoveryTime * 1000);    // Some devices need a recovery time after this test
            } // LOOP ($i)
        } // Firmware target is upgradable
    } // LOOP ($target)

TESTSEQ END


/**
 * InvalidFragmentSize
 * Checks if the DUT sends a RequestReport with Status 0x02, if invalid Fragment Size is provided
 *
 * CC versions: 4
 */

TESTSEQ InvalidFragmentSize: "Check for correct behavior if invalid Fragment Size is provided"

    $timeoutStatusReport = 150;        // Timeout for StatusReport in seconds, if update process was initiated
    $recoveryTime = 5;                 // Recovery Time in seconds. Some devices need a recovery time after this test. You MAY change this value.

    SEND FirmwareUpdateMd.FirmwareMdGet( );
    EXPECT FirmwareUpdateMd.FirmwareMdReport(
        $manufacturerId = ManufacturerId in (0x0000 ... 0xFFFF),
        $firmware0Id = Firmware0Id in (0x0000 ... 0xFFFF),
        $firmware0Checksum = Firmware0Checksum in (0x0000 ... 0xFFFF),
        $firmwareUpgradable = FirmwareUpgradable in (0x00, 0xFF),
        $numberOfFirmwareTargets = NumberOfFirmwareTargets in (0x00 ... 0xFF),
        $maxFragmentSize = MaxFragmentSize in (0x0000 ... 0xFFFF),
        $firmwareIds = vg1);
    MSG ("Firmware IDs = {0}", $firmwareIds);

    IF     ($numberOfFirmwareTargets == 0) { $firmwareTargets = [0x00]; }
    ELSEIF ($numberOfFirmwareTargets == 1) { $firmwareTargets = [0x00, 0x01]; }
    ELSE                                   { $firmwareTargets = [0x00, 0x01, $numberOfFirmwareTargets]; }

    LOOP ($target; 0; LENGTH($firmwareTargets) - 1)
    {
        $firmwareTarget = $firmwareTargets[$target];
        IF (($target == 0) && ($firmwareUpgradable == 0x00))
        {
            MSG ("Test is not applicable for Firmware Target 0.");
        }
        ELSE
        {
            $invFragmentSize = [$maxFragmentSize + 1, 0xFFFF, 0];
            IF ($target == 0) { $testFirmwareId = $firmware0Id; }
            ELSE              { $testFirmwareId = $firmwareIds[($firmwareTargets[$target] - 1) * 2] * 256 + $firmwareIds[(($firmwareTargets[$target] - 1) * 2) + 1]; }

            LOOP ($i; 0; LENGTH($invFragmentSize) - 1)
            {
                $fragmentSize = $invFragmentSize[$i];
                MSG ("Send RequestGet with invalid Fragment Size {0} to target 0x{1:X2}...", UINT($fragmentSize), $firmwareTarget);
                SEND FirmwareUpdateMd.RequestGet(
                    ManufacturerId = CONV($manufacturerId, 2),
                    FirmwareId = CONV($testFirmwareId, 2),
                    Checksum = CONV(0x0001, 2),
                    FirmwareTarget = $firmwareTarget,
                    FragmentSize = CONV($fragmentSize, 2),
                    Activation = 0,
                    Reserved = 0xFE);
                EXPECT FirmwareUpdateMd.RequestReport($status = Status == 0x02);

                IF (ISNULL($status))     { MSGFAIL ("Status Report missing (both IDs invalid)"); }
                ELSEIF ($status == 0xFF) { MSGFAIL ("Status 0xFF: Firmware update is initiated with invalid Fragment Size {0}", $fragmentSize); }
                ELSEIF ($status == 0x00) { MSGFAIL ("Status 0x00: DUT Manufacturer ID and/or Firmware ID does not match the RequestGet Command"); }
                ELSEIF ($status == 0x01) { MSGFAIL ("Status 0x01: Missing expected authentication event."); }
                ELSEIF ($status == 0x02) { MSGPASS ("Status 0x02: Fragment size exceeds Max Fragment Size."); }
                ELSEIF ($status == 0x03) { MSGFAIL ("Status 0x03: Firmware target is not upgradable."); }
                ELSE                     { MSGFAIL ("Invalid Status 0x{0:X2}", $status); }
                // If the DUT starts the update process, it must be lead to it's timeout (150 sec) to perform further tests
                IF ($status == 0xFF)
                {
                    MSG ("Cancelling the initiated Update for target 0x{0:X2} and the whole test. Wait {1} seconds for the Status Report...", $firmwareTarget, UINT($timeoutStatusReport));
                    EXPECT FirmwareUpdateMd.StatusReport(
                        $timeoutStatusReport,
                        Status == 0x01,
                        $waitTime = Waittime in (0x0000 ... 0xFFFE));

                    IF (ISNULL($waitTime) || $waitTime == []) { MSGFAIL ("Status Report frame or Wait Time field missing"); }
                    ELSE
                    {
                        IF (UINT($waitTime) == 0) { MSG ("Device is ready after aborted Firmware Update"); }
                        ELSE
                        {
                            MSG ("Waiting the reported time: {0} seconds", UINT($waitTime));
                            WAIT (UINT($waitTime) * 1000);
                        }
                    }

                    $i = LENGTH($invFragmentSize);         // exiting the inner LOOP
                    $target = LENGTH($firmwareTargets);    // exiting the outer LOOP
                }

                WAIT ($recoveryTime * 1000);    // Some devices need a recovery time after this test
            } // LOOP ($i)
        } // Firmware target is upgradable
    } // LOOP ($target)

TESTSEQ END


/**
 * InvalidTarget0Upgrade
 * Checks if the DUT sends a RequestReport with Status 0x03, if target 0 is not upgradable
 *
 * CC versions: 4
 */

TESTSEQ InvalidTarget0Upgrade: "Check behavior if target 0 is not upgradable"

    $recoveryTime = 5;    // Recovery Time in seconds. Some devices need a recovery time after this test. You MAY change this value.

    SEND FirmwareUpdateMd.FirmwareMdGet( );
    EXPECT FirmwareUpdateMd.FirmwareMdReport(
        $manufacturerId = ManufacturerId in (0x0000 ... 0xFFFF),
        $firmware0Id = Firmware0Id in (0x0000 ... 0xFFFF),
        $firmware0Checksum = Firmware0Checksum in (0x0000 ... 0xFFFF),
        $firmwareUpgradable = FirmwareUpgradable in (0x00, 0xFF),
        $numberOfFirmwareTargets = NumberOfFirmwareTargets in (0x00 ... 0xFF),
        $maxFragmentSize = MaxFragmentSize in (0x0000 ... 0xFFFF),
        $firmwareIds = vg1);
    MSG ("Firmware IDs = {0}", $firmwareIds);

    IF ($firmwareUpgradable == 0x00)
    {
        MSG ("Try to upgrade a non-upgradable target 0...");
        SEND FirmwareUpdateMd.RequestGet(
            ManufacturerId = CONV($manufacturerId, 2),
            FirmwareId = CONV($firmware0Id, 2),
            Checksum = CONV(0x0001, 2),
            FirmwareTarget = 0,
            FragmentSize = CONV($maxFragmentSize, 2),
            Activation = 0,
            Reserved = 0xFE);
        EXPECT FirmwareUpdateMd.RequestReport($status = Status == 0x03);
        IF (ISNULL($status))     { MSGFAIL ("Status Report missing (both IDs invalid)"); }
        ELSEIF ($status == 0xFF) { MSGFAIL ("Status 0xFF: Firmware update is initiated"); }
        ELSEIF ($status == 0x00) { MSGFAIL ("Status 0x00: DUT Manufacturer ID and/or Firmware ID does not match the RequestGet Command"); }
        ELSEIF ($status == 0x01) { MSGFAIL ("Status 0x01: Missing expected authentication event."); }
        ELSEIF ($status == 0x02) { MSGFAIL ("Status 0x02: Fragment size exceeds Max Fragment Size."); }
        ELSEIF ($status == 0x03) { MSGPASS ("Status 0x03: Firmware target is not upgradable."); }
        ELSE                     { MSGFAIL ("Invalid Status 0x{0:X2}", $status); }

        // If the DUT starts the update process, it must be lead to it's timeout (150 sec) to perform further tests
        IF ($status == 0xFF)
        {
            MSG ("Cancelling the initiated update for target 0x{0:X2} and the whole test. Wait {1} seconds for the Status Report...", $firmwareTarget, UINT($timeoutStatusReport));
            EXPECT FirmwareUpdateMd.StatusReport(
                $timeoutStatusReport,
                Status == 0x01,
                $waitTime = Waittime in (0x0000 ... 0xFFFE));

            IF (ISNULL($waitTime) || $waitTime == []) { MSGFAIL ("Status Report frame or Wait Time field missing"); }
            ELSE
            {
                IF (UINT($waitTime) == 0) { MSG ("Device is ready after aborted Firmware Update"); }
                ELSE
                {
                    MSG ("Waiting the reported time: {0} seconds", UINT($waitTime));
                    WAIT (UINT($waitTime) * 1000);
                }
            }
        }

        WAIT ($recoveryTime * 1000);    // Some devices need a recovery after this test
    }
    ELSE
    {
        MSG ("Test is not applicable.");
    }

TESTSEQ END


/**
 * AbortedFirmwareUpdate
 * Checks if the DUT sends a StatusReport with Status 'Aborted', if transmission fails:
 * A device receiving data SHOULD stop retransmitting Firmware Update Meta Data Get commands 2 minutes
 * after the last successful reception of a Firmware Update Meta Data Report Command.
 *
 * CC versions: 4
 */

TESTSEQ AbortedFirmwareUpdate: "Check for correct behavior if transmission fails"

    $timeoutGetCommand = 25;           // Timeout for Get command. You MAY increase this value.
    $timeoutStatusReport = 150;        // Timeout for StatusReport in seconds (see header comment).
    $firmwareChecksum = 0x0002;        // This is a checksum for RequestGet.
    $recoveryTime = 5;                 // Recovery Time in seconds. Some devices need a recovery time after this test. You MAY change this value.

    SEND FirmwareUpdateMd.FirmwareMdGet( );
    EXPECT FirmwareUpdateMd.FirmwareMdReport(
        $manufacturerId = ManufacturerId in (0x0000 ... 0xFFFF),
        $firmware0Id = Firmware0Id in (0x0000 ... 0xFFFF),
        $firmware0Checksum = Firmware0Checksum in (0x0000 ... 0xFFFF),
        $firmwareUpgradable = FirmwareUpgradable in (0x00, 0xFF),
        $numberOfFirmwareTargets = NumberOfFirmwareTargets in (0x00 ... 0xFF),
        $maxFragmentSize = MaxFragmentSize in (0x0000 ... 0xFFFF),
        $firmwareIds = vg1);
    MSG ("Firmware IDs = {0}", $firmwareIds);

    IF     ($numberOfFirmwareTargets == 0) { $firmwareTargets = [0x00]; }
    ELSEIF ($numberOfFirmwareTargets == 1) { $firmwareTargets = [0x00, 0x01]; }
    ELSE                                   { $firmwareTargets = [0x00, 0x01, $numberOfFirmwareTargets]; }

    LOOP ($target; 0; LENGTH($firmwareTargets) - 1)
    {
        $firmwareTarget = $firmwareTargets[$target];
        IF (($target == 0) && ($firmwareUpgradable == 0x00))
        {
            MSG ("Test is not applicable for Firmware Target 0.");
        }
        ELSE
        {
            IF ($target == 0) { $testFirmwareId = $firmware0Id; }
            ELSE              { $testFirmwareId = $firmwareIds[($firmwareTargets[$target] - 1) * 2] * 256 + $firmwareIds[(($firmwareTargets[$target] - 1) * 2) + 1]; }

            MSG ("Send RequestGet to target 0x{0:X2}...", $firmwareTarget);
            SEND FirmwareUpdateMd.RequestGet(
                ManufacturerId = CONV($manufacturerId, 2),
                FirmwareId = CONV($testFirmwareId, 2),
                Checksum = CONV($firmwareChecksum, 2),
                FirmwareTarget = $firmwareTarget,
                FragmentSize = CONV($maxFragmentSize, 2),
                Activation = 0,
                Reserved = 0xFE);

            EXPECT FirmwareUpdateMd.RequestReport($status = Status in (0xFF, 0x03));
            IF (ISNULL($status))     { MSGFAIL ("Status Report missing"); }
            ELSEIF ($status == 0xFF) { MSGPASS ("Status 0xFF: Firmware update is initiated"); }
            ELSEIF ($status == 0x00) { MSGFAIL ("Status 0x00: DUT Manufacturer ID and/or Firmware ID does not match the RequestGet Command"); }
            ELSEIF ($status == 0x01) { MSGFAIL ("Status 0x01: Missing expected authentication event."); }
            ELSEIF ($status == 0x02) { MSGFAIL ("Status 0x02: Fragment size exceeds Max Fragment Size."); }
            ELSEIF ($status == 0x03) { MSGPASS ("Status 0x03: Firmware target is not upgradable."); }
            ELSE                     { MSGFAIL ("Invalid Status 0x{0:X2}", $status); }

            IF ($status == 0xFF)
            {
                MSG ("Expecting a Get command for 1st firmware fragment of target 0x{0:X2}...", $firmwareTarget);
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 1);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;

                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));

                MSG ("End of test for target 0x{0:X2}. Wait {1} seconds for the Status Report...", $firmwareTarget, UINT($timeoutStatusReport));
                EXPECT FirmwareUpdateMd.StatusReport(
                    $timeoutStatusReport,
                    Status == 0x01,
                    $waitTime = Waittime in (0x0000 ... 0xFFFE));

                IF (ISNULL($waitTime) || $waitTime == []) { MSGFAIL ("Status Report frame or Wait Time field missing"); }
                ELSE
                {
                    IF (UINT($waitTime) == 0) { MSG ("Device is ready after aborted Firmware Update"); }
                    ELSE
                    {
                        MSG ("Waiting the reported time: {0} seconds", UINT($waitTime));
                        WAIT (UINT($waitTime) * 1000);
                    }
                }
                // USE NoOperation CMDCLASSVER = 1;
                // SENDRAW NoOperation( )

            } // IF ($status == 0xFF)
        } // Firmware target is upgradable
    } // LOOP ($target)

    WAIT ($recoveryTime * 1000);    // Some devices need a recovery time after this test

TESTSEQ END


/**
 * InvalidChecksumAndFragment
 * Checks the behavior if an invalid checksum and/or an invalid fragment number is provided.
 * The last step of this sequence MAY fail, if the DUT does not support CRC-CCITT with initial value 0x1D0F and polynomium 0x1021.
 *
 * CRC calculator: https://www.lammertbies.nl/comm/info/crc-calculation.html
 * Another CRC calculator: www.zorc.breitbandkatze.de/crc.html
 *
 * CC versions: 4
 */

TESTSEQ InvalidChecksumAndFragment: "Check for correct behavior if invalid checksum or fragment number is provided"

    $timeoutGetCommand = 25;           // Timeout for Get command. You MAY increase this value.
    $timeoutStatusReport = 150;        // Timeout for StatusReport in seconds (see header comment of sequence 'AbortedFirmwareUpdate').
    $firmwareDataBytes = [0x30, 0x30, 0x30];
    $validFirmwareChecksum = 0xE76F;   // This is the valid checksum in RequestGet for the used firmware data bytes (3 bytes in 1 report).
    $invalidFirmwareChecksum = 0xFFFF; // This is an invalid checksum in RequestGet for the used firmware data bytes (3 bytes in 1 report).
    $validReportChecksum = 0x626B;     // This is the valid checksum for the used Report Command: report #1 with Last=0 (7A 06 00 01  30 30 30)
    $invalidReportChecksum = 0xEEEE;   // This is an invalid checksum for the used Report Command.
    $recoveryTime = 1;                 // Recovery Time in seconds. Some devices need a recovery time after this test. You MAY increase this value.

    SEND FirmwareUpdateMd.FirmwareMdGet( );
    EXPECT FirmwareUpdateMd.FirmwareMdReport(
        $manufacturerId = ManufacturerId in (0x0000 ... 0xFFFF),
        $firmware0Id = Firmware0Id in (0x0000 ... 0xFFFF),
        $firmware0Checksum = Firmware0Checksum in (0x0000 ... 0xFFFF),
        $firmwareUpgradable = FirmwareUpgradable in (0x00, 0xFF),
        $numberOfFirmwareTargets = NumberOfFirmwareTargets in (0x00 ... 0xFF),
        $maxFragmentSize = MaxFragmentSize in (0x0000 ... 0xFFFF),
        $firmwareIds = vg1);
    MSG ("Firmware IDs = {0}", $firmwareIds);

    IF     ($numberOfFirmwareTargets == 0) { $firmwareTargets = [0x00]; }
    ELSEIF ($numberOfFirmwareTargets == 1) { $firmwareTargets = [0x00, 0x01]; }
    ELSE                                   { $firmwareTargets = [0x00, 0x01, $numberOfFirmwareTargets]; }

    LOOP ($target; 0; LENGTH($firmwareTargets) - 1)
    {
        $firmwareTarget = $firmwareTargets[$target];
        IF (($target == 0) && ($firmwareUpgradable == 0x00))
        {
            MSG ("Test is not applicable for Firmware Target 0.");
        }
        ELSE
        {
            IF ($target == 0) { $testFirmwareId = $firmware0Id; }
            ELSE              { $testFirmwareId = $firmwareIds[($firmwareTargets[$target] - 1) * 2] * 256 + $firmwareIds[(($firmwareTargets[$target] - 1) * 2) + 1]; }

            MSG ("Send RequestGet to target 0x{0:X2}...", $firmwareTarget);
            SEND FirmwareUpdateMd.RequestGet(
                ManufacturerId = CONV($manufacturerId, 2),
                FirmwareId = CONV($testFirmwareId, 2),
                Checksum = CONV($invalidFirmwareChecksum, 2),
                FirmwareTarget = $firmwareTarget,
                FragmentSize = CONV(LENGTH($firmwareDataBytes), 2),
                Activation = 0,
                Reserved = 0xFE);

            EXPECT FirmwareUpdateMd.RequestReport($status = Status in (0xFF, 0x03));
            IF (ISNULL($status))     { MSGFAIL ("Status Report missing"); }
            ELSEIF ($status == 0xFF) { MSGPASS ("Status 0xFF: Firmware update is initiated"); }
            ELSEIF ($status == 0x00) { MSGFAIL ("Status 0x00: DUT Manufacturer ID and/or Firmware ID does not match the RequestGet Command"); }
            ELSEIF ($status == 0x01) { MSGFAIL ("Status 0x01: Missing expected authentication event."); }
            ELSEIF ($status == 0x02) { MSGFAIL ("Status 0x02: Fragment size exceeds Max Fragment Size."); }
            ELSEIF ($status == 0x03) { MSGPASS ("Status 0x03: Firmware target is not upgradable."); }
            ELSE                     { MSGFAIL ("Invalid Status 0x{0:X2}", $status); }

            IF ($status == 0xFF)
            {
                MSG ("Expecting a Get command for 1st firmware fragment (Report Number = 1) of target 0x{0:X2}...", $firmwareTarget);
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 1);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;
                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));
                IF ($reportNumber != 1)
                {
                    MSGFAIL ("Requested starting report number {0} is invalid (expected: 1)", UINT($reportNumber));
                }

                // part 1: firmware fragment with valid fragment number 1 and invalid checksum
                MSG ("Sending a firmware fragment with valid fragment number and invalid checksum...");
                SEND FirmwareUpdateMd.Report(
                    ReportNumber1 = $reportNumber1,
                    Last = 0,
                    ReportNumber2 = $reportNumber2,
                    Data = $firmwareDataBytes,
                    Checksum = CONV($invalidReportChecksum, 2));

                MSG ("Expecting a Get command for 1st firmware fragment (Report Number = 1) again...");
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 1);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;
                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));
                IF ($reportNumber != 1)
                {
                    MSGFAIL ("Requested starting report number {0} is invalid (expected: 1)", UINT($reportNumber));
                }

                // part 2: firmware fragment with invalid fragment number (Report Number = 2) and invalid checksum
                MSG ("Sending a firmware fragment with invalid fragment number (Report Number = 2) and invalid checksum...");
                SEND FirmwareUpdateMd.Report(
                    ReportNumber1 = $reportNumber1,
                    Last = 0,
                    ReportNumber2 = $reportNumber2 + 1,
                    Data = $firmwareDataBytes,
                    Checksum = CONV($invalidReportChecksum, 2));

                MSG ("Expecting a Get command for 1st firmware fragment (Report Number = 1) again...");
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 1);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;
                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));
                IF ($reportNumber != 1)
                {
                    MSGFAIL ("Requested starting report number {0} is invalid (expected: 1)", UINT($reportNumber));
                }

                // part 3: firmware fragment with invalid fragment number (Report Number = 2) and valid checksum
                MSG ("Sending a firmware fragment with invalid fragment number (Report Number = 2) and valid checksum...");
                SEND FirmwareUpdateMd.Report(
                    ReportNumber1 = $reportNumber1,
                    Last = 0,
                    ReportNumber2 = $reportNumber2 + 1,
                    Data = $firmwareDataBytes,
                    Checksum = CONV($validReportChecksum, 2));

                MSG ("Expecting a Get command for 1st firmware fragment (Report Number = 1) again...");
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 1);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;
                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));
                IF ($reportNumber != 1)
                {
                    MSGFAIL ("Requested starting report number {0} is invalid (expected: 1)", UINT($reportNumber));
                }

                // final: wait for Status Report
                MSG ("End of test #1 of 2 for target 0x{0:X2}. Wait {1} seconds for the Status Report...", $firmwareTarget, UINT($timeoutStatusReport));
                EXPECT FirmwareUpdateMd.StatusReport(
                    $timeoutStatusReport,
                    Status == 0x01,
                    $waitTime = Waittime in (0x0000 ... 0xFFFE));

                IF (ISNULL($waitTime) || $waitTime == []) { MSGFAIL ("Status Report frame or Wait Time field missing"); }
                ELSE
                {
                    IF (UINT($waitTime) == 0) { MSG ("Device is ready after aborted Firmware Update"); }
                    ELSE
                    {
                        MSG ("Waiting the reported time: {0} seconds", UINT($waitTime));
                        WAIT (UINT($waitTime) * 1000);
                    }
                }
                // USE NoOperation CMDCLASSVER = 1;
                // SENDRAW NoOperation( )
            } // IF ($status == 0xFF)

            // Here we try to give two fragments, both with the correct fragment checksum, but not matching
            // the actual Firmware Update checksum given initially in FirmwareUpdateMd.RequestGet. In that case,
            // the DUT is expected to return a FirmwareUpdateMd.StatusReport(Status=0x00), but we tolerate 0x01.
            // The checksum covers the fields Command Class, Command, RepNr1+Last, RepNr2, and Data (7A 06 nn nn xx xx xx ...).
            // The checksum algorithm is CRC-CCITT with initial value 0x1D0F and polynomium 0x1021.
            // First  Report (Last=0): 7A 06 00 01  30 30 30  62 6B
            // Second Report (Last=1): 7A 06 80 02  30 30 30  DB 67
            $data = [0x30, 0x30, 0x30];
            $usedFirmwareChecksum = 0x0055; // This is an invalid checksum in RequestGet for the used firmware data bytes (all bytes of all reports).

            MSG ("Send RequestGet to target 0x{0:X2}...", $firmwareTarget);
            MSG ("Used firmware checksum 0x{0:X4} is invalid.", $usedFirmwareChecksum);

            SEND FirmwareUpdateMd.RequestGet(
                ManufacturerId = CONV($manufacturerId, 2),
                FirmwareId = CONV($testFirmwareId, 2),
                Checksum = CONV($usedFirmwareChecksum, 2),
                FirmwareTarget = $firmwareTarget,
                FragmentSize = CONV(LENGTH($data), 2),
                    Activation = 0,
                    Reserved = 0xFE);

            EXPECT FirmwareUpdateMd.RequestReport($status = Status in (0xFF, 0x03));
            IF (ISNULL($status))     { MSGFAIL ("Status Report missing"); }
            ELSEIF ($status == 0xFF) { MSGPASS ("Status 0xFF: Firmware update is initiated"); }
            ELSEIF ($status == 0x00) { MSGFAIL ("Status 0x00: DUT Manufacturer ID and/or Firmware ID does not match the RequestGet Command"); }
            ELSEIF ($status == 0x01) { MSGFAIL ("Status 0x01: Missing expected authentication event."); }
            ELSEIF ($status == 0x02) { MSGFAIL ("Status 0x02: Fragment size exceeds Max Fragment Size."); }
            ELSEIF ($status == 0x03) { MSGPASS ("Status 0x03: Firmware target is not upgradable."); }
            ELSE                     { MSGFAIL ("Invalid Status 0x{0:X2}", $status); }

            IF ($status == 0xFF)
            {
                // Handle first report (of 2)
                MSG ("Expecting a Get command for 1st firmware fragment (Report Number = 1) of target 0x{0:X2}...", $firmwareTarget);
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 1);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;

                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));
                IF ($reportNumber != 1)
                {
                    MSGFAIL ("Requested starting report number {0} is invalid (expected: 1)", UINT($reportNumber));
                }

                $last = 0;
                MSG ("Sending a firmware fragment (Last={0}) with valid fragment number and valid checksum...", $last);
                // $reportChecksum is for the current Report (including command header)
                $reportChecksum = 0x626B;

                SEND FirmwareUpdateMd.Report(
                    ReportNumber1 = $reportNumber1,
                    Last = $last,
                    ReportNumber2 = $reportNumber2,
                    Data = $data,
                    Checksum = CONV($reportChecksum, 2));

                // Handle second (=last) report
                MSG ("Expecting a Get command for 2nd firmware fragment (Report Number = 2)...");
                EXPECT FirmwareUpdateMd.Get($timeoutGetCommand,
                    $numberOfReports = NumberOfReports in (0 ... 255),
                    $reportNumber1 = ReportNumber1 == 0,
                    zero == 0,
                    $reportNumber2 = ReportNumber2 == 2);
                $reportNumber = $reportNumber1 * 256 + $reportNumber2;
                MSG ("Number of requested reports: {0} - Requested starting report number: {1}", UINT($numberOfReports), UINT($reportNumber));
                IF ($reportNumber != 2)
                {
                    MSGFAIL ("Requested starting report number {0} is invalid (expected: 2)", UINT($reportNumber));
                }

                $last = 1;
                // $reportChecksum is for the current Report (including command header)
                $reportChecksum = 0xDB67;

                SEND FirmwareUpdateMd.Report(
                    ReportNumber1 = $reportNumber1,
                    Last = $last,
                    ReportNumber2 = $reportNumber2,
                    Data = $data,
                    Checksum = CONV($reportChecksum, 2));

                // Wait for status report with 150 sec timeout
                MSG ("End of test #2 of 2 for target 0x{0:X2}. Wait {1} seconds for the Status Report...", $firmwareTarget, UINT($timeoutStatusReport));
                EXPECT FirmwareUpdateMd.StatusReport(
                    $timeoutStatusReport,
                    Status in (0x00, 0x01),    // 0x00 = checksum error, 0x01 = unable to receive
                    $waitTime = Waittime in (0x0000 ... 0xFFFE));

                IF (ISNULL($waitTime) || $waitTime == []) { MSGFAIL ("Status Report frame or Wait Time field missing"); }
                ELSE
                {
                    IF (UINT($waitTime) == 0) { MSG ("Device is ready after aborted Firmware Update"); }
                    ELSE
                    {
                        MSG ("Waiting the reported time: {0} seconds", UINT($waitTime));
                        WAIT (UINT($waitTime) * 1000);
                    }
                }
            } // IF ($status == 0xFF)

        } // Firmware target is upgradable
    } // LOOP ($target)

    WAIT ($recoveryTime * 1000);    // Some devices need a recovery time after this test

TESTSEQ END
