﻿PACKAGE BasicMappingWindowCoveringPositionEndpointAware; // do not modify this line
USE Basic CMDCLASSVER = 1;

/**
 * Basic Mapping Test Script for Window Covering Position Endpoint Aware Device Type
 * Last Update: September 3, 2013
 *
 * Basic Set = 255 maps to Multilevel Switch = 255
 * Basic Set = 0 maps to Multilevel Switch = 0
 * Basic Set = 1-99 maps to Multilevel Switch = 1-99
 * Basic Get/Report maps to Multilevel Switch Get/Report.
 *
 */
 
TESTSEQ AdjustAnnouncement: "Window Covering Position Endpoint Aware Device Type adjust announcement"

	MSG("Please note: If the Window Covering Position Endpoint Aware Device Type Test script run fails, it may be necessary to adjust the $TIMEOUT variable of each Test Sequence.");
	
TESTSEQ END


TESTSEQ TestBasicMappingToMultilevel: "Test Basic Mapping to Multilevel"
 

	$TIMEOUT =12000; // 20 seconds
	
	MSG("Testing start motion in direction A");
	
	SEND Basic.Set(Value = 0x00);
	WAIT($TIMEOUT);
	SEND Basic.Get();
	EXPECT Basic.Report( Value == 0x00);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value == 0x00);


	MSG("Testing values 0x01 - 0x63");

	SEND Basic.Set(Value = 0x21);
	WAIT($TIMEOUT / 2);
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x20...0x22));
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x20...0x22));	

	SEND Basic.Set(Value = 0x42);
	WAIT($TIMEOUT / 2);
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x41...0x43));
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x41...0x43));	
	
	
	MSG("Testing values 0x64 - 0xFE are ignored");
	
	SEND Basic.Set(Value = 0xB2);
	WAIT($TIMEOUT);
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x41...0x43));
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x41...0x43));	


	MSG("Proceed moving to direction B (0x63)");
	
	SEND Basic.Set(Value = 0x63);
	WAIT($TIMEOUT / 2);
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x63, 0xFE));
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x63, 0xFE));


	MSG("Return to endpoint A");

	SEND Basic.Set(Value = 0x00);
	WAIT($TIMEOUT);
	SEND Basic.Get();
	EXPECT Basic.Report( Value == 0x00);


	MSG("Testing start motion in direction B (0xFF)");
	
	SEND Basic.Set(Value = 0x63); // ER 2015/02/12 - note that for the shade FF maps to the HOME position and not the limit so we have to send in 0x63 to go to the limit.
	WAIT($TIMEOUT / 2);
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x00 ... 0x62));
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x00 ... 0x62));

	WAIT($TIMEOUT / 2);
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x63, 0xFF));
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x63, 0xFF));
	

TESTSEQ END


TESTSEQ TestMultilevelMappingToBasic: "Test Multilevel Mapping to Basic"
 

	$TIMEOUT = 12000;

	MSG("Testing start motion in direction A");
	
	SEND SwitchMultilevel.Set(Value = 0x00);
	WAIT($TIMEOUT);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value == 0x00);
	SEND Basic.Get();
	EXPECT Basic.Report( Value == 0x00);


	MSG("Testing values 0x01 - 0x63");

	SEND SwitchMultilevel.Set(Value = 0x21);
	WAIT($TIMEOUT / 2);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x20...0x22));
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x20...0x22));	

	SEND SwitchMultilevel.Set(Value = 0x35);
	WAIT($TIMEOUT / 2);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x34...0x36));
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x34...0x36));	


	MSG("Testing values 0x64 - 0xFE are ignored");

	SEND SwitchMultilevel.Set(Value = 0xB2);
	WAIT($TIMEOUT);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x34...0x36));
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x34...0x36));	


	MSG("Proceed moving to direction B (0x63)");

	SEND SwitchMultilevel.Set(Value = 0x63);
	WAIT($TIMEOUT / 2);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x63, 0xFE));
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x63, 0xFE));


	MSG("Return to endpoint A");

	SEND SwitchMultilevel.Set(Value = 0x00);
	WAIT($TIMEOUT);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value == 0x00);


	MSG("Testing start motion in direction B (0xFF)");
	
	SEND SwitchMultilevel.Set(Value = 0x63);
	WAIT($TIMEOUT / 2);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x00 ... 0x62));
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x00 ... 0x62));

	WAIT($TIMEOUT / 2);
	SEND SwitchMultilevel.Get();
	EXPECT SwitchMultilevel.Report( Value in (0x63, 0xFF));
	SEND Basic.Get();
	EXPECT Basic.Report( Value in (0x63, 0xFF));


TESTSEQ END