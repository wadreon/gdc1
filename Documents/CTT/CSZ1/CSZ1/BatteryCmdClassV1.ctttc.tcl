﻿PACKAGE BatteryCmdClassV1; // do not modify this line
USE Battery CMDCLASSVER = 1;

/**
 * Battery Command Class Version 1 Test Script
 * Last Update: September 19th, 2016
 * Command Class Specification: SDS12657-11
 * Formatting Conventions: Version 2016-05-19
 *
 * PLEASE NOTE:
 * The test sequence 'WaitForBatteryLowReport' should be run separately due to long timeout.
 * For details see header comment above the test sequence.
 *
 * ChangeLog:
 *
 * March 15th, 2012     - First Release
 * August 5th, 2016     - Refactoring. New test sequence 'WaitForBatteryLowReport'.
 * September 19th, 2016 - Improved message and comments in 'WaitForBatteryLowReport'.
 */


/**
 * ReportFormat
 * Ensures the correct range of reported battery level
 *
 * CC versions: 1
 */

TESTSEQ ReportFormat: "Ensure correct range of battery level"

    SEND Battery.Get( );
    EXPECT Battery.Report($batteryLevel = BatteryLevel in (0 ... 0x64, 0xFF));

    IF (ISNULL($batteryLevel))
    {
        MSGFAIL ("Frame missing! Device could sleep.");
    }
    ELSEIF ($batteryLevel >= 0x00 && $batteryLevel <= 0x64)
    {
        MSGPASS ("Current Battery Level: {0}%", UINT($batteryLevel));
    }
    ELSEIF ($batteryLevel == 0xFF)
    {
        MSGPASS ("Battery Low Warning reported!");
    }
    ELSE
    {
        MSGFAIL ("Battery Level {0} = 0x{1:X2}: Value out of range. Allowed Values: 0x00 ... 0x64, 0xFF", UINT($batteryLevel), $batteryLevel);
    }

TESTSEQ END


/**
 * WaitForBatteryLowReport
 * Waiting for the unsolicited Battery Low report
 *
 * PLEASE NOTE:
 * This test sequence should be run separately due to long (infinite) timeout.
 * After a prompt the script will wait indefinitively until it receives a Battery Report from the DUT.
 * A low battery warning in the Report is expected.
 * The tester is liable to prepare the DUT to emit a low battery warning report.
 * If the tester cannot get the DUT to emit a low battery warning report, he must abort the test.
 *
 * CC versions: 1
 */

TESTSEQ WaitForBatteryLowReport: "Waiting for Battery Low report"

    // Get current Battery Level for logging purposes. This sequence runs separately from 'ReportFormat' sequence.
    SEND Battery.Get( );
    EXPECT Battery.Report($batteryLevel = BatteryLevel in (0 ... 0x64, 0xFF));

    IF (ISNULL($batteryLevel))
    {
        MSGFAIL ("Frame missing! Device could sleep.");
    }
    ELSEIF ($batteryLevel == 0xFF)
    {
        MSGFAIL ("Battery Low Warning reported. DUT MUST NOT report Battery Low before the test starts.");
    }
    ELSEIF ($batteryLevel >= 0x65 && $batteryLevel <= 0xFE)
    {
        MSGFAIL ("Battery Level {0} = 0x{1:X2}: Value out of range.", UINT($batteryLevel), $batteryLevel);
    }
    ELSEIF ($batteryLevel == 0x00)
    {
        MSGFAIL ("Battery Level 0 (empty) reported. DUT MUST NOT report Battery Empty before the test starts.");
    }
    ELSE
    {
        MSGPASS ("Current Battery Level: {0}%", UINT($batteryLevel));
    }

    // Test preparation prompt
    MSGBOXYES ("WaitForBatteryLowReport: This test may need a very long time. After clicking 'Yes', the script will wait indefinitively until it receives a Battery Report from the DUT. A low battery warning in the Report is expected. If you cannot get the DUT to emit a low battery warning report, then click 'No'.                                                 Are you ready?     If not, click 'No' and then press the 'Stop' button in the CTT toolbar to abort this test.");

    // Infinite wait for an unsolicited Battery Low report
    EXPECT Battery.Report(0, $lowBatteryLevel = BatteryLevel == 0xFF);
    IF     (ISNULL($lowBatteryLevel)) { MSGFAIL ("DUT did not send a Low Battery report or test was aborted"); }
    ELSEIF ($lowBatteryLevel != 0xFF) { MSGFAIL ("DUT did not report Low Battery state, BatteryLevel was {0}% = 0x{1:X2}", UINT($lowBatteryLevel), $lowBatteryLevel); }
    ELSE                              { MSGPASS ("DUT reports Low Battery state"); }

TESTSEQ END
