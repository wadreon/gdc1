﻿PACKAGE VersionCmdClassV2_2; // do not modify this line
USE Version CMDCLASSVER = 2;

/**
 * Version Command Class Version 2 Test Script
 * Last Update: November 18th, 2016
 * Command Class Specification: SDS12652-10
 * Formatting Conventions: Version 2016-05-19
 *
 * ChangeLog:
 *
 * October 23rd, 2013   - Initial Release
 * April 20th, 2016     - Refactoring, update of Command Classes (ZW_classcmd.h generated 29.10.2015)
 * April 20th, 2016     - Checks version number of ALL Command Classes
 * May 25th, 2016       - Minor improvements
 * June 2nd, 2016       - Minor improvements, SDK Version list updated
 * August 16th, 2016    - Bugfix in 'VersionParameters'
 * August 22nd, 2016    - Minor improvements, SDK Version list updated
 * November 18th, 2016  - SDK version list removed
 *
 */

TESTSEQ AdjustAnnouncement: "Version Command Class adjust announcement"

    MSGFAIL ("Please note: To run the Version Command Class Test Script the 'CmdClassVersionNumber' Test Sequence must be adjusted according to supported Command Classes of the DUT.");
    MSGFAIL ("For details refer to the header comment of the Test Sequence header 'CmdClassVersionNumber'.");
    MSGFAIL ("Please deactivate this Test Sequence in Test Case Selection Tree (Project->Setup Test Cases) when this Test Script has been adjusted.");

TESTSEQ END

/**
 * VersionParameters
 * Validates the range of Report values
 *
 * CC versions: 2
 */

TESTSEQ VersionParameters: "Check Version Report parameters"

    SEND Version.Get( );
    EXPECT Version.Report(
        $zWaveLibraryType        = ZWaveLibraryType        == 0x03,
        $zWaveProtocolVersion    = ZWaveProtocolVersion    == 0x06,
        $zWaveProtocolSubVersion = ZwaveProtocolSubVersion == 0x02,   /* SDK 6.81 */
        $firmware0Version        = Firmware0Version        in (0x00 ... 0x0C),
        $firmware0SubVersion     = Firmware0SubVersion     in (0x00 ... 0xFF),
        $hardwareVersion         = HardwareVersion         in (0x00,0x01,0xFF),
        $firmwareTargets         = NumberOfFirmwareTargets == 0x00,
        $remainingFirmwareVersions = vg );

    MSG ("Z-Wave Library Type: 0x{0:X2}", $zWaveLibraryType);

    // See <SDK>\Z-Wave\include\config_lib.h for Protocol Version and Protocol SubVersion of your SDK
    // See http://zts.sigmadesigns.com Software Release Note chapter 2 for actual SDK versions
    // See http://zts.sigmadesigns.com/maintained-and-monitored-z-wave-sdks for older SDK versions
    MSG ("Z-Wave Protocol Version:     0x{0:X2} = {1}", $zWaveProtocolVersion, UINT($zWaveProtocolVersion));
    MSG ("Z-Wave Protocol Sub Version: 0x{0:X2} = {1}", $zWaveProtocolSubVersion, UINT($zWaveProtocolSubVersion));

    MSG ("Firmware 0 Version:      0x{0:X2} = {1}", $firmware0Version, UINT($firmware0Version));
    MSG ("Firmware 0 Sub Version:  0x{0:X2} = {1}", $firmware0SubVersion, UINT($firmware0SubVersion));

    MSG ("Hardware Version: 0x{0:X2}", $hardwareVersion);

    MSG ("Number of Firmware Targets in devices: {0}", UINT($firmwareTargets));
    IF (UINT($firmwareTargets) * 2 != LENGTH($remainingFirmwareVersions))
    {
        MSGFAIL ("Reported NumberOfFirmwareTargets ({0}) requires {1} subsequent bytes with Firmware Version and Sub Version. Reported Firmware Targets length is {2}.",
            UINT($firmwareTargets), UINT($firmwareTargets) * 2, LENGTH($remainingFirmwareVersions));
    }

    MSG ("Version String for Firmware Targets: {0}", $remainingFirmwareVersions);
    //MSG ("Please verify manually that the string above has Version and Sub Version for the intended number of targets ({0})", UINT($firmwareTargets));
    //MSGBOXYES ("Check Output tab in Message Log window: Is the Version String for Firmware Targets OK?");

TESTSEQ END


/**
 * CmdClassVersionNumber
 * Checks the version number of supported Command Classes
 *
 * Note: The array variable $commandClassesList of this Test Sequence
 *       configures the supported Z-Wave Command Classes of the DUT and
 *       their Version numbers.
 *       The variable contains in its initial state a list of all
 *       Z-Wave Command Classes with Version number set to 0x00, which
 *       means 'Unsupported Command Class'.
 *
 *       To sucessful run this Test Sequence the 2nd column of the array
 *       variable $commandClassesList MUST contain the correct Version
 *       number of all supported Z-Wave Command Classes.
 *       The Node Information Frame and the Certification Form should
 *       contain the necessary information to correctly fill this list.
 *       For all unsupported Command Classes the 2nd column MUST contain
 *       the value 0x00.
 *
 * CC versions: 1, 2
 */

TESTSEQ CmdClassVersionNumber: "Check version number of supported Command Classes"

    $delay = 200;   // Delay between Command Class Get commands in milliseconds. May be increased.

    $commandClassesList = [
    //   CC  Version      Command Class Name
        0x5D, 0x00,    // ANTITHEFT
        0x57, 0x00,    // APPLICATION CAPABILITY
        0x22, 0x00,    // APPLICATION STATUS
        0x85, 0x02,    // ASSOCIATION
        0x9B, 0x00,    // ASSOCIATION COMMAND CONFIGURATION
        0x59, 0x01,    // ASSOCIATION GROUP INFO
        0x95, 0x00,    // AV CONTENT DIRECTORY MD
        0x97, 0x00,    // AV CONTENT SEARCH MD
        0x96, 0x00,    // AV RENDERER STATUS
        0x99, 0x00,    // AV TAGGING MD
        0x66, 0x00,    // BARRIER OPERATOR MD
        0x20, 0x02,    // BASIC
        0x36, 0x00,    // BASIC TARIFF INFO
        0x50, 0x00,    // BASIC WINDOW COVERING
        0x80, 0x01,    // BATTERY
        0x5B, 0x00,    // CENTRAL SCENE
        0x2A, 0x00,    // CHIMNEY FAN
        0x46, 0x00,    // CLIMATE CONTROL SCHEDULE
        0x81, 0x00,    // CLOCK
        0x70, 0x00,    // CONFIGURATION
        0x21, 0x00,    // CONTROLLER REPLICATION
        0x56, 0x00,    // CRC 16 ENCAP
        0x3A, 0x00,    // DCP CONFIG
        0x3B, 0x00,    // DCP MONITOR
        0x5A, 0x01,    // DEVICE RESET LOCALLY
        0x65, 0x00,    // DMX
        0x62, 0x00,    // DOOR LOCK
        0x4C, 0x00,    // DOOR LOCK LOGGING
        0x90, 0x00,    // ENERGY PRODUCTION
        0x7A, 0x04,    // FIRMWARE UPDATE MD
        0x8C, 0x00,    // GEOGRAPHIC LOCATION
        0x7B, 0x00,    // GROUPING NAME
        0x82, 0x00,    // HAIL
        0x39, 0x00,    // HRV CONTROL
        0x37, 0x00,    // HRV STATUS
        0x6D, 0x00,    // HUMIDITY_CONTROL_MODE
        0x6E, 0x00,    // HUMIDITY_CONTROL_OPERATING_STATE
        0x64, 0x00,    // HUMIDITY_CONTROL_SETPOINT
        0x87, 0x00,    // INDICATOR
        0x5C, 0x00,    // IP_ASSOCIATION
        0x9A, 0x00,    // IP CONFIGURATION
        0x6B, 0x00,    // IRRIGATION
        0x89, 0x00,    // LANGUAGE
        0x76, 0x00,    // LOCK
        0x69, 0x00,    // MAILBOX
        0x91, 0x00,    // MANUFACTURER PROPRIETARY
        0x72, 0x02,    // MANUFACTURER SPECIFIC
        0xEF, 0x00,    // MARK
        0x32, 0x00,    // METER
        0x35, 0x00,    // METER PULSE
        0x3C, 0x00,    // METER TBL CONFIG
        0x3D, 0x00,    // METER TBL MONITOR
        0x3E, 0x00,    // METER TBL PUSH
        0x51, 0x00,    // MTP WINDOW COVERING
        0x8E, 0x00,    // MULTI CHANNEL ASSOCIATION V2 / ex MULTI INSTANCE ASSOCIATION
        0x60, 0x00,    // MULTI CHANNEL V2 / ex MULTI INSTANCE
        0x8F, 0x00,    // MULTI CMD
        0x4D, 0x00,    // NETWORK MANAGEMENT BASIC
        0x34, 0x00,    // NETWORK MANAGEMENT INCLUSION
        0x67, 0x00,    // NETWORK MANAGEMENT INSTALLATION_MAINTENANCE
        0x54, 0x00,    // NETWORK MANAGEMENT PRIMARY
        0x52, 0x00,    // NETWORK MANAGEMENT PROXY
        0x00, 0x00,    // NO OPERATION
        0x77, 0x00,    // NODE NAMING
        0xF0, 0x00,    // NON INTEROPERABLE
        0x71, 0x00,    // NOTIFICATION / ALARM
        0x73, 0x01,    // POWERLEVEL
        0x3F, 0x00,    // PREPAYMENT
        0x41, 0x00,    // PREPAYMENT ENCAPSULATION
        0x88, 0x00,    // PROPRIETARY
        0x75, 0x00,    // PROTECTION
        0x48, 0x00,    // RATE TBL CONFIG
        0x49, 0x00,    // RATE TBL MONITOR
        0x7D, 0x00,    // REMOTE ASSOCIATION
        0x7C, 0x00,    // REMOTE ASSOCIATION ACTIVATE
        0x2B, 0x00,    // SCENE ACTIVATION
        0x2C, 0x00,    // SCENE ACTUATOR CONF
        0x2D, 0x00,    // SCENE CONTROLLER CONF
        0x53, 0x00,    // SCHEDULE
        0x4E, 0x00,    // SCHEDULE ENTRY LOCK
        0x93, 0x00,    // SCREEN ATTRIBUTES
        0x92, 0x00,    // SCREEN MD
        0x98, 0x00,    // SECURITY
        0x9F, 0x01,    // SECURITY_2
        0x24, 0x00,    // SECURITY PANEL MODE
        0x2E, 0x00,    // SECURITY PANEL ZONE
        0x2F, 0x00,    // SECURITY PANEL ZONE SENSOR
        0x9C, 0x00,    // SENSOR ALARM
        0x30, 0x00,    // SENSOR BINARY
        0x9E, 0x00,    // SENSOR CONFIGURATION
        0x31, 0x00,    // SENSOR MULTILEVEL
        0x9D, 0x00,    // SILENCE ALARM
        0x94, 0x00,    // SIMPLE AV CONTROL
        0x6C, 0x01,    // SUPERVISION
        0x27, 0x00,    // SWITCH ALL
        0x25, 0x00,    // SWITCH BINARY
        0x33, 0x00,    // SWITCH COLOR
        0x26, 0x04,    // SWITCH MULTILEVEL
        0x28, 0x00,    // SWITCH TOGGLE BINARY
        0x29, 0x00,    // SWITCH TOGGLE MULTILEVEL
        0x4A, 0x00,    // TARIFF CONFIG
        0x4B, 0x00,    // TARIFF TBL MONITOR
        0x44, 0x00,    // THERMOSTAT FAN MODE
        0x45, 0x00,    // THERMOSTAT FAN STATE
        0x38, 0x00,    // THERMOSTAT HEATING
        0x40, 0x00,    // THERMOSTAT MODE
        0x42, 0x00,    // THERMOSTAT OPERATING STATE
        0x47, 0x00,    // THERMOSTAT SETBACK
        0x43, 0x00,    // THERMOSTAT SETPOINT
        0x8A, 0x00,    // TIME
        0x8B, 0x00,    // TIME PARAMETERS
        0x55, 0x02,    // TRANSPORT SERVICE
        0x63, 0x00,    // USER CODE
        0x86, 0x03,    // VERSION
        0x84, 0x00,    // WAKE UP
        0x6A, 0x00,    // WINDOW COVERING
        0x23, 0x00,    // ZIP
        0x4F, 0x00,    // ZIP 6LOWPAN
        0x5F, 0x00,    // ZIP GATEWAY
        0x68, 0x00,    // ZIP NAMING
        0x58, 0x00,    // ZIP ND
        0x61, 0x00,    // ZIP PORTAL
        0x5E, 0x02     // ZWAVE PLUS INFO
    ];

    LOOP ($i; 0; LENGTH($commandClassesList) - 2)
    {
        SEND Version.CommandClassGet(RequestedCommandClass = $commandClassesList[$i]);
        EXPECT Version.CommandClassReport(
            $requestedCommandClass = RequestedCommandClass == $commandClassesList[$i],
            $commandClassVersion   = CommandClassVersion   == $commandClassesList[$i + 1]);
        IF (ISNULL($requestedCommandClass))
        {
            MSGFAIL ("Report Frame missing for Command Class 0x{0}.", $commandClassesList[$i]);
        }
        ELSE
        {
            IF     ($requestedCommandClass == 0x5D) { MSG ("ANTITHEFT v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x57) { MSG ("APPLICATION CAPABILITY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x22) { MSG ("APPLICATION STATUS v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x85) { MSG ("ASSOCIATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x9B) { MSG ("ASSOCIATION COMMAND CONFIGURATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x59) { MSG ("ASSOCIATION GROUP INFO v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x95) { MSG ("AV CONTENT DIRECTORY MD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x97) { MSG ("AV CONTENT SEARCH MD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x96) { MSG ("AV RENDERER STATUS v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x99) { MSG ("AV TAGGING MD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x66) { MSG ("BARRIER OPERATOR MD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x20) { MSG ("BASIC v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x36) { MSG ("BASIC TARIFF INFO v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x50) { MSG ("BASIC WINDOW COVERING v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x80) { MSG ("BATTERY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x5B) { MSG ("CENTRAL SCENE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x2A) { MSG ("CHIMNEY FAN v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x46) { MSG ("CLIMATE CONTROL SCHEDULE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x81) { MSG ("CLOCK v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x70) { MSG ("CONFIGURATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x21) { MSG ("CONTROLLER REPLICATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x56) { MSG ("CRC 16 ENCAP v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x3A) { MSG ("DCP CONFIG v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x3B) { MSG ("DCP MONITOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x5A) { MSG ("DEVICE RESET LOCALLY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x65) { MSG ("DMX v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x62) { MSG ("DOOR LOCK v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x4C) { MSG ("DOOR LOCK LOGGING v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x90) { MSG ("ENERGY PRODUCTION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x7A) { MSG ("FIRMWARE UPDATE MD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x8C) { MSG ("GEOGRAPHIC LOCATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x7B) { MSG ("GROUPING NAME v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x82) { MSG ("HAIL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x39) { MSG ("HRV CONTROL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x37) { MSG ("HRV STATUS v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x6D) { MSG ("HUMIDITY_CONTROL_MODE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x6E) { MSG ("HUMIDITY_CONTROL_OPERATING_STATE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x64) { MSG ("HUMIDITY_CONTROL_SETPOINT v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x87) { MSG ("INDICATOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x5C) { MSG ("IP_ASSOCIATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x9A) { MSG ("IP CONFIGURATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x6B) { MSG ("IRRIGATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x89) { MSG ("LANGUAGE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x76) { MSG ("LOCK v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x69) { MSG ("MAILBOX v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x91) { MSG ("MANUFACTURER PROPRIETARY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x72) { MSG ("MANUFACTURER SPECIFIC v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0xEF) { MSG ("MARK v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x32) { MSG ("METER v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x35) { MSG ("METER PULSE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x3C) { MSG ("METER TBL CONFIG v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x3D) { MSG ("METER TBL MONITOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x3E) { MSG ("METER TBL PUSH v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x51) { MSG ("MTP WINDOW COVERING v{0}", UINT($commandClassVersion)); }
//          ELSEIF ($requestedCommandClass == 0x8E) { MSG ("MULTI INSTANCE ASSOCIATION v{0}", UINT($commandClassVersion)); }    // discontinued CC name
            ELSEIF ($requestedCommandClass == 0x8E) { MSG ("MULTI CHANNEL ASSOCIATION V2 v{0}", UINT($commandClassVersion)); }
//          ELSEIF ($requestedCommandClass == 0x60) { MSG ("MULTI INSTANCE v{0}", UINT($commandClassVersion)); }    // discontinued CC name
            ELSEIF ($requestedCommandClass == 0x60) { MSG ("MULTI CHANNEL V2 v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x8F) { MSG ("MULTI CMD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x4D) { MSG ("NETWORK MANAGEMENT BASIC v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x34) { MSG ("NETWORK MANAGEMENT INCLUSION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x67) { MSG ("NETWORK MANAGEMENT INSTALLATION_MAINTENANCE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x54) { MSG ("NETWORK MANAGEMENT PRIMARY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x52) { MSG ("NETWORK MANAGEMENT PROXY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x00) { MSG ("NO OPERATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x77) { MSG ("NODE NAMING v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0xF0) { MSG ("NON INTEROPERABLE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x71) { MSG ("NOTIFICATION / ALARM v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x73) { MSG ("POWERLEVEL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x3F) { MSG ("PREPAYMENT v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x41) { MSG ("PREPAYMENT ENCAPSULATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x88) { MSG ("PROPRIETARY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x75) { MSG ("PROTECTION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x48) { MSG ("RATE TBL CONFIG v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x49) { MSG ("RATE TBL MONITOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x7D) { MSG ("REMOTE ASSOCIATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x7C) { MSG ("REMOTE ASSOCIATION ACTIVATE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x2B) { MSG ("SCENE ACTIVATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x2C) { MSG ("SCENE ACTUATOR CONF v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x2D) { MSG ("SCENE CONTROLLER CONF v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x53) { MSG ("SCHEDULE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x4E) { MSG ("SCHEDULE ENTRY LOCK v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x93) { MSG ("SCREEN ATTRIBUTES v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x92) { MSG ("SCREEN MD v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x98) { MSG ("SECURITY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x9F) { MSG ("SECURITY_2 v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x24) { MSG ("SECURITY PANEL MODE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x2E) { MSG ("SECURITY PANEL ZONE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x2F) { MSG ("SECURITY PANEL ZONE SENSOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x9C) { MSG ("SENSOR ALARM v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x30) { MSG ("SENSOR BINARY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x9E) { MSG ("SENSOR CONFIGURATION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x31) { MSG ("SENSOR MULTILEVEL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x9D) { MSG ("SILENCE ALARM v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x94) { MSG ("SIMPLE AV CONTROL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x6C) { MSG ("SUPERVISION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x27) { MSG ("SWITCH ALL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x25) { MSG ("SWITCH BINARY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x33) { MSG ("SWITCH COLOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x26) { MSG ("SWITCH MULTILEVEL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x28) { MSG ("SWITCH TOGGLE BINARY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x29) { MSG ("SWITCH TOGGLE MULTILEVEL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x4A) { MSG ("TARIFF CONFIG v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x4B) { MSG ("TARIFF TBL MONITOR v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x44) { MSG ("THERMOSTAT FAN MODE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x45) { MSG ("THERMOSTAT FAN STATE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x38) { MSG ("THERMOSTAT HEATING v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x40) { MSG ("THERMOSTAT MODE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x42) { MSG ("THERMOSTAT OPERATING STATE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x47) { MSG ("THERMOSTAT SETBACK v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x43) { MSG ("THERMOSTAT SETPOINT v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x8A) { MSG ("TIME v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x8B) { MSG ("TIME PARAMETERS v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x55) { MSG ("TRANSPORT SERVICE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x63) { MSG ("USER CODE v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x86) { MSG ("VERSION v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x84) { MSG ("WAKE UP v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x6A) { MSG ("WINDOW COVERING v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x23) { MSG ("Z/IP v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x4F) { MSG ("Z/IP 6LOWPAN v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x5F) { MSG ("Z/IP GATEWAY v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x68) { MSG ("Z/IP NAMING v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x58) { MSG ("Z/IP ND v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x61) { MSG ("Z/IP PORTAL v{0}", UINT($commandClassVersion)); }
            ELSEIF ($requestedCommandClass == 0x5E) { MSG ("ZWAVE PLUS INFO v{0}", UINT($commandClassVersion)); }
            ELSE
            {
                MSG ("Command Class: 0x{0:X2}", $requestedCommandClass);
                MSG ("Command Class Version: {0}", UINT($commandClassVersion));
            }
        }
        WAIT ($delay);
        $i = $i + 1;
    } // LOOP ($i)

    MSG ("Test finished.");

TESTSEQ END
