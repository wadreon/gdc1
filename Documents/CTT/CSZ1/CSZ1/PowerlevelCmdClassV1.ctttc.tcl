﻿PACKAGE PowerlevelCmdClassV1; // do not modify this line
USE Powerlevel CMDCLASSVER = 1;

/**
 * Powerlevel Command Class Version 1 Test Script
 * Last Update: November 18th, 2016
 * Formatting Conventions: Version 2016-05-19
 *
 * ChangeLog:
 *
 * October 23rd, 2013   - First release
 * April 4th, 2016      - Refactoring, new Test Sequences for LinkTests
 * April 15th, 2016     - Refactoring
 * April 27th, 2016     - USB disconnect message fixed
 * May 25th, 2016       - Minor improvements
 * June 1st, 2016       - Minor improvements
 * June 16th, 2016      - Timing and user guidance changed in 'LinkTestDisconnectedNode'
 * September 28th, 2016 - User guidance for battery devices added in 'LinkTestDisconnectedNode'
 * November 18th, 2016  - Timeout $timeToWait increased in 'LinkTestDisconnectedNode'
 *
 */


/**
 * InitialValues
 * Validates the current powerlevel values
 *
 * CC versions: 1
 */

TESTSEQ InitialValues: "Check initial values"

    SEND Powerlevel.Get( );
    EXPECT Powerlevel.Report(
        $powerLevel = PowerLevel in (0x00 ... 0x09),
        $timeout    = Timeout);

    IF ($powerLevel == 0x00)
    {
        MSGPASS ("PowerLevel: NormalPower");
    }
    ELSEIF (($powerLevel >= 0x01) && ($powerLevel <= 0x09))
    {
        IF ($timeout > 0)
        {
            MSGPASS ("PowerLevel: minus{0}dBm   Timeout: {1} seconds", UINT($powerLevel), UINT($timeout));
        }
        ELSE
        {
            MSGFAIL ("PowerLevel: minus{0}dBm   Timeout: 0. It MUST be greater than 0 if the PowerLevel is not 'NormalPower'", UINT($powerLevel));
        }
    }
    ELSE
    {
        MSGFAIL ("PowerLevel: {0} - Not allowed   Timeout: {1} seconds", UINT($powerLevel), UINT($timeout));
    }

TESTSEQ END


/**
 * TimeoutTest
 * Tests the timeout functionality
 *
 * CC versions: 1
 */

TESTSEQ TimeoutTest: "Test a timeout"

    $powerLevel = 7;
    $timeoutInSeconds = 20;

    MSG ("Configure a Powerlevel of minus{0}dBm for {1} seconds", UINT($powerLevel), UINT($timeoutInSeconds));
    SEND Powerlevel.Set(
        PowerLevel = $powerLevel,
        Timeout    = $timeoutInSeconds);
    SEND Powerlevel.Get( );
    EXPECT Powerlevel.Report(
        PowerLevel == $powerLevel,
        Timeout    > ($timeoutInSeconds - 2));

    // make sure to wait longer than the timeout
    WAIT ($timeoutInSeconds * 1000 + 1000);

    // expect NormalPower
    SEND Powerlevel.Get( );
    MSG ("Expecting NormalPower after timeout elapsed...");
    EXPECT Powerlevel.Report(
        PowerLevel == 0,
        Timeout    == 0);

TESTSEQ END


/**
 * TimeoutIgnore
 * Verifies that timeout is ignored with powerlevel 0
 *
 * CC versions: 1
 */

TESTSEQ TimeoutIgnore: "Check that timeout is ignored with powerlevel = 0"

    $timeoutInSeconds = 120;
    MSG ("Configure a Powerlevel of 0 for {0} seconds", UINT($timeoutInSeconds));
    SEND Powerlevel.Set(
        PowerLevel = 0,
        Timeout    = $timeoutInSeconds);
    SEND Powerlevel.Get( );
    MSG ("Expecting NormalPower immediately...");
    EXPECT Powerlevel.Report(
        PowerLevel == 0,
        Timeout    == 0);

TESTSEQ END


/**
 * InvalidPowerlevel
 * Checks handling of invalid powerlevel values
 *
 * CC versions: 1
 */

TESTSEQ InvalidPowerlevel: "Check invalid powerlevel"

    $invalidPowerLevel = 20;
    $timeoutInSeconds = 60;

    // Set to NormalPower
    MSG ("Set to NormalPower");
    SEND Powerlevel.Set(
        PowerLevel = 0,
        Timeout    = 0);
    WAIT (1000);
    // Set to an invalid power level
    MSG ("Configure an invalid Powerlevel of minus{0}dBm for {1} seconds", UINT($invalidPowerLevel), UINT($timeoutInSeconds));
    SEND Powerlevel.Set(
        PowerLevel = $invalidPowerLevel,
        Timeout    = $timeoutInSeconds);
    SEND Powerlevel.Get( );
    MSG ("Expecting NormalPower immediately...");
    // Expecting power level 0 (timeout will be ignored)
    EXPECT Powerlevel.Report(
        $powerLevel = PowerLevel == 0x00,
        $currentTimeout = Timeout);
    // This check is only necessary, if a DUT interpolates the invalid power level value to minus9dBm (not specified so!)
    /*IF (($powerLevel != 0) && ($currentTimeout != 0) && ($currentTimeout < $timeoutInSeconds - 2))
    {
        MSGFAIL ("Invalid Timeout Value {0} reported.", UINT($currentTimeout));
    }*/

TESTSEQ END


/**
 * InitialTestReport
 * Checks the initial Test Report for valid values
 *
 * CC versions: 1
 */

TESTSEQ InitialTestReport: "Check initial Test Report"

    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        $testNodeId        = TestNodeId        in (0 ... 232),
        $statusOfOperation = StatusOfOperation in (0, 1, 2),
        $testFrameCount    = TestFrameCount    in (0x00 ... 0xFFFF));

    IF (ISNULL($testNodeId))
    {
        MSGFAIL ("The DUT does not respond a Test Node Report");
    }
    ELSE
    {
        MSG ("Test NodeID:       0x{0:X2}", $testNodeId);
        MSG ("Test frame count:    {0}", UINT($testFrameCount));

        IF     ($statusOfOperation == 0) { MSG ("Status of operation: ZW_TEST_FAILED"); }
        ELSEIF ($statusOfOperation == 1) { MSG ("Status of operation: ZW_TEST_SUCCESS"); }
        ELSEIF ($statusOfOperation == 2) { MSG ("Status of operation: ZW_TEST_INPROGRESS"); }
        ELSE                             { MSGFAIL ("Status of operation: 0x{0:X2} - Reserved", $statusOfOperation); }
    }

TESTSEQ END


/**
 * LinkTestValidNode
 * Checks the Test Node functionality with a valid node ID
 *
 * CAUTION: Please check the value of the variable $testNodeId (below).
 *          It MUST be set to the node ID of your Static Controller.
 *          The Static Controller normally has the node ID 0x01.
 *
 * CC versions: 1
 */

TESTSEQ LinkTestValidNode: "Check Test Node functionality with a valid node ID"

    $testNodeId = 0x01;     // MUST be the Static Controller node ID
    $powerLevel = 3;        // power level for this test
    $framesToSend = 100;    // number of frames to send
    $delay1 = 2000;         // delay for Get Report DURING the powerlevel test. May be decreased.

    MSG ("TestNodeSet for node 0x{0:X2} with {2} frames and power level {1}.", $testNodeId, UINT($powerLevel), UINT($framesToSend));
    MSG ("May be the node ID (variable $testNodeId) has to be altered.");
    SEND Powerlevel.TestNodeSet(
        TestNodeId = $testNodeId,
        PowerLevel = $powerLevel,
        TestFrameCount = CONV($framesToSend, 2));

    WAIT ($delay1);

    // verify the DUT answers correctly to a Test Node Get commands DURING a powerlevel test
    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        TestNodeId == $testNodeId,
        StatusOfOperation == 2,
        TestFrameCount in (0 ... $framesToSend));

    $timeToWait = 10000;
    MSG ("Wait {0} ms until the test is completed. (May be this interval has to be altered)", UINT($timeToWait));
    WAIT ($timeToWait);

    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        $testNodeId        = TestNodeId        == $testNodeId,
        $statusOfOperation = StatusOfOperation == 0x01,
        $testFrameCount    = TestFrameCount    in (0 ... $framesToSend));

    MSG ("Test NodeID:       0x{0:X2}", $testNodeId);
    MSG ("Test frame count:    {0}", UINT($testFrameCount));

    IF     ($statusOfOperation == 0) { MSGFAIL ("Status of operation: ZW_TEST_FAILED (try to alter node ID)"); }
    ELSEIF ($statusOfOperation == 1) { MSGPASS ("Status of operation: ZW_TEST_SUCCESS"); }
    ELSEIF ($statusOfOperation == 2) { MSGFAIL ("Status of operation: ZW_TEST_INPROGRESS (try to increase variable $timeToWait)"); }
    ELSE                             { MSGFAIL ("Status of operation: 0x{0:X2} - Reserved", $statusOfOperation); }

    // After the powerlevel test the RF power level has to be reset to normal power level
    SEND Powerlevel.Get( );
    EXPECT Powerlevel.Report(
        PowerLevel == 0,
        Timeout    == 0);

TESTSEQ END // LinkTestValidNode


/**
 * LinkTestInvalidNode
 * Checks the Test Node functionality with an invalid node ID
 *
 * CAUTION: Please check the value of the variable $testNodeId (below).
 *          It MUST be set to an invalid node ID.
 *
 * CC versions: 1
 */

TESTSEQ LinkTestInvalidNode: "Check Test Node functionality with an invalid node ID"

    $testNodeId = 0xAA;     // MUST be an invalid node ID
    $powerLevel = 3;        // power level for this test
    $framesToSend = 10;     // number of frames to send
    $delay1 = 500;         // delay for Get Report DURING the powerlevel test. May be decreased.

    MSG ("TestNodeSet for node 0x{0:X2} with {2} frames and power level {1}.", $testNodeId, UINT($powerLevel), UINT($framesToSend));
    MSG ("May be the node ID (variable $testNodeId) has to be altered.");
    SEND Powerlevel.TestNodeSet(
        TestNodeId = $testNodeId,
        PowerLevel = $powerLevel,
        TestFrameCount = CONV($framesToSend, 2));

    WAIT ($delay1);

    // verify the DUT answers correctly to a Test Node Get commands DURING the powerlevel test
    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        TestNodeId == $testNodeId,
        StatusOfOperation == 2,
        TestFrameCount == 0);

    $timeToWait = 5000;
    MSG ("Wait {0} ms until the test is completed. (May be this interval has to be altered)", UINT($timeToWait));
    WAIT ($timeToWait);

    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        $testNodeId        = TestNodeId        == $testNodeId,
        $statusOfOperation = StatusOfOperation == 0x00,
        $testFrameCount    = TestFrameCount == 0);

    MSG ("Test NodeID:       0x{0:X2}", $testNodeId);
    MSG ("Test frame count:    {0}", UINT($testFrameCount));

    IF     ($statusOfOperation == 0) { MSGPASS ("Status of operation: ZW_TEST_FAILED"); }
    ELSEIF ($statusOfOperation == 1) { MSGFAIL ("Status of operation: ZW_TEST_SUCCESS (try to alter node ID)"); }
    ELSEIF ($statusOfOperation == 2) { MSGFAIL ("Status of operation: ZW_TEST_INPROGRESS (try to increase variable $timeToWait)"); }
    ELSE                             { MSGFAIL ("Status of operation: 0x{0:X2} - Reserved", $statusOfOperation); }

    // After the powerlevel test the RF power level has to be reset to normal power level
    SEND Powerlevel.Get( );
    EXPECT Powerlevel.Report(
        PowerLevel == 0,
        Timeout    == 0);

TESTSEQ END // LinkTestInvalidNode


/**
 * LinkTestDisconnectedNode
 * Checks the Test Node functionality with a further valid node ID. This node is disconnected during the test.
 *
 * CAUTION: Please check the values of the variables $controllerNodeId and $testNodeId (below).
 *          Variable $controllerNodeId MUST be set to the node ID of your Static Controller.
 *          The Static Controller normally has the node ID 0x01.
 *          Variable $testNodeId MUST be set to the node ID of a Z-Wave Developer Board with a
 *          Z-Wave 'Switch On/Off' Sample Application loaded, which is already included in the
 *          network. This is not the node ID of the DUT!
 *
 *          Test sequence preparation:
 *          - Prepare a Z-Wave Developer Board with the 'Switch On/Off' Sample Application
 *          - Include this device into the Z-Wave network (as additional device)
 *          - Set the variable $testNodeId to the node ID of this device
 *          - Check the value of the variable $cntrlNodeId (your Static Controller)
 *          Test sequence execution:
 *          - Start this Test Sequence
 *          - The preparation check takes approximately 12 seconds
 *          - Ensure that a battery device is awake before clicking 'Yes' in the confirmation window
 *          - You will see the following demand message in the Output Window of CTT:
 *            "----- Now disconnect the Developer Board from USB port for 2 seconds! -----"
 *            Disconnect the Developer Board from it's USB port for 2 seconds.
 *          - After 2 seconds you will see the following demand message in the Output Window of CTT:
 *            "----- Now connect the Developer Board to USB port again! -----"
 *            Connect the Developer Board to it's USB port again.
 *          - After finishing the test successful exclude the Developer Board from the network
 *          Note: You MAY change the timing of the Test Sequence according to your DUT.
 *          Suggestion: You MAY run this Test Sequence separately.
 *
 * CC versions: 1
 */

TESTSEQ LinkTestDisconnectedNode: "Check Test Node functionality with a further valid node ID, the node is disconnected during the test"

    $cntrlNodeId = 0x01;    // MUST be the Static Controller node ID
    $testNodeId = 0x03;     // MUST be the node ID of a Z-Wave Developer Board with a Z-Wave 'Switch On/Off' Sample Application
                            //   loaded, which is already included in the network. Not the DUT node ID!
    $powerLevel = 3;        // power level for this test
    $framesToSend = 150;    // number of frames to send

    $delayInCheck = 5000;           // delay for Get Report in the check of Test Sequence preparation. May be increased.
    $delayBeforeDisconnect = 2000;  // wait time before 'disconnect device' message
    $delayBeforeReconnect = 6000;   // wait time before 'reconnect device' message = time to disconnect the Developer Board

    // Try to check the Test Sequence preparation of the customer
    MSG ("Check for correct Test Sequence preparation");
    IF ($cntrlNodeId == $testNodeId)
    {
        MSGFAIL ("The Test Sequence is not correctly configured. Please read Test Sequence header comment.");
    }
    MSG ("Check access to Z-Wave Developer Board (node ID 0x{0:X2})", $testNodeId);
    SEND Powerlevel.TestNodeSet(
        TestNodeId = $testNodeId,
        PowerLevel = $powerLevel,
        TestFrameCount = CONV(5, 2));
    WAIT ($delayInCheck);
    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        TestNodeId == $testNodeId,
        $status1 = StatusOfOperation == 1,
        ($testFrameCount1 = TestFrameCount) > 0);
    MSG ("Check access to Static Controller (node ID 0x{0:X2})", $cntrlNodeId);
    SEND Powerlevel.TestNodeSet(
        TestNodeId = $cntrlNodeId,
        PowerLevel = $powerLevel,
        TestFrameCount = CONV(5, 2));
    WAIT ($delayInCheck);
    SEND Powerlevel.TestNodeGet( );
    EXPECT Powerlevel.TestNodeReport(
        TestNodeId == $cntrlNodeId,
        $status2 = StatusOfOperation == 1,
        ($testFrameCount2 = TestFrameCount) > 0);
    IF (($status1 != 1) || (UINT($testFrameCount1) == 0))
    {
        MSGFAIL ("Developer Board with 'Switch On/Off' Sample App (testNodeId 0x{0:X2}) not reached or StatusOfOperation is not 'Success'. Is the Test Sequence correctly configured? Please read Test Sequence header comment.", $testNodeId);
    }
    ELSEIF (($status2 != 1) || (UINT($testFrameCount2) == 0))
    {
        MSGFAIL ("Controller (cntrlNodeId 0x{0:X2}) not reached or StatusOfOperation is not 'Success'. Is the Test Sequence correctly configured? Please read Test Sequence header comment.", $cntrlNodeId);
    }
    ELSE
    {
        // the test itself starts here
        MSGBOXYES ("LinkTestDisconnectedNode - this test requires tester interaction. Please refer to the Test Sequence header comment. Are you ready (battery device is awake) ?");

        MSG ("TestNodeSet for node 0x{0:X2} with {2} frames and power level {1}.", $testNodeId, UINT($powerLevel), UINT($framesToSend));
        SEND Powerlevel.TestNodeSet(
            TestNodeId = $testNodeId,
            PowerLevel = $powerLevel,
            TestFrameCount = CONV($framesToSend, 2));

        WAIT ($delayBeforeDisconnect);
        MSG ("----- Now disconnect the Developer Board from USB port for {0} seconds! -----", UINT($delayBeforeReconnect / 1000));
        MSG ("----- You will see a message here if this time has expired.");
        WAIT ($delayBeforeReconnect);
        MSG ("----- Now connect the Developer Board to USB port again! -----");

        $timeToWait = 20000;    // may be increased if final operation status is ZW_TEST_INPROGRESS
        MSG ("Wait {0} seconds until the test is completed. You may increase this interval ($timeToWait) if necessary.", UINT($timeToWait / 1000));
        MSG ("Be sure to wake up a battery powered device in time.");
        WAIT ($timeToWait);

        SEND Powerlevel.TestNodeGet( );
        EXPECT Powerlevel.TestNodeReport(
            $testNodeId        = TestNodeId        == $testNodeId,
            $statusOfOperation = StatusOfOperation in (0 ... 2),
            $testFrameCount    = TestFrameCount    in (0 ... $framesToSend));

        MSG ("Test NodeID:       0x{0:X2}", $testNodeId);
        MSG ("Test frame count:  {0} of {1}", UINT($testFrameCount), UINT($framesToSend));

        IF     ($testFrameCount == 0)             { MSGFAIL ("Test node not reached. Check the Test Sequence configuration."); }
        ELSEIF ($testFrameCount == $framesToSend) { MSGFAIL ("Test node has received all frames. Try to decrease $delayBeforeDisconnect."); }
        ELSE                                      { MSGPASS ("Test node was replying during it's connection with USB power."); }

        IF     ($statusOfOperation == 0) { MSGFAIL ("Received status of operation: ZW_TEST_FAILED"); }
        ELSEIF ($statusOfOperation == 1) { MSGPASS ("Received status of operation: ZW_TEST_SUCCESS"); }
        ELSEIF ($statusOfOperation == 2) { MSGFAIL ("Received status of operation: ZW_TEST_INPROGRESS (try to increase variable $timeToWait)"); }
        ELSE                             { MSGFAIL ("Status of operation: 0x{0:X2} - Reserved", $statusOfOperation); }
    } // IF (preparation was ok)

    // After the powerlevel test the RF power level has to be reset to normal power level
    SEND Powerlevel.Get( );
    EXPECT Powerlevel.Report(
        PowerLevel == 0,
        Timeout    == 0);

TESTSEQ END // LinkTestDisconnectedNode
