﻿PACKAGE MultilevelSwitchCmdClassV4; // do not modify this line
USE SwitchMultilevel CMDCLASSVER = 4;

/**
 * Multilevel Switch Command Class Version 4 Test Script
 * Last Update: November 2nd, 2016
 * Command Class Specification: SDS12657-9
 * Formatting Conventions: Version 2016-05-19
 *
 * PLEASE NOTE:
 * The values of variables '$onOffTime' and '$delay...' MAY be
 * changed (depending from DUT capabilities) for testing purposes.
 * $onOffTime is the time the DUT needs to change from OFF/CLOSED
 * to ON/OPEN or backwards.
 *
 * Matching Command Class specification: SDS12657-9 (with changes for Primary and Secondary Switch Type)
 *
 * ChangeLog:
 *
 * April 04, 2016       - First release, adapted from CC V3
 * April 15, 2016       - Refactoring
 * May 25th, 2016       - Minor Refactoring
 * June 1st, 2016       - Improvements in 'ResumeToLastActive' and 'DurationLevelChange'
 * October 26th, 2016   - WAITs added for slow devices
 * November 2nd, 2016   - Improvements in 'DurationLevelChangeWithStartLevel'
 *
 */


TESTSEQ AdjustAnnouncement: "Multilevel Switch Command Class adjust announcement"

    MSGFAIL ("Please note: You MUST import the 'MultilevelSwitchCmdClassV3' Test Case manually.");
    MSGFAIL ("Steps: Project -> Import Existing Test Case -> select file 'GeneralCTTProject.cttproj' -> select 'MultilevelSwitchCmdClassV3'.");
    MSGFAIL ("Ensure that the Test Case and all Test Sequences are activated.");
    MSGFAIL ("Please note: You MAY change the $onOffTime and $delay variables according to DUT capabilities.");
    MSGFAIL ("Please deactivate this Test Sequence in the Test Case Selection Tree (Project -> Setup Test Cases) when this Test Case has been added.");

TESTSEQ END

// NOTE NOTE NOTE - SWF window shades do NOT support DURATION and the FF values sets the shade to the HOME position and not the last On Level.

/**
 * InitialValues
 * Verifies the range of current values
 *
 * CC versions: 4
 */

TESTSEQ InitialValues: "Check initial values"

    SEND SwitchMultilevel.SupportedGet();
    EXPECT SwitchMultilevel.SupportedReport(
        $primarySwitchType   = PrimarySwitchType,
        $secondarySwitchType = SecondarySwitchType);

    MSG ("Primary Switch Type: {0:X2}", UINT($primarySwitchType));
    IF ($primarySwitchType == 0x00)
    {
        MSG ("  0x00      (Direction/Endpoint A): Undefined / Not supported");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Undefined / Not supported");

        /* From SDS12657-9 the Primary Switch Type 0x00 has been OBSOLETED. The
        following comment is invalid for this spec change.
        // In case the Primary Switch Type is set to 0x00 (Undefined / Not supported)
        // the application must respond to Multilevel Switch Get with Multilevel Switch
        // Report (Value = 0xFE) for unknown state/position.
        SEND SwitchMultilevel.Get();
        EXPECT SwitchMultilevel.Report(Value == 0xFE);*/
    }
    ELSEIF ($primarySwitchType == 0x01)
    {
        MSG ("  0x00      (Direction/Endpoint A): Off");
        MSG ("  0x63/0xFF (Direction/Endpoint B): On");
    }
    ELSEIF ($primarySwitchType == 0x02)
    {
        MSG ("  0x00      (Direction/Endpoint A): Down");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Up");
    }
    ELSEIF ($primarySwitchType == 0x03)
    {
        MSG ("  0x00      (Direction/Endpoint A): Close");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Open");
    }
    ELSEIF ($primarySwitchType == 0x04)
    {
        MSG ("  0x00      (Direction/Endpoint A): Counter-Clockwise");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Clockwise");
    }
    ELSEIF ($primarySwitchType == 0x05)
    {
        MSG ("  0x00      (Direction/Endpoint A): Left");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Right");
    }
    ELSEIF ($primarySwitchType == 0x06)
    {
        MSG ("  0x00      (Direction/Endpoint A): Reverse");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Forward");
    }
    ELSEIF ($primarySwitchType == 0x07)
    {
        MSG ("  0x00      (Direction/Endpoint A): Pull");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Push");
    }
    IF (($primarySwitchType & 0xF8) != 0)
    {
        MSGFAIL ("  0x{0:X2}      (Reserved)", $primarySwitchType);
    }

    MSG ("Secondary Switch Type: {0:X2}", $secondarySwitchType);
    IF ($secondarySwitchType == 0x00)
    {
        MSG ("  0x00      (Direction/Endpoint A): Undefined / Not supported");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Undefined / Not supported");
    }
    ELSEIF ($secondarySwitchType == 0x01)
    {
        MSG ("  0x00      (Direction/Endpoint A): Off");
        MSG ("  0x63/0xFF (Direction/Endpoint B): On");
    }
    ELSEIF ($secondarySwitchType == 0x02)
    {
        MSG ("  0x00      (Direction/Endpoint A): Down");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Up");
    }
    ELSEIF ($secondarySwitchType == 0x03)
    {
        MSG ("  0x00      (Direction/Endpoint A): Close");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Open");
    }
    ELSEIF ($secondarySwitchType == 0x04)
    {
        MSG ("  0x00      (Direction/Endpoint A): Counter-Clockwise");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Clockwise");
    }
    ELSEIF ($secondarySwitchType == 0x05)
    {
        MSG ("  0x00      (Direction/Endpoint A): Left");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Right");
    }
    ELSEIF ($secondarySwitchType == 0x06)
    {
        MSG ("  0x00      (Direction/Endpoint A): Reverse");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Forward");
    }
    ELSEIF ($secondarySwitchType == 0x07)
    {
        MSG ("  0x00      (Direction/Endpoint A): Pull");
        MSG ("  0x63/0xFF (Direction/Endpoint B): Push");
    }
    IF (($secondarySwitchType & 0xF8) != 0)
    {
        MSGFAIL ("  0x{0:X2}      (Reserved)", $secondarySwitchType);
    }

TESTSEQ END


/**
 * ResumeToLastActive
 * Verifies the 'restore most recent non-zero state' functionality
 *
 * CC versions: 4
 */

TESTSEQ ResumeToLastActive: "Check the DUT resumes to last active level on receiving a Value of 0xFF"

    $onOffTime = 2000; // This value MAY be changed (depending from DUT capabilities) for testing purposes
    $dimmimgDurationLevelChange = 5;  // Duration in seconds

    $value = 0x10;
    MSG ("Setting Value to 0x{0:X2}...", $value);
    SEND SwitchMultilevel.Set(
        Value           = $value,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == $value,
        TargetValue == $value,
        Duration == 0x00);

    MSG ("Setting Value to 0x00...");
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    MSG ("Setting Value to 0xFF, expecting CurrentValue of 0x{0:X2}...", $value);
    SEND SwitchMultilevel.Set(
        Value           = 0xFF,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == $value,
        TargetValue == $value,
        Duration == 0x00);

    // Test the value 0xFF when the DUT is already to a non-zero level. 
    // In that case the DUT should keep the same value.
    MSG ("Setting Value to 0x32...");
    SEND SwitchMultilevel.Set(
        Value           = 0x32,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x32,
        TargetValue == 0x32,
        Duration == 0x00);

    MSG ("Setting Value to 0xFF, expecting the DUT to stay at 0x32 and not come back to 0x{0:X2}...", $value);
    SEND SwitchMultilevel.Set(
        Value           = 0xFF,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x32,
        TargetValue == 0x32,
        Duration == 0x00);

    // ResumeToLastActive is meant to be the last non-zero value received in a Set Command. 
    // Therefore if the DUT now is at 0x32 and dims slowly towards zero, the expected last active value 
    // is actually 0x32 and not any value crossed between 0x00 and 0x32 while dimming.
    SEND SwitchMultilevel.StartLevelChange(
        Reserved         = 0,
        IncDec           = 0,
        IgnoreStartLevel = 1,           // Start from actual level
        UpDown           = 1,           // Decrease level
        StartLevel       = 0x32,
        DimmingDuration  = $dimmimgDurationLevelChange,
        StepSize         = 0);
    WAIT (($dimmimgDurationLevelChange * 1000) + 500);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    SEND SwitchMultilevel.Set(
        Value           = 0xFF,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x32,
        TargetValue == 0x32,
        Duration == 0x00);

TESTSEQ END


/**
 * DurationOnOffInstantly
 * Verifies the implementation of duration in normal 'Set' commands, instantly
 *
 * Many types of devices are not capable of jumping immediately to
 * a specific level (e.g. High Intensity Discharge lamps, motor controlled devices).
 * For this reason, it is permitted for this devices not implementing instantly
 * jumping to a specific level.
 *
 * CC versions: 4
 */

TESTSEQ DurationOnOffInstantly: "Check duration implementation in normal set, instantly"

    $delay = 100; // This value MAY be changed (depending from DUT capabilities) for testing purposes

    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($delay);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Move to 0x42 instantly
    SEND SwitchMultilevel.Set(
        Value           = 0x42,
        DimmingDuration = 0x00);
    WAIT ($delay);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x42,
        TargetValue == 0x42,
        Duration == 0x00);

    // Move to 0x21 instantly
    SEND SwitchMultilevel.Set(
        Value           = 0x21,
        DimmingDuration = 0x00);
    WAIT ($delay);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x21,
        TargetValue == 0x21,
        Duration == 0x00);

    // Move to 0x03 instantly
    SEND SwitchMultilevel.Set(
        Value           = 0x03,
        DimmingDuration = 0x00);
    WAIT ($delay);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x03,
        TargetValue == 0x03,
        Duration == 0x00);

    // Move to 0x42 instantly
    SEND SwitchMultilevel.Set(
        Value           = 0x42,
        DimmingDuration = 0x00);
    WAIT ($delay);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x42,
        TargetValue == 0x42,
        Duration == 0x00);

    // Move to MAX (0x63) instantly
    SEND SwitchMultilevel.Set(
        Value           = 0x63,
        DimmingDuration = 0x00);
    WAIT ($delay);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x63,
        TargetValue == 0x63,
        Duration == 0x00);

TESTSEQ END


/**
 * DurationOnOffSec
 * Verifies the implementation of duration in normal 'Set' command; range: seconds
 * The certification item will NOT FAIL, if the check after $dimmimgDuration/2 seconds fails.
 *
 * CC versions: 4
 */

TESTSEQ DurationOnOffSec: "Check duration implementation in normal 'Set', 10 seconds"

    $onOffTime = 2000; // This value MAY be changed (depending from DUT capabilities) for testing purposes
    $dimmimgDuration = 10;  // Duration in seconds

    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Move to MAX in $dimmimgDuration seconds
    SEND SwitchMultilevel.Set(
        Value           = 0x63,
        DimmingDuration = $dimmimgDuration);
    WAIT (($dimmimgDuration * 1000) / 2);

    // Expect current Value and Duration near the middle of the range
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        $value = CurrentValue in (0x28 ... 0x3C),
        TargetValue == 0x63,
        Duration in (0x03 ... 0x07, 0xFE));
    IF (($value < 0x28) || ($value > 0x3C))
    {
        MSG ("A supporting device SHOULD respect the specified Duration value.");
        MSG ("The certification item will not fail, if the check fails.");
    }

    WAIT ((($dimmimgDuration * 1000) / 2) + 1000);
    // Expect current Value at target value MAX
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x63,
        TargetValue == 0x63,
        Duration == 0x00);

TESTSEQ END


/**
 * DurationOnOffMin
 * Verifies the implementation of duration in normal 'Set' command; range: minutes
 * The certification item will NOT FAIL, if the check after 1 minute fails.
 *
 * CC versions: 4
 */

TESTSEQ DurationOnOffMin: "Check duration implementation in normal 'Set', 2 minutes"

    $onOffTime = 2000; // This value MAY be changed (depending from DUT capabilities) for testing purposes
    $dimmimgDuration = 2;  // Duration in minutes

    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Move to MAX in $dimmimgDuration minutes
    SEND SwitchMultilevel.Set(
        Value           = 0x63,
        DimmingDuration = 0x7F + $dimmimgDuration);
    WAIT (($dimmimgDuration * 60000) / 2);
    // Expect CurrentValue near the middle of the range
    // Expect remaining Duration between 50 and 70 secs, or 0xFE if duration is unknown
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        $value = CurrentValue in (0x28 ... 0x3C),
        TargetValue == 0x63,
        Duration in (0x32 ... 0x46, 0xFE));
    IF (($value < 0x28) || ($value > 0x3C))
    {
        MSG ("A supporting device SHOULD respect the specified Duration value.");
        MSG ("The certification item will not fail, if the check fails.");
    }

    WAIT ((($dimmimgDuration * 60000) / 2) + 5000);
    // Expect CurrentValue at target value MAX
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x63,
        TargetValue == 0x63,
        Duration == 0x00);

TESTSEQ END


/**
 * DurationOnOffFactoryDefault
 * Verifies the implementation of factory default duration in normal 'Set' command
 *
 * CC versions: 4
 */

TESTSEQ DurationOnOffFactoryDefault: "Check duration implementation in normal set, factory default"

    $onOffTime = 2000; // This value MAY be changed (depending from DUT capabilities) for testing purposes

    // Set to OFF
    MSG ("Set to OFF (0x00)...");
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Set using V1 command syntax
    MSG ("Move to MAX (0x63) with Set command using V1 syntax...");
    USE SwitchMultilevel CMDCLASSVER = 1;
    SEND SwitchMultilevel.Set(Value = 0x63);
    USE SwitchMultilevel CMDCLASSVER = 4;
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        ($tV1 = CurrentValue) in (0x00 ... 0x63),
        TargetValue == 0x63,
        Duration in (0x00 ... 0xFE));

    // Set to OFF
    MSG ("Set to OFF (0x00) again...");
    SEND SwitchMultilevel.Set(
        Value = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Set using V2/V3/V4 command syntax
    MSG ("Move to MAX (0x63) with Set command using V2-V4 syntax...");
    SEND SwitchMultilevel.Set(
        Value = 0x63,
        DimmingDuration = 0xFF);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        ($tFF = CurrentValue) in (0x00 ... 0x63),
        TargetValue == 0x63,
        Duration in (0x00 ... 0xFE));

    IF ($tV1 != $tFF)
    {
        MSGPASS ("Factory default dimming duration is not compatible with version 1 implementation of the Multilevel Command Class.");
    }
    ELSE
    {
        MSGPASS ("Factory default dimming duration is compatible with version 1 implementation of the Multilevel Command Class.");
    }

TESTSEQ END


/**
 * DurationLevelChangeWithStartLevel
 * Verifies the implementation of duration without ignoring StartLevel
 *
 * CC versions: 4
 */

TESTSEQ DurationLevelChangeWithStartLevel: "Check duration implementation in level change without ignoring StartLevel"

    $onOffTime = 2000; // This value MAY be changed (depending from DUT capabilities) for testing purposes
    $startLevel = 0x32;// This Value MUST NOT be changed

    MSG ("Increase from a Start Level of 50%");
    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Move UP from 50% (expecting a duration of 5 seconds)
    SEND SwitchMultilevel.StartLevelChange(
        Reserved         = 0,
        IncDec           = 0,
        IgnoreStartLevel = 0,
        UpDown           = 0,
        StartLevel       = $startLevel,
        DimmingDuration  = 0x0A, // This value MUST NOT be changed
        StepSize         = 0);

    $outOfRange = 0;
    LOOP ($i; 1; 4)
    {
        WAIT (1000);
        SEND SwitchMultilevel.Get( );
        EXPECT SwitchMultilevel.Report(
            ($value = CurrentValue) in ($startLevel ... 0x63),
            TargetValue in (0x00 ... 0x63),
            Duration in (0x00 ... 0x0A, 0xFE));

        $min = $startLevel + ($i * 10) - 10;
        $max = $startLevel + ($i * 10) + 10;

        IF ($value < $startLevel)
        {
            MSGFAIL ("Value 0x{0:X2} is out of range. StartLevel 0x{1:X2} was ignored.", $value, $startLevel);
            $outOfRange = 1;
        }
        ELSEIF (($value < $min) || ($value > $max))
        {
            MSGFAIL ("Value 0x{0:X2} is out of range. Expected value range: 0x{1:X2} ... 0x{2:X2}", $value, $min, $max);
            $outOfRange = 1;
        }
    }
    IF ($outOfRange == 1)
    {
        MSG ("A supporting device SHOULD respect the Ignore Start Level bit (it is 0) and the specified Duration value.");
        MSG ("The certification item will not fail, if the check fails.");
    }

    MSG ("Decrease from a Start Level of 50%");
    // Set to MAX
    SEND SwitchMultilevel.Set(
        Value           = 0x63,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x63,
        TargetValue == 0x63,
        Duration == 0x00);

    // Move DOWN from 50% (expecting a duration of 5 seconds)
    SEND SwitchMultilevel.StartLevelChange(
        Reserved         = 0,
        IncDec           = 0,
        IgnoreStartLevel = 0,
        UpDown           = 1,
        StartLevel       = $startLevel,
        DimmingDuration  = 0x0A, // This value MUST NOT be changed
        StepSize         = 0);

    $outOfRange = 0;
    LOOP ($i; 1; 4)
    {
        WAIT (1000);
        SEND SwitchMultilevel.Get( );
        EXPECT SwitchMultilevel.Report(
            ($value = CurrentValue) in (0x00 ... $startLevel),
            TargetValue in (0x00 ... 0x63),
            Duration in (0x00 ... 0x0A, 0xFE));

        $min = $startLevel - ($i * 10) - 10;
        $max = $startLevel - ($i * 10) + 10;

        IF ($value > $startLevel)
        {
            MSGFAIL ("Value 0x{0:X2} is out of range. StartLevel 0x{1:X2} was ignored.", $value, $startLevel);
            $outOfRange = 1;
        }
        ELSEIF (($value < $min) || ($value > $max))
        {
            MSGFAIL ("Value 0x{0:X2} is out of range. Expected value range: 0x{1:X2} ... 0x{2:X2}", $value, $min, $max);
            $outOfRange = 1;
        }
    }
    IF ($outOfRange == 1)
    {
        MSG ("A supporting device SHOULD respect the Ignore Start Level bit (it is 0) and the specified Duration value.");
        MSG ("The certification item will not fail, if the check fails.");
    }

TESTSEQ END


/**
 * DurationLevelChange
 * Verifies the implementation of duration in 'Start Level Change' command, without a specified start level
 *
 * CC versions: 4
 */

TESTSEQ DurationLevelChange: "Check duration implementation in Multilevel Switch Level Change Command"

    $onOffTime = 2000; // This value MAY be changed (depending from DUT capabilities) for testing purposes

    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    // Move UP from OFF for 20 seconds
    // Set a value different than 0 here (if the DUT fails to ignore the start level, it would still start from 0x00)
    MSG ("Increase level from OFF for 20 seconds");
    SEND SwitchMultilevel.StartLevelChange(
        Reserved         = 0,
        IncDec           = 0,
        IgnoreStartLevel = 1,
        UpDown           = 0,
        StartLevel       = 0x21,
        DimmingDuration  = 0x14,
        StepSize         = 0);

    $outOfRange = 0;
    LOOP ($i; 1; 4)
    {
        WAIT (4000);
        SEND SwitchMultilevel.Get( );
        EXPECT SwitchMultilevel.Report(
            ($value = CurrentValue) in (0x00 ... 0x63),
            TargetValue in (0x00 ... 0x63),
            Duration in (0x00 ... 0x14, 0xFE));

        $min = 0x00 + ($i * 20) - 10;
        $max = 0x00 + ($i * 20) + 10;

        IF (($value < $min) || ($value > $max))
        {
            MSGFAIL ("Value 0x{0:X2} is out of range. Expected value range: 0x{1:X2} ... 0x{2:X2}", $value, $min, $max);
            $outOfRange = 1;
        }
    }
    IF ($outOfRange == 1)
    {
        MSG ("A supporting device SHOULD respect the specified Duration value.");
        MSG ("The certification item will not fail, if the check fails.");
    }

    // Set to MAX
    SEND SwitchMultilevel.Set(
        Value           = 0x63,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x63,
        TargetValue == 0x63,
        Duration == 0x00);

    // Move DOWN from MAX for 20 seconds
    MSG ("Decrease level from MAX for 20 seconds");
    SEND SwitchMultilevel.StartLevelChange(
        Reserved         = 0,
        IgnoreStartLevel = 1,
        IncDec           = 0,
        UpDown           = 1,
        StartLevel       = 0,
        DimmingDuration  = 0x14,
        StepSize         = 0);

    $outOfRange = 0;
    LOOP ($i; 1; 4)
    {
        WAIT (4000);
        SEND SwitchMultilevel.Get( );
        EXPECT SwitchMultilevel.Report(
            ($value = CurrentValue) in (0x00 ... 0x63),
            TargetValue in (0x00 ... 0x63),
            Duration in (0x00 ... 0x14, 0xFE));

        $min = 0x63 - ($i * 20) - 10;
        $max = 0x63 - ($i * 20) + 10;

        IF (($value < $min) || ($value > $max))
        {
            MSGFAIL ("Value 0x{0:X2} is out of range. Expected value range: 0x{1:X2} ... 0x{2:X2}", $value, $min, $max);
            $outOfRange = 1;
        }
    }
    IF ($outOfRange == 1)
    {
        MSG ("A supporting device SHOULD respect the specified Duration value.");
        MSG ("The certification item will not fail, if the check fails.");
    }

TESTSEQ END


/**
 * LevelChangeReservedFields
 * Verifies ignoring reserved fields in 'Level Change' commands
 *
 * CC versions: 4
 */

TESTSEQ LevelChangeReservedFields: "Check reserved fields ignored"

    $onOffTime = 12000; // This value MAY be changed (depending from DUT capabilities) for testing purposes
    $dimmimgDuration = 12;  // Duration in seconds

    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

    SEND SwitchMultilevel.StartLevelChange(
        Reserved         = 0x07,
        IncDec           = 0,
        IgnoreStartLevel = 1,
        UpDown           = 0,
        StartLevel       = 0,
        DimmingDuration  = $dimmimgDuration,
        StepSize         = 1);
    WAIT (($dimmimgDuration + 1) * 1000);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x63,
        TargetValue == 0x63,
        Duration == 0x00);

    // Set to OFF
    SEND SwitchMultilevel.Set(
        Value           = 0x00,
        DimmingDuration = 0x00);
    WAIT ($onOffTime);
    SEND SwitchMultilevel.Get( );
    EXPECT SwitchMultilevel.Report(
        CurrentValue == 0x00,
        TargetValue == 0x00,
        Duration == 0x00);

TESTSEQ END


/**
 * IncrementDecrementLevelChange
 * Check increment and decrement mechanism
 *
 * CC versions: 3, 4
 */

TESTSEQ IncrementDecrementLevelChange: "If supported by the DUT check the increment and decrement mechanism (new feature V3+)"

    SEND SwitchMultilevel.SupportedGet();
    EXPECT SwitchMultilevel.SupportedReport(
        $primarySwitchType   = PrimarySwitchType,
        $secondarySwitchType = SecondarySwitchType);

    IF ($secondarySwitchType & 0x07)
    {
        MSG ("The following commands apply to the secondary function of the DUT");

        $stepSize = 10;
        SEND SwitchMultilevel.StartLevelChange(
            Reserved         = 0,
            IncDec           = 1,
            IgnoreStartLevel = 1,
            UpDown           = 0,
            StartLevel       = 0,
            DimmingDuration  = 0x00,
            StepSize         = $stepSize);
        WAIT (5000);
        SEND SwitchMultilevel.StopLevelChange ( );
        MSGBOXYES ("Did the DUT increase its level?");

        $stepSize = 50;
        SEND SwitchMultilevel.StartLevelChange(
            Reserved         = 0,
            IncDec           = 1,
            IgnoreStartLevel = 1,
            UpDown           = 0,
            StartLevel       = 0,
            DimmingDuration  = 0x00,
            StepSize         = $stepSize);
        WAIT (5000);
        SEND SwitchMultilevel.StopLevelChange ( );
        MSGBOXYES ("Did the DUT increase its level faster than before?");

        $stepSize = 10;
        SEND SwitchMultilevel.StartLevelChange(
            Reserved         = 0,
            IncDec           = 1,
            IgnoreStartLevel = 1,
            UpDown           = 0,
            StartLevel       = 0,
            DimmingDuration  = 0x00,
            StepSize         = $stepSize);
        WAIT (5000);
        SEND SwitchMultilevel.StopLevelChange ( );
        MSGBOXYES ("Did the DUT decrease its level?");

        $stepSize = 50;
        SEND SwitchMultilevel.StartLevelChange(
            Reserved         = 0,
            IncDec           = 1,
            IgnoreStartLevel = 1,
            UpDown           = 0,
            StartLevel       = 0,
            DimmingDuration  = 0x00,
            StepSize         = $stepSize);
        WAIT (5000);
        SEND SwitchMultilevel.StopLevelChange ( );
        MSGBOXYES ("Did the DUT decrease its level faster than before?");
    }
    ELSE
    {
        MSGPASS ("The DUT does not support a secondary function");
    }

TESTSEQ END
