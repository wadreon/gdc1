﻿PACKAGE ZWavePlusInfoCmdClassV2; // do not modify this line
USE ZwaveplusInfo CMDCLASSVER = 2;
USE Version CMDCLASSVER = 1;

/**
 * Z-Wave Plus Info Command Class Version 2 Test Script
 * Last Update: November 21st, 2016
 * Command Class Specification: SDS12652-11
 * Formatting Conventions: Version 2016-05-19
 *
 * ChangeLog:
 *
 * Unknown              - Initial version
 * June 8th, 2016       - Refactoring, minor improvements
 *                      - New test sequence 'GetEndpoints' to inform the tester about necessary tests with these End Points
 * September 28th, 2016 - Minor improvements in 'GetEndpoints'
 * November 21st, 2016  - Clarification for securely included devices in 'VerifyVersion' and 'GetEndpoints'
 *
 */


/**
 * VerifyVersion
 * Verify DUT is implementing at least version 2 of Z-Wave Info Command Class
 *
 * PLEASE NOTE: If the DUT is securely included, and this test sequence runs without 'Enable Security' setting, and the
 * DUT does not send a Version Command Class Report, this test will not fail, and the test sequence may be deactivated.
 *
 * CC versions: 2
 */

TESTSEQ VerifyVersion: "Verify device is implementing at least version 2 of Z-Wave Info Command Class"

    SEND Version.CommandClassGet(RequestedCommandClass = 0x5E);
    EXPECT Version.CommandClassReport(
        RequestedCommandClass == 0x5E,
        $version = CommandClassVersion in (0x02));

    IF (ISNULL($version))
    {
        MSGFAIL ("Device does not report the version of Z-Wave Plus Info Command Class.");
        MSG ("If the device is securely included, and this test sequence runs without 'Enable Security' setting, and the device does not send a Version Command Class Report, this test will not fail, and the test sequence may be deactivated.");
    }
    ELSEIF ($version == 0)
    {
        MSGFAIL ("Device does not implement version 2 or later of the Z-Wave Plus Info Command Class.");
    }
    ELSEIF ($version == 1)
    {
        MSGFAIL ("Device implements version 1 of the Z-Wave Plus Info Command Class - this version is obsoleted and not allowed.");
    }
    ELSE
    {
        MSGPASS ("Device implements version {0} of the Z-Wave Plus Info Command Class", UINT($version));
    }

TESTSEQ END


/**
 * ZWavePlusInfoReport
 * Verifies general content of Z-Wave Plus Info Report
 *
 * Please note: This Test Sequence must be run with every endpoint of the device too
 *
 * Defined Role Types for V2+: see <SDK>\Z-Wave\include\ZW_cmdclass.h and search for "ROLE_TYPE_*_V2"
 * Defined Node Types for V2+: see <SDK>\Z-Wave\include\ZW_cmdclass.h and search for "ZWAVEPLUS_INFO_REPORT_NODE_TYPE_ZWAVEPLUS_*_V2"
 * Defined Icon Types: see <SDK>\Z-Wave\include\ZW_cmdclass.h and search for "ICON_TYPE"
 *
 * CC versions: 2
 */

TESTSEQ ZWavePlusInfoReport: "Verifies general content of Z-Wave Plus Info Report"

    SEND ZwaveplusInfo.Get( );
    EXPECT ZwaveplusInfo.Report(
        ZWaveVersion == 1,
        RoleType in (0x00 ... 0x07),
        NodeType in (0x00, 0x02),
        $installerIcon = InstallerIconType in (0x0000 ... 0xFFFF),
        $userIcon = UserIconType in (0x0000 ... 0xFFFF) );

    MSG ("Refer to document 'Z-Wave Plus Assigned Icon Type' (ZAD13111.xlsx) to verify the following items:");
    MSG ("- Verify that Installer Icon Type 0x{0:X4} is a valid icon type", UINT($installerIcon));
    MSG ("- Verify that User Icon Type 0x{0:X4} is a valid icon type", UINT($userIcon));

    MSG ("Please note: The Z-Wave Plus Tester must be run to verify that DUT uses the correct Role Type in relation to it's Device Type.");

TESTSEQ END


/**
 * GetEndpoints
 * Shows number of device endpoints to inform the tester about rules for the test sequence 'ZWavePlusInfoReport'
 *
 * PLEASE NOTE: If the DUT is securely included, and this test sequence runs without 'Enable Security' setting, and the
 * DUT does not send a Version Command Class Report, this test will not fail, and the test sequence may be deactivated.
 *
 * CC versions: 2
 */

TESTSEQ GetEndpoints: "Gets number of End Points"

    USE MultiChannel CMDCLASSVER = 3;

    SEND Version.CommandClassGet(RequestedCommandClass = 0x60);
    EXPECT Version.CommandClassReport(
        RequestedCommandClass == 0x60,
        $version = CommandClassVersion in (0x00 ... 0xFF));

    IF (ISNULL($version))
    {
        MSGFAIL ("Device does not report the version of Multi Channel Command Class.");
        MSG ("If the device is securely included, and this test sequence runs without 'Enable Security' setting, and the device does not send a Version Command Class Report, this test will not fail, and the test sequence may be deactivated.");
    }
    ELSEIF ($version >= 3)
    {
        SEND MultiChannel.EndPointGet( );
        EXPECT MultiChannel.EndPointReport(
            Res1 == 0,
            Identical in (0, 1),
        $dynamic = Dynamic in (0, 1),
        $endpoints = EndPoints in (0 ... 127),
        Res2 == 0);

        IF ($endpoints > 0)
        {
            IF ($dynamic == 0) { MSG ("This device has a static number of {0} Multichannel End Points.", UINT($endpoints)); }
            ELSE               { MSG ("This device has a dynamic number of at least {0} Multichannel End Points.", UINT($endpoints)); }

            MSG ("Please note that the Test Sequence 'ZWavePlusInfoReport' must be run with every End Point of the device.");
            MSG ("Select 'Enable Multichannel' and the matching End Points in the CTT Encapsulation Toolbar.");
        }
        ELSE
        {
            MSGPASS ("This device has no Multichannel End Points. No further actions required.");
        }
    }
    ELSE
    {
        MSGPASS ("This device doesn't implement Multi Channel Command Class V3+. No further actions required.");
    }

TESTSEQ END
