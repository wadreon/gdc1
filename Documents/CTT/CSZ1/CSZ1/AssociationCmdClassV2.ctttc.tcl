﻿PACKAGE AssociationCmdClassV2; // do not modify this line
USE Association CMDCLASSVER = 2;

/**
 * Association Command Class Version 2 Test Script
 * Last Update: November 25rd, 2016
 * Command Class Specification: SDS12657-9
 * Formatting Conventions: Version 2016-05-19
 *
 * Note: The certification item will NOT FAIL, if the Test Sequence 'InvalidGroupID' fails.
 *
 * ChangeLog:
 *
 * October 15th, 2015   - DuplicateNodeIDs - Test sequence added. This sequence checks if node ids can be added twice in groups
 * October 15th, 2015   - RemoveCommand - Test sequence added from version 1 script
 * March 14th, 2016     - Refactoring
 * March 16th, 2016     - Save and restore Lifeline Association Group in every test case
 * March 18th, 2016     - RemoveSpecificNodeInAllGroups test sequence improved
 * March 30th, 2016     - SetGetSequence test sequence improved
 * April 15th, 2016     - Refactoring
 * April 25th, 2016     - Bugfix in InitialValues
 * May 19th, 2016       - Refactoring
 * May 27th, 2016       - Bugfix in InvalidGroupId
 * June 2nd, 2016       - Minor fixes in InitialValues, and RemoveCommand, improvement in GetSpecificGroup
 * July 20th, 2016      - Minor improvements and fixes from review
 * July 25th, 2016      - Remove all MultiChannel Associations (in 'Initial Values' only)
 * November 23rd, 2016  - Test sequence 'InitLifeline' added
 */


/**
 * InitLifeline
 * Initializes the lifeline Association Group 1 with the controller node id 1
 *
 * CC versions: 1, 2
 */

TESTSEQ InitLifeline: "Initializes the lifeline with a controller node id"

    $lifelineNodeId = 0x01;
    $lifelineGroup = 0x01;

    MSG ("Try to get number of supported Association Groups");
    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (0 ... 0xFF));
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    IF (UINT($supgroups) >= 1)
    {
        SEND Association.Get(GroupingIdentifier = $lifelineGroup);
        EXPECT Association.Report(
            GroupingIdentifier == $lifelineGroup,
            ($maxNodes = MaxNodesSupported) in (0 ... 0xFF));

        IF ($maxNodes >= 1)
        {
            MSG ("Clear all node ID's in Association Group {0}", UINT($lifelineNodeId));
            SEND Association.Remove(
                GroupingIdentifier = $lifelineGroup,
                NodeID = [ ]);
            MSG ("Set node ID 0x{0:X2} into Association Group {1}", UINT($lifelineNodeId), UINT($lifelineGroup));
            SEND Association.Set(
                GroupingIdentifier = $lifelineGroup,
                NodeID = $lifelineNodeId);
            SEND Association.Get(GroupingIdentifier = $lifelineGroup);
            EXPECT Association.Report(
                GroupingIdentifier == $lifelineGroup,
                ($maxNodes = MaxNodesSupported) in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [ $lifelineNodeId ]);
        }
    }

TESTSEQ END


/**
 * InitialValues
 * Verifies the range of current association values (normally the initial values after inclusion process)
 * Removes all Multi Channel Associations from all groups
 *
 * CC versions: 1, 2
 */

TESTSEQ InitialValues: "Verify Initial Values"

    USE Version CMDCLASSVER = 1;

    SEND Version.CommandClassGet(RequestedCommandClass = 0x8E);
    EXPECT Version.CommandClassReport(
        RequestedCommandClass == 0x8E,
        $version = CommandClassVersion in (0x00 ... 0xFF));

    IF ($version >= 2)
    {
        USE MultiChannelAssociation CMDCLASSVER = 2;
        MSG ("Multi Channel Association Command Class version {0} is supported.", UINT($version));
        MSG ("Remove all Node ID's from all Multi Channel Association Groups.");
        SEND MultiChannelAssociation.Remove(
            GroupingIdentifier = 0,
            NodeId = [ ],
            Marker = [ ],
            Vg = [ ]);
    }
    ELSE
    {
        MSG ("Multi Channel Association Command Class is not supported, nothing to remove.");
    }

    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (1 ... 0xFF));

    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            MaxNodesSupported in (0 ... 0xFF),
            ($reps = ReportsToFollow) in (0 ... 0xFF),
            ANYBYTE(NodeID) in (1 ... 232));

        IF ($reps > 0)
        {
            LOOP ($n; 1; $reps)
            {
                EXPECT Association.Report(
                    GroupingIdentifier == $grp,
                    MaxNodesSupported in (0 ... 0xFF),
                    ReportsToFollow == ($reps - $n),
                    ANYBYTE(NodeID) in (1 ... 232));
            }
        }
    }

TESTSEQ END


/**
 * SetGetSequence
 * Verifies that MaxNodesSupported node ID's can be added to each supported Association Group.
 * This test checks due to performance reasons only up to 10 node ID's, but can be improved to
 * test up to 232 node ID's per Association Group.
 *
 * CC versions: 1, 2
 */

TESTSEQ SetGetSequence: "Verify Set/Get sequences"

    $testNodesPerAG = 10;   // We will test only up to 10 nodes per Association Group
    $startNodeId = 0x11;    // We start with node ID 0x11

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    MSG ("Try to get number of supported Association Groups");
    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (0 ... 0xFF));
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    MSG ("Clear all node ID's in all Association Groups (V2)");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    LOOP ($grp; 1; $supgroups)
    {
        MSG ("Verify Association Group: {0}", UINT($grp));

        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            ($maxNodes = MaxNodesSupported) in (0 ... 0xFF),
            ReportsToFollow == 0,
            NodeID eq [ ]);
        $testedNodes = $testNodesPerAG;
        IF ($maxNodes < $testedNodes)
        {
            $testedNodes = $maxNodes;
        }
        MSG ("Association Group: {0}. Max Nodes Supported: {1}. Testing with {2} node IDs.", UINT($grp), UINT($maxNodes), UINT($testedNodes));

        // Try to set $maxNodes (or 10) plus 1 more node ID's into current empty Association Group
        MSG ("Association Group: {0}. Try to add {1} node IDs plus 1 more", UINT($grp), UINT($testedNodes));
        LOOP ($node; $startNodeId; $startNodeId + $testedNodes)
        {
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = $node);
        }

        // Expect $testedNodes nodes (not $testedNodes + 1) with correct IDs (if $testedNodes <= 5)
        SEND Association.Get(GroupingIdentifier = $grp);

        IF ($testedNodes == 1)
        {
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11]);
        }
        ELSEIF ($testedNodes == 2)
        {
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11, 0x12]);
        }
        ELSEIF ($testedNodes == 3)
        {
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11, 0x12, 0x13]);
        }
        ELSEIF ($testedNodes == 4)
        {
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11, 0x12, 0x13, 0x14]);
        }
        ELSEIF ($testedNodes == 5)
        {
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11, 0x12, 0x13, 0x14, 0x15]);
        }
        ELSE
        {
            // Expect a correct summation for the tested number of node IDs
            // This test could be enhanced to test up to 232 node IDs
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                $nodeIds = NodeID);
            $expectedSum = 0;
            $receivedSum = 0;
            LOOP ($node; $startNodeId; $startNodeId + $testedNodes - 1)
            {
                $expectedSum = $expectedSum + $node;
            }
            // correct $expectedSum (expect $testesNodes + 1 nodes), if DUT supports more than $testedNodes node IDs
            IF ($maxNodes > $testedNodes)
            {
                $expectedSum = $expectedSum + $startNodeId + $testedNodes;
            }

            $receivedNodes = LENGTH($nodeIds);
            LOOP ($node; 0; $receivedNodes - 1)
            {
                $receivedSum = $receivedSum + $nodeIds[$node];
            }

            // correct $testedNodes, if DUT supports more than $testedNodes node IDs
            IF ($maxNodes > $testedNodes)
            {
                $testedNodes = $testedNodes + 1;
            }
            IF ($receivedNodes == $testedNodes)
            {
                MSGPASS ("Expected number of node ID's '{0}' received.", UINT($receivedNodes));
            }
            ELSE
            {
                MSGFAIL ("Expected number of node ID's: '{0}' Received: '{1}'.", UINT($testedNodes), UINT($receivedNodes));
            }
            IF ($expectedSum == $receivedSum)
            {
                MSGPASS ("Expected node ID's summation '{0}' received.", UINT($receivedSum));
            }
            ELSE
            {
                MSGFAIL ("Expected node ID's summation: '{0}' Received: '{1}'.", UINT($expectedSum), UINT($receivedSum));
            }
        }

        SEND Association.Remove(
            GroupingIdentifier = $grp,
            NodeID = [ ]
        );
    }

    // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * RemoveCommand
 * Tests possibility to remove one or two nodes from an Association Group.
 * Has three paths for MaxNodesSupported = 1 or 2 or >=3 with different Add/Remove actions.
 *
 * CC versions: 1, 2
 */

TESTSEQ RemoveCommand: "Verify Remove Command"

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    MSG ("Try to get number of supported Association Groups");
    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (0 ... 0xFF));
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    LOOP ($grp; 1; $supgroups)
    {
        MSG ("Clear all node ID's in Association Group {0}", UINT($grp));
        SEND Association.Remove(
            GroupingIdentifier = $grp,
            NodeID = [ ]);
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            ($maxNodes = MaxNodesSupported) in (0 ... 0xFF),
            ReportsToFollow == 0,
            NodeID eq [ ]);

        MSG ("Add and remove node ID(s) in Association Group {0}", UINT($grp));
        IF ($maxNodes == 1) {
            // Test sequence: Add 11  Expect [11]  Remove 11  Expect [ ]  Add 11  Remove All
            MSG("DUT supports 1 node in Association Group {0}", UINT($grp));

            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x11]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11]);
            SEND Association.Remove(
                GroupingIdentifier = $grp,
                NodeID = [0x11]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [ ]);

            // Add Node 11 again for the final Remove All test
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x11]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11]);
        }
        ELSEIF ($maxNodes == 2)
        {
            // Test sequence: Add 11+12  Remove 11+12  Expect [ ]  Add 11+12  Remove 12  Expect 11  Add 12  Remove All
            MSG("DUT supports 2 nodes in Association Group {0}", UINT($grp));

            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x11, 0x12]);
            SEND Association.Remove(
                GroupingIdentifier = $grp,
                NodeID = [0x11, 0x12]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [ ]);

            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x11, 0x12]);
            SEND Association.Remove(
                GroupingIdentifier = $grp,
                NodeID = [0x12]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11]);

            // Add Node 12 again for the final Remove All test
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x12]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11, 0x12]);
        }
        ELSEIF ($maxNodes >= 3)
        {
            // Test sequence: Add 11+12  Remove 11  Expect 12  Add 11+13  Remove 12,13  Expect 11  Add 12+13  Remove All
            MSG("DUT supports more than 2 nodes in Association Group {0}", UINT($grp));

            MSG("Add node ID's 0x11, 0x12 to Association Group {0}", UINT($grp));
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x11, 0x12]);
            MSG("Remove node ID 0x11 from Association Group {0}", UINT($grp));
            SEND Association.Remove(
                GroupingIdentifier = $grp,
                NodeID = [0x11]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x12]);
            MSG("Add node ID's 0x11, 0x13 to Association Group {0}", UINT($grp));
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x11, 0x13]);
            MSG("Remove node ID 0x12 from Association Group {0}", UINT($grp));
            SEND Association.Remove(
                GroupingIdentifier = $grp,
                NodeID = [0x12, 0x13]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11]);

            // Add Node 12+13 again for the final Remove All test
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [0x12, 0x13]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported in (0 ... 0xFF),
                ReportsToFollow == 0,
                NodeID eq [0x11, 0x12, 0x13]);
        }

        // Test sequence: Remove All (from test steps above)  Expect [ ]
        MSG("Remove all nodes from Multi Channel Association Group {0}", UINT($grp));
        SEND Association.Remove(
            GroupingIdentifier = $grp,
            NodeID = [ ]);
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            MaxNodesSupported in (0 ... 0xFF),
            ReportsToFollow == 0,
            NodeID eq [ ]);
    }

    // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * DuplicateNodeIDs
 * Verify that an already added node ID cannot be added to the same Association Group again.
 *
 * CC versions: 1, 2
 */

TESTSEQ DuplicateNodeIDs: "Verify that an already added node ID cannot be added to the same Association Group again."

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    MSG ("Try to get number of supported Association Groups");
    SEND Association.GroupingsGet();
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (0 ... 0xFF));
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    LOOP ($grp; 1; $supgroups)
    {
        MSG ("Clear all node ID's in Association Group {0}", UINT($grp));
        SEND Association.Remove(
            GroupingIdentifier = $grp,
            NodeID = [ ]);

        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            ($maxNodes = MaxNodesSupported) in (0...0xFF),
            ReportsToFollow in (0...0xFF),
            NodeID eq [ ]);

        IF ($maxNodes > 1)
        {
            MSG ("Try to add node ID 0x11 to Association Group {0} twice", UINT($grp));
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeId = [0x11]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported == $maxNodes,
                ReportsToFollow in (0...0xFF),
                $initNodes = NodeId == [0x11]);
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeId = [0x11]);
            SEND Association.Get(GroupingIdentifier = $grp);
            EXPECT Association.Report(
                GroupingIdentifier == $grp,
                MaxNodesSupported == $maxNodes,
                ReportsToFollow in (0...0xFF),
                NodeId == $initNodes);
        }
    }

    // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * InvalidGroupId
 * Check for returning report for Association Group 1 if report for an unsupported AG is requested.
 * This feature is marked as SHOULD in the AG CC spec.
 * The certification item will NOT FAIL, if this test sequence fails.
 *
 * CC versions: 1, 2
 */

TESTSEQ InvalidGroupId: "Check for returning report for Association Group 1 if report for an unsupported AG is requested"

    $testNodesPerAG = 5;    // We will test only up to 5 nodes per Association Group
    $grp = 0x01;

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (1 ... 0xFF));
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    MSG ("Clear all node ID's in Association Group {0}", UINT($grp));
    SEND Association.Remove(
        GroupingIdentifier = $grp,
        NodeId = [ ]);

    SEND Association.Get(GroupingIdentifier = $grp);
    EXPECT Association.Report(
        GroupingIdentifier == $grp,
        ($maxNodes = MaxNodesSupported) in (0x00 ... 0xE8),
        ReportsToFollow in (0 ... 0xFF),
        NodeID eq [ ]);

    IF ($maxNodes > $testNodesPerAG)
    {
        $maxNodes = $testNodesPerAG;
    }

    MSG ("Set {0} node ID's in Association Group {1}", UINT($maxNodes), UINT($grp));
    LOOP ($node; 1; $maxNodes)
    {
        SEND Association.Set(
            GroupingIdentifier = $grp,
            NodeId = $node);
    }

    MSG ("Get report for Association Group {0}", UINT($grp));
    SEND Association.Get(GroupingIdentifier = $grp);
    EXPECT Association.Report(
        GroupingIdentifier == $grp,
        MaxNodesSupported in (0 ... 0xFF),
        ReportsToFollow in (0 ... 0xFF),
        $nodeIDs = NodeID);

    MSG ("Get report for Association Group {0}", UINT(0x00));
    SEND Association.Get(GroupingIdentifier = 0x00);
    EXPECT Association.Report(
        ($grpReceived = GroupingIdentifier) == $grp,
        MaxNodesSupported in (0 ... 0xFF),
        ReportsToFollow in (0 ... 0xFF),
        NodeID eq $nodeIDs);
    IF ($grpReceived != $grp)
    {
        MSG ("A receiving node that receives an unsupported Grouping Identifier SHOULD return");
        MSG ("information relating to Grouping Identifier 1.");
        MSG ("The certification item will NOT FAIL, if this test sequence fails.");
    }

    IF ($supgroups < 0xFF)
    {
        MSG ("Get report for Association Group {0}", UINT(0xFF));
        SEND Association.Get(GroupingIdentifier = 0xFF);
        EXPECT Association.Report(
            ($grpReceived = GroupingIdentifier) == $grp,
            MaxNodesSupported in (0 ... 0xFF),
            ReportsToFollow in (0 ... 0xFF),
            NodeID eq $nodeIDs);
        IF ($grpReceived != $grp)
        {
            MSG ("A receiving node that receives an unsupported Grouping Identifier SHOULD return");
            MSG ("information relating to Grouping Identifier 1.");
            MSG ("The certification item will not fail, if this test sequence fails.");
        }
    }

    SEND Association.Remove(
        GroupingIdentifier = $grp,
        NodeId = [ ]);

        // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * RemoveAllNodesInSpecificGroup
 * Test sequence:
 * - Fill each AG with MaxNodesSupported (or 5)
 * - Remove all nodes in one specific AG
 * - Check if this AG is empty and all other AGs are filled correctly.
 *
 * CC versions: 1, 2
 */

TESTSEQ RemoveAllNodesInSpecificGroup: "Clear all node IDs in a specific group"

    $testNodesPerAG = 5;    // We will test only up to 5 nodes per Association Group
    $startNodeId = 0x11;    // We start with node ID 0x11

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport($supgroups = SupportedGroupings);
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    MSG ("Clear all node ID's in all Association Groups (V2)");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Fill each group with max count of supported node ID's.
    // If a group supports more than 5 node ID's only associate 5 node ID's
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report($maxNodes = MaxNodesSupported);

        $testedNodes = $testNodesPerAG;
        IF ($maxNodes < $testedNodes)
        {
            $testedNodes = $maxNodes;
        }

        MSG ("Fill Association Group {0} with {1} node ID's", UINT($grp), UINT($maxNodes));
        LOOP ($node; $startNodeId; $startNodeId + $testedNodes - 1)
        {
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [$node]);
        }

        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report($nodeIds = NodeID);

        IF (LENGTH($nodeIds) != $testedNodes)
        {
            MSGFAIL ("Tried to associate {0} nodes, DUT reports {1} nodes associated", UINT($testedNodes), UINT(LENGTH($nodeIds)));
        }
        ELSE
        {
            MSGPASS ("Association Group {0} filled with {1} node ID's", UINT($grp), UINT($testedNodes));
        }
    }

    MSG ("Clear all Association Groups separately");
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        WAIT(100);
        EXPECT Association.Report($nodeIds = NodeID);

        MSG ("Clear all node ID's from Group {0}", UINT($grp));
        SEND Association.Remove(
            GroupingIdentifier = $grp,
            NodeID = [ ]);

        // Check that the group of this iteration is empty,
        // all other groups still should be filled with max count of supported node ID's
        MSG ("Only Group {0} should be empty", UINT($grp));
        LOOP ($j; 1; $supgroups)
        {
            SEND Association.Get(GroupingIdentifier = $j);
            EXPECT Association.Report(
                GroupingIdentifier == $j,
                $maxNodes = MaxNodesSupported,
                $tmpNodeIds = NodeID);

            MSG ("Group {0}, associated node ID's: {1}", UINT($j), $tmpNodeIds);

            $testedNodes = $testNodesPerAG;
            IF ($maxNodes < $testedNodes)
            {
                $testedNodes = $maxNodes;
            }

            IF ($j == $grp)
            {
                IF (LENGTH($tmpNodeIds) != 0)
                {
                    MSGFAIL ("Association Group {0} not fully cleared", UINT($j));
                }
                ELSE
                {
                    MSGPASS ("Association Group {0} fully cleared", UINT($j));
                }
            }
            ELSE
            {
                IF (LENGTH($tmpNodeIds) != $testedNodes)
                {
                    MSGFAIL ("Node ID's removed from group {0}", UINT($j));
                }
            }
        }

        MSG ("Refill the cleared Group {0} to recreate the initial state", UINT($grp));
        SEND Association.Set(
            GroupingIdentifier = $grp,
            NodeID = $nodeIds);
    }

    MSG ("Test sequence processed. Clear all node ID's in all Association Groups (V2)");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * RemoveSpecificNodeInAllGroups
 * Test sequence:
 * - Fill each AG with MaxNodesSupported (or 5)
 * - Determine a node ID, which is available in all AGs
 * - Verify that this node ID has been removed from all AGs
 *
 * CC versions: 2
 */

TESTSEQ RemoveSpecificNodeInAllGroups: "Clear specified node ID in all Association Groups"

    $testNodesPerAG = 5;    // We will test only up to 5 nodes per Association Group
    $startNodeId = 0x11;    // We start with node ID 0x11

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport($supgroups = SupportedGroupings);
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    MSG ("Clear all node ID's in all Association Groups");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Fill each group with max count of supported node ID's.
    // If a group supports more than 5 node ID's only associate 5 node ID's
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report($maxNodes = MaxNodesSupported);

        $testedNodes = $testNodesPerAG;
        IF ($maxNodes < $testedNodes)
        {
            $testedNodes = $maxNodes;
        }

        MSG ("Fill Association Group {0} with {1} node ID's", UINT($grp), UINT($maxNodes));
        LOOP ($node; $startNodeId; $startNodeId + $testedNodes - 1)
        {
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [$node]);
        }

        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report($nodeIds = NodeID);

        IF (LENGTH($nodeIds) != $testedNodes)
        {
            MSGFAIL ("Tried to associate {0} nodes, DUT reports {1} nodes associated", UINT($testedNodes), UINT(LENGTH($nodeIds)));
        }
        ELSE
        {
            MSGPASS ("Association Group {0} filled with {1} node ID's", UINT($grp), UINT($testedNodes));
        }
    }

    // determine a node ID which is available in each Association Group
    $minNodes = 0xFF;
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            $maxNodes = MaxNodesSupported);

        IF ($maxNodes > $testNodesPerAG)
        {
            $maxNodes = $testNodesPerAG;
        }

        IF (($maxNodes > 0) && ($maxNodes < $minNodes))
        {
            $minNodes = $maxNodes;
        }
    }
    $removeNode = (($minNodes + 1) / 2) + $startNodeId - 1;

    // Clear specific node ID in all Association Groups (V2)
    MSG ("Remove node ID 0x{0:X2} in each group", $removeNode);
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = $removeNode);

    // Verify $removeNode has been removed from all Association Groups
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            $maxNodes = MaxNodesSupported,
            $nodeIds = NodeID);

        IF ($maxNodes > $testNodesPerAG)
        {
            $maxNodes = $testNodesPerAG;
            MSG ("Testing with {0} nodes only.", UINT($maxNodes));
        }

        IF ($maxNodes == 0)
        {
            MSGFAIL ("Reported MaxNodesSupported is 0");
        }
        ELSE
        {
            MSG ("Association Group {0} (maxNodes = {1}): {2}", UINT($grp), UINT($maxNodes), $nodeIds);
            IF (($maxNodes - 1) != (LENGTH($nodeIds)))
            {
                MSGFAIL ("Expected {0} node ID's in Association Group {1}, {2} node ID's reported", UINT($maxNodes - 1), UINT($grp), UINT(LENGTH($nodeIds)));
            }
            ELSE
            {
                MSG ("Expected node ID's: {0}, reported node ID's: {1} ", UINT($maxNodes - 1), UINT(LENGTH($nodeIds)));
            }
            IF (LENGTH($nodeIds) > 0)
            {
                LOOP ($j; 0; LENGTH($nodeIds) - 1)
                {
                    IF ($removeNode == $nodeIds[$j])
                    {
                        MSGFAIL ("Node ID 0x{0:X2} not removed from Association Group {1}", $removeNode, UINT($grp));
                    }
                }
            }
        }
    }

    MSG ("Test sequence processed. Clear all node ID's in all groups");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * RemoveAllNodesInAllGroups
 * Test sequence:
 * - Fill each AG with MaxNodesSupported (or 5)
 * - Remove all node IDs from all AGs (V2)
 * - Verify that all node IDs has been removed from all AGs
 *
 * CC versions: 2
 */

TESTSEQ RemoveAllNodesInAllGroups: "Clear all node IDs in all groupings"

    $testNodesPerAG = 5;    // We will test only up to 5 nodes per Association Group
    $startNodeId = 0x11;    // We start with node ID 0x11

    // Back up current lifeline associations
    SEND Association.Get(GroupingIdentifier = 0x01);
    EXPECT Association.Report($nodesInLifeline = NodeID);

    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport($supgroups = SupportedGroupings);
    MSG ("Supported Association Groups: {0}", UINT($supgroups));

    MSG ("Clear all node ID's in all Association Groups");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Fill each group with max count of supported node ID's.
    // If a group supports more than 5 node ID's only associate 5 node ID's
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report($maxNodes = MaxNodesSupported);

        $testedNodes = $testNodesPerAG;
        IF ($maxNodes < $testedNodes)
        {
            $testedNodes = $maxNodes;
        }

        MSG ("Fill Association Group {0} with {1} node ID's", UINT($grp), UINT($maxNodes));
        LOOP ($node; $startNodeId; $startNodeId + $testedNodes - 1)
        {
            SEND Association.Set(
                GroupingIdentifier = $grp,
                NodeID = [$node]);
        }

        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report($nodeIds = NodeID);

        IF (LENGTH($nodeIds) != $testedNodes)
        {
            MSGFAIL ("Tried to associate {0} nodes, DUT reports {1} nodes associated", UINT($testedNodes), UINT(LENGTH($nodeIds)));
        }
        ELSE
        {
            MSGPASS ("Association Group {0} filled with {1} node ID's", UINT($grp), UINT($testedNodes));
        }
    }

    MSG ("Clear all node ID's in all groups");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Verify all node ID's are removed from all group ID's
    LOOP ($grp; 1; $supgroups)
    {
        SEND Association.Get(GroupingIdentifier = $grp);
        EXPECT Association.Report(
            GroupingIdentifier == $grp,
            $nodeIds = NodeID);

        IF (LENGTH($nodeIds) == 0)
        {
            MSGPASS ("Association Group {0} is empty", UINT($grp));
        }
        ELSE
        {
            MSGFAIL ("Association Group {0} is not empty", UINT($grp));
        }
    }

    MSG ("Test sequence processed. Clear all node ID's in all groups");
    SEND Association.Remove(
        GroupingIdentifier = 0,
        NodeID = [ ]);

    // Restore current lifeline associations
    SEND Association.Set(
        GroupingIdentifier = 0x01,
        NodeID = $nodesInLifeline);

TESTSEQ END


/**
 * GetSpecificGroup
 * Checks for formal valid reply in report
 *
 * CC versions: 2
 */

TESTSEQ GetSpecificGroup: "Test Association Specific Group Command"

    SEND Association.GroupingsGet( );
    EXPECT Association.GroupingsReport(($supgroups = SupportedGroupings) in (1 ... 255));

    SEND Association.SpecificGroupGet();
    // first check how many groups are supports and then check that the number is in the range (0 ... $supgroups)
    EXPECT Association.SpecificGroupReport(($grp = Group) in (0 ... $supgroups));
    MSG ("Specific Group is {0}", UINT($grp));

TESTSEQ END
