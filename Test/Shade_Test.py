''' Springs Window Fashions CSZ1/RSZ1 testing scripts
    Express Controls LLC - Merrimack, NH USA - www.ExpressControls.com

    Usage: python Shade_Test.py [COMxx]
    Where COMxx is the windows COM port is the com port of a UZB which already has shade connected to it.
    Pressing ?<enter> will display a list of available commands.
    This program is mean to be run on a Windows PC with a UZB.
    Use the PC Controller program to reset the UZB then add a CSZ1 as NodeID=02.

'''

# if a module is not installed, run "pip install <module name>" to install it. 
import serial           # serial port connection to the PSoC
import time             # time/date/sleep utilities
import sys              # basic system utilities
import os               # Operating System utilities
import random           # random number generator
from   struct import *  # pack bytes for sending out the UARTs


# Some ZWAVE Defines
SOF = 0x01
ACK = 0x06
NAK = 0x15
CAN = 0x18
REQUEST = 0x00
RESPONSE = 0x01
FUNC_ID_ZW_SET_DEFAULT = 0x42
FUNC_ID_ZW_SEND_DATA = 0x13
FUNC_ID_ZW_ADD_NODE_TO_NETWORK = 0x4A
FUNC_ID_ZW_REMOVE_NODE_FROM_NETWORK = 0x4B
FUNC_ID_MEMORY_GET_ID = 0x20
COMMAND_CLASS_BASIC = 0x20
BASIC_SET = 0x01
COMMAND_CLASS_SWITCH_MULTILEVEL = 0x26
CC_ASSOCIATION = 0X85
SWITCH_MULTILEVEL_SET = 0x01
SWITCH_MULTILEVEL_GET = 0x02
SWITCH_MULTILEVEL_REPORT = 0x03
SWITCH_MULTILEVEL_START_LEVEL_CHANGE = 0X04
SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE = 0X05
CC_METER = 0x32
METER_GET = 0x01
ASSOCIATION_SET = 0X01
TRANSMIT_COMPLETE_OK = 0x00
TRANSMIT_COMPLETE_FAIL = 0x02
COMMAND_CLASS_BATTERY = 0X80
BATTERY_GET = 0X02
BATTERY_REPORT = 0x03


# Debugging messages are printed when DEBUG>0. Off when DEBUG=0.
DEBUG = 5

class Shade_Test:
    ''' CSZ1/RSZ1 SDK 6.81 test suite'''
    def __init__(self):
        ''' Connect to the UZB and setup a bunch of handy variables'''
        if len(sys.argv)==1:    # default to COM13
            comport="\\.\COM13"
        else:
            if "COM" in sys.argv[1]:
                comport=sys.argv[1]
            else:
                self.usage()
                sys.exit()
        try:
            print comport
            self.ser=serial.Serial( comport, '115200',timeout=2) # open the serial port to the UZB
        except Exception as e:
            print "Failed to connect to the UZB:{}".format(e)
            sys.exit()
        # TODO - initialize varables here...


    def Z_SerialAPI(self, funcid, params, timeout=1):
        '''Send the Serial API Function Id and PARAMs to the Z-Wave chip after adding the SerialAPI header and checksum.
            funcid is an integer which is the SerialAPI function ID 
            params is a string of hex pairs and is optional - no spaces!
            timeout is the number of seconds to wait for a response which is by default 1s
            Returns the ACK/NAK/CAN response after delivery of the frame as an integer. 
            This is just the SerialAPI having delivered the command over the UART, not the ACK that the frame was sent over the radio.
            Typically this would be followed up with a Z_SerialAPIGet to fetch any response from the FuncID.
        '''
        # the SerialAPI command must be wrapped with the SOF, Len, Type, FuncID, Params, Checksum
        length=len(params)/2 + 3
        chksum=0x0ff ^ length ^ funcid # initialize checksum
        while self.ser.inWaiting(): # purge any data from the PSOC from previous commands
            self.ser.read()
        self.ser.write(pack('B',0x01))            # SOF
        self.ser.write(pack('B',length&0xff))     # Length
        self.ser.write(b'\x00')            # Type in this application is always request
        self.ser.write(pack('B',funcid))          # SerialAPI function ID
        for i in range(len(params)/2):
            val=int(params[i*2:(i*2)+2],16)       # params must be pairs of hex chars
            self.ser.write(pack('B',val))          # Checksum
            chksum ^= int(val)
        self.ser.write(pack('B',chksum))          # Checksum
        timer=timeout*1000
        while timer>0 and not self.ser.inWaiting():
            time.sleep(.001)
            timer-=1
        if self.ser.inWaiting():
            c=ord(self.ser.read())
            return c                    # worked - return the response - could check for NAK and retry...
        
    def TimeStartTest(self):
        ''' Initialize the timing variables'''
        self.TestTimeStart=time.time()
        self.TestTimeDelta=time.time()

    def TimeDelta(self):
        ''' Returns the time from the last call to this routine'''
        retval=time.time()-self.TestTimeDelta
        self.TestTimeDelta=time.time()
        return retval

    def Z_SerialAPIGet(self,timeout=0.1):
        ''' Returns any data from the SerialAPI interface since the last call.
            Returns None if there is no data or ACKs.
            Sends an ACK if the 1st byte is 0x01 so the Z-Wave chip doesn't resend the frame.
            Waits up to TIMEOUT seconds before returning.
            Waits for a pause in the serial stream before returning so usually has a complete command.
        '''
        line=[]
        timer=timeout*1000
        while timer>0 and not self.ser.inWaiting(): # wait for at least 1 char to arrive
            time.sleep(.001)
            timer-=1
        while self.ser.inWaiting(): 
            line.append(ord(self.ser.read()))       # get chars in the buffer
            if not self.ser.inWaiting():      # if no more chars, wait to see if more are coming
                time.sleep(.001)              # Generally the minimum resolution of time.sleep is 1ms which is about a dozen chars 
        if len(line) == 0:
            return(None)
        else:
            if line[0]==SOF:
                self.ser.write(b'\x06') # send an ACK so we don't get retries
            return line

    def Z_SetDefault(self):
        ''' Reset the Z-Wave chip which deletes the current network and selects a new HomeID
            The new HomeID and NodeID are returned as a string
        '''
        line=self.Z_SerialAPI(FUNC_ID_ZW_SET_DEFAULT,"") # 77 is the funcID
        print line
        #line=self.Z_GetHomeID()         # should be back online
        # TODO - if no response - toggle reset?
        return line

    def Z_SendData(self, DestNodeID, cmdclass, cmd, data, TxOpts='05', callback='77'):
        ''' Send the cmdclass and cmd to DestNodeID. All parameters are pairs of hex chars
            Returns the delivery ack/nak of the frame to the destination (00=OK, 01=no ack, 02=FAIL)
            CMDCLASS, CMD and DATA must be strings of hex pairs 
            TxOpts is optional and defaults to NO_ROUTE with ACK which will ACK/NAK in less than 1s
            callback is optional which is appended to all callbacks from the Z-Wave chip
        '''
        params='{0}{1:02X}{2}'.format(DestNodeID, len(cmdclass + cmd + data)/2, cmdclass + cmd + data + TxOpts + callback)
        c=self.Z_SerialAPI(FUNC_ID_ZW_SEND_DATA,params)
        if c!=ACK:
            return c
        self.Z_SerialAPIGet() # Get the ACK that the command was sent to the Radio - ignore this
        if TxOpts=='11':
            line=self.Z_SerialAPIGet(1) # Wait for the deliver ACK which can take up to 1s
        else:
            line=self.Z_SerialAPIGet(15) # Wait for the delivery ACK/NAK which can take up to 15s for routing/explorers
        if line==None or len(line)<6:
            print "failed {}".format(line)
            return NAK
        if line[0]==0x01 and line[4]==int(callback,16):
            return line[5]  # 00=OK, 01=NO_ACK
        else:
            return TRANSMIT_COMPLETE_FAIL

    def Z_SendData_NoWait(self, DestNodeID, cmdclass, cmd, data) :
        ''' Send the cmdclass and cmd to DestNodeID. All parameters are pairs of hex chars
            does not wait for an ack
        '''
        params='{0}{1:02X}{2}'.format(DestNodeID, len(cmdclass + cmd + data)/2, cmdclass + cmd + data + '11' + '77')
        c=self.Z_SerialAPI(FUNC_ID_ZW_SEND_DATA,params)
        if c!=ACK:
            return c
        self.Z_SerialAPIGet() # Get the ACK that the command was sent to the Radio - ignore this
        self.Z_SerialAPIGet() # Wait for the deliver ACK which can take up to 1s

    def Z_Include(self):
        ''' Enter Inclusion mode on the Z-Wave chip.
            Returns of the NodeID included as an integer.
            Returns -1 of no node added within 20s.
        '''
        retval=-1
        line=self.Z_SerialAPI(FUNC_ID_ZW_ADD_NODE_TO_NETWORK,"0188") #01 is REMOVE_NODE_ANY, 88 is the funid
        line=self.Z_SerialAPIGet(1) # fetch the ACK
        # TODO could check that the ACK is OK here and we have entered exlusion mode
        print "Press button on device"
        timeout=20
        while timeout>0:
            line=self.Z_SerialAPIGet(1) # for someone to press the button
            if line!=None and len(line)>6:
                if line[5] == 3:    # Node found - this is the NodeID
                    retval=line[6]  # node removed but will be 0 if not a member of this network
                if line[5] == 5 or line[5] == 6: # Protocol_done or just DONE - sometimes only get 1
                    print "Node {} Included".format(retval)
                    break
            print " {}".format(timeout),
            sys.stdout.flush()
            print line
            time.sleep(1)
            timeout-=1
        if timeout!=0 :
            node = '{:02X}'.format(retval)
            CC = '{:02X}'.format(CC_ASSOCIATION)
            cmd = '{:02X}'.format(ASSOCIATION_SET)
            line=self.Z_SendData(node,CC,cmd,'0101')
        return retval

    def Z_Exclusion(self):
        ''' Enter Exclusion Mode on the Z-Wave chip'''
        retval=-1
        line=self.Z_SerialAPI(FUNC_ID_ZW_REMOVE_NODE_FROM_NETWORK,"0199") #01 is REMOVE_NODE_ANY, 99 is the funid
        line=self.Z_SerialAPIGet(1) 
        print line
        return retval

    def Z_Exclude(self,timeout=20):
        ''' Enter Exclude mode on the Z-Wave chip and wait for a node to be excluded within timeout seconds.
            Returns the NodeID excluded as an integer. 
            NodeID will be 0 if the node was not part of this network.
            Returns -1 if no node excluded.
        '''
        retval=-1
        line=self.Z_SerialAPI(FUNC_ID_ZW_REMOVE_NODE_FROM_NETWORK,"0199") #01 is REMOVE_NODE_ANY, 99 is the funid
        line=self.Z_SerialAPIGet(1) 
        # TODO could check that the ACK is OK here and we have entered exlusion mode
        print "Press button on device"
        timeout=20
        while timeout>0:
            line=self.Z_SerialAPIGet(1) # for someone to press the button
            if line!=None and len(line)>6:
                if line[5] == 3:    # Node found - this is the NodeID
                    retval=line[6]  # node removed but will be 0 if not a member of this network
                if line[5] == 6:
                    print "Node {} excluded".format(retval)
                    break
            print " {}".format(timeout),
            sys.stdout.flush()
            time.sleep(1)
            timeout-=1
        return retval
        
    def Z_GetHomeID(self):
        ''' Return the HomeID and NodeID of the Z-Wave chip'''
        line=self.Z_SerialAPI(FUNC_ID_MEMORY_GET_ID,"")
        line=self.Z_SerialAPIGet(3)
        print line
        if line!=None and len(line)>=6:
            return 'HomeID={0:2X} {1:2X} {2:2X} {3:2X} NodeID={4:03d}'.format(line[4],line[5],line[6],line[7],line[8])
        else:
            return 'Error - {}'.format(line)
        
    def SupervisionGet(self,SessionID,StatusUpdates,cmddata):
        ''' Send a Supervision get to DUT. The supervision get will encapsulate the command in the input cmddata'''
        # Byte2 in has Status Updates and Session ID
        if StatusUpdates==True :
            byte2=0x80 | SessionID
        else : byte2=SessionID
        length=len(cmddata)/2
        data="{0:02X}{1:02X}{2}".format(byte2,length,cmddata)
        line=self.Z_SendData(testNode,'6C','01',data)
        while line!=0 : # Send the Supervion Get - resend until success
            time.sleep(1)
            line=self.Z_SendData(testNode,'6C','01',data)
        return(line)

    def RcvSupervisionReport(self,sessionID) :
        ''' Receive a Supervision report, return the report'''
        # Recieve a Supervision Report
        timeout=20
        while timeout!= 0 :
            line=self.Z_SerialAPIGet()
            if DEBUG>5 and line != None :
                print ' '.join(hex(i) for i in line)
            if line == None :
                time.sleep(0.5)
            elif line[7]==0x6C :
                line=line[7:] # Throw out anything before the CC
                RcvdSessionID = line[2] & 0x3F
                print "SessionID: {}".format(RcvdSessionID)
                if RcvdSessionID == sessionID :
                    print "Status: {:02X}".format(line[3])
                    print "Duration: {}".format(line[4])
                    return line
            timeout-=1
        return None

    def shadeLevelChange(self,dir) :
        CC="{:02X}".format(COMMAND_CLASS_SWITCH_MULTILEVEL)
        zwaveCMD="{:02X}".format(SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
        if dir == 'up' :
            doneLevel = 99
            data='200000'
        elif dir == 'down' :
            doneLevel = 0
            data='600000'
        timeout=20
        line=self.Z_SendData(testNode,CC,zwaveCMD,data)
        while line!=0 and timeout!=0 :
            time.sleep(0.1)
            line=self.Z_SendData(testNode,CC,zwaveCMD,data)
            timeout-=1
        if timeout==0 :
            print "Could not send command"
            return
        zwaveCMD="{:02X}".format(SWITCH_MULTILEVEL_GET)
        timeout=100
        while timeout>0 :
            line=self.Z_SendData(testNode,CC,zwaveCMD,'','05')
            report = self.RcvMultilevelSwitchReport(1)
            if report != None :
                print report[2]
                if report[2]==doneLevel : break
            time.sleep(1)
            timeout-=1

                
    def RcvMultilevelSwitchReport(self,timeout=80) :
        while timeout!=0:
            line=self.Z_SerialAPIGet()
            if line==None or len(line) < 8 :
                time.sleep(0.5)
                timeout-=1
            elif line[7]==COMMAND_CLASS_SWITCH_MULTILEVEL :
                return line[7:]
        return None # didn't receive report 


    def SupervisionBasicSet(self,level,SessionID,StatusReports=True) :
        ''' Send a shade a Basic Set encapsulated in a Supervision Get '''
        # The basic set command that will be encapsulated
        BasicSetCMD="{0:02X}{1:02X}{2:02X}".format(COMMAND_CLASS_BASIC,BASIC_SET,level)
        self.SupervisionGet(SessionID,StatusReports,BasicSetCMD) # Send the Supervison Get
        status=self.SupervisionRcvReports(SessionID)
        if status!= None : print status


    def SupervisionRcvReports(self,SessionID) :
        ''' Called after sending a supervision get, will
        1) Recieve first supervision report sent immediately
        2) Recieve multilevel switch report sent when action complete
        3) If called for receive the second supervision report'''
        CmdSentTime=time.time()
        # First report always sent immediately
        report = self.RcvSupervisionReport(SessionID)
        if report==None :
            return "ERROR : Did not receive First Supervision report"
        if (report[2] & 0x80)==0x80 :
            moreReports = True
        else : moreReports = False
        if moreReports==True :
            report = self.RcvSupervisionReport(SessionID)
            if report==None :
                return "ERROR : Did not receive second Supervision report"
            if report[3]!=0xFF :
                return "ERROR : Status not 0xFF"

        report = self.RcvMultilevelSwitchReport()
        if report==None :
            return "ERROR : Did not receive Multilevel Switch Report"
        else :
            CmdTime=time.time() - CmdSentTime
            print "Shade at {}".format(report[2])
            print "Actual Duration : {}".format(CmdTime)
        return None

    def SupervisionTest(self) :
        ''' Test supervision CC on the DUT. This test all commands that can be used with 
        Supervision CC (MULTILEVEL_SWITCH start, stop and set and BASIC set. It tests
        the Status Updates bit, and tests that multiple commands sent quickly with the
        same session ID will be ignored'''
        # First test all the commands that can be used with supervision CC
        sessionID = 0
        print "Sending Multilevel Set 50, Status Updates = False"
        cmd = "{0:02X}{1:02X}3200".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_SET)
        self.SupervisionGet(sessionID,False,cmd) # send the supervision get, status updates = false
        status = self.SupervisionRcvReports(sessionID)
        if status == None : print status
        sessionID += 1
        print "Sending Basic Set 80, Status Updates = False"
        status = self.SupervisionBasicSet(80,sessionID,False) # basic set
        sessionID += 1
        print "Sending Multilevel Start Up, Satus Updates = False"
        cmd = "{0:02X}{1:02X}200000".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
        self.SupervisionGet(sessionID,False,cmd)
        status = self.SupervisionRcvReports(sessionID)
        if status == None : print status
        sessionID += 1
        print "Sending Basic Set 0"
        status = self.SupervisionBasicSet(0,sessionID) # basic set
        sessionID += 1
        print "Sending Multilevel Set 20"
        cmd = "{0:02X}{1:02X}1400".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_SET)
        self.SupervisionGet(sessionID,True,cmd) # send the supervision get
        status = self.SupervisionRcvReports(sessionID)
        sessionID += 1
        print "Sending Multilevel Set 60"
        cmd = "{0:02X}{1:02X}3200".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_SET)
        self.SupervisionGet(sessionID,True,cmd) # send the supervision get
        status = self.SupervisionRcvReports(sessionID)
        sessionID += 1
        print "Sending Multilevel Start Down"
        cmd = "{0:02X}{1:02X}600000".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
        self.SupervisionGet(sessionID,True,cmd)
        status = self.SupervisionRcvReports(sessionID)
        sessionID += 1
        print "Sending Multilevel Start Up"
        cmd = "{0:02X}{1:02X}200000".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
        self.SupervisionGet(sessionID,True,cmd)
        report = self.RcvSupervisionReport(sessionID) # Just need to receive the first report
        time.sleep(2)
        sessionID += 1
        print "Sending Multilevel Stop"
        cmd = "{0:02X}{1:02X}".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE)            
        self.SupervisionGet(sessionID,True,cmd)
        status = self.SupervisionRcvReports(sessionID)
        sessionID += 1
        print "Sending Multilevel Start Up"
        cmd = "{0:02X}{1:02X}200000".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
        self.SupervisionGet(sessionID,True,cmd)
        report = self.RcvSupervisionReport(sessionID)
        print "Sending Multilevel Stop, should be ignored"
        cmd = "{0:02X}{1:02X}".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE)            
        self.SupervisionGet(sessionID,True,cmd)
        print "Sending Multilevel Set 0, should be ignored"
        cmd = "{0:02X}{1:02X}0000".format(COMMAND_CLASS_SWITCH_MULTILEVEL,SWITCH_MULTILEVEL_SET)
        self.SupervisionGet(sessionID,True,cmd)
        print "Sending Basic Set 0, should be ignored"
        cmd = "{0:02X}{1:02X}00".format(COMMAND_CLASS_BASIC,BASIC_SET)
        self.SupervisionGet(sessionID,True,cmd)
        
        report = self.RcvMultilevelSwitchReport() # Get multilevel switch report make sure at expected position
        if report[2]!=0x63 :
            print "Error: shade not at expected position"
        report = self.RcvSupervisionReport(sessionID)

    def SendJog(self,num) :
        ''' Send Jogs to shade, num = number of jogs to send '''
        data = "2A044A4F4731"
        for i in range (0,num,1) :
            line=self.Z_SendData(testNode,'70','04',data)

    def GetMeterReport(self) :
        timeout=10
        CC="{:02X}".format(CC_METER)
        zwaveCMD="{:02X}".format(METER_GET)
        line=self.Z_SendData(switchNode,CC,zwaveCMD,'','05')
        while timeout!=0:
            line=self.Z_SerialAPIGet()
            if line==None or len(line) < 8 :
                time.sleep(0.5)
                timeout-=1
            elif line[7]==CC_METER :
                line=line[7:]
                break
        if timeout==0 : print "No Meter Report Received"
        else :
            print "Meter Value = {0} {1}".format(line[4],line[5])

        
            
    def startStopTest(self,dir) :
        ''' Send multilevel starts followed by multilevel stops with a decreasing interval'''
        print "Start Stop Test"
        if dir == 'up' :
            data = '200000'
        else :
            data = '600000'
        line=self.Z_SendData(testNode,'20','03','00','05') # send a basic report just to wake up the node - will be ignored
        time.sleep(1.4)
        NumCmds=3
        for i in range(0,NumCmds) :
            # Send multilevel start, then wait
            print i
            CC="{:02X}".format(COMMAND_CLASS_SWITCH_MULTILEVEL)
            zwaveCMD="{:02X}".format(SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
            line=self.Z_SendData(testNode,CC,zwaveCMD,data,'05')
            #delay=((NumCmds-i)/100.0)
            time.sleep(0.5)
            #print delay
            #self.GetMeterReport()
            # Send multilelvel stop
            CC="{:02X}".format(COMMAND_CLASS_SWITCH_MULTILEVEL)
            zwaveCMD="{:02X}".format(SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE)
            line=self.Z_SendData(testNode,CC,zwaveCMD,'','05')
            time.sleep(1.0)
            #self.GetMeterReport()
            # Request a multilevel report
            #zwaveCMD="{:02X}".format(SWITCH_MULTILEVEL_GET)
            #for i in range (0,5) :
            #    line=self.Z_SendData(testNode,CC,zwaveCMD,'','05')
            #    while line!=00 :
            #        line=self.Z_SendData(testNode,CC,zwaveCMD,'','05')
            #    report = self.RcvMultilevelSwitchReport() # Get multilevel switch report make sure at expected position
            #    if report!=None and report[4]==0 : break;
            #print report
            #if report==None or report[4]!=0 or report[2]==0 or report[2]==99: break
            #    print "Shade did not stop"

    def startTimingTest(self,delay) :

        print "Multilevel Start Timing Test"
        CC="{:02X}".format(COMMAND_CLASS_SWITCH_MULTILEVEL)
        zwaveCMDGet="{:02X}".format(SWITCH_MULTILEVEL_GET)
        line=self.Z_SendData(testNode,CC,zwaveCMDGet,'','05')
        report = self.RcvMultilevelSwitchReport()
        print "Shade at {}".format(report[2])
        if report[2]<45 or report[2]>55 :
            print "Setting shade to 50%"
            self.SupervisionBasicSet(50,0)
        dataUp = '200000'
        dataDown = '600000'
        zwaveCMDStart="{:02X}".format(SWITCH_MULTILEVEL_START_LEVEL_CHANGE)
        print "Up"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataUp,'05') 
        time.sleep(delay)
        print "Stop"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataDown,'05') 
        time.sleep(delay)
        print "Up"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataUp,'05') 
        time.sleep(delay)
        print "Stop"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataDown,'05')
        time.sleep(2)
        line=self.Z_SendData(testNode,CC,zwaveCMDGet,'','05')
        report = self.RcvMultilevelSwitchReport()
        print "Shade at {}".format(report[2])
        if report[4]!=0 :
            print "ERROR: Shade not stopped"
            return
        print "Down"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataDown,'05')
        time.sleep(delay)
        print "Stop"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataUp,'05') 
        time.sleep(delay)
        print "Down"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataDown,'05') 
        time.sleep(delay)
        print "Stop"
        line=self.Z_SendData(testNode,CC,zwaveCMDStart,dataUp,'05')
        report = self.RcvMultilevelSwitchReport()
        print "Shade at {}".format(report[2])
        if report[2]<40 or report[2]>60 :
            print "ERROR - Shade not near 50%"
        
        
        
            
    def usage(self):
        print "Usage: python Shade_Test.py COMxx"
        print "SWF shade Z-Wave testing utility"
        print "Menu:"
        print "include = include a node into the network"
        print "exclude = exclude a node from the network"
        print "set x = set the shade to level x (using supervision CC)"
        print "svtest = Run the Supervision CC test"
        print "jog x = send jog to the shade x times"
        print "node nn = set node nn to the node under test"
        print "timing x = run the start timing test, x is the delay as a float"
        print "exit = exit the program"
        print "? - this help menu"

        
if __name__ == "__main__":
    ''' Test program for the SWF shades'''
    self=Shade_Test()                # initialize the serial connection

    self.usage()
    cmd=''
    sessionID=0
    testNode='02' #initialize the test node to 02
    switchNode='06'
    while not ('exit' in cmd or 'Exit' in cmd):
        cmd=raw_input(">")
        cmd_lst = cmd.split()
        if '?' in cmd or 'help' in cmd:
            self.usage()
        elif 'acquire' in cmd:                      # Acquire the DUT
            print self.AcquireDUT()         # connect to the DUT in programming mode
        elif 'getNVR' in cmd:                      # get the NVR
            self.AcquireDUT()               # connect to the DUT in programming mode
            line=self.GetNVR()              # get and update the NVR
            if line!=None:                  # NVR update ok?
                print line                  # no
            # TODO more to come here!
        elif 'SetDefault' in cmd:             # reset the Z-Wave chip
            self.Z_SetDefault()
        elif 'HomeID' in cmd:
            line=self.Z_GetHomeID()
            print line
        elif 'startstop' in cmd :
            self.startStopTest(cmd_lst[1])
        elif 'timing' in cmd :
            delay=float(cmd_lst[1])
            self.startTimingTest(delay)
        elif 'set' in cmd : 
            if len(cmd_lst)>3 and cmd_lst[3]=='False' :
                self.SupervisionBasicSet(int(cmd_lst[1]),int(cmd_lst[2]),False)
            elif len(cmd_lst)>2 :
                self.SupervisionBasicSet(int(cmd_lst[1]),int(cmd_lst[2]))
            else :
                self.SupervisionBasicSet(int(cmd_lst[1]),sessionID)
                sessionID+=1
        elif 'svtest' in cmd : # Supervision CC test
            self.SupervisionTest()
        elif 'basic' in cmd :
            CC="{:02X}".format(COMMAND_CLASS_BASIC)
            zwaveCMD="{:02X}".format(BASIC_SET)
            line=self.Z_SendData(testNode,CC,zwaveCMD,cmd_lst[1])
        elif 'up' in cmd : # Make the shade go to all the way up
            self.shadeLevelChange('up')
        elif 'down' in cmd : # Make the shade go to all the way up
            self.shadeLevelChange('down')
        elif 'jog' in cmd :
            self.SendJog(int(cmd_lst[1]))
        elif 'batt' in cmd :
            CC="{:02X}".format(COMMAND_CLASS_BATTERY)
            zwaveCMD="{:02X}".format(BATTERY_GET)
            self.Z_SendData(testNode,CC,zwaveCMD,"")
            line = self.Z_SerialAPIGet()
            line = line[7:10]
            print "Battery level: {}".format(line[2])
        elif 'zwave' in cmd:                      # Send a Z-Wave command
            c=self.Z_SendData( testNode, '20', '02', "") # BASIC GET
            if c!=TRANSMIT_COMPLETE_OK:
                print "Failed to deliver the frame over the radio 01=NAK - {}".format(c) 
                continue
            listofbytes=self.Z_SerialAPIGet()         # Wait for the response
            print listofbytes
        elif 'exclude' in cmd: 
            NodeID=self.Z_Exclude()
            print NodeID
        elif 'include' in cmd: 
            NodeID=self.Z_Include()
            if NodeID==-1:
                print "Node Node added"
            else:
                print "Added Node {:03d}".format(NodeID)
            self.Z_SendData( "{:02X}".format(NodeID),'20','02',"") # basic get
        elif 'node' in cmd :
            testNode=cmd_lst[1]
        elif 'x' == cmd:
            break
        elif 'Z' in cmd:                      # Get a Z-Wave command
            line=self.Z_SerialAPIGet()    
            print line

    sys.exit()
